# Тест нейронной сети
import os
from matplotlib import pyplot as plt
from keras.callbacks import EarlyStopping
import numpy as np
import cv2

import app.neuronet.words as w
from keras.models import Sequential
from keras.layers import Dense, Convolution2D, Dropout, Flatten, MaxPooling2D
from keras.utils import to_categorical
import time
import random


def text_model_3():
    model = Sequential()
    model.add(
        Convolution2D(filters=16, kernel_size=(3, 3), padding='valid', input_shape=(12, 12, 1), activation='relu'))
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(43, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model

def text_model_4():
    model = Sequential()
    model.add(
        Convolution2D(filters=8, kernel_size=(3, 3), padding='valid', input_shape=(12, 12, 1), activation='relu'))
    model.add(Convolution2D(filters=16, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(43, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model

def text_model():
    model = Sequential()
    model.add(
        Convolution2D(filters=4, kernel_size=(3, 3), padding='valid', input_shape=(12, 12, 1), activation='relu'))
    model.add(Convolution2D(filters=8, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(43, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model

# Количество выходов - 43

path = "../../img/words/"

words = w.words.items()


images = []

my_x_y_train = []
for word in words:
    # print(word)

    file_name = path + word[0] + '.jpg'
    img = cv2.imread(file_name)
    matrix = np.array(img)


    z = 0
    while z < 1000:
        new_img = []
        for i in matrix:
            for j in i:
                data = round(1 - j[0] / 255, 1)
                new_img.append(data)

        n_arr = np.array(new_img)

        x_train_r = np.reshape(n_arr, (12, 12))

        my_x_y_train.append([x_train_r, word[1][1]])
        #my_x_train.append(x_train_r)
        #my_y_train.append(word[1][1])
        #images.append([word[0], word[1], new_img])
        z += 1

random.shuffle(my_x_y_train)
my_x_train = []
my_y_train = []
for item in my_x_y_train:
    my_x_train.append(item[0])
    my_y_train.append(item[1])

my_y_train_cat = to_categorical(my_y_train)
my_x_train_np = np.array(my_x_train)
my_x_train_np = my_x_train_np.reshape(my_x_train_np.__len__(), 12, 12, 1)

model = text_model()
batch_size = 128

model_name = 'text_5'
model_path = 'model/' + model_name + '.h5'
fig_path = 'model/img/' + model_name + '.png'

X = np.asarray(my_x_train_np)
Y = np.asarray(my_y_train_cat)


X_train = X_test = X
y_train = y_test = Y

# Train the model
callbacks = [
    EarlyStopping(monitor='val_accuracy',
                  min_delta=1e-3,
                  patience=5,
                  mode='max',
                  restore_best_weights=True,
                  verbose=1),
]

history = model.fit(x=X_train,
                    y=y_train,
                    batch_size=batch_size,
                    epochs=100,
                    validation_data=(X_test, y_test),
                    callbacks=callbacks)

# Plot accuracies
plt.plot(history.history['accuracy'], label='train')
plt.plot(history.history['val_accuracy'], label='val')
plt.legend()
plt.savefig(fig_path)

# Save the model
model.save(model_path)