# import d3dshot
import numpy as np
from mss import mss
import time
import app.config as cfg

class D3dService:
    target_fps = 75
    d = False
    screenshot = []

    def grab(self, window=None):
        time1 = time.time()
        if self.d is False:
            # TODO UNUSED with python 3.10
            # self.d = d3dshot.create(capture_output="numpy")
            # self.d.capture(D3d.target_fps)
            pass

        # img = None
        # img = self.d.get_latest_frame()
        # while img is None:
        # if img is None:
        img = self.d.screenshot()

        img = np.flip(img[:, :, :3], 2)

        if window:
            # Обрезаем скрин по образцу
            # window = {'left': 910, 'top': 576, 'width': 71, 'height': 20}
            img = np.array(
                img[window['top']:window['top'] + window['height'], window['left']:window['left'] + window['width']])

        time2 = time.time()
        total = time2 - time1
        fps = 0
        if total > 0:
            fps = 1 / total
        # print(img.shape, total, fps)
        return img

    def grabCache(self, window=None, update=False):
        if update:
            self.screenshot = []

        if not len(self.screenshot):
            self.screenshot = self.grab()

        if window:
            return np.array(
                self.screenshot[window['top']:window['top'] + window['height'],
                window['left']:window['left'] + window['width']])

        return self.screenshot


class MssService:
    d = False
    screenshot = []

    def grab(self, window=None):
        if self.d is False:
            self.d = mss()

        time1 = time.time()
        img = self.d.grab(window)

        img = np.array(img)
        img = img[:, :, :3]

        time2 = time.time()
        total = time2 - time1
        fps = 0
        if total > 0:
            fps = 1 / total
        # print(img.shape, total, fps)

        return img

    def grabCache(self, window=None, update=False):
        if update:
            self.screenshot = []

        if not len(self.screenshot):
            full_window = {'left': 0, 'top': 0, 'width': cfg.screen_shift['x'] + cfg.full_window['x'],
                           'height': cfg.screen_shift['y'] + cfg.full_window['y']}
            self.screenshot = self.grab(full_window)

        if window:
            return np.array(
                self.screenshot[window['top']:window['top'] + window['height'],
                window['left']:window['left'] + window['width']])

        return self.screenshot


def get_screen_service():
    if cfg.use_d3dshot:
        service = D3dService()
    else:
        service = MssService()
    return service


class FoundEnemy:
    # Найденные имена врагов
    found = dict()


class FoundInScreen:
    found = dict()


class Mss:
    sct = get_screen_service()


class WalkInfo:
    local_target_crd = []


class ActionsDone:
    actions = dict()


class UnitCircleInfo:
    # Последний центр группы: t, [x,y]
    last_center = [0, [0, 0]]


class InventStatus:
    # Меню инвентаря
    invent = False
    magic = True
    pass


class UserPotions:
    # Элексиры в иневентаре юнита. Только для активного юнита
    # key : [num, count]
    name_hp = 'hp'
    name_mp = 'mp'
    name_mr = 'mr'
    potions = dict()


class Items:
    # Цвета элементов инвентаря
    regen_magic = [0, 0, 66, 113, 214]
    potion_magic = [3, 10, 107, 174, 255]
    potion_health = [3, 10, 231, 105, 115]
    pass


class Cmd:
    # TODO Unused
    # Команды героев

    cmd = dict()

    @staticmethod
    def reset():
        Cmd.cmd = dict()
        pass

    @staticmethod
    def set(key, value):
        # Назначить команду
        pass

    @staticmethod
    def get(key):
        # Получить команду
        pass

    pass


class UnitStatus:
    # Текущий статус всех юнитов:
    unknown = 0
    stop = 1
    walk = 2
    retreat = 3
    guard = 4
    walks = 5

    units = dict()

    @staticmethod
    def reset():
        UnitStatus.units = dict()

    @staticmethod
    def items():
        return UnitStatus.units.items()

    @staticmethod
    def set(key, val):
        curr_time = time.time()
        UnitStatus.units[key] = [val, curr_time]

    @staticmethod
    def set_group(val):
        curr_time = time.time()
        for key, value in UnitStatus.items():
            UnitStatus.units[key] = [val, curr_time]
            pass

    @staticmethod
    def get(key):
        if key in UnitStatus.units:
            unit_data = UnitStatus.units.get(key)
            unit_status = unit_data[0]
            return unit_status
        return UnitStatus.unknown

    @staticmethod
    def time(key):
        if key in UnitStatus.units:
            unit_data = UnitStatus.units.get(key)
            unit_time = unit_data[1]
            return unit_time
        return 0


class TM:
    defeat = 0
    win = dict()
    stage = ''


def allow_action(func):
    def wrapper(*args, **kwargs):
        if TM.defeat:
            print('allow_action', 'Действие запрещено:', str(func))
            return False
        else:
            return func(*args, **kwargs)

    return wrapper
