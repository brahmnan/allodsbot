# Действия, которые порождают задачи
from app.commands import Mouse, Color, Keys
from app.ineterface import MainMenu, validate_left_cicle
from app.mission.actions import Actions, Magic
from app.mission.monitor import Names
from app.mission.store import UnitStatus, UserPotions, Items, InventStatus
from app.funcitons import pr
import app.config as cfg



class Action:
    act_id = 0

    # Тип действия
    # 0 - не указано,
    # 1 - движение,
    # 2 - центровка
    # 5 - Каст магии
    # 10 - Поиск врага
    act_type = 0

    # Приоритет выполнения
    priority = 10
    # id родительской задачи
    task_id = 0
    # id задачи, котороая может заблокировать действие
    block_task_ids = []
    # Может ли задача быть заблокирована
    blocked = True

    # Группа, котороя должна совершить действие
    unit_group = 1
    unit = 1

    # Цель
    unit_target = 0

    act_name = ''

    # Используемые классы
    act = Actions()
    magic = Magic()
    mouse = Mouse()
    keys = Keys()

    # Статус поражения
    defeat = 0

    # Результат выполнения действия
    result = 0

    def __init__(self):
        self.block_task_ids = []

    def do(self):
        # Выполнить действие
        pass

    def module_name(self):
        return 'Action-' + str(self.task_id) + '-' + str(self.act_id)


class ClearChat(Action):
    # Очистка чата
    priority = 1
    act_type = 2
    blocked = False

    def __init__(self):
        self.act_name = 'Очистка чата'

    def do(self):
        for i in range(10):
            self.act.chat(str(i))


class Center(Action):
    # Центрирование группы
    priority = 1
    act_type = 2
    blocked = False

    def __init__(self, unit_group=1):
        self.blocked = False

        self.unit_group = unit_group
        self.act_name = 'Центрирование группы: ' + str(unit_group)

    def do(self):
        # После центрирования группы нужно подождать 20 мс, чтобы обновился экран, с учётом 60 кадров/сек.
        self.act.center_group(self.unit_group, 20)


class Walk(Action):
    # Действие по передвижению к цели
    abs_crd = []
    priority = 5
    act_type = 1
    group = []

    def __init__(self, unit_group=1, abs_crd=[], group=[]):
        self.priority = 5
        self.act_type = 1
        self.group = group

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Движение группы: ' + str(unit_group) + ' к координатам: ' + str(abs_crd[0]) + ' ' + str(
            abs_crd[1])

    def do(self):
        self.act.select_group(self.unit_group)
        if len(self.group):
            # Выделяем дополнительные группы
            self.act.shift_down()
            for hero in self.group:
                self.act.select_group(hero.group)
            self.act.shift_up()

        self.act.m()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        self.result = 1
        UnitStatus.set(self.unit_group, UnitStatus.walk)

    pass


class WalkRetreat(Action):
    # Отступление с помощью действия движения
    abs_crd = []
    priority = 1
    act_type = 1

    def __init__(self, unit_group=1, abs_crd=[]):
        self.priority = 5
        self.act_type = 1

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Отступление группы: ' + str(unit_group) + ' к координатам: ' + str(abs_crd[0]) + ' ' + str(
            abs_crd[1])

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.m()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        UnitStatus.set(self.unit_group, UnitStatus.retreat)

    pass


class Teleport(Action):
    # Действие по передвижению к цели
    abs_crd = []
    priority = 5
    act_type = 1
    cursor = True

    def __init__(self, unit_group=1, abs_crd=[]):
        self.priority = 5
        self.act_type = 1

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Телепортация группы: ' + str(unit_group) + ' к координатам: ' + str(abs_crd[0]) + ' ' + str(
            abs_crd[1])

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.teleport()
        next_crd = [self.abs_crd[0] + 16, self.abs_crd[1] + 16]
        if self.cursor:
            # Валидация курсора
            self.mouse.move(next_crd[0], next_crd[1], 40)
            if not self.act.color.validate(next_crd[0] - 3, next_crd[1], 222, 166, 82):
                # Валидация не пройдена. Отменяем телепортацию.
                pr(self.module_name(), 'Ошибка валидации курсора. Отмена действия')
                self.mouse.mouse_hide()
                return

        self.mouse.left(next_crd[0], next_crd[1])
        self.result = 1

    pass


class WalkS(Walk):

    def do(self):
        self.act.select_group(self.unit_group)
        if len(self.group):
            # Выделяем дополнительные группы
            self.act.shift_down()
            for hero in self.group:
                self.act.select_group(hero.group)
            self.act.shift_up()
        self.act.s()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        UnitStatus.set(self.unit_group, UnitStatus.walks)


class Shift(Action):
    # Действие по передвижению к цели
    abs_crd = []
    priority = 1
    act_type = 1
    target_group = 1

    def __init__(self, unit_group=1, target_group=1, abs_crd=[]):
        self.priority = 1
        self.act_type = 1

        self.unit_group = unit_group
        self.target_group = target_group
        self.abs_crd = abs_crd
        self.act_name = 'Смещение группы: ' + str(unit_group) + ' группе ' + str(
            self.target_group) + ' по координатам: ' + str(abs_crd)

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.m()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])

    pass


class GroupWalkS(Walk):
    # Движение группы к цели
    def do(self):
        self.act.select_all()
        self.act.s()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        UnitStatus.set_group(UnitStatus.walks)


class GroupWalk(Walk):
    # Движение группы к цели
    def do(self):
        self.act.select_all()
        self.act.m()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        UnitStatus.set_group(UnitStatus.walk)


class GetLoot(Action):
    # Сбор мешков
    priority = 4
    act_type = 3
    abs_crd = []
    loot_key = ''

    def __init__(self, unit_group=1, abs_crd=[], loot_key=''):
        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.loot_key = loot_key
        self.act_name = 'Сбор лута группой ' + str(unit_group) + ' по координатам ' + str(abs_crd)

    def do(self):
        self.act.select_group(self.unit_group)
        self.mouse.move(self.abs_crd[0], self.abs_crd[1] + 5, 10)
        self.mouse.left(self.abs_crd[0], self.abs_crd[1] + 5)
        UnitStatus.set(self.unit_group, UnitStatus.walk)
        self.result = 1


class Heal(Action):
    # Лечение цели
    abs_crd = []
    priority = 1
    stop_hero = False
    act_type = 1002
    cursor = True
    dst_hero = 1

    def __init__(self, unit_group=1, abs_crd=[], dst_hero=1, stop_hero=False):
        self.stop_hero = stop_hero
        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.dst_hero = dst_hero
        self.act_name = 'Лечение цели ', dst_hero, abs_crd, ' группой ', unit_group

    def do(self):
        if self.stop_hero:
            # Принудительная остановка цели
            self.act.select_group(self.dst_hero)
            pr(self.module_name(), 'Герой останавливается для лечения', self.dst_hero)
            self.act.t()
            UnitStatus.set(self.stop_hero, UnitStatus.stop)
            pass

        if self.unit_group == -1:
            # Выбор всей группы
            self.act.select_all()
            UnitStatus.set_group(UnitStatus.stop)
        else:
            self.act.select_group(self.unit_group)
            UnitStatus.set(self.unit_group, UnitStatus.stop)

        if self.cursor:
            # Валидация курсора
            self.mouse.move(self.abs_crd[0], self.abs_crd[1], 30)
            if not self.act.color.validate(self.abs_crd[0] + 1, self.abs_crd[1] + 3, 222, 170, 90):
                # Валидация не пройдена. Отменяем щит.
                pr(self.module_name(), 'Ошибка валидации курсора. Отмена действия')
                self.mouse.mouse_hide()
                return

        self.act.magic_heal()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        self.result = 1


class Shield(Action):
    # Магический щит
    abs_crd = []
    priority = 1
    act_type = 1001
    cursor = False
    force_stop = False

    def __init__(self, unit_group=1, abs_crd=[], force_stop=False):
        self.unit_group = unit_group
        self.unit_target = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Магический щит группы ', unit_group
        self.force_stop = force_stop

    def do(self):
        self.act.select_group(self.unit_group)
        if self.force_stop:
            # Принудительная остановка
            self.act.t()

        if self.cursor:
            # Валидация курсора
            self.mouse.move(self.abs_crd[0], self.abs_crd[1], 30)
            if not self.act.color.validate(self.abs_crd[0] + 1, self.abs_crd[1] + 3, 222, 170, 90):
                # Валидация не пройдена. Отменяем щит.
                pr(self.module_name(), 'Ошибка валидации курсора. Отмена действия')
                self.mouse.mouse_hide()
                return

        self.act.use_shield()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        self.result = 1
        UnitStatus.set(self.unit_group, UnitStatus.stop)


class SelfHeal(Action):
    # Принятие элексиров
    priority = 0
    act_type = 4
    # Количество нажатий
    custom_num = 1

    def __init__(self, unit_group=1, custom_num=1):
        self.unit_group = unit_group
        self.act_name = 'Принятие элексира здоровья группой ', unit_group
        self.custom_num = custom_num

    def do(self):
        self.act.select_group(self.unit_group)

        for i in range(self.custom_num):
            self.act.heal_up()


class CastMagic(Action):
    abs_crd = []
    act_type = 5
    target_id = 0
    magic_name = 'fire'
    cursor = True
    attack = False
    up_mana = 0

    def __init__(self, unit_group=1, abs_crd=[], magic_name='fire', unit_target=0, attack=False, cursor=True,
                 priority=2, up_mana=0):
        self.act_type = 5
        self.target_id = 0

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.magic_name = magic_name
        self.unit_target = unit_target
        self.attack = attack
        self.cursor = cursor
        self.priority = priority
        self.up_mana = up_mana
        self.act_name = 'Группа ' + str(unit_group) + ' использует магию ' + magic_name + ', на цель ' + str(
            self.unit_target) + ', по координатам ' + str(abs_crd)

    def do(self):
        self.act.select_group(self.unit_group)
        # Проверяем, открыто ли меню магии
        if not InventStatus.magic:
            # Открываем
            mm = MainMenu()
            mm.show_magic_menu()
            pass
        self.magic.select_magic(self.magic_name)
        if self.cursor:
            # Валидация курсора
            self.mouse.move(self.abs_crd[0], self.abs_crd[1], 30)
            if not self.act.color.validate(self.abs_crd[0] - 3, self.abs_crd[1], 222, 166, 82):
                # Валидация не пройдена. Отменяем атаку.
                pr(self.module_name(), 'Ошибка валидации курсора. Отмена действия')
                self.mouse.mouse_hide()
                return

        if not self.attack:
            self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        else:
            if not self.magic.is_auto(self.magic_name):
                self.act.auto_magic()
            self.act.a()
            self.mouse.left(self.abs_crd[0], self.abs_crd[1])

        if self.up_mana:
            invent_is_close = False
            if not InventStatus.invent:
                invent_is_close = True
                mm = MainMenu()
                mm.open_invent()

            up = UsePotion(self.unit_group, UserPotions.name_mp, self.up_mana)
            up.do()

            if invent_is_close:
                mm.close_invent()
            # if not InventStatus.invent:
            # Открываем инвентарь

            # for i in range(self.up_mana):
            #    self.act.mana_up()
        self.result = 1


class Attack(CastMagic):
    # Атака на цель
    priority = 2
    validate_target = False
    target_id = 0
    stop = False

    def __init__(self, unit_group=1, abs_crd=[], target_code='', target_id=0, stop=False):
        self.priority = 2
        self.validate_target = False

        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.target_code = target_code
        self.target_id = target_id
        self.stop = stop
        self.act_name = 'Атака группы ', unit_group, ' по координатам ', abs_crd

    def do(self):
        self.act.select_group(self.unit_group)
        if self.target_code:
            names = Names()
            self.mouse.move(self.abs_crd[0], self.abs_crd[1], 10)
            # Получаем имя юнита
            name = names.get_name()
            self.mouse.mouse_hide()
            self.result = name
            if name == self.target_code:
                pr(self.module_name(), 'Цель подтверждена', name)
            else:
                pr(self.module_name(), 'Ошибка атаки, имя цели не совпадает с заданым')
                if self.stop:
                    self.act.select_group(self.unit_group)
                    pr(self.module_name(), 'Остановка', self.unit_group)
                    self.act.t()
                return

        self.act.a()
        self.mouse.left(self.abs_crd[0], self.abs_crd[1])
        if not self.target_code:
            self.result = 1


class StoneWall(CastMagic):
    priority = 2
    magic_name = 'stone_wall'

    def __init__(self, unit_group=1, abs_crd=[]):
        self.priority = 2
        self.magic_name = 'stone_wall'
        self.unit_group = unit_group
        self.abs_crd = abs_crd
        self.act_name = 'Каменная стена группы ', unit_group, ' на координаты ', abs_crd


class Retreat(Action):
    # Отступление игрока
    priority = 3
    act_type = 2001

    def __init__(self, unit_group=1):
        self.unit_group = unit_group
        self.act_name = 'Герой', unit_group, ' отступает'

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.r()
        UnitStatus.set(self.unit_group, UnitStatus.retreat)


class Stop(Action):
    # Остановка игрока
    priority = 3
    act_type = 5

    def __init__(self, unit_group=1):
        self.unit_group = unit_group
        self.act_name = 'Герой', unit_group, ' останавливается'

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.t()
        UnitStatus.set(self.unit_group, UnitStatus.stop)


class Guard(Action):
    # Остановка игрока
    priority = 3
    act_type = 5

    def __init__(self, unit_group=1):
        self.unit_group = unit_group
        self.act_name = 'Герой', unit_group, ' держит позицию'

    def do(self):
        self.act.select_group(self.unit_group)
        self.act.g()
        UnitStatus.set(self.unit_group, UnitStatus.guard)


class BindAutoMagic(Action):
    # Назначение авто-магии
    priority = 1
    act_type = 5
    magic_name = ''

    def __init__(self, unit_group=1, magic_name=''):
        self.priority = 1
        self.act_type = 5

        self.unit_group = unit_group
        self.magic_name = magic_name
        self.act_name = 'Герой', unit_group, ' назначает авто-магию', magic_name

    def do(self):
        self.act.select_group(self.unit_group)
        # Проверяем цвет. Если он не соответствует магии, назначаем бинд.
        if not self.magic.is_auto(self.magic_name):
            if not InventStatus.magic:
                # Открываем
                mm = MainMenu()
                mm.show_magic_menu()
            self.magic.select_magic(self.magic_name)
            self.act.auto_magic()

    pass


class BindHotKeyMagic(Action):
    # Назначение авто-магии
    priority = 1
    act_type = 5
    key = ''
    magic_name = ''

    def __init__(self, unit_group=1, key='', magic_name=''):
        self.unit_group = unit_group
        self.key = key
        self.magic_name = magic_name
        self.act_name = 'Герой', unit_group, ' назначает горячую клавишу', key

    def do(self):
        self.act.select_group(self.unit_group)
        if not InventStatus.magic:
            # Открываем
            mm = MainMenu()
            mm.show_magic_menu()
        self.magic.select_magic(self.magic_name)
        self.act.ctrl_key(self.key)


class ValidateLife(Action):
    # Валидация жизни юнита
    priority = 7
    act_type = 5
    hero = []

    color = Color()

    def __init__(self, hero=[]):
        self.priority = 7
        self.act_type = 5

        self.hero = hero
        self.unit_group = hero.group
        self.act_name = 'Проверка жизни группы ', self.unit_group

    def do(self):
        if not self.validate_life(20):
            # Вторая проверка
            if not self.validate_life(20):
                # Герой мёртв
                pr(self.module_name(), 'Герой мёртв', self.unit_group)
                self.result = 1

    def validate_life(self, wait=20):
        self.act.select_group(self.unit_group, wait)
        f = self.hero.face_pixel
        return self.color.validate(f[0], f[1], f[2], f[3], f[4])


class AllStop(Action):
    # Остановка группы
    priority = 5
    act_type = 2

    def __init__(self):
        self.priority = 5
        self.act_type = 2

        self.act_name = 'Остановка группы'

    def do(self):
        self.act.select_all()
        self.act.t()


class AllGuard(Action):
    # Защита группы
    priority = 1
    act_type = 2

    def __init__(self):
        self.priority = 1
        self.act_type = 2

        self.act_name = 'Защита группы'

    def do(self):
        self.act.select_all()
        self.act.g()
        UnitStatus.set_group(UnitStatus.guard)


class HealRegen(Action):
    # Регенерация здоровья группы
    priority = 2
    act_type = 2

    def __init__(self, unit_group=1):
        self.priority = 2
        self.act_type = 2
        self.unit_group = unit_group
        self.act_name = 'Регенерация здоровья группы: ' + str(unit_group)

    def do(self):
        # После центрирования группы нужно подождать 20 мс, чтобы обновился экран, с учётом 60 кадров/сек.
        self.act.select_group(self.unit_group)
        self.act.heal_regen()


class ManaRegen(Action):
    # Регенерация здоровья группы
    priority = 2
    act_type = 2

    def __init__(self, unit_group=1):
        self.priority = 2
        self.act_type = 2
        self.unit_group = unit_group
        self.act_name = 'Регенерация маны группы: ' + str(unit_group)

    def do(self):
        # После центрирования группы нужно подождать 20 мс, чтобы обновился экран, с учётом 60 кадров/сек.
        self.act.select_group(self.unit_group)
        self.act.mana_regen()


class Speed(Action):
    # Изменение скорости
    priority = 0
    act_type = 2
    speed = 4
    plus = True

    def __init__(self, speed=1, plus=True):
        self.priority = 0
        self.act_type = 2

        self.speed = speed
        self.plus = plus
        if plus:
            self.act_name = 'Увеличение скорости ' + str(speed)
        else:
            self.act_name = 'Уменьшение скорости ' + str(speed)

    def do(self):
        if self.plus:
            self.act.speed_plus(self.speed)
        else:
            self.act.speed_minus(self.speed)


class MagicMenu(Action):
    # Показать/скрыть меню магии
    priority = 0
    act_type = 2
    show = True

    def __init__(self, show=True):
        self.priority = 0
        self.act_type = 2

        self.show = show
        if show:
            self.act_name = 'Открытие меню магии'
        else:
            self.act_name = 'Скрытие меню магии'

    def do(self):
        mm = MainMenu()
        if self.show:
            mm.show_magic_menu()
        else:
            mm.hide_magic_menu()


class InventMenu(Action):
    # Показать/скрыть инвентарь
    priority = 0
    act_type = 2
    show = True

    def __init__(self, show=True):
        self.priority = 0
        self.act_type = 2

        self.show = show
        if show:
            self.act_name = 'Открытие меню инвентаря'
        else:
            self.act_name = 'Скрытие меню инвентаря'

    def do(self):
        mm = MainMenu()
        if self.show:
            mm.open_invent()
        else:
            mm.close_invent()


class SetGroups(Action):
    # Назначение группы
    priority = 5
    act_type = 2
    units = []
    ms = []

    def __init__(self, units=[], ms=[]):
        self.priority = 5
        self.act_type = 2

        self.units = units
        self.ms = ms
        self.act_name = 'Назначение групп'

    def screen_to_abs(self, x, y):
        # Перевод координат экрана в абсолютные
        screen_crd = [x + cfg.screen_shift['x'], y + cfg.screen_shift['y']]
        return screen_crd

    def do(self):

        color = Color()
        total = 0

        for hero in self.units:
            # Обновляем карту
            self.ms.find()

            if self.ms.get_hero(hero.group):
                # Смотрим, назначен ли уже данный герой
                total += 1
            else:
                # Если герой не назначен, назначаем
                other = self.ms.other
                if other:
                    for item in other:
                        screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
                        self.mouse.move(screen[0], screen[1], 30)

                        f = hero.face_pixel
                        if color.validate(f[0], f[1], f[2], f[3], f[4]):
                            self.act.select_group(9)
                            # Назначаем группу героя
                            self.mouse.left(screen[0], screen[1], sleep=100)
                            self.act.make_group(hero.group)
                            pr(self.module_name(), 'Создана группа', hero.group, 'по координатам', screen)
                            # Добаляем в список назначеных героев
                            total += 1
                            pass

        if total < len(self.units):
            pr(self.module_name(), 'Провал. Количество юнитов меньше необходимых групп')
            self.defeat = 1

    def doUnused(self):
        other = self.ms.other
        color = Color()
        total = 0
        if other:
            # mm = MainMenu()
            # mm.hide_magic_menu()
            for item in other:
                if total >= len(self.units):
                    break

                screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
                self.mouse.move(screen[0], screen[1], 30)
                for hero in self.units:
                    f = hero.face_pixel
                    if color.validate(f[0], f[1], f[2], f[3], f[4]):
                        self.act.select_group(9)
                        # Назначаем группу героя
                        self.mouse.left(screen[0], screen[1], sleep=100)
                        self.act.make_group(hero.group)
                        pr(self.module_name(), 'Создана группа', hero.group, 'по координатам', screen)
                        total += 1
                        pass
            # mm.show_magic_menu()
        if total < len(self.units):
            pr(self.module_name(), 'Провал. Количество юнитов меньше необходимых групп')
            self.defeat = 1


class SetGroupsV2(Action):
    # Назначение группы
    priority = 5
    act_type = 2
    units = []
    ms = []

    def __init__(self, units=[], ms=[]):
        self.priority = 5
        self.act_type = 2

        self.units = units
        self.ms = ms
        self.act_name = 'Назначение групп'

    def screen_to_abs(self, x, y):
        # Перевод координат экрана в абсолютные
        screen_crd = [x + cfg.screen_shift['x'], y + cfg.screen_shift['y']]
        return screen_crd

    def do(self):

        color = Color()

        other = self.ms.other

        item_found = False

        for item in other:
            # Проходим по списку всех найденных на карте юнитов.

            screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
            # Передвигаем курсор мыши на юнит
            self.mouse.move(screen[0], screen[1], 50)

            for hero in self.units:
                f = hero.face_pixel
                if color.validate(f[0], f[1], f[2], f[3], f[4]):
                    self.act.select_group(0)
                    # Назначаем группу героя
                    self.mouse.left(screen[0], screen[1], sleep=100)
                    self.act.make_group(hero.group)
                    pr(self.module_name(), 'Создана группа', hero.group, 'по координатам', screen)
                    item_found = True
                    break
            if item_found:
                break
        pass


class SetMultiGroup(Action):

    def __init__(self, unit_group=1, units=[], ms=[]):
        self.unit_group = unit_group
        self.units = units
        self.ms = ms
        self.act_name = 'Назначение групп'

    def screen_to_abs(self, x, y):
        # Перевод координат экрана в абсолютные
        screen_crd = [x + cfg.screen_shift['x'], y + cfg.screen_shift['y']]
        return screen_crd

    def do(self):
        color = Color()
        other = self.ms.other
        self.act.select_group(0)

        # Проверяем список назначеных юнитов
        new_units = []

        hero = self.ms.cmd.get(self.unit_group)
        if hero and hero[0] > 0:
            pr(self.module_name(), 'Юнит ', self.unit_group, 'есть, его не надо назначать', hero)
            # Юнит есть, его не надо назначать
            pass
        else:
            # Юнита нет, его нужно назначить
            shift_down = False
            units_count = len(self.units)
            units_found = 0
            for item in other:
                # Проходим по списку всех найденных на карте юнитов.
                screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
                # Передвигаем курсор мыши на юнит
                self.mouse.move(screen[0], screen[1], 30)

                for hero in self.units:
                    f = hero.face_pixel
                    if color.validate(f[0], f[1], f[2], f[3], f[4]):
                        if not shift_down:
                            self.act.shift_down()
                            shift_down = True

                        # Назначаем группу героя
                        self.mouse.left(screen[0], screen[1], sleep=100)
                        units_found += 1
                        pr(self.module_name(), 'Группа найдена', units_found)
                        break

            if units_found == units_count:
                self.act.make_group(self.unit_group)
                pr(self.module_name(), 'Создана группа', self.unit_group)
            else:
                pr(self.module_name(), f'Ошибка создания группы [{self.unit_group}]: найдено:{units_found} != нужно:{units_count}')
                self.defeat = 1

            if shift_down:
                self.act.shift_up()

        self.result = 1


class SetOtherGroups(Action):
    # Назначение группы
    priority = 5
    act_type = 2
    unit_group = 6
    units = []
    ms = []

    def __init__(self, unit_group=6, ms=[]):
        self.priority = 5
        self.act_type = 2
        self.units = []

        self.ms = ms
        self.unit_group = unit_group
        self.act_name = 'Назначение групп'

    def screen_to_abs(self, x, y):
        # Перевод координат экрана в абсолютные
        screen_crd = [x + cfg.screen_shift['x'], y + cfg.screen_shift['y']]
        return screen_crd

    def do(self):
        other = self.ms.other
        if other:
            mm = MainMenu()
            mm.hide_magic_menu()
            self.act.select_group(self.unit_group)
            self.keys.keyDown('shift')
            for item in other:
                screen = self.ms.get_shift(self.screen_to_abs(item[1], item[0]))
                self.mouse.move(screen[0], screen[1], 100)
                self.mouse.left(screen[0], screen[1], sleep=100)

            self.keys.keyUp('shift')
            self.act.make_group(self.unit_group)
            mm.show_magic_menu()


class TargetInfo(Action):
    # Информация о цели
    abs_crd = []
    priority = 2
    act_type = 10
    target_id = 0
    stop = False

    def __init__(self, abs_crd=[], target_id=0, unit_group=1, stop=False):
        self.priority = 2
        self.act_type = 10

        self.abs_crd = abs_crd
        self.target_id = target_id
        self.unit_group = unit_group
        self.stop = stop
        self.act_name = 'Информация о цели ', target_id, abs_crd

    def do(self):
        if self.stop:
            self.act.select_group(self.unit_group)
            pr(self.module_name(), 'Остановка', self.unit_group)
            self.act.t()
        self.mouse.move(self.abs_crd[0], self.abs_crd[1], 10)
        # Получаем имя юнита
        names = Names()
        name = names.get_name()
        self.mouse.mouse_hide()
        self.result = name
        pr(self.module_name(), 'Получено имя', name)


class Click(Action):
    act_name = 'Клик по координатам'
    abs_crd = []
    double = False

    def __init__(self, abs_crd=[], double=False):
        self.abs_crd = abs_crd
        self.double = double
        self.act_name = 'Клик по координатам ', abs_crd

    def do(self):
        self.mouse.left(self.abs_crd[0], self.abs_crd[1], self.double, 10)


class FindPotions(Action):
    # Поиск эликсиров в инвентаре
    priority = 2
    act_type = 2
    ms = []

    def __init__(self, unit_group=1, ms=[]):
        self.unit_group = unit_group
        self.ms = ms
        self.act_name = 'Поиск эликсиров в инвентаре группы: ' + str(unit_group)
        # Сброс информации о эликсирах
        UserPotions.potions = dict()

    def do(self):
        self.act.select_group(self.unit_group)
        if not InventStatus.invent:
            # Открываем
            mm = MainMenu()
            mm.open_invent()

        # Поиск секторов в инвентаре
        items_to_find = {
            UserPotions.name_hp: Items.potion_health,
            UserPotions.name_mp: Items.potion_magic,
            UserPotions.name_mr: Items.regen_magic}

        self.ms.ui.find(items_to_find)


class DeEquipPosoh(Action):
    # Поиск эликсиров в инвентаре
    priority = 2
    act_type = 2
    ms = []
    crd = []
    equip = False

    def __init__(self, unit_group=1, crd = []):
        self.unit_group = unit_group
        self.crd = crd
        self.act_name = 'Деэкипировка посоха: ' + str(unit_group)

    def do(self):
        self.act.select_group(self.unit_group)
        if len(self.crd):
            validate_left_cicle(self.crd[0],self.crd[1],double=True)


class UsePotion(Action):
    act_name = 'Использование эликсира'
    potion_key = UserPotions.name_hp
    potion_count = 1
    priority = 1
    act_type = 1010

    def __init__(self, unit_group=1, potion_key=UserPotions.name_hp, potion_count=1):
        self.unit_group = unit_group
        self.potion_key = potion_key
        self.potion_count = potion_count
        self.act_name = 'Использование эликсира ', potion_key, 'группой', unit_group, 'в количестве', potion_count

    def do(self):
        # Вычисляем координаты клика
        x = 111
        y = 754
        self.result = 0
        if self.potion_key in UserPotions.potions:
            potion = UserPotions.potions.get(self.potion_key)
            invent_num = potion[0]
            potion_count = potion[1]
            if potion_count > 0:
                invent_is_close = False
                if not InventStatus.invent:
                    invent_is_close = True
                    mm = MainMenu()
                    mm.open_invent()
                # Определяем количество кликов
                click_count = self.potion_count
                if click_count > potion_count:
                    click_count = potion_count
                # Находим координаты клика
                x1 = x + (invent_num - 1) * 80
                for i in range(click_count):
                    self.mouse.left(x1, y, True, 10)
                    # Тратим эликсир
                    UserPotions.potions[self.potion_key][1] -= 1
                    pr(self.module_name(), 'Эликсир', self.potion_key, 'использован, остаток',
                       UserPotions.potions[self.potion_key][1])

                if invent_is_close:
                    mm.close_invent()
                self.result = 1


class ManaRegenByClick(Action):
    # Регенерация здоровья группы
    priority = 2
    act_type = 2
    potion_count = 1
    potion_key = UserPotions.name_mr

    def __init__(self, unit_group=1):
        self.priority = 2
        self.act_type = 2
        self.unit_group = unit_group
        self.act_name = 'Регенерация маны группы: ' + str(unit_group)

    def do(self):
        self.act.select_group(self.unit_group)
        self.result = 0
        x = 111
        y = 754
        if self.potion_key in UserPotions.potions:
            potion = UserPotions.potions.get(self.potion_key)
            invent_num = potion[0]
            potion_count = potion[1]
            if potion_count > 0:

                invent_is_close = False
                if not InventStatus.invent:
                    invent_is_close = True
                    mm = MainMenu()
                    mm.open_invent()

                # Определяем количество кликов
                click_count = self.potion_count
                # Находим координаты клика
                x1 = x + (invent_num - 1) * 80
                for i in range(click_count):
                    self.mouse.left(x1, y, True, 10)
                    # Тратим эликсир
                    UserPotions.potions[self.potion_key][1] -= 1
                    pr(self.module_name(), 'Мазь маны', self.potion_key, 'использована, остаток',
                       UserPotions.potions[self.potion_key][1])

                if invent_is_close:
                    mm.close_invent()

            self.result = 1


class Join(Action):
    # Соединение групп
    priority = 1
    act_type = 2
    join_group = 2

    def __init__(self, unit_group=1, join_group=2):
        self.join_group = join_group
        self.unit_group = unit_group
        self.act_name = 'Центрирование группы: ' + str(unit_group)

    def do(self):
        # После центрирования группы нужно подождать 20 мс, чтобы обновился экран, с учётом 60 кадров/сек.
        self.act.select_group(self.unit_group)
        self.act.shift_down()
        self.act.select_group(self.join_group)
        self.act.shift_up()
        self.act.make_group(self.unit_group)


class Defeat(Action):
    act_name = 'Мы проиграли'
    priority = -10

    def do(self):
        self.defeat = 1
