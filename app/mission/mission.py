# Управление персонажем на миссии
# Для каждой миссии свой набор контрольных точек и сигналов
from app.mission.actions import *
from app.mission.task_manager import TaskManager
import app.mission.task as task
from app.commands import Mouse
from app.mission.store import TM
import app.ineterface as inter
from app.mission.heroes import *
import time


class Mission:
    # Управление миссиями
    mm = inter.MainMenu()

    # Время старта миссии
    start_mission_time = 0

    wp = {}

    @classmethod
    def init_tm(cls, validate=None, bs=False):
        tm = TaskManager(validate=validate, bs=bs, wp=cls.wp)
        return tm

    @classmethod
    def w(cls, key):
        return cls.wp.get(key)[0]

    @classmethod
    def enter_game(cls, change_menu=True):
        cls.start_mission_time = time.time()
        cls.mm.enter_game(change_menu)


class M1(Mission):
    wp = {
        0: [[899, 166], 1],
        1: [[937, 129], 1],
        2: [[947, 121], 0],
        3: [[928, 91], 0],
        4: [[971, 76], 0],
        5: [[994, 59], 1],
    }

    @classmethod
    def m_1(cls):
        # Первая миссия
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.SetHeroesGroupV2([Fergard]))

        # Назначаем магию
        tm.run_task(task.BindHotKeyMagic(Fergard, 'f5', 'heal'))
        tm.run_task(task.BindHotKeyMagic(Fergard, 'f6', 'shield'))
        # Ускоряем игру
        tm.run_task(task.SpeedPlus())

        loot = tm.run_task(task.FindLoot([Fergard]))

        # Идём по координатам к крестьянке
        tm.run_task(task.MagicShield(Fergard, 10))
        tm.run_task(task.HealthMon(Fergard))

        # Отправление к контрольной точке Мага
        tm.run_task(task.WalkS(cls.w(1), Fergard), block=loot)

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_1_2(cls):
        # Первая миссия
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.SetHeroesGroupV2([Znaharka]))
        tm.set_validate_group([Fergard, Znaharka])
        tm.run_task(task.MagicShield(Fergard, 10))
        tm.run_task(task.HealthMon(Fergard))

        tm.run_task(task.WalkS(cls.w(2), Fergard, group=[Znaharka], walk_min_ct_radius=5))
        tm.run_task(task.WalkS(cls.w(3), Fergard, group=[Znaharka], walk_min_ct_radius=5))
        tm.run_task(task.WalkS(cls.w(4), Fergard, group=[Znaharka], walk_min_ct_radius=5))
        tm.run_task(task.WalkS(cls.w(5), Fergard, group=[Znaharka]))

        if TM.defeat:
            return False
        return True


class M2(Mission):
    wp = {
        0: [[885, 55], 1],
        1: [[965, 91], 0],
        2: [[980, 112], 0],
        3: [[952, 137], 0],
        4: [[985, 174], 1],
    }

    @classmethod
    def m_1(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.SetHeroesGroupV2([Snut, Dubin_1, Dubin_2, Dubin_3]))

        tm.set_validate_group([Fergard, Snut])

        # Сбор лута
        loot = tm.run_task(task.FindLoot([Fergard]))

        tm.run_task(task.MagicShield(Fergard, 10))
        tm.run_task(task.HealthMon(Fergard, health_level=5))
        tm.run_task(task.Retreat([Snut], 5))

        # Отправление к контрольной точке Мага
        tm.run_task(task.UnitCircle(cls.w(1), Dubin_1), block=loot)
        tm.run_task(task.UnitCircle(cls.w(2), Dubin_1), block=loot)
        tm.run_task(task.UnitCircle(cls.w(3), Dubin_1), block=loot)
        tm.run_task(task.UnitCircle(cls.w(4), Snut), block=loot)
        tm.run_task(task.Wait(30))
        if not tm.win:
            TM.defeat = 1

        if TM.defeat:
            return False
        return True


class M3(Mission):
    wp = {
        0: [[895, 168], 1],
        1: [[901, 93], 0],
        2: [[920, 80], 0],
        3: [[952, 85], 1],
        4: [[968, 135], 0],
        5: [[957, 164], 0],
        6: [[997, 160], 0],
        7: [[995, 56], 1],
    }

    @classmethod
    def m_1(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))

        tm.run_task(task.SetHeroesGroupV2([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3]))

        tm.set_validate_group([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3])
        tm.run_task(task.LifeHiredUnits2([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3]))

        # tm.run_task(task.MagicShield(Fergard, 5))
        # tm.run_task(task.MagicShield(Ren, 5))

        tm.run_task(task.HealthMon(Fergard, health_level=5))
        tm.run_task(task.HealthMon(Ren, health_level=5))

        retreat = tm.run_task(task.Retreat([Fergard, Ren], 3))
        loot = tm.run_task(task.FindLoot([Fergard, Ren]))

        tm.run_task(task.UnitCircle(cls.w(1), Dubin_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(2), Dubin_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(3), Dubin_1), block=[loot])

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3])
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.LifeHiredUnits2([Dubin_1, Dubin_2, Dubin_3]))

        # tm.run_task(task.MagicShield(Fergard, 5))
        # tm.run_task(task.MagicShield(Ren, 5))

        tm.run_task(task.HealthMon(Fergard))
        tm.run_task(task.HealthMon(Ren))

        retreat = tm.run_task(task.Retreat([Fergard, Ren], 3))
        loot = tm.run_task(task.FindLoot([Fergard, Ren]))
        # tm.run_task(task.MoveGroup(Fergard, [Ren, Dubin_1, Dubin_2, Dubin_3]), block=[loot, retreat])
        tm.run_task(task.UnitCircle(cls.w(4), Dubin_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(5), Dubin_1), block=[loot])
        tm.run_task(task.EnemyClear([Fergard, Ren], min_dst=5))
        tm.run_task(task.UnitCircle(cls.w(6), Dubin_1), block=[loot])

        tm.remove_task(retreat)
        tm.run_task(task.WalkS(cls.w(7), Fergard, group=[Ren, Dubin_1, Dubin_2, Dubin_3]))

        tm.run_task(task.Wait(30))
        if not tm.win:
            TM.defeat = 1

        if TM.defeat:
            return False
        return True


class M4(Mission):
    wp = {
        0: [[912, 53], 1],
        1: [[937, 88], 0],
        2: [[941, 108], 0],
        3: [[922, 124], 0],
        4: [[926, 136], 1],

        5: [[945, 148], 1],

        6: [[960, 165], 0],
        7: [[980, 160], 1],

        8: [[985, 144], 0],
        9: [[975, 115], 1],

        10: [[969, 125], 1],
    }

    @classmethod
    def init_units(cls, tm, retreat=3):
        # tm.run_task(task.MagicShield(Fergard, 5))
        # tm.run_task(task.MagicShield(Ren, 5))
        tm.run_task(task.LifeHiredUnits2([Dubin_1, Dubin_2, Dubin_3]))
        tm.run_task(task.HealthMon(Fergard))
        tm.run_task(task.HealthMon(Ren))
        # tm.run_task(task.Retreat([Fergard, Ren], retreat))
        return tm
        pass

    @classmethod
    def m_1(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.SetHeroesGroupV2([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3]))
        tm.set_validate_group([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3])

        cls.init_units(tm)

        loot = tm.run_task(task.FindLoot([Fergard, Ren]))

        tm.run_task(task.UnitCircle(cls.w(1), Dubin_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(2), Dubin_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(3), Dubin_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(4), Dubin_1), block=[loot])

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3])
        tm.run_task(task.Center(Fergard))

        tm = cls.init_units(tm)

        tm.run_task(task.MoveGroup(Fergard, [Ren, Dubin_1, Dubin_2, Dubin_3]))
        tm.run_task(task.WalkGroup(cls.w(5), Fergard, walk_s=False))
        # tm.run_task(task.UnitCircle(cls.w(5), Dubin_1))

        tm.run_task(task.SetHeroesGroupV2([Brian]))
        tm.set_validate_group([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3, Brian])
        tm.run_task(task.Center(Brian))
        tm.run_task(task.EnemyClear([Brian], min_dst=5, validation_interval=2))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3, Brian])
        tm.run_task(task.Center(Brian))

        tm = cls.init_units(tm, 4)

        retreat2 = tm.run_task(task.Retreat([Dubin_1, Dubin_2, Dubin_3], 2))
        loot = tm.run_task(task.FindLoot([Brian, Fergard, Ren], war_mode=[Brian, Fergard]))

        # Бриан далеко не уходит от мага
        # tm.run_task(task.MoveGroup(Fergard, [Brian], max_dst=7, stop=True), block=[loot])
        # tm.run_task(task.MoveGroup(Brian, [Ren, Fergard, Dubin_1, Dubin_2, Dubin_3]), block=[loot])
        # tm.run_task(task.WalkGroup([960, 165], Brian), block=[loot, retreat])
        # tm.run_task(task.WalkGroup([980, 160], Brian), block=[loot, retreat, retreat2])
        tm.run_task(task.UnitCircle(cls.w(6), Brian), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(7), Brian), block=[loot])

        tm.run_task(task.EnemyClear([Brian, Fergard, Ren], min_dst=5))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_4(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3, Brian])
        tm.run_task(task.Center(Brian))

        tm = cls.init_units(tm, 4)

        loot = tm.run_task(task.FindLoot([Brian, Fergard, Ren], war_mode=[Brian, Fergard]))

        # Бриан далеко не уходит от мага
        # tm.run_task(task.MoveGroup(Fergard, [Brian], min_dst=6, max_dst=7), block=[loot])
        # tm.run_task(task.MoveGroup(Brian, [Ren, Fergard, Dubin_1, Dubin_2, Dubin_3]), block=[loot])
        # tm.run_task(task.WalkGroup([985, 144], Brian), block=[loot, retreat])
        # tm.run_task(task.WalkGroup([975, 115], Brian), block=[loot, retreat])

        tm.run_task(task.UnitCircle(cls.w(8), Brian), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(9), Brian), block=[loot])

        tm.run_task(task.EnemyClear([Brian, Fergard, Ren], min_dst=5))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_5(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3, Brian])
        tm.run_task(task.Center(Brian))

        tm = cls.init_units(tm, 2)

        # Бриан далеко не уходит от мага
        tm.run_task(task.MoveGroup(Brian, [Ren, Fergard,Dubin_1, Dubin_2, Dubin_3]))

        tm.run_task(task.WalkS(cls.w(10), Brian))
        tm.run_task(task.EnemyClear([Brian, Fergard, Ren], min_dst=5, validation_interval=2))

        tm.run_task(task.GetLoot(Brian))
        if not tm.win:
            TM.defeat = 1

        if TM.defeat:
            return False
        return True


class M5(Mission):
    wp = {
        0: [[884, 94], 1],
        1: [[906, 79], 0],
        2: [[953, 86], 0],
        3: [[970, 95], 0],
        4: [[983, 90], 0],
        5: [[993, 73], 0],
        6: [[1002, 66], 1],
    }

    @classmethod
    def m_1(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.SetHeroesGroupV2([Fergard, Brian, Ren]))
        tm.run_task(task.Center(Fergard))
        tm.set_validate_group([Fergard, Ren, Brian])

        tm.run_task(task.MagicShield(Fergard, 5))
        tm.run_task(task.MagicShield(Ren, 5))
        tm.run_task(task.HealthMon(Fergard))
        tm.run_task(task.HealthMon(Ren))

        # retreat = tm.run_task(task.Retreat([Fergard, Ren], 4))
        loot = tm.run_task(task.FindLoot([Brian, Fergard, Ren], war_mode=[Brian, Fergard]))

        # Бриан далеко не уходит от мага
        tm.run_task(task.MoveGroup(Fergard, [Brian], min_dst=6, max_dst=7), block=[loot])
        # brian_move = tm.run_task(task.MoveGroup(Brian, [Ren, Fergard]), block=[loot])

        # tm.run_task(task.WalkGroup(cls.w(1), Brian), block=[loot])
        # tm.run_task(task.WalkGroup(cls.w(2), Brian), block=[loot])
        # tm.run_task(task.WalkGroup(cls.w(3), Brian), block=[loot])
        # tm.run_task(task.WalkGroup(cls.w(4), Brian), block=[loot])
        # tm.run_task(task.WalkGroup(cls.w(5), Brian), block=[loot])

        tm.run_task(task.WalkS(cls.w(1), Brian, group=[Fergard, Ren], walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.WalkS(cls.w(2), Brian, group=[Fergard, Ren], walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.WalkS(cls.w(3), Brian, group=[Fergard, Ren], walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.WalkS(cls.w(4), Brian, group=[Fergard, Ren], walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.WalkS(cls.w(5), Brian, group=[Fergard, Ren], walk_min_ct_radius=5), block=[loot])

        tm.run_task(task.Walk(cls.w(6), Fergard))
        tm.run_task(task.Wait(30))
        if not tm.win:
            TM.defeat = 1

        if TM.defeat:
            return False
        return True


class M6(Mission):
    wp = {
        0: [[955, 173], 1],
        1: [[964, 162], 0],
        2: [[960, 145], 0],
        3: [[965, 173], 0],
        4: [[991, 169], 0],
        5: [[1005, 157], 0],
        6: [[976, 127], 0],
        7: [[982, 85], 1],

        8: [[976, 52], 0],
        9: [[984, 83], 1],

        10: [[951, 70], 0],
        11: [[934, 76], 0],
        12: [[933, 71], 0],
        13: [[912, 78], 0],
        14: [[908, 73], 0],
        15: [[901, 80], 0],
        16: [[896, 77], 0],
        17: [[893, 67], 1],

        18: [[882, 66], 0],
        19: [[882, 83], 0],
        20: [[885, 65], 0],
        21: [[897, 74], 1],
    }

    @classmethod
    def setup(cls, tm):
        # Настройки поведения героев для 6 миссии

        tm.set_validate_group([Fergard, Ren, Brian, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        tm.run_task(task.MagicShield(Fergard, 5))
        tm.run_task(task.MagicShield(Ren, 5))
        tm.run_task(task.HealthMon(Fergard))
        tm.run_task(task.HealthMon(Ren))

        loot = tm.run_task(task.FindLoot([Brian, Fergard, Ren], war_mode=[Brian]))

        # Мечник далеко не уходит от мага
        tm.run_task(task.MoveGroup(Fergard, [SwordMan_1], min_dst=6, max_dst=7), block=[loot])
        swordman_move = tm.run_task(task.MoveGroup(SwordMan_1, [Ren, Fergard]), block=[loot])

        retreat = tm.run_task(task.Retreat([Fergard, Ren, Brian], 3))

        return tm, retreat, swordman_move, loot

    @classmethod
    def m_1(cls):
        # Кликаем по карте, чтобы войти в игру
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.SetHeroesGroupV2([Fergard, Brian, Ren, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        tm.run_task(task.Center(Fergard))

        tm, retreat, swordman_move, loot = cls.setup(tm)

        # Идём на сервер к людоеду
        tm.run_task(task.UnitCircle(cls.w(1), SwordMan_1), block=[loot])
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=6))
        tm.run_task(task.UnitCircle(cls.w(2), SwordMan_1), block=[loot])
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=6))
        tm.run_task(task.UnitCircle(cls.w(3), SwordMan_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(4), SwordMan_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(5), SwordMan_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(6), SwordMan_1), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(7), SwordMan_1), block=[loot])

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Center(Brian))
        loot = tm.run_task(task.FindLoot([Brian]))
        tm.run_task(task.Walk(cls.w(8), Brian), block=[loot])
        tm.run_task(task.Walk(cls.w(9), Brian), block=[loot])

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Center(Fergard))

        tm, retreat, swordman_move, loot = cls.setup(tm)
        tm.run_task(task.WalkGroup(cls.w(10), SwordMan_1), block=[loot, retreat])
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=6))

        tm.win_final = 0

        tm.run_task(task.WalkGroup(cls.w(11), SwordMan_1), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(12), SwordMan_1), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(13), SwordMan_1), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(14), SwordMan_1), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(15), SwordMan_1), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(16), SwordMan_1), block=[loot, retreat])

        tm.run_task(task.WalkGroup(cls.w(17), Brian), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(18), Brian), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(19), Brian), block=[loot, retreat])
        tm.run_task(task.WalkGroup(cls.w(20), Brian), block=[loot, retreat])
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=6))
        tm.run_task(task.WalkGroup(cls.w(21), Brian), block=[loot, retreat])

        if TM.defeat:
            return False

        if tm.win:
            cls.mm.win_option()
            return True

        return False


class M7(Mission):
    wp = {
        0: [[912, 170], 1],

        1: [[909, 145], 1],

        2: [[916, 123], 0],
        3: [[954, 121], 0],
        4: [[963, 101], 1],

        5: [[978, 72], 0],
        6: [[988, 60], 1],

    }

    @classmethod
    def setup(cls, tm):
        # Настройки поведения героев для 6 миссии

        tm.set_validate_group([Fergard, Ren, Brian, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        tm.run_task(task.MagicShield(Fergard, 5))
        tm.run_task(task.MagicShield(Ren, 5))
        tm.run_task(task.HealthMon(Fergard))
        tm.run_task(task.HealthMon(Ren))

        loot = tm.run_task(task.FindLoot([Brian, Nayra], war_mode=[Brian, Nayra]))

        return tm, loot

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.SetHeroesGroupV2([Fergard, Brian, Ren, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.Center(Fergard))
        tm.set_validate_group([Fergard, Ren, Brian, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        # Идём на сервер к лучнице
        tm.run_task(task.WalkGroup(cls.w(1), Fergard))
        tm.run_task(task.AllStop())
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.SetHeroesGroupV2([Nayra]))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm, loot = cls.setup(tm)

        tm.run_task(task.WalkGroup(cls.w(2), SwordMan_1), block=[loot])
        tm.run_task(task.WalkGroup(cls.w(3), SwordMan_1), block=[loot])
        tm.run_task(task.WalkGroup(cls.w(4), SwordMan_1), block=[loot])
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm, loot = cls.setup(tm)
        tm.win_final = 0

        tm.run_task(task.WalkGroup(cls.w(5), SwordMan_1), block=[loot])
        tm.run_task(task.WalkGroup(cls.w(6), SwordMan_1), block=[loot])
        loot = tm.run_task(task.FindLoot([Brian, Nayra]))
        tm.run_task(task.Wait(10))

        if TM.defeat:
            return False

        if tm.win:
            cls.mm.win_option()
            return True

        return False


class M8(Mission):
    """
    Могила короля
    """

    wp = {
        0: [[987, 61], 1],

        1: [[977, 78], 0],
        2: [[955, 84], 0],
        3: [[957, 98], 0],
        4: [[912, 104], 0],
        5: [[899, 91], 1],

        6: [[916, 120], 0],
        7: [[907, 130], 0],
        8: [[915, 148], 0],
        9: [[911, 158], 1],

        10: [[909, 171], 1],

        11: [[917, 86], 0],
        12: [[889, 53], 1],
    }

    @classmethod
    def setup(cls, tm):
        # Настройки поведения героев для 8 миссии

        tm.set_validate_group([Fergard, Ren, Brian, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        tm.run_task(task.MagicShield(Fergard, 5))
        tm.run_task(task.MagicShield(Ren, 5))
        tm.run_task(task.HealthMon(Fergard))
        tm.run_task(task.HealthMon(Ren))

        loot = tm.run_task(task.FindLoot([Brian, Nayra], war_mode=[Brian, Nayra]))

        # Мечник далеко не уходит от мага
        # fergard_move = tm.run_task(task.MoveGroup(Fergard, [SwordMan_1], min_dst=6, max_dst=7), block=[loot])
        # swordman_move = tm.run_task(task.MoveGroup(SwordMan_1, [Ren, Fergard]), block=[loot])

        return tm, loot

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        # В начале карты на нас наступают белки, поэтому надо обезопасить магов
        # Нужно сразу отправить магов в отступление, а уже потом назначать группы.
        # Кликаем по карте, чтобы войти в игру

        # Выбираем всех юнитов
        tm.run_task(task.AllGuard())
        tm.run_task(task.Wait(5))

        tm.run_task(task.SetHeroesGroupV2([Fergard, Brian, Ren, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm, loot = cls.setup(tm)
        retreat = tm.run_task(task.Retreat([Fergard, Ren, Brian, Nayra], 3))
        tm.run_task(task.UnitCircle(cls.w(1), SwordMan_1, walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(2), SwordMan_1, walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(3), SwordMan_1, walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(4), SwordMan_1, walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(5), Brian), block=[loot])

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Center(Fergard))
        tm, loot = cls.setup(tm)

        tm.run_task(task.UnitCircle(cls.w(6), SwordMan_1, walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(7), SwordMan_1, walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.UnitCircle(cls.w(8), SwordMan_1, walk_min_ct_radius=5), block=[loot])
        tm.run_task(task.WalkGroup(cls.w(9), SwordMan_1), block=[loot])

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Center(Fergard))
        # Идём за телепортом
        tm.run_task(task.Walk(cls.w(10), Fergard))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.Click([110, 753], True))
        tm.run_task(task.OpenInvent(False))
        tm.run_task(task.BindHotKeyMagic(Fergard, 'f7', 'teleport'))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_4(cls):
        cls.enter_game()
        tm = cls.init_tm()
        # Кликаем по карте, чтобы войти в игру
        # Передвижение с помощью телепорта:
        # 1. Открываем инвентарь, убираем меню магии.
        # 2. Ставим точку на мини-карте, передвигаемся телепортом к ней.
        # 3. Мониторим ману, принимаем эликсиры регенерации.
        # 4. Мониторим состояние щита.
        # Герой должен находить оптимальный маршрут до точки
        # Отступать, если мало хп и враг близко, затем лечиться, для этого у него должны сохранятся в память безопасные зоны
        #   для создания маршрутов отсутпления. Возможно создание нового маршрута, к команде по уже исследованому пути
        # Должен запоминать исследованое пространство и использовать для отсутпления только его, для этого он должен
        #   составлять неразрывную карту местности в своей памяти

        tm.run_task(task.Center(Fergard))
        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))
        tm.run_task(task.SelfManaRegen(Fergard, 30))
        tm.run_task(task.MagicShield(Fergard, 1, True))
        tm.run_task(task.SelfHealMon(Fergard, 100, 9))
        tm.run_task(task.MultiPort(cls.w(11), Fergard, min_mana=100))
        tm.run_task(task.MultiPort(cls.w(12), Fergard, war_mode=True, health_level=10, min_mana=100))

        if TM.defeat:
            return False
        return True


class M9(Mission):
    """
    Замок Каргалас
    """
    wp = {
        0: [[992, 172], 1],

        1: [[982, 143], 0],
        2: [[996, 100], 0],
        3: [[992, 92], 1],

        4: [[987, 87], 0],
        5: [[977, 80], 1],

        6: [[975, 77], 0],

        7: [[974, 79], 0],
        8: [[974, 73], 1],

        9: [[974, 68], 1],

        10: [[986, 87], 0],
        11: [[998, 86], 1],

        12: [[996, 81], 0],
        13: [[997, 77], 1],

        14: [[976, 76], 0],
        15: [[973, 67], 0],
        16: [[947, 56], 1],

        17: [[980, 61], 1],
    }

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))

        tm.run_task(task.SetHeroesGroupV2(
            [Fergard, Brian, Ren, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        # Настройки поведения героев для 9 миссии
        tm.set_validate_group([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))

        tm.run_task(task.UnitCircle(cls.w(1), SwordMan_1))
        tm.run_task(task.UnitCircle(cls.w(2), SwordMan_1))
        tm.run_task(task.UnitCircle(cls.w(3), SwordMan_1))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        # Назначаем групы для формирования строя

        archers = [SwordMan_1,
                   SwordMan_2,
                   SwordMan_3,
                   SwordMan_4]
        mages = [Brian,
                 Fergard,
                 Magican,
                 Ren]

        # Расчищаем место
        # group = Group(2, 4)
        # group.set_group(Nayra, archers)
        # tm.run_task(task.Group(group, max_dst=2, max_stop=1))
        # Занимаем позицию
        tm.run_task(task.Walk(cls.w(4), Nayra))

        group = Group(1, 4)
        group.set_group(Nayra, archers, mages)
        tm.run_task(task.Group(group, rotate=1))
        tm.run_task(task.ShiftWalk(Nayra, Ren, [0, -1], 1))
        tm.run_task(task.Wait(2))
        tm.run_task(task.AllStop())

        # Приманиваем первую группу врагов
        tm.run_task(task.LifeHiredUnits2([SwordMan_4]))
        tm.run_task(task.ClearSector(cls.w(5), SwordMan_4))
        tm.run_task(task.Wait(2))
        tm.run_task(task.Guard(SwordMan_4))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, Magican]))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=1))
        tm.run_task(task.ShiftWalk(Magican, Fergard, [0, -1], 1))
        tm.run_task(task.Wait(1))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        # Приманиваем первую группу врагов
        tm.run_task(task.LifeHiredUnits2([SwordMan_4]))
        tm.run_task(task.ClearSector(cls.w(6), SwordMan_4))
        tm.run_task(task.Wait(2))
        tm.run_task(task.Guard(SwordMan_4))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, Magican]))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=1))
        tm.run_task(task.FindLoot([Brian, Nayra], daemon=False, ignore_enemy=True))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_4(cls):
        # Поднимается наверх
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))

        tm.run_task(task.UnitCircle(cls.w(7), SwordMan_1, use_neuromap=False))

        archers = [SwordMan_1,
                   SwordMan_2,
                   SwordMan_3,
                   SwordMan_4]
        mages = [Brian,
                 Fergard,
                 Magican,
                 Ren]

        # Расчищаем место
        # group = Group(3, 4)
        # group.set_group(Nayra, archers)
        # tm.run_task(task.Group(group, max_dst=2, max_stop=1))

        tm.run_task(task.Walk(cls.w(8), Nayra))

        # Назначаем групы для формирования строя
        group = Group(1, 5)
        group.set_group(Nayra, archers, mages)
        tm.run_task(task.Group(group))

        tm.run_task(task.ShiftWalk(Nayra, Ren, [1, 0], 1))
        tm.run_task(task.Wait(2))
        tm.run_task(task.AllStop())

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.Wait(1))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_5(cls):
        # Поднимается наверх
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        # Приманиваем первую группу врагов
        tm.run_task(task.LifeHiredUnits2([SwordMan_2]))
        tm.run_task(
            task.LightResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4], Magican, wait_time=30, max_dst=6))
        tm.run_task(task.Wait(5))

        tm.run_task(task.SpeedMinus())
        tm.run_task(task.ClearSector(cls.w(9), SwordMan_2))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9, health_group=True))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=1))
        tm.run_task(task.SpeedPlus())

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_6(cls):
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        # Идём к людоеду
        tm.run_task(task.FindLoot([Brian, Nayra], daemon=False, ignore_enemy=True))

        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9, health_group=True, force_retreat=3))

        tm.run_task(task.UnitCircle(cls.w(10), SwordMan_1, use_neuromap=False))
        tm.run_task(task.UnitCircle(cls.w(11), SwordMan_1, use_neuromap=False))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.Wait(2))
        tm.run_task(task.AllStop())

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_7(cls):
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        # Приманиваем людоеда
        # Настройки поведения героев для 9 миссии
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))

        archers = [SwordMan_1,
                   SwordMan_2,
                   SwordMan_3,
                   SwordMan_4]
        mages = [Brian,
                 Fergard,
                 Magican,
                 Ren]

        # Расчищаем место
        group = Group(3, 4)
        group.set_group(Nayra, archers)
        tm.run_task(task.Group(group, max_dst=2, max_stop=1))

        tm.run_task(task.Walk(cls.w(12), Nayra))

        # Назначаем групы для формирования строя
        group = Group(1, 2)
        group.set_group(Nayra, mages, archers)

        tm.run_task(task.Group(group))
        tm.run_task(task.Wait(2))
        tm.run_task(task.AllStop())

        tm.run_task(task.ClearSector(cls.w(13), SwordMan_4))

        tm.run_task(task.EnemyClear([Nayra], min_dst=10, validation_interval=1, enemy_type=1))
        tm.run_task(task.FindLoot([Brian, Nayra], daemon=False, ignore_enemy=True))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_8(cls):
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))
        # Идём к замку
        tm.win_final = 0

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9, health_group=True, force_retreat=3))
        tm.run_task(task.Retreat([Fergard, Ren, Brian, Nayra], 2))

        tm.run_task(task.UnitCircle(cls.w(14), SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
        tm.run_task(task.UnitCircle(cls.w(15), SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
        tm.run_task(task.UnitCircle(cls.w(16), SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
        tm.run_task(task.EnemyClear([Nayra], min_dst=10, validation_interval=1))

        TM.win['9_7'] = False
        if tm.win:
            TM.win['9_7'] = True

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_9(cls):
        cls.enter_game()
        # Настройки валидации героев
        tm = cls.init_tm([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        # Сбор лута
        tm.win_final = 0
        tm.run_task(task.FindLoot([Brian, Nayra], daemon=False, ignore_enemy=True))

        loot = tm.run_task(task.FindLoot([Brian, Nayra]))
        tm.run_task(task.UnitCircle(cls.w(17), SwordMan_1, walk_min_ct_radius=4, use_neuromap=False), block=loot)

        tm.run_task(task.FindLoot([Brian, Nayra], daemon=False, ignore_enemy=True))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.Wait(1))

        if TM.defeat:
            return False

        win = tm.win
        if not win:
            win = TM.win['9_7']

        if win:
            cls.mm.win_option()
            return True

        return False


class M10(Mission):
    """
    За плащом
    """
    wp = {
        0: [[893, 60], 1],
        1: [[919, 56], 0],
        2: [[948, 97], 0],
        3: [[983, 76], 0],
        4: [[1003, 66], 0],
        5: [[1003, 60], 1],
    }

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.SetHeroesGroupV2([Fergard]))

        # Настройки поведения героев для 10 миссии
        tm.set_validate_group([Fergard])
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))

        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        # ms = tm.run_task(task.MagicShield(Fergard, 1, True))
        hp = tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        tm.run_task(task.SpeedMinus())

        tm.run_task(task.MultiPort(cls.w(1), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=1500))
        tm.run_task(task.MultiPort(cls.w(2), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=1500))
        tm.run_task(task.MultiPort(cls.w(3), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=1500))
        tm.run_task(task.MultiPort(cls.w(4), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=1500))

        tm.run_task(task.SpeedPlus())

        walk = task.MultiPort(cls.w(5), Fergard, min_mana=70)
        walk.walk_min_ct_radius = 1
        walk.walk_main_ct_radius = 1
        tm.run_task(walk)

        tm.remove_task(mr)
        tm.remove_task(hp)
        tm.remove_task(mp)
        # tm.remove_task(ms)

        tm.run_task(task.MagicMenu(True))
        tm.run_task(task.OpenInvent(False))

        # Лечим Данаса
        tm.run_task(task.Wait(1))

        tm.run_task(task.CastMagic(Fergard, [-5, -6], 'fireball'))
        tm.run_task(task.EnemyClear([Fergard], min_dst=10, validation_interval=1, max_wait=180))

        if TM.defeat:
            return False
        return True


class M11(Mission):
    """
    Разбойники
    """
    wp = {
        0: [[898, 157], 1],
        1: [[988, 160], 0],
        2: [[1000, 160], 0],
        3: [[1004, 117], 0],
        4: [[994, 111], 0],
        5: [[955, 97], 0],
        6: [[963, 85], 0],
        7: [[1002, 69], 0],
    }

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))

        tm.win_final = 0
        tm.run_task(task.SetHeroesGroupV2([Fergard]))

        # Настройки поведения героев для 10 миссии
        tm.set_validate_group([Fergard])
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))

        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        # ms = tm.run_task(task.MagicShield(Fergard, 1, True))
        hp = tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        #tm.run_task(task.SpeedMinus())

        # Идём на  восток
        tm.run_task(task.MultiPort(cls.w(1), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=500))
        tm.run_task(task.MultiPort(cls.w(2), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=500))
        # Идём на север
        tm.run_task(task.MultiPort(cls.w(3), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=500))

        # Идём на запад
        tm.run_task(task.MultiPort(cls.w(4), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=500))

        tm.run_task(task.MultiPort(cls.w(5), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=4,
                                   validation_interval=500))

        # Собираем посох
        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True))

        # Обновляем информацию об эликсирах
        tm.run_task(task.FindPotions(Fergard))

        tm.run_task(task.MultiPort(cls.w(6), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5,
                                   validation_interval=500))

        tm.run_task(task.MultiPort(cls.w(7), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=3,
                                   validation_interval=500))

        #tm.run_task(task.SpeedPlus())

        # Собираем арбалет
        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True))

        # Ожидаем
        tm.run_task(task.Wait(1))

        if tm.win:
            cls.mm.win_option()
            return True

        return False


class M12(Mission):
    """
    Обет Бриана
    """
    wp = {
        0: [[906, 59], 1],
        1: [[904, 95], 1],

        2: [[903, 107], 0],
        3: [[896, 107], 0],
        4: [[904, 128], 1],

        5: [[896, 140], 1],

        6: [[904, 149], 0],
        7: [[897, 138], 1],

        8: [[925, 128], 0],
        9: [[931, 126], 0],
        10: [[931, 127], 0],
        11: [[939, 124], 1],

        12: [[939, 128], 1],

        13: [[939, 134], 1],

        14: [[941, 127], 1],

        15: [[938, 140], 0],
        16: [[944, 154], 1],

    }

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        # Отвести всех от края
        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.AllGuard())
        tm.run_task(task.Click([470, 359]))
        tm.run_task(task.Wait(5))
        tm.run_task(task.AllStop())

        tm.run_task(task.SetHeroesGroupV2(
            [Fergard, Brian, Ren, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        # Соединяем вместе две группы
        tm.run_task(task.JoinGroups(Nayra, Brian))

        tm.run_task(task.SetHeroesGroupV2([Danas]))

        # TODO Проверка на врага. Если присутствует враг, значит команда назначена неверно - провал.

        # Настройки поведения героев для 9 миссии
        tm.set_validate_group([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))

        tm.run_task(task.UnitCircle(cls.w(1), SwordMan_1, walk_min_ct_radius=4))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Разбираемся с людоедами, собираем лут
        # Настройки поведения героев для 9 миссии
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))
        tm.run_task(task.UnitCircle(cls.w(2), SwordMan_1, walk_min_ct_radius=4))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=1, enemy_type=1))

        tm.run_task(task.MultiPort(cls.w(3), Fergard, min_mana=70, priority_custom=0))
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True))
        tm.run_task(task.OpenInvent(False))
        tm.run_task(task.MagicMenu(True))
        tm.run_task(task.FindLoot([Danas], ignore_enemy=True))
        tm.run_task(task.UnitCircle(cls.w(4), SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Разбираемся с людоедами, собираем лут

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9))
        tm.run_task(task.FindLoot([Danas], ignore_enemy=True))
        tm.run_task(task.Retreat([Fergard, Ren, Magican], 3))

        tm.run_task(task.Center(Danas))

        archers = [SwordMan_1,
                   SwordMan_2,
                   SwordMan_3,
                   SwordMan_4]
        mages = [Fergard,
                 Magican,
                 Ren]

        # Расчищаем место
        group = Group(3, 5)
        group.set_group(Danas, mages, archers)
        tm.run_task(task.Group(group, max_dst=2, rotate=2, max_stop=2))

        tm.run_task(task.UnitCircle(cls.w(5), SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=2))
        tm.run_task(task.FindLoot([Danas], daemon=False, ignore_enemy=True))
        tm.run_task(task.AllGuard())

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_4(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Собираем магом лут, подходим к мосту
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))
        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        tm.run_task(task.MultiPort(cls.w(6), Fergard, min_mana=70, priority_custom=0))

        # Собираем кольцо
        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True))

        # Обновляем информацию об эликсирах
        tm.run_task(task.FindPotions(Fergard))

        tm.run_task(task.MultiPort(cls.w(7), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_5(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])

        # Идём к мосту, попутно убиваем мышь
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9, force_retreat=3))
        tm.run_task(task.Retreat([Fergard, Ren, Magican], 3))

        tm.run_task(task.UnitCircle(cls.w(8), SwordMan_1, walk_min_ct_radius=5, use_neuromap=False))

        tm.add_task(task.Walk(cls.w(9), SwordMan_1))
        tm.add_task(task.Walk(cls.w(10), SwordMan_2))
        tm.run()

        tm.run_task(task.AllGuard())

        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=5, validation_interval=1))

        tm.run_task(task.UnitCircle(cls.w(11), SwordMan_1, walk_min_ct_radius=5, use_neuromap=False))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_6(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Идём к мосту, попутно убиваем мышь
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

        tm.add_task(task.Walk([939, 128], SwordMan_1))
        tm.add_task(task.Walk([938, 128], SwordMan_2))
        tm.add_task(task.Walk([937, 128], SwordMan_3))
        tm.add_task(task.Walk([936, 128], SwordMan_4))

        tm.add_task(task.Walk([935, 117], Brian))
        tm.add_task(task.Walk([936, 117], Nayra))

        tm.add_task(task.Walk([935, 122], Magican))
        tm.add_task(task.Walk([937, 122], Ren))
        tm.add_task(task.Walk([936, 122], Fergard))
        tm.run()

        tm.run_task(task.Wait(1))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_7(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))
        tm.run_task(task.OpenInvent(False))

        # Настройки поведения героев для 9 миссии
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

        resist = tm.run_task(
            task.LightResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Ren, Fergard, Magican], Magican,
                             wait_time=100, max_dst=9))
        tm.run_task(task.Wait(8))
        tm.remove_task(resist)
        tm.run_task(task.ClearSector(cls.w(13), SwordMan_2))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9, health_group=True))

        tm.run_task(task.SpeedMinus())
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=3, validation_interval=2))
        tm.run_task(task.Wait(10))
        tm.run_task(task.KillEnemyMagic(5, Fergard, "fireball", target_type=3, max_dst=20, wait_time=500, up_mana=1,
                                        far_target=False))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=15, validation_interval=2, enemy_type=3))
        tm.run_task(task.SpeedPlus())

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_8(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))
        # Настройки поведения героев для 9 миссии
        tm.run_task(task.Walk(cls.w(14), Fergard))

        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))
        tm.run_task(task.SelfManaRegen(Fergard, 30))
        hp = tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))
        tm.run_task(task.OpenInvent(False))
        tuman = tm.run_task(task.KillEnemyMagic(5, Fergard, "tuman", max_dst=10, wait_time=400, far_target=False))
        tuman2 = tm.run_task(task.KillEnemyMagic(7, Fergard, "tuman", max_dst=10, wait_time=400, far_target=False))
        tm.run_task(task.EnemyClear([Fergard], min_dst=10, validation_interval=2, max_wait=60))
        tm.remove_task(tuman)
        tm.remove_task(tuman2)

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

        tm.run_task(task.KillEnemyMagic(4, Fergard, "fire", max_dst=20, wait_time=200, far_target=False))
        tm.run_task(task.EnemyClear([Fergard], min_dst=20, validation_interval=2, max_wait=60))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_9(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.win_final = 0

        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.AllGuard())
        tm.run_task(task.Walk([939, 126], SwordMan_1))
        tm.run_task(task.Walk([937, 124], Danas))
        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.MultiPort([943, 133], Fergard, min_mana=70, priority_custom=0))
        # Собираем мешки справа
        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True, max_loot_dst=2))
        tm.run_task(task.MultiPort([930, 136], Fergard, min_mana=70, priority_custom=0))
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.FindLoot([Fergard, Danas], daemon=False, ignore_enemy=True))
        tm.run_task(task.MultiPort([938, 126], Fergard, min_mana=70, priority_custom=0))
        loot = tm.run_task(task.FindLoot([Danas], ignore_enemy=True))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
        tm.run_task(task.OpenInvent(False))
        tm.run_task(task.MagicMenu(True))
        tm.run_task(task.WalkGroup(cls.w(15), SwordMan_1))
        tm.run_task(task.FindLoot([Danas], daemon=False, ignore_enemy=True))

        # Спускаемся вниз
        tm.run_task(task.Center(Danas))

        archers = [SwordMan_1,
                   SwordMan_2,
                   SwordMan_3,
                   SwordMan_4]
        mages = [Fergard,
                 Magican,
                 Ren]

        # Расчищаем место
        group = Group(3, 5)
        group.set_group(Danas, mages, archers)
        tm.run_task(task.Group(group, max_dst=2, rotate=2, max_stop=2))
        loot = tm.run_task(task.FindLoot([Danas], ignore_enemy=True))
        tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9, force_retreat=7, health_group=True))
        tm.run_task(task.Retreat([Fergard, Ren, Magican, Brian, Danas], 3))
        tm.run_task(task.SpeedMinus())
        tm.run_task(task.UnitCircle(cls.w(16), SwordMan_1, walk_min_ct_radius=3), block=loot)
        tm.run_task(task.SpeedPlus())
        tm.run_task(task.Wait(5))

        if TM.defeat:
            return False

        if tm.win:
            cls.mm.win_option()
            return True

        return False


class M13(Mission):
    """
    Свиток Тенсеса
    """
    wp = {
        0: [[937, 170], 1],
        1: [[912, 158], 0],
        2: [[907, 133], 1],

        3: [[912, 129], 0],
        4: [[907, 135], 1],

        5: [[902, 126], 0],
        6: [[913, 116], 0],
        7: [[938, 117], 1],

        8: [[937, 112], 0],
        9: [[939, 99], 0],
        10: [[942, 96], 0],
        11: [[951, 85], 1],

        12: [[968, 87], 0],
        13: [[971, 81], 1],

        14: [[953, 87], 0],
        15: [[938, 117], 0],
        16: [[923, 117], 0],
        17: [[904, 107], 1],

        18: [[904, 107], 1],
    }

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        tm.run_task(task.SetHeroesGroupV2(
            [Fergard, Ren, Danas, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.SetMultiGroup(Arbalet_1, [Arbalet_1, Arbalet_2, Arbalet_3, Arbalet_4]))
        tm.run_task(task.SetMultiGroup(Nayra, [Brian, Nayra]))
        tm.set_validate_group([Fergard, Ren, Danas, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9, force_retreat=3, health_group=True))
        tm.run_task(task.Retreat([Fergard, Ren, Magican, Nayra, Danas], 3))
        tm.run_task(task.UnitCircle(cls.w(1), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(2), SwordMan_1, walk_min_ct_radius=3))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        tm = cls.init_tm()
        # Собираем магом лут, подходим к мосту
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))
        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        tm.run_task(task.MultiPort(cls.w(3), Fergard, min_mana=70, priority_custom=0))

        # Собираем кольцо
        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True))

        # Обновляем информацию об эликсирах
        tm.run_task(task.FindPotions(Fergard))

        tm.run_task(task.MultiPort(cls.w(4), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Идём наверх
        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9, force_retreat=3, health_group=True))

        tm.run_task(task.UnitCircle(cls.w(5), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(6), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(7), SwordMan_1, walk_min_ct_radius=3))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_4(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Атакуем мышей
        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9, force_retreat=3, health_group=True))

        tm.run_task(task.WalkS(cls.w(8), Arbalet_1, walk_min_ct_radius=5))
        tm.run_task(task.UnitCircle(cls.w(9), SwordMan_1, walk_min_ct_radius=3, use_neuromap=False))
        tm.run_task(task.WalkS(cls.w(10), Arbalet_1, walk_min_ct_radius=5))
        tm.run_task(task.UnitCircle(cls.w(11), SwordMan_1, walk_min_ct_radius=3, use_neuromap=False))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_5(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Атакуем мага
        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9, force_retreat=3, health_group=True))

        tm.run_task(task.FindLoot([Fergard], ignore_enemy=True))
        tm.run_task(task.UnitCircle(cls.w(12), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(13), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=5, validation_interval=1, max_wait=60, enemy_type=3))
        tm.run_task(task.AllGuard())
        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))

        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_6(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Ren, Danas, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])

        # Атакуем мышей
        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9, force_retreat=3, health_group=True))

        tm.run_task(task.UnitCircle(cls.w(14), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(15), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(16), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(17), SwordMan_1, walk_min_ct_radius=3))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_7(cls):
        cls.enter_game()
        # Собираем магом лут, подходим к мосту
        tm = cls.init_tm()
        tm.win_final = 0
        tm.run_task(task.Center(SwordMan_1))
        tm.add_task(task.Walk([893, 103], Arbalet_1, walk_min_ct_radius=5))
        tm.add_task(task.Walk([893, 102], SwordMan_1, walk_min_ct_radius=5))
        tm.add_task(task.Walk([893, 103], SwordMan_2, walk_min_ct_radius=5))
        tm.add_task(task.Walk([892, 105], SwordMan_3, walk_min_ct_radius=5))
        tm.add_task(task.Walk([894, 104], SwordMan_4, walk_min_ct_radius=5))
        tm.run()
        tm.run_task(task.Wait(3))

        tm.run_task(task.AllGuard())
        tm.run_task(task.Center(Ren))
        walk = task.Walk([891, 103], Ren, walk_min_ct_radius=3)
        walk.remove_task_if_hero_not_found = True
        tm.run_task(walk)
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=5, validation_interval=1, max_wait=60, enemy_type=3))
        tm.run_task(task.UnitCircle(cls.w(18), SwordMan_1, walk_min_ct_radius=3))

        tm.run_task(task.Walk([897, 105], Fergard, walk_min_ct_radius=3))
        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindLoot([Fergard], daemon=False, ignore_enemy=True))
        tm.run_task(task.Wait(3))

        if TM.defeat:
            return False

        if tm.win:
            cls.mm.win_option()
            return True

        return False


class M14(Mission):
    """
    Дорога к башне
    """
    wp = {
        0: [[890, 105], 1],
        1: [[901, 105], 0],
        2: [[906, 97], 1],

        3: [[935, 95], 0],
        4: [[948, 84], 1],

        5: [[963, 83], 0],
        6: [[969, 88], 0],
        7: [[988, 99], 1],

        8: [[998, 95], 0],
        9: [[1001, 109], 1],

    }

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))
        # Дорога к башне
        tm.run_task(task.SetHeroesGroupV2([Fergard]))

        # Настройки поведения героев для 10 миссии
        tm.set_validate_group([Fergard])
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.MagicMenu(False))
        tm.run_task(task.OpenInvent(True))
        tm.run_task(task.FindPotions(Fergard))

        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        ms = tm.run_task(task.MagicShield(Fergard, 1, True))
        hp = tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        # tm.run_task(task.SpeedMinus())

        # Идём на  восток
        tm.run_task(task.MultiPort(cls.w(1), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))
        tm.run_task(task.MultiPort(cls.w(2), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game(False)
        tm = cls.init_tm()
        tm.run_task(task.FindPotions(Fergard))
        tm.run_task(task.Center(Fergard))

        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        ms = tm.run_task(task.MagicShield(Fergard, 1, True))
        hp = tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        tm.run_task(task.MultiPort(cls.w(3), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))
        tm.run_task(task.MultiPort(cls.w(4), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game(False)
        tm = cls.init_tm()
        tm.run_task(task.Center(Fergard))
        tm.run_task(task.FindPotions(Fergard))

        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        ms = tm.run_task(task.MagicShield(Fergard, 1, True))
        hp = tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        tm.run_task(task.MultiPort(cls.w(5), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))
        tm.run_task(task.MultiPort(cls.w(6), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))
        tm.run_task(task.MultiPort(cls.w(7), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_4(cls):
        cls.enter_game(False)
        tm = cls.init_tm()

        tm.run_task(task.Center(Fergard))
        tm.run_task(task.DeEquipPosoh(Fergard, [905, 321]))
        tm.run_task(task.FindPotions(Fergard))

        mr = tm.run_task(task.SelfManaRegen(Fergard, 30))
        ms = tm.run_task(task.MagicShield(Fergard, 1, True))
        hp = tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
        mp = tm.run_task(task.SelfManaMon(Fergard, 200, 80))

        tm.run_task(task.MultiPort(cls.w(8), Fergard, min_mana=70, priority_custom=0, walk_min_ct_radius=5))
        tm.remove_task(hp)
        tm.remove_task(ms)
        tm.remove_task(mp)
        tm.remove_task(mr)
        tm.run_task(task.MultiPort(cls.w(9), Fergard, min_mana=100, priority_custom=0, walk_min_ct_radius=3, teleport_radius=14))
        tm.run_task(task.Wait(5))

        if TM.defeat:
            return False
        return True


class M15(Mission):
    """
    Последняя битва
    """
    wp = {
        0: [[891, 142], 1],
        1: [[899, 136], 1],

        2: [[902, 132], 1],

        3: [[920, 134], 1],

        4: [[915, 134], 0],
        5: [[909, 124], 0],
        6: [[925, 144], 0],
        7: [[951, 151], 0],
        8: [[961, 144], 1],

        9: [[954, 146], 0],
        10: [[958, 138], 0],
        11: [[960, 128], 1],

        12: [[988, 134], 1],

        13: [[980, 101], 1],

        14: [[959, 144], 0],
        15: [[982, 140], 0],
        16: [[999, 130], 0],
        17: [[997, 94], 1],

        18: [[973, 64], 1],

        19: [[961, 56], 1],

        20: [[942, 66], 1],

        21: [[937, 58], 0],
        22: [[921, 56], 1],

        23: [[905, 61], 0],
        24: [[900, 72], 0],
        25: [[895, 68], 1],

        26: [[921, 88], 0],
        27: [[915, 87], 1],

        28: [[901, 87], 1],

        29: [[924, 90], 0],
        30: [[933, 97], 0],
        31: [[930, 100], 1],

        32: [[940, 105], 1],

    }

    @classmethod
    def m_1(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Click(cls.w(0)))

        tm.run_task(task.SetHeroesGroupV2(
            [Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.SetMultiGroup(Nayra, [Brian, Nayra]))

        tm.run_task(task.ValidateHeroesGroup())

        tm.set_validate_group([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        tm.run_task(task.UnitCircle(cls.w(1), SwordMan_1, walk_min_ct_radius=3))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_2(cls):
        cls.enter_game()
        tm = cls.init_tm()
        # Группировка на мосту
        tm.run_task(task.Center(Fergard))

        tm.add_task(task.Walk(cls.w(2), SwordMan_3))
        tm.add_task(task.Walk([902, 137], SwordMan_4))
        tm.run()

        tm.run_task(task.Walk([911, 134], SwordMan_1, group=[SwordMan_1, SwordMan_2], walk_min_ct_radius=3))
        tm.run_task(task.Walk([911, 134], SwordMan_1))
        tm.run_task(task.Walk([911, 136], SwordMan_2))

        tm.add_task(task.Walk([908, 134], Skrakan))
        tm.add_task(task.Walk([908, 135], Danas))
        tm.run()

        tm.add_task(task.Walk([907, 134], Fergard))
        tm.add_task(task.Walk([907, 135], Rud))
        tm.run()

        tm.run_task(task.Walk([906, 134], Nayra))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_3(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Сражение на мосту
        tm.run_task(task.Center(SwordMan_2))

        tm.run_task(task.ClearSector(cls.w(3), SwordMan_2))
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))
        tm.run_task(task.EnemyClear([SwordMan_1, SwordMan_4], min_dst=10, validation_interval=1))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_4(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.Center(Fergard))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))
        tm.run_task(task.UnitCircle(cls.w(4), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.WalkS(cls.w(5), SwordMan_1, group=[SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4],
                               walk_min_ct_radius=3))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=1))
        tm.run_task(task.UnitCircle(cls.w(6), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(7), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(8), SwordMan_1, walk_min_ct_radius=3))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_5(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Сражение с гоблинами
        tm.run_task(task.Center(Nayra))
        tm.run_task(task.Walk(cls.w(9), Nayra))
        tm.run_task(task.Center(Danas))
        tm.run_task(task.Walk(cls.w(10), Danas))

        archers = [SwordMan_1,
                   SwordMan_2,
                   SwordMan_3,
                   SwordMan_4]
        mages = [Rud,
                 Fergard,
                 Skrakan]

        # Расчищаем место
        group = Group(1, 3)
        group.set_group(Danas, archers, mages)
        tm.run_task(task.Group(group))
        tm.run_task(task.ShiftWalk(Danas, SwordMan_4, [1, 1]))

        tm.run_task(task.MagicShield(Fergard, 30))
        tm.run_task(task.MagicShield(Rud, 90))
        tm.run_task(task.MagicShield(Skrakan, 90))

        tm.run_task(task.ClearSector([960, 128], SwordMan_4))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))
        tm.run_task(task.AllGuard())
        tm.run_task(task.Stop(Nayra))

        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=2))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_6(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Center(Rud))
        tm.run_task(task.MultiPort(cls.w(12), Rud, min_mana=70, walk_min_ct_radius=5, teleport_radius=10))
        tm.run_task(task.Center(Skrakan))
        tm.run_task(task.MultiPort([988, 132], Skrakan, min_mana=70, walk_min_ct_radius=5))
        tm.run_task(task.Walk([988, 134], Rud))
        tm.run_task(task.Walk([988, 132], Skrakan))
        tm.run_task(task.Wait(1))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_7(cls):
        cls.enter_game()
        tm = cls.init_tm()
        tm.run_task(task.Center(Rud))
        tm.run_task(task.EarthResist([Skrakan, Rud], Skrakan, wait_time=20))
        tm.run_task(
            task.KillEnemyMagic(3, Skrakan, "firewall", max_dst=20, wait_time=500, far_target=False))
        tm.run_task(
            task.KillEnemyMagic(6, Skrakan, "firewall", max_dst=20, wait_time=500, far_target=False))

        tm.run_task(task.EnemyClear([Skrakan], min_dst=15, validation_interval=2))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_8(cls):
        cls.enter_game()
        tm = cls.init_tm()
        # Битва с людоедами
        tm.run_task(task.Center(Skrakan))
        tm.run_task(task.MultiPort(cls.w(13), Skrakan, min_mana=70, walk_min_ct_radius=5))
        tm.run_task(task.Walk([980, 102], Skrakan))
        tm.run_task(task.Wait(2))
        tm.run_task(task.WaterResist([Skrakan], Skrakan, wait_time=20))
        tm.run_task(task.SelfHealMon(Skrakan, 100, 8))
        tm.run_task(
            task.KillEnemyMagic(3, Skrakan, "tuman", max_dst=20, wait_time=500, far_target=False))
        tm.run_task(
            task.KillEnemyMagic(6, Skrakan, "tuman", max_dst=20, wait_time=500, far_target=False))
        tm.run_task(task.EnemyClear([Skrakan], min_dst=15, validation_interval=7))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_9(cls):
        cls.enter_game()
        tm = cls.init_tm()
        # Сбор группы
        tm.run_task(task.Center(Skrakan))
        tm.run_task(task.MultiPort(cls.w(14), Skrakan, min_mana=70, walk_min_ct_radius=5, teleport_radius=7))
        tm.run_task(task.Center(Rud))
        tm.run_task(task.MultiPort([960, 144], Rud, min_mana=70, walk_min_ct_radius=5, teleport_radius=10))

        tm.set_validate_group([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))
        tm.run_task(task.UnitCircle(cls.w(15), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(16), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.UnitCircle(cls.w(17), SwordMan_1, walk_min_ct_radius=3))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_10(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Черепашки
        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))

        tm.run_task(task.MagicShield(Fergard, 30))
        tm.run_task(task.MagicShield(Rud, 90))
        tm.run_task(task.MagicShield(Skrakan, 90))

        tm.run_task(task.UnitCircle(cls.w(18), SwordMan_1, walk_min_ct_radius=3))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_11(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])

        tm.run_task(task.Center(Fergard))
        tm.run_task(
            task.EarthResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4], Skrakan,
                             wait_time=180, max_dst=5))
        tm.run_task(task.Wait(5))
        tm.run_task(
            task.LightResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4], Skrakan,
                             wait_time=180, max_dst=5))
        tm.run_task(task.Wait(5))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))
        tm.run_task(task.WalkS(cls.w(19), SwordMan_1, group=[SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4],
                               walk_min_ct_radius=3))

        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=1))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_12(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Змейки
        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))

        tm.run_task(task.MagicShield(Fergard, 30))
        tm.run_task(task.MagicShield(Rud, 90))
        tm.run_task(task.MagicShield(Skrakan, 90))

        tm.run_task(task.UnitCircle(cls.w(20), SwordMan_1, walk_min_ct_radius=3))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=10, validation_interval=1))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_13(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Skrakan])
        # Мышки
        tm.run_task(task.Center(Rud))

        tm.run_task(task.MultiPort(cls.w(21), Rud, min_mana=70, teleport_radius=10))
        tm.run_task(task.MultiPort(cls.w(22), Rud, min_mana=70, walk_min_ct_radius=1, teleport_radius=10))

        tm.run_task(task.MultiPort(cls.w(21), Fergard, min_mana=70))
        tm.run_task(task.MultiPort([923, 56], Fergard, min_mana=70, walk_min_ct_radius=1))

        tm.run_task(task.MultiPort(cls.w(21), Skrakan, min_mana=70))
        tm.run_task(task.MultiPort([920, 59], Skrakan, min_mana=70, walk_min_ct_radius=1))

        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))

        tm.run_task(
            task.KillEnemyMagic(6, Rud, "grad", max_dst=20, wait_time=500, far_target=False))
        tm.run_task(task.EnemyClear([Skrakan], min_dst=10, validation_interval=1))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_14(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Skrakan])
        # Дракон
        tm.run_task(task.Center(Rud))

        tm.run_task(task.MultiPort(cls.w(23), Rud, min_mana=70, teleport_radius=10))
        tm.run_task(task.MultiPort(cls.w(24), Rud, min_mana=70, walk_min_ct_radius=1, teleport_radius=10))
        tm.run_task(task.MultiPort(cls.w(23), Skrakan, min_mana=70))
        tm.run_task(task.MultiPort([900, 70], Skrakan, min_mana=70, walk_min_ct_radius=1))
        tm.run_task(task.MultiPort(cls.w(23), Fergard, min_mana=70))
        tm.run_task(task.MultiPort([899, 72], Fergard, min_mana=70, walk_min_ct_radius=1))

        tm.run_task(task.Center(Skrakan))
        tm.run_task(task.LightResist([Fergard], Skrakan, wait_time=180, max_dst=10))
        tm.run_task(task.LightResist([Rud], Skrakan, wait_time=180, max_dst=10))
        tm.run_task(task.Wait(2))

        tm.run_task(task.MultiPort(cls.w(25), Skrakan, min_mana=70, walk_min_ct_radius=1))
        tm.run_task(
            task.KillEnemyMagic(0, Skrakan, "prok", max_dst=20, wait_time=3000, far_target=False, priority_custom=1))

        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))

        tm.run_task(task.Center(Skrakan))

        # Свет на скракана
        tm.run_task(task.DarkDetector(Fergard, Skrakan, 3, True))

        tm.run_task(
            task.KillEnemyMagic(3, Skrakan, "grad", max_dst=20, wait_time=500, far_target=False, priority_custom=3))

        tm.run_task(
            task.KillEnemyMagic(3, Rud, "grad", max_dst=20, wait_time=500, far_target=False, priority_custom=3))

        tm.run_task(task.EnemyClear([Skrakan], min_dst=10, validation_interval=2))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_15(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
        # Орки
        tm.run_task(task.Center(SwordMan_1))

        # TODO проходят через духов. Разбить маршрут.
        tm.run_task(task.Walk(cls.w(26), SwordMan_1, walk_min_ct_radius=4, group=[SwordMan_1, SwordMan_2, SwordMan_3,
                                                                                  SwordMan_4, Danas, Nayra]))

        tm.run_task(task.MultiPort([919, 88], Rud, min_mana=70, teleport_radius=10))
        tm.run_task(task.MultiPort([919, 89], Skrakan, min_mana=70))
        tm.run_task(task.MultiPort([918, 88], Fergard, min_mana=70))

        tm.run_task(task.Walk(cls.w(27), Danas))

        archers = [SwordMan_1,
                   SwordMan_2,
                   SwordMan_3,
                   SwordMan_4]
        mages = [Rud,
                 Fergard,
                 Skrakan]

        # Расчищаем место
        group = Group(1, 3)
        group.set_group(Danas, archers, mages)
        tm.run_task(task.Group(group, rotate=1))
        tm.run_task(task.ShiftWalk(Danas, SwordMan_4, [1, 0]))
        tm.run_task(task.ShiftWalk(SwordMan_2, SwordMan_4, [0, -2]))
        tm.run_task(task.Wait(3))
        tm.run_task(task.AllStop())

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_16(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])

        # Борьба с орками
        tm.run_task(task.Center(SwordMan_1))

        tm.run_task(task.MagicShield(Fergard, 30))
        tm.run_task(task.MagicShield(Rud, 90))
        tm.run_task(task.MagicShield(Skrakan, 90))

        tm.run_task(task.ClearSector(cls.w(28), SwordMan_1))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.HealthMon(Rud, wait_time=200, health_level=9, health_group=True))

        tm.run_task(
            task.KillEnemyMagic(4, Skrakan, "firewall", max_dst=20, wait_time=1000, far_target=False))
        tm.run_task(
            task.KillEnemyMagic(4, Rud, "firewall", max_dst=20, wait_time=1000, far_target=False))

        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=8, validation_interval=2))
        if TM.defeat:
            return False
        return True

    @classmethod
    def m_17(cls):
        cls.enter_game()
        tm = cls.init_tm([Fergard, Rud, Danas, Skrakan, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])

        # Духи
        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9))
        tm.run_task(task.UnitCircle(cls.w(29), SwordMan_1, walk_min_ct_radius=3))

        tm.run_task(task.MagicShield(Fergard, 30))
        tm.run_task(task.MagicShield(Rud, 90))
        tm.run_task(task.MagicShield(Skrakan, 90))

        tm.run_task(task.Wait(2))
        tm.run_task(task.AllGuard())
        tm.run_task(task.WalkS(cls.w(30), SwordMan_1, group=[SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=8, validation_interval=1))

        tm.run_task(task.UnitCircle(cls.w(31), SwordMan_1, walk_min_ct_radius=3))

        tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))

        if TM.defeat:
            return False
        return True

    @classmethod
    def m_18(cls):
        cls.enter_game()
        tm = cls.init_tm()

        # Сражение с демоном
        tm.any_dialog_win = 1

        tm.run_task(task.Center(Fergard))
        tm.run_task(task.DeEquipPosoh(Fergard))
        tm.run_task(task.Center(SwordMan_1))

        tm.run_task(task.EarthResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Fergard], Skrakan, wait_time=180,
                                     max_dst=10))
        tm.run_task(task.Wait(6))
        tm.run_task(
            task.LightResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Fergard, Rud], Skrakan, wait_time=180,
                             max_dst=10))
        tm.run_task(task.Wait(7))
        tm.run_task(task.FireResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Fergard], Skrakan, wait_time=180,
                                    max_dst=10))
        tm.run_task(task.Wait(6))
        tm.run_task(task.WaterResist([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Fergard], Skrakan, wait_time=180,
                                     max_dst=10))
        tm.run_task(task.Wait(6))

        tm.run_task(task.MultiPort([936, 99], Skrakan, min_mana=70, walk_min_ct_radius=3))
        tm.run_task(task.WalkS(cls.w(32), SwordMan_1, group=[SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4],
                               walk_min_ct_radius=3))
        tm.run_task(task.MultiPort([941, 99], Fergard, min_mana=70, walk_min_ct_radius=3))
        tm.run_task(task.MultiPort([942, 99], Skrakan, min_mana=70, walk_min_ct_radius=3))
        tm.run_task(task.MultiPort([943, 99], Rud, min_mana=70, walk_min_ct_radius=3))
        tm.run_task(task.HealthMon(Fergard, wait_time=200, health_level=9, health_group=True))

        tm.run_task(task.Center(SwordMan_1))
        tm.run_task(task.EnemyClear([SwordMan_1], min_dst=8, validation_interval=1))

        # TODO  Миссия завершается, а бот об этом не знает

        if TM.defeat:
            return False
        return True