# Идея. Добавлять в менеджер задач, задания с разным приоритетом, чтобы менеджер решал их
import time
import app.mission.action as action
import app.config as cfg
from app.mission.monitor import Names
from app.mission.actions import Actions
from app.mission.monitor import MainScreen, Positions
from app.mission.heroes import *
from app.mission.store import FoundEnemy, WalkInfo, UserPotions, UnitCircleInfo, UnitStatus, ActionsDone
from app.funcitons import pr
import random

import time
from hashlib import sha1
import numpy as np
import os.path
import cv2
import keras
from app.mission.store import FoundInScreen


class Task:
    # Задача, добавляемая в цикл задач
    # Уникальный номер задачи. Назначается, при добавлении в менеджер задач.
    task_id = 0
    # Родительская задача.
    # 0 - Нет родителя
    # id - родителя. Задача будет выполнятся только после выполнения родительской
    task_parent = 0
    # id высшей задачи, которая может блокировать данную задачу.
    block_task_ids = []
    # Блокировка связанной задачи
    block = 0
    # id - действия, порождённого задачей
    act_id = 0
    last_run_act_id = 0
    last_run_act_result = 0

    # Имя задачи
    task_name = ''

    # Юнит, который должен выполнить задачу
    unit_group = 1
    unit = Hero()

    # Время создания задачи
    start_time = 0

    # Время, по истечении которого, задача снимается
    close_time = 0

    # Что делать в случаи истечения времени
    # 0 - задача считается успешно выполненной
    # 1 - остановка скрипта
    # 2 - загрузка игры
    time_out_action = 0

    # Результат:
    #   0 - действие не требуется,
    #   1 - победа,
    #   2 - остановка скрипта,
    #   3 - загрузка игры
    result = 0

    # Вес задачи:
    #   0 - Основные задчаи. Если их нет, цикл прерывается. Например достижение КТ
    #   1 - Дополнительные задачи. Не влияют на приривание цикла. Например каст защиты от магии.
    weight = 1

    # Статус задачи.
    #   0 - На удаление.
    #   1 - Активная.
    #   2 - Ожидает выполнения родительской.
    status = 1

    # Действие, которое нужно совершить
    action = False
    last_action = False

    # Приоритет задач
    priority_custom = 0

    blocked = 0

    # Основная информация с экрана
    ms = MainScreen()

    def __init__(self):
        self.block_task_ids = []
        pass

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        self.ms = ms
        self.last_run_act_id = last_run_act_id
        self.last_run_act_result = last_run_act_result
        self.last_action = self.action
        self.action = False

    def get_action(self):
        # Получить задачу
        return self.action

    def module_name(self):
        return 'Задача-' + str(self.task_id)

    def task_blocked(self, blocked=False):
        # Выполненяется в случае блокирования данной задачи другой задачей
        self.blocked = blocked

    pass


class TaskOne(Task):
    # Одинарная задача. Снимается после выполнения.
    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем, когда была последняя команда
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Задача была выполнена, снимаем её
            self.status = 0
            return
        self.default_action()

    def default_action(self):
        pass


class WalkTask(Task):
    # Набор функций, используемых для ходьбы

    def walk_shift_from_mini_map_abs_crd(self, crd):
        # Смещение координаты y для ходьбы
        return [crd[0] + 15, crd[1] + 15]

    def walk_shift_abs_crd(self, crd):
        # Смещение координаты y для ходьбы
        return [crd[0] - 2, crd[1] + 13]

    def get_distance(self, path, hero):
        # Дистанция до мини цели
        x_path_sm = path[0] - hero[0]
        y_path_sm = path[1] - hero[1]
        # Дистанция до мини цели
        cpm_dist = round((abs(x_path_sm) ** 2 + abs(y_path_sm) ** 2) ** 0.5, 1)
        return cpm_dist

    def validate_screen_crd(self, crd):
        # Валидация клика в пределах экрана
        if cfg.screen_shift.get('x') < crd[0] < cfg.main_window.get('x') and \
                cfg.screen_shift.get('y') < crd[1] < cfg.main_window.get('ym'):
            return True
        return False


class Walk(WalkTask):
    # Идти к контрольной точке. Задача создаётся в модуле стратегии
    task_name = 'Передвижение к цели'
    priority = 5
    # Основная задача
    weight = 1
    # Координаты группы на экране
    unit_abs = []
    unit_abs_last = []
    # Координаты следующиего шага на экране
    next_click_abs_crd = []
    # Координаты следующиего шага на карте
    next_click_map_crd = []

    # Координаты главной КТ, на мини-карте
    main_target_crd = []
    # Координаты, локальной КТ, на мини-карте
    local_target_crd = []
    # Текущие координаты юнита, на мини-карте
    unit_crd = []
    unit_crd_last = []
    # Отношение размера экрана к размеру карты
    k_size = 0
    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 2
    # Разброс дистацнии для локальной цели
    walk_min_move_radius = 5
    walk_main_ct_radius = 5

    # Последняя команда движения
    last_move_time = 0
    # Последнее центрирование
    last_center_time = 0
    # Максимальное количество центрирований подряд
    need_center_max_count = 5
    # Всего центрирований
    need_center_count = 0

    # Влияние передвижения персонажа, на создание нового движения к КТ.
    # Счётчик остановки юнита. Обнуляется после создания действия по передвижению.
    unit_stop = 0
    # Если юнит стоит, больше допустимого, создаётся следующая задача по передвжиению.
    max_unit_stop = 10

    # Центрирование положения героя на карте
    last_center_map_validate = 0

    # Центрируем с небольшим интервалом, чтобы не забивать эфир в случае,
    # когда герой стоит на горе и не проходит валидацию.
    # Если герой стоит, понижаем интервал обновления центра.
    center_map_validate_interval = 1

    # Последние координаты передвижения
    last_abs_crd = []
    # Количество повторяющихся координат
    abs_crd_count = 0
    # Максимальное количество повторяющихся координат
    max_abs_crd_count = 3
    # Дополнительный радиус движения
    append_move_radius = 0
    max_move_radius = 10
    # Добавление радиуса движения к дополнительному
    shift_move_radius = 5

    # Движение в группе
    walk_group = False
    # Движение в режиме боеготовности
    walk_s = False

    # Использованные невалидные сектора
    used_sectors = dict()

    # Обзор юнита
    unit_view_radius = 9

    # Использование карты нейросети, для определения движения
    use_neuromap = False
    is_retreat = False

    remove_task_if_hero_not_found = False

    # Группы передвижения
    group = []

    # Идея для движения в группе. Если маги и слабые юниты слишком близко к цели, они должны отходить назад
    # или останавливаться, чтобы проспустить вперёд танков. Иначе они могут нарваться на сильных врагов и помереть.
    # Останавливаться не выгодно, потому, что они могут заблокировать проход или мост. Лучше если они будут
    # отходить от цели на расстояние большее чем главный юнит, идущий к цели.

    def __init__(self, target_abs=[], unit=False, use_neuromap=False, walk_min_ct_radius=2, group=[]):
        self.unit_abs = []
        self.unit_abs_last = []
        self.next_click_abs_crd = []
        self.next_click_map_crd = []
        self.main_target_crd = []
        self.local_target_crd = []
        self.unit_crd = []
        self.unit_crd_last = []
        self.last_abs_crd = []
        self.use_neuromap = use_neuromap
        self.walk_min_ct_radius = walk_min_ct_radius
        self.group = group

        if len(target_abs):
            self.set_main_target(target_abs)
        self.unit_group = 1
        if unit:
            self.unit_group = unit.group
        self.used_sectors = dict()

        WalkInfo.local_target_crd = []

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        if not len(self.main_target_crd):
            # Если нет координат, снимаем задачу
            self.status = 0
            return

        # Проверяем, когда была последняя команда центрирования других задач по передвижению
        if self.last_run_act_id:
            if not self.last_action or self.last_run_act_id != self.last_action.act_id:
                last_action = ActionsDone.actions[self.last_run_act_id]
                if last_action.act_type == 2:
                    # Уже была запущена команда центрирования другим модулем, ожидаем своей очереди.
                    return

        # Получаем координаты героя на мини-карте
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.get_hero_map_crd()
        if not self.unit_crd:
            pr(self.module_name(), 'Координаты героя не найдены', self.ms.cmd)
            if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id \
                    and self.last_action.act_type == 2:

                if self.remove_task_if_hero_not_found:
                    pr(self.module_name(), 'Герой не найден, отмена действия')
                    self.status = 0
                    return

                pr(self.module_name(), 'Берём координаты по центру карты')
                # Последняя выполненная задача - центрирование
                # Берём координаты по центру карты
                # y,x
                unit_abs = [332, 425]
                hero_shift = self.ms.get_shift(unit_abs)
                self.unit_crd = self.abs_to_map_crd(hero_shift)
            else:
                # Центрируем
                self.action = action.Center(self.unit_group)
                return

        # Получаем координаты героя на экране
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.ms.get_hero_abs(self.unit_group)

        # Требуется ли центрирование?
        if self.need_center():
            self.action = action.Center(self.unit_group)
            return

        # Была ли достигнута локальная цель?
        task_target_done = False
        action_target_done = False
        if self.local_target_crd:
            # Да. Проверяем её достижение
            # Расстояние до локальной цели
            local_target_dst = self.get_distance(self.local_target_crd, self.unit_crd)
            # print('DST abs',
            #      self.mon_map_abs(self.local_target_crd), self.mon_map_abs(self.unit_crd))
            # print('DST', local_target_dst, self.local_target_crd, self.unit_crd)

            # Минимальный радиус до цели
            min_radius = self.walk_min_ct_radius + self.append_move_radius
            if local_target_dst > 10:
                # При большом расстоянии до цели, перемещаемся свободно
                min_radius = self.walk_min_move_radius + self.append_move_radius

            if local_target_dst <= min_radius:
                main_target_dst = self.get_distance(self.main_target_crd, self.unit_crd)
                # print('DST1', self.main_target_crd, self.unit_crd, main_target_dst)
                # Была ли достигнута глобальная цель:
                if main_target_dst <= self.walk_main_ct_radius + self.append_move_radius:
                    # Глобальная цель достигнута
                    task_target_done = True
                # Локальная цель достигнута, создаём новую.
                action_target_done = True

        if task_target_done:
            # Если цель достигнута. Ставим статус задачи - на удаление. Выходим из цикла.
            pr(self.module_name(), 'Мы прибыли к КТ')
            self.status = 0
            WalkInfo.local_target_crd = []
            return

        if action_target_done:
            # Мы достигли мини-цели. Создаём новую цель
            self.last_move_time = 0
            self.add_walk_action()
            return

        # Ожидаем прибытия к цели. Создаём новую цель, если герой стоит на месте.

        # Это первый вход в цикл?
        if not self.local_target_crd:
            # Создаём новую цель
            self.add_walk_action()
            return

        # Проверяем, когда была последняя команда движения
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 1:
                # Если это была команда передвижения
                # Ждём 1 сек.
                self.last_move_time = time.time()
                # Сбрасываем счётчик остановки
                self.unit_stop = 0

        # Проверка времени последней команды движения
        if self.last_move_time:
            curr_time = time.time()
            if curr_time - self.last_move_time < 1:
                # Последняя команда была менее секунды назад, выходим.
                return

        # Проверяем движение к цели. Прошло более секунды, после последней команды
        if self.max_unit_stop_count():
            # Цель стоит более 10 циклов. Создаём новую задачу перемещения
            self.add_walk_action()

        pass

    def vaildate_click_crd(self):
        self.ms.ss.get_valid_places(self.unit_group, self.unit_view_radius)

        valid_crd = self.next_click_abs_crd
        # Находим валидный фрагмент для перемещения
        # Проверяем его с предыдущим, если он такой-же и дистанция не поменялась, ищем следующий
        scale = 32
        screen_crd_x = int((self.next_click_abs_crd[0] - cfg.screen_shift['x']) / scale)
        screen_crd_y = int((self.next_click_abs_crd[1] - cfg.screen_shift['y']) / scale)
        screen_crd = [screen_crd_x, screen_crd_y]
        # Находим расстояние до точки перемещения от валидных точек

        safe_sectors = []
        unsafe_sectors = []

        if len(self.ms.ss.valid_places):
            for key, sector in self.ms.ss.valid_places.items():
                sector_crd = [sector[0], sector[1]]
                sector_num = sector[2]
                sector_dst = sector[4]
                dst = self.get_distance(screen_crd, sector_crd)
                item = [dst, sector_crd, key, sector_dst]
                if sector_num == 1:
                    safe_sectors.append(item)
                else:
                    unsafe_sectors.append(item)
                pass

        # Сначала просматриваем безопасные варианты
        new_crd = self.get_crd_from_sectors(safe_sectors)

        if not len(new_crd):
            new_crd = self.get_crd_from_sectors(unsafe_sectors)

        if new_crd:
            pr(self.module_name(), 'Назначена валидные координаты ', new_crd, ' для группы ', self.unit_group)
            valid_crd = new_crd

        return valid_crd

    def get_crd_from_sectors(self, sectors):
        # Позиция героя на карте
        map_position = str(self.unit_crd[0]) + '-' + str(self.unit_crd[1])
        used_sectors = self.used_sectors.get(map_position)

        new_crd = []

        reverse = False
        if self.is_retreat:
            # Находим самую дальнюю точку от кт, чтобы отступить назад
            reverse = True

        if len(sectors):
            sectors.sort(key=lambda x: x[0], reverse=reverse)

            for sector in sectors:
                sector_crd = sector[1]
                sector_key = sector[2]
                sector_dst = sector[3]

                # Проверка на использованные сектора
                if used_sectors:
                    used_sector = used_sectors.get(sector_key)
                    if used_sector:
                        if used_sector == 2:
                            # сектор не валидный
                            continue
                        else:
                            if self.unit_crd[0] == self.unit_crd_last[0] and self.unit_crd[1] == self.unit_crd_last[1]:
                                # Мы стоим на месте
                                pr(self.module_name(), 'Координаты не подходят для перемещения, поиск новых.',
                                   sector_key)
                                used_sectors[sector_key] = 2
                                continue
                            pass

                else:
                    used_sectors = dict()

                if self.is_retreat:
                    # Находим самую дальнюю точку от кт, чтобы отступить назад.
                    # Если точка не будет найдена, берём последнюю валидную
                    print(sector_dst, self.retreat_radius)
                    if sector_dst > self.retreat_radius:
                        new_crd = self.abs_crd_32(sector_crd)
                        continue

                used_sectors[sector_key] = 1
                new_crd = self.abs_crd_32(sector_crd)

                break

        self.used_sectors[map_position] = used_sectors

        return new_crd

    def abs_crd_32(self, crd):
        # Получаем абслютные координаты мыши из пикселей экрана
        self.update_k_size()
        scr_crd = [crd[0] * 32, crd[1] * 32]
        abs_crd = [cfg.screen_shift['x'] + scr_crd[0], cfg.screen_shift['y'] + scr_crd[1]]
        return abs_crd

    def max_unit_stop_count(self):
        # Счётчик движения юинита
        ret = False
        if not self.unit_move_validate():
            self.unit_stop += 1

        if self.unit_stop > self.max_unit_stop:
            ret = True

        return ret

    def unit_move_validate(self):
        # Валидация движения юнита
        move = True
        # Если абсолютные коодринаты равны
        if self.unit_abs_last and self.unit_abs and self.unit_abs_last[0] == self.unit_abs[0] \
                and self.unit_abs_last[1] == self.unit_abs[1]:
            # И если кординаты на мини-карте равны
            if self.unit_crd_last and self.unit_crd_last[0] == self.unit_crd[0] \
                    and self.unit_crd_last[1] == self.unit_crd[1]:
                move = False

        return move

    def need_center(self):
        # Проверяем, когда была последняя команда центрирования
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 2:
                # Если это была команда центрирования
                # Ждём 1 сек.
                self.last_center_time = time.time()

        # Центрирование каждую секунду
        if self.last_center_time == 0:
            # Обновления небыло ни разу
            pr(self.module_name(), '# Первое центрирование')
            return True

        curr_time = time.time()

        if curr_time - self.last_center_time > 1:
            # Если прошло больше 1 секунды, центруем.
            pr(self.module_name(), '# Центрирование каждую секунду')
            return True

        # Логика центрирования карты вокруг героя
        need_center = True
        # Наши действия:
        # 1. Проверяем положение целевой группы на карте. Группа на карте?
        if self.unit_abs:
            # 1. Да.
            # Проверяем группа в центре карты или с краю. Группа в центре?
            if self.center_map_validate():
                # 1. Да - центровка не требуется
                need_center = False
                self.need_center_count = 0
            else:
                # 2. Нет. Мы с краю мини-карты или нет?
                if self.map_far_away_border_validate():
                    # 1. Далеко от края. Центруем
                    self.need_center_count += 1
                    pr(self.module_name(), '# Мы не в центре карты и далеко от края')
                else:
                    # 2. С краю - создаём задание для перемещения.
                    need_center = False
                    self.need_center_count = 0
        else:
            # 2. Нет - центруем карту вогкруг группы.
            need_center = True
            pr(self.module_name(), '# Координаты героя не найдены, центрируем', self.need_center_count)
            self.need_center_count += 1
            # Возможно группа просто не найдена
            pass

        if self.need_center_count > self.need_center_max_count:
            self.need_center_count = 0
            need_center = False

        return need_center

    def add_walk_action(self):
        # Создание действия перемещения героя к контрольной точке.
        self.next_step()

        if self.walk_group:
            # Движение в группе
            if self.walk_s:
                self.action = action.GroupWalkS(self.unit_group, self.next_click_abs_crd)
            else:
                self.action = action.GroupWalk(self.unit_group, self.next_click_abs_crd)
        else:
            # Движение одного юнита
            if self.walk_s:
                self.action = action.WalkS(self.unit_group, self.next_click_abs_crd, self.group)
            else:
                self.action = action.Walk(self.unit_group, self.next_click_abs_crd, self.group)

    def set_main_target(self, target_abs):
        # Назначить КТ
        # Перевод в относительные координаты
        self.main_target_crd = [target_abs[0] - cfg.mon_map['x'], target_abs[1] - cfg.mon_map['y']]
        pr(self.module_name(), 'Назначена контрольная точка ', self.main_target_crd, ' для группы ', self.unit_group)
        pass

    def map_far_away_border_validate(self):
        # Проверка положения героя. С края карты или нет
        min_b = 15
        if min_b < self.unit_crd[0] < 128 - min_b and min_b < self.unit_crd[1] < 128 - min_b:
            # Герой далеко от края
            return True
        # Герой с краю карты
        return False

    def center_map_validate(self):
        curr_time = time.time()
        if curr_time - self.last_center_map_validate < self.center_map_validate_interval:
            # Проверяем движение героя
            if self.ms.hero_is_stay(self.unit_group):
                # Если герой стоит, пропускаем проверки и центрируем не чаще чем указано
                # в настройках center_map_validate_interval
                return True

        self.last_center_map_validate = curr_time

        # Проверка нахождения героя в центре экрана
        # hero_center = {'x': 368, 'y': 271, 'width': 150, 'height': 150}
        hero_center = cfg.hero_center
        if hero_center['x'] < self.unit_abs[0] < (hero_center['x'] + hero_center['width']) \
                and hero_center['y'] < self.unit_abs[1] < (hero_center['y'] + hero_center['height']):
            return True
        else:
            pr(self.module_name(), 'Герой не в центре :', hero_center['x'], '<', self.unit_abs[0], '<',
               (hero_center['x'] + hero_center['width']), 'and', hero_center['y'], '<', self.unit_abs[1], '<',
               (hero_center['y'] + hero_center['height']))
        # Валидация положения героя по центру карты
        return False

    def abs_to_map(self, crd):
        # Координаты x, y
        return self.abs_to_map_crd([crd[1], crd[0]])

    def abs_to_map_crd(self, hero_abs):
        # Координаты y,x
        # Положение героя на маленьком экране
        # +14 смещение вниз по y, чтобы указать на центр квадрата.
        mini_crd = self.abs_to_mini(hero_abs[1], hero_abs[0] + 14)
        # print('mini_crd', mini_crd)
        # Вычисляем координаты героя на мини-карте
        hero = self.hero_in_map(mini_crd)
        # print('hero', hero)
        return hero

    def get_hero_map_crd(self, unit_group=-1):
        if unit_group == -1:
            unit_group = self.unit_group

        hero = []
        hero_abs = self.ms.get_hero(unit_group)
        # print('hero_abs', hero_abs)
        if hero_abs:
            hero = self.abs_to_map_crd(hero_abs)
        return hero

    def abs_to_mini(self, x, y):
        # Перевод из кординат экрана в координаты мини карты
        self.update_k_size()
        screen_crd = [x - cfg.screen_shift['x'], y - cfg.screen_shift['y']]
        mini_x = round(screen_crd[0] / self.k_size, 0)
        mini_y = round(screen_crd[1] / self.k_size, 0)
        mini_crd = [mini_x, mini_y]
        return mini_crd

    def update_k_size(self):
        if self.k_size == 0:
            self.k_size = self.ms.mm.k_size()

    def hero_in_map(self, hero):
        # Положение героя на карте
        hero_map = [hero[0] + self.ms.mm.screen[0][0] + cfg.mm_x_shift, hero[1] +
                    self.ms.mm.screen[0][1] + cfg.mm_y_shift]
        return hero_map

    def hero_screen(self, hero):
        # Положение героя на экране
        hero_screen = [hero[0] - self.ms.mm.screen[0][0] - cfg.mm_x_shift,
                       hero[1] - self.ms.mm.screen[0][1] - cfg.mm_y_shift]
        return hero_screen

    def abs_crd(self, x, y):
        # Получаем абслютные координаты мыши из относительных коодринат мини-карты
        self.update_k_size()
        scr_crd = [x * self.k_size, y * self.k_size]
        abs_crd = [cfg.screen_shift['x'] + scr_crd[0], cfg.screen_shift['y'] + scr_crd[1]]
        return abs_crd

    def mon_map_abs(self, monmap):
        return [monmap[0] + cfg.mon_map['x'], monmap[1] + cfg.mon_map['y']]

    def next_step(self):
        self.last_abs_crd = self.next_click_abs_crd
        # Вычилсяем данные для следующего шага
        # Получаем координаты героя на большой карте
        hero = self.unit_crd
        # pr(self.module_name(),'hero', hero)
        # Путь по x, y, который предстоит пройти
        x_path = self.main_target_crd[0] - hero[0]
        y_path = self.main_target_crd[1] - hero[1]
        # path = [x_path, y_path]
        # pr(self.module_name(),"path", path)

        # Модуль пути
        x_path_abs = abs(x_path)
        y_path_abs = abs(y_path)
        # Дистанция до цели
        dist = round((x_path_abs ** 2 + y_path_abs ** 2) ** 0.5, 1)
        pr(self.module_name(), 'Дистанция до КТ :', dist)

        # Определяем сектор нашего движения
        # Размер экрана
        screen_hw = self.ms.mm.screen_hw()
        # pr(self.module_name(),'screen_hw', screen_hw)

        # Находится ли цель в пределах экрана
        if self.ms.mm.screen[0][0] < self.main_target_crd[0] < self.ms.mm.screen[1][0] and self.ms.mm.screen[0][1] < \
                self.main_target_crd[1] < self.ms.mm.screen[1][1]:
            # Назначаем координаты цели
            pr(self.module_name(), 'КТ в пределах видимости')
            mini_map_crd = self.hero_screen(self.main_target_crd)

            # Локальная цель приравнивается к глобальной
            self.local_target_crd = self.main_target_crd

        else:
            pr(self.module_name(), 'КТ за пределами видимости')
            radius = screen_hw[1] / 2
            if x_path >= 0 and y_path <= 0:
                # 0-90
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
            elif x_path >= 0 and y_path >= 0:
                # 90-180
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
                pass
            elif x_path <= 0 and y_path >= 0:
                # 180 - 270
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

                pass
            else:
                # 270 - 360
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

            dist_small = [x_small, y_small]
            # pr(self.module_name(),'dist_small', dist_small)

            # Координаты мини-цели на мини-карте
            self.local_target_crd = [round(dist_small[0] + hero[0], 0), round(dist_small[1] + hero[1], 0)]
            pr(self.module_name(), 'Назначена текущая мини цель: ', self.local_target_crd)

            # Позиция героя на экране
            hero_screen = self.hero_screen(hero)
            # pr(self.module_name(),'hero_screen', hero_screen)

            # Координаты на экране
            mini_map_crd = [hero_screen[0] + (dist_small[0]), hero_screen[1] + (dist_small[1])]

        mini_map_crd = [round(mini_map_crd[0], 0), round(mini_map_crd[1], 0)]

        # Определяем относительные координаты мыши
        abs_crd = self.abs_crd(mini_map_crd[0], mini_map_crd[1])

        # Валидация координат относительно реального экрана
        border = 30
        min_left = cfg.screen_shift['x'] + border
        if abs_crd[0] < min_left:
            abs_crd[0] = min_left

        max_right = cfg.screen_shift['x'] + cfg.main_window['x'] - border
        if abs_crd[0] > max_right:
            abs_crd[0] = max_right

        min_top = cfg.screen_shift['y'] + border
        if abs_crd[1] < min_top:
            abs_crd[1] = min_top

        max_bottom = cfg.screen_shift['y'] + cfg.main_window['ym'] - border - 60

        if abs_crd[1] > max_bottom:
            abs_crd[1] = max_bottom

        # pr(self.module_name(),'abs_crd', abs_crd)
        walk_crd = self.walk_shift_from_mini_map_abs_crd(abs_crd)
        self.next_click_abs_crd = [int(walk_crd[0]), int(walk_crd[1])]

        # Валидация остановки
        if self.last_abs_crd and self.next_click_abs_crd[0] == self.last_abs_crd[0] and self.next_click_abs_crd[1] == \
                self.last_abs_crd[1]:

            if not self.blocked:
                # Если на задачу не действует блокировка
                self.abs_crd_count += 1
                if self.abs_crd_count >= self.max_abs_crd_count:
                    self.abs_crd_count = 0
                    if self.append_move_radius < self.max_move_radius:
                        self.append_move_radius += self.shift_move_radius
                        pr(self.module_name(), 'Радуис точки прибытия увеличен на ', self.append_move_radius)
            else:
                # pr(self.module_name(), 'Задача по передвижению заблокирована другой задачей')
                pass
        else:
            self.abs_crd_count = 0
            if self.append_move_radius > 0:
                self.append_move_radius = 0
                pr(self.module_name(), 'Радис точки прибытия установлен по умолчанию')

        if self.use_neuromap:
            self.next_click_abs_crd = self.vaildate_click_crd()

        self.next_click_map_crd = self.abs_to_map_crd([self.next_click_abs_crd[1], self.next_click_abs_crd[0]])
        WalkInfo.local_target_crd = self.next_click_map_crd
        self.local_target_crd = self.next_click_map_crd


class MultiPort(Walk):
    # Перемещаться к контрольной точке с помощью телепорта
    task_name = 'Телепортация к цели к цели'
    priority = 5
    # Основная задача
    weight = 1
    # Координаты группы на экране
    unit_abs = []
    unit_abs_last = []
    # Координаты следующиего шага на экране
    next_click_abs_crd = []

    # Координаты главной КТ, на мини-карте
    main_target_crd = []
    # Координаты, локальной КТ, на мини-карте
    local_target_crd = []
    # Локальная цель предыдущего хода
    local_last_target_crd = []
    # Дистанция до основной цели
    main_target_dst = -1
    # Текущие координаты юнита, на мини-карте
    unit_crd = []
    unit_crd_last = []
    # Отношение размера экрана к размеру карты
    k_size = 0
    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 2
    # Разброс дистацнии для локальной цели
    walk_min_move_radius = 5
    walk_main_ct_radius = 5

    # Последняя команда движения
    last_move_time = 0
    # Последнее центрирование
    last_center_time = 0
    # Максимальное количество центрирований подряд
    need_center_max_count = 5
    # Всего центрирований
    need_center_count = 0

    # Влияние передвижения персонажа, на создание нового движения к КТ.
    # Счётчик остановки юнита. Обнуляется после создания действия по передвижению.
    unit_stop = 0
    # Если юнит стоит, больше допустимого, создаётся следующая задача по передвжиению.
    max_unit_stop = 10

    # Центрирование положения героя на карте
    last_center_map_validate = 0

    # Центрируем с небольшим интервалом, чтобы не забивать эфир в случае,
    # когда герой стоит на горе и не проходит валидацию.
    # Если герой стоит, понижаем интервал обновления центра.
    center_map_validate_interval = 1

    # Последние координаты передвижения
    last_abs_crd = []
    # Количество повторяющихся координат
    abs_crd_count = 0
    # Максимальное количество повторяющихся координат
    max_abs_crd_count = 3
    # Дополнительный радиус движения
    append_move_radius = 0
    max_move_radius = 10
    # Добавление радиуса движения к дополнительному
    shift_move_radius = 5

    # Радиус телепортации
    teleport_radius = 8
    retreat_radius = 6

    # Интервал между валидациями
    validation_interval = 500
    # Последнее время телепортации
    last_teleport_time = 0

    # Минимальное количество маны, необходимое для следующей телепортации
    min_mana = 60
    # Идти вперёд, если нет маны
    walk_no_mana = False

    # Отступать, если враг близко, воевать с врагами
    war_mode = False

    # Минимальная дистанция до цели, перед отступлением
    stop_dst = 5
    retreat_dst = 3
    health_level = 10
    # Отступление
    is_retreat = False

    used_sectors = dict()

    # Движение осуществляется по следующему алгоритму
    # 1. Центрирование, проверка маны.
    # 2. Если мана есть выбор координат.
    #   Проверяем расстояние до цели, если телепорт не первый и расстояние не изменилось,
    #   возможно мы не смогли телепортироваться
    #   Проверяем хешкод последней цели перемещения, если он совпадает, выбираем новую цель.
    # Если цель перемещения валидна, телепортируемся на неё.
    # Если маны нет, ожидание, либо приём эликсира.

    def __init__(self, target_abs=[], unit=1, min_mana=60, war_mode=False, stop_dst=5, retreat_dst=2, health_level=10,
                 priority_custom=5, walk_min_ct_radius=2, validation_interval=500, teleport_radius=8):
        self.unit = unit
        self.unit_abs = []
        self.unit_abs_last = []
        self.next_click_abs_crd = []
        self.main_target_crd = []
        self.local_target_crd = []
        self.unit_crd = []
        self.unit_crd_last = []
        self.last_abs_crd = []
        self.set_main_target(target_abs)
        self.unit_group = unit.group
        self.min_mana = min_mana
        self.war_mode = war_mode
        self.stop_dst = stop_dst
        self.retreat_dst = retreat_dst
        self.health_level = health_level
        self.used_sectors = dict()
        self.priority_custom = priority_custom
        self.walk_min_ct_radius = walk_min_ct_radius
        self.validation_interval = validation_interval
        self.teleport_radius = teleport_radius

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Получаем координаты героя на мини-карте
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.get_hero_map_crd()
        if not self.unit_crd:
            pr(self.module_name(), 'Координаты героя не найдены', self.ms.cmd)
            if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id \
                    and self.last_action.act_type == 2:
                pr(self.module_name(), 'Берём координаты по центру карты')
                # Последняя выполненная задача - центрирование
                # Берём координаты по центру карты
                # y,x
                unit_abs = [332, 425]
                hero_shift = self.ms.get_shift(unit_abs)
                self.unit_crd = self.abs_to_map_crd(hero_shift)
            else:
                # Центрируем
                self.action = action.Center(self.unit_group)
                return

        # Получаем координаты героя на экране
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.ms.get_hero_abs(self.unit_group)

        # Проверяем, когда была последняя команда телепортирования
        curr_time = time.time()
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 1:
                # Если это была команда телепортирования
                if self.last_run_act_result == 0:
                    # Команда не сработала, ищем новую точку
                    pass
                else:
                    # Ждём 1 сек.
                    self.last_teleport_time = time.time()

        wait_time = curr_time - self.last_teleport_time
        if wait_time < self.validation_interval / 1000:
            # Если прошло больше 1 секунды, центруем.
            pr(self.module_name(), 'Ожидаем', wait_time, '<', self.validation_interval / 1000)
            return

        # Требуется ли центрирование?
        if self.need_center():
            self.action = action.Center(self.unit_group)
            return

        self.local_last_target_crd = self.local_target_crd

        # Проверяем врагов рядом, если враги найдены, телепортируемся в точку наиболее удалённую от врагов
        self.is_retreat = False
        enemy_dst = []

        if self.war_mode:
            # Проверяем врагов
            self.ms.update_units()
            enemy = self.ms.p.get_valid_units()

            if enemy:
                # Рассчитываем дистанцию до героя
                hero = self.ms.cmd.get(self.unit_group)
                if hero and hero[0] > 0:
                    for e in enemy:
                        dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                        # Дистанция, враг, герой
                        enemy_dst.append([dst, e])
                        pass
                pass

            if enemy_dst:
                enemy_dst.sort(key=lambda x: x[0], reverse=False)
                min_dst = enemy_dst[0]
                if min_dst[0] < self.retreat_dst * 32:
                    # Враг близко, отступаем
                    health_status = self.health_status()
                    if health_status != -1 and health_status > self.health_level:
                        # Здоровье не потрачено, не отступаем
                        return

                    # Должна быть мана на каст
                    pr(self.module_name(), 'Враг близко, отступаем', min_dst[0], '<', self.retreat_dst * 32)
                    self.is_retreat = True
                    self.add_teleport_action()
                    return

                elif min_dst[0] < self.stop_dst * 32:
                    # Враг в пределах видимости, остановка
                    pr(self.module_name(), 'Враг в пределах видимости, остановка', min_dst[0], '<', self.stop_dst * 32)
                    return

        if self.main_target_dst == -1:
            # Это первый вход в цикл
            self.add_teleport_action()
            return

        # Была ли достигнута локальная цель телепорта?
        task_target_done = False

        if self.main_target_dst:
            # Это не первый вход в цикл
            local_target_dst = self.get_distance(self.local_target_crd, self.unit_crd)

            # Минимальный радиус до цели
            min_radius = self.walk_min_ct_radius + self.append_move_radius
            if local_target_dst > 10:
                # При большом расстоянии до цели, перемещаемся свободно
                min_radius = self.walk_min_move_radius + self.append_move_radius

            if local_target_dst <= min_radius:
                main_target_dst = self.get_distance(self.main_target_crd, self.unit_crd)
                # Была ли достигнута глобальная цель:
                if main_target_dst <= self.walk_main_ct_radius + self.append_move_radius:
                    # Глобальная цель достигнута
                    task_target_done = True

        if task_target_done:
            # Если цель достигнута. Ставим статус задачи - на удаление. Выходим из цикла.
            pr(self.module_name(), 'Мы прибыли к КТ')
            self.status = 0
            return

        # Цель не достигнута? Создаём новую
        self.add_teleport_action()
        return

    def add_teleport_action(self):
        # Проверяем количество маны
        mana = self.ms.ss.curr_mana
        min_mana = self.min_mana
        user_mana = -1
        if mana[1] != -1:
            if min_mana > mana[1]:
                # Максимальная мана не может быть больше, чем у юнита
                min_mana = mana[1]
            user_mana = mana[0]

        # Проверяем количество маны
        need_mana = False
        if user_mana != -1:
            if self.is_retreat:
                if user_mana < 60:
                    need_mana = True
                    pass
            elif user_mana < min_mana:
                need_mana = True
                pass

        teleport_action = True

        if need_mana:
            # Ожидаем восстановления маны
            pr(self.module_name(), 'Ожидаем восстановления маны', mana[0], '<', min_mana)
            if not self.is_retreat and self.walk_no_mana:
                # Если разрешено идти, движемся дальше
                teleport_action = False
                pass
            else:
                return

        # Создание действия перемещения героя к контрольной точке.
        self.next_step()
        # Валидация нового шага
        valid_crd = self.vaildate_click_crd()

        if self.walk_no_mana:
            target_dst = self.get_distance(valid_crd, self.unit_crd)

        if teleport_action:
            self.action = action.Teleport(self.unit_group, valid_crd)
            self.action.priority = self.priority_custom
        else:
            # Проверяем, когда была последняя команда движения
            if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
                # Наша предыдущая команад была запущена
                if self.last_action.act_type == 1:
                    # Если это была команда передвижения
                    # Ждём 1 сек.
                    self.last_move_time = time.time()
                    # Сбрасываем счётчик остановки
                    self.unit_stop = 0

            # Проверка времени последней команды движения
            if self.last_move_time:
                curr_time = time.time()
                if curr_time - self.last_move_time < 1:
                    # Последняя команда была менее секунды назад, выходим.
                    return

            # Проверяем движение к цели. Прошло более секунды, после последней команды
            if self.max_unit_stop_count():
                # Цель стоит более 10 циклов. Создаём новую задачу перемещения
                self.action = action.WalkS(self.unit_group, valid_crd)

        if self.is_retreat:
            self.action.priority = 0

    def vaildate_click_crd(self):
        self.ms.ss.get_valid_places(self.unit_group, self.teleport_radius)

        valid_crd = self.next_click_abs_crd
        # Находим валидный фрагмент для перемещения
        # Проверяем его с предыдущим, если он такой-же и дистанция не поменялась, ищем следующий
        scale = 32
        screen_crd_x = int((self.next_click_abs_crd[0] - cfg.screen_shift['x']) / scale)
        screen_crd_y = int((self.next_click_abs_crd[1] - cfg.screen_shift['y']) / scale)
        screen_crd = [screen_crd_x, screen_crd_y]
        # Находим расстояние до точки перемещения от валидных точек

        safe_sectors = []
        unsafe_sectors = []

        if len(self.ms.ss.valid_places):
            for key, sector in self.ms.ss.valid_places.items():
                sector_crd = [sector[0], sector[1]]
                sector_num = sector[2]
                sector_dst = sector[4]
                dst = self.get_distance(screen_crd, sector_crd)
                item = [dst, sector_crd, key, sector_dst]
                if sector_num == 1:
                    safe_sectors.append(item)
                else:
                    unsafe_sectors.append(item)
                pass

        # Сначала просматриваем безопасные варианты
        new_crd = self.get_crd_from_sectors(safe_sectors)

        if not len(new_crd):
            new_crd = self.get_crd_from_sectors(unsafe_sectors)

        if new_crd:
            pr(self.module_name(), 'Назначена валидные координаты ', new_crd, ' для группы ', self.unit_group)
            valid_crd = new_crd

        return valid_crd

    def get_crd_from_sectors(self, sectors):
        # Позиция героя на карте
        map_position = str(self.unit_crd[0]) + '-' + str(self.unit_crd[1])
        used_sectors = self.used_sectors.get(map_position)

        new_crd = []

        reverse = False
        if self.is_retreat:
            # Находим самую дальнюю точку от кт, чтобы отступить назад
            reverse = True

        if len(sectors):
            sectors.sort(key=lambda x: x[0], reverse=reverse)

            for sector in sectors:
                sector_crd = sector[1]
                sector_key = sector[2]
                sector_dst = sector[3]

                # Проверка на использованные сектора
                if used_sectors:
                    used_sector = used_sectors.get(sector_key)
                    if used_sector:
                        if used_sector == 2:
                            # сектор не валидный
                            continue
                        else:
                            if self.unit_crd[0] == self.unit_crd_last[0] and self.unit_crd[1] == self.unit_crd_last[1]:
                                # Мы стоим на месте
                                pr(self.module_name(), 'Координаты не подходят для перемещения, поиск новых.',
                                   sector_key)
                                used_sectors[sector_key] = 2
                                continue
                            pass

                else:
                    used_sectors = dict()

                if self.is_retreat:
                    # Находим самую дальнюю точку от кт, чтобы отступить назад.
                    # Если точка не будет найдена, берём последнюю валидную
                    print(sector_dst, self.retreat_radius)
                    if sector_dst > self.retreat_radius:
                        new_crd = self.abs_crd_32(sector_crd)
                        continue

                used_sectors[sector_key] = 1
                new_crd = self.abs_crd_32(sector_crd)

                break

        self.used_sectors[map_position] = used_sectors

        return new_crd

    def abs_crd_32(self, crd):
        # Получаем абслютные координаты мыши из пикселей экрана
        self.update_k_size()
        scr_crd = [crd[0] * 32, crd[1] * 32]
        abs_crd = [cfg.screen_shift['x'] + scr_crd[0], cfg.screen_shift['y'] + scr_crd[1]]
        return abs_crd

    def next_step(self):
        self.last_abs_crd = self.next_click_abs_crd
        # Вычилсяем данные для следующего шага
        # Получаем координаты героя на большой карте
        hero = self.unit_crd
        # pr(self.module_name(),'hero', hero)
        # Путь по x, y, который предстоит пройти
        x_path = self.main_target_crd[0] - hero[0]
        y_path = self.main_target_crd[1] - hero[1]
        # path = [x_path, y_path]
        # pr(self.module_name(),"path", path)

        # Модуль пути
        x_path_abs = abs(x_path)
        y_path_abs = abs(y_path)
        # Дистанция до цели
        dist = round((x_path_abs ** 2 + y_path_abs ** 2) ** 0.5, 1)
        self.main_target_dst = dist
        pr(self.module_name(), 'Дистанция до КТ :', dist)

        # Определяем сектор нашего движения
        # Размер экрана
        screen_hw = self.ms.mm.screen_hw()
        # pr(self.module_name(),'screen_hw', screen_hw)

        # Находится ли цель в пределах экрана
        if self.ms.mm.screen[0][0] < self.main_target_crd[0] < self.ms.mm.screen[1][0] and self.ms.mm.screen[0][1] < \
                self.main_target_crd[1] < self.ms.mm.screen[1][1]:
            # Назначаем координаты цели
            pr(self.module_name(), 'КТ в пределах видимости')
            mini_map_crd = self.hero_screen(self.main_target_crd)

            # Локальная цель приравнивается к глобальной
            self.local_target_crd = self.main_target_crd

        else:
            pr(self.module_name(), 'КТ за пределами видимости')
            radius = screen_hw[1] / 2
            if x_path >= 0 and y_path <= 0:
                # 0-90
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
            elif x_path >= 0 and y_path >= 0:
                # 90-180
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = radius
                    y_small = x_small * y_path / x_path
                pass
            elif x_path <= 0 and y_path >= 0:
                # 180 - 270
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

                pass
            else:
                # 270 - 360
                if x_path_abs <= y_path_abs:
                    # Составляем пропроцию
                    y_small = -radius
                    x_small = y_small * x_path / y_path
                else:
                    x_small = -radius
                    y_small = x_small * y_path / x_path

            dist_small = [x_small, y_small]
            # pr(self.module_name(),'dist_small', dist_small)

            # Координаты мини-цели на мини-карте
            self.local_target_crd = [round(dist_small[0] + hero[0], 0), round(dist_small[1] + hero[1], 0)]
            pr(self.module_name(), 'Назначена текущая мини цель: ', self.local_target_crd)

            # Позиция героя на экране
            hero_screen = self.hero_screen(hero)
            # pr(self.module_name(),'hero_screen', hero_screen)

            # Координаты на экране
            mini_map_crd = [hero_screen[0] + (dist_small[0]), hero_screen[1] + (dist_small[1])]

        mini_map_crd = [round(mini_map_crd[0], 0), round(mini_map_crd[1], 0)]

        # Определяем относительные координаты мыши
        abs_crd = self.abs_crd(mini_map_crd[0], mini_map_crd[1])

        # Валидация координат относительно реального экрана
        border = 30
        min_left = cfg.screen_shift['x'] + border
        if abs_crd[0] < min_left:
            abs_crd[0] = min_left

        max_right = cfg.screen_shift['x'] + cfg.main_window['x'] - border
        if abs_crd[0] > max_right:
            abs_crd[0] = max_right

        min_top = cfg.screen_shift['y'] + border
        if abs_crd[1] < min_top:
            abs_crd[1] = min_top

        max_bottom = cfg.screen_shift['y'] + cfg.main_window['ym'] - border - 60

        if abs_crd[1] > max_bottom:
            abs_crd[1] = max_bottom

        # pr(self.module_name(),'abs_crd', abs_crd)
        walk_crd = self.walk_shift_from_mini_map_abs_crd(abs_crd)
        self.next_click_abs_crd = [int(walk_crd[0]), int(walk_crd[1])]

    def health_status(self):
        health_status = -1

        if self.ms.ss.curr_code == self.unit.code_v2:
            # 0 - full hp
            # 1 - half hp
            # 2 - small hp

            # 3 - big hp
            # 4 - max hp

            health = self.ms.ss.curr_health
            if health[0] != -1:
                health_status = 10
                health_procent = int((health[0] / health[1]) * 100)
                if health_procent < 30:
                    health_status = 0
                elif health_procent < 50:
                    health_status = 5
                elif health_procent < 75:
                    health_status = 7
                elif health_procent < 100:
                    health_status = 9

        if health_status == -1:
            health_status = self.ms.health_status_v2(self.unit_group)

        return health_status


class WalkS(Walk):
    task_name = 'Движение в режиме боеготовности'
    walk_s = True


class ClearSector(Walk):
    # Разведка сектора и приманивание целей к группе
    # Герой запоминает свои координаты
    # Идёт к точке зачистки, если видит врага - сокращает с ним дистанцию до установленной.
    # Затем идёт к своей сохранённой точке. Если дистанция до врага разрывается, ожидает.
    # Когда возвращается на базу, миссия выполнена.

    # Начальные координаты героя
    unit_crd_first = []

    # Прогресс достижения цели
    walk_progress = 0

    # Отступление героя
    retreat_hero = 0

    # Минимальная дистанция до героя
    min_dst = 5
    # Максимальная дистанция до героя
    max_dst = 5
    # Дистанция, после которой мы возвращаемя к объекту
    return_dst = 7

    # Основная цель, назначенная в самом начале
    main_target_crd_first = []
    # Куда идёт герой. Домой или к врагам
    #   0 - к врагам
    #   1 - домой
    way_to_home = 0

    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 2
    # Разброс дистацнии для локальной цели
    walk_min_move_radius = 5
    walk_main_ct_radius = 2
    # Цель обнаружена
    target_detected = False
    # Ожидание врагов, при достижении цели.
    target_done_wait_validate = 3
    # Время достижения цели
    target_done_time = 0
    # Убивать юнитов определённого типа, если они близко
    type_to_kill = []
    # Если эти юниты присутствуют близко, уходим
    exclude_close = dict()
    max_close = 2
    min_other_enemy_dst = 40

    def __init__(self, target_abs=[], unit_group=Hero, min_dst=5, max_dst=5, return_dst=7, first_abs=[],
                 type_to_kill=[],
                 exclude_close=[], use_neuromap=False, walk_main_ct_radius=2):
        self.unit_crd_first = []

        self.set_main_target(target_abs)
        self.main_target_crd_first = self.main_target_crd
        self.unit_group = unit_group.group
        self.min_dst = min_dst
        self.max_dst = max_dst
        self.return_dst = return_dst
        self.type_to_kill = type_to_kill
        self.use_neuromap = use_neuromap
        self.walk_main_ct_radius = walk_main_ct_radius
        if exclude_close:
            self.exclude_close = dict()
            for exclude in exclude_close:
                self.exclude_close[exclude] = 1

        self.task_name = 'Разведка сектора: ' + str(target_abs)
        if first_abs:
            # Назначаем стартовую точку героя
            self.set_unit_crd(first_abs)

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Получаем координаты героя на мини-карте
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.get_hero_map_crd()
        if not self.unit_crd:
            pr(self.module_name(), self.ms.cmd)
            pr(self.module_name(), '# Центрирование, нет координат героя на мини-карте')
            self.action = action.Center(self.unit_group)
            self.action.priority = 1
            return

        if not self.unit_crd_first:
            # Рассчитываем текущие координаты
            self.unit_crd_first = self.unit_crd
            pr(self.module_name(), '# Назначены начальные координаты героя', self.unit_group, self.unit_crd_first,
               self.mon_map_abs(self.unit_crd_first))

        # Получаем координаты героя на экране
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.ms.get_hero_abs(self.unit_group)

        # Требуется ли центрирование?
        if self.need_center():
            pr(self.module_name(), '# Необходимо центрирование')
            self.action = action.Center(self.unit_group)
            self.action.priority = 1
            return

        # Враг обнаружен?
        self.ms.update_units()

        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        enemy_dst = []
        if enemy:
            # Рассчитываем дистанцию до героя
            hero = self.ms.cmd.get(self.unit_group)
            if hero and hero[0] > 0:
                for e in enemy:
                    # pr(self.module_name(),hero): [427, 705]
                    # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                    dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                    # Дистанция, враг, герой
                    enemy_dst.append([dst, e])
                    pass
            pass
        # Враг обнаружен?
        if enemy_dst:
            # Враги обнаружены
            enemy_dst.sort(key=lambda x: x[0], reverse=False)
            # [163, 7]
            min_dst = enemy_dst[0]
            pr(self.module_name(), 'Враг обнаружен, дистанция:', min_dst[0], 'количество:', len(enemy_dst), 'состав:',
               enemy_dst)
            if min_dst[0] < self.min_dst * 32:
                if not self.way_to_home:
                    # Проверяем только если сейчас идёт цикл поиска врагов
                    # Враг близко
                    # Для создания действия к отступлению, герой должен подтвердить, что его враг ближайший.
                    # if self.retreat_hero != 1:
                    #    self.retreat_hero = 1
                    #    return
                    kill_target = False
                    enemy = enemy_dst[0][1]
                    if self.type_to_kill:
                        for target_type in self.type_to_kill:
                            if enemy[1][5] == target_type:
                                kill_target = True
                                break
                    if kill_target:
                        # Ближайшая цель, входит в список убийств. Смотрим расстояние до других целей.
                        other_close = 0
                        for en in enemy_dst:
                            enemy_o = en[1]
                            # Если враг ближе 40 пикселей
                            if en[0] < self.min_other_enemy_dst:
                                add_other = True
                                for target_type in self.type_to_kill:
                                    if enemy_o[1][5] == target_type:
                                        if not self.exclude_close.get(target_type):
                                            add_other = False
                                            break
                                if add_other:
                                    other_close += 1
                        if other_close < self.max_close:
                            # Если есть ещё враги других типов, но их количество допустимо (<2), атакуем цель
                            enemy_abs = self.ms.get_unit_abs_crd(enemy)
                            pr(self.module_name(), 'Атака на цель', enemy, 'врагов:', other_close)
                            self.attack_action(enemy_abs, enemy[0])
                            return
                        else:
                            pr(self.module_name(), 'Цель близко, но врагов слишком много:', other_close)

                    pr(self.module_name(), 'Отступаем')
                    # Создаём действие к остановке. Меняем координаты на возврат домой.
                    self.main_target_crd = self.unit_crd_first
                    self.local_target_crd = self.unit_crd
                    self.action = action.Stop(self.unit_group)
                    self.action.priority = 1
                    self.way_to_home = 1
                    # Цель обнаружена, приманиваем
                    self.target_detected = True
                    return

            if self.way_to_home:
                if self.max_dst * 32 < min_dst[0] < self.return_dst * 32:
                    # Дистанция слишком велика, останавливаемся
                    pr(self.module_name(), 'Дистанция слишком велика, останавливаемся')
                    self.action = action.Stop(self.unit_group)
                    return

                if min_dst[0] > self.return_dst * 32:
                    # Враг отступает, возвращаемся
                    pr(self.module_name(), 'Враг отступает, возвращаемся')
                    self.way_to_home = 0
                    self.main_target_crd = self.main_target_crd_first
                    self.local_target_crd = self.unit_crd
                    self.action = action.Stop(self.unit_group)
                    return

        else:
            # Враг не был обнаружен
            if self.way_to_home and self.target_detected:
                # Если мы шли домой и цель была ранее обнаружена. Возвращаемся к цели
                # Враг отступает, возвращаемся
                pr(self.module_name(), 'Враг потерян из виду, возвращаемся')
                self.way_to_home = 0
                self.main_target_crd = self.main_target_crd_first
                self.local_target_crd = self.unit_crd
                self.action = action.Stop(self.unit_group)
                return
            pass

        # Была ли достигнута локальная цель?
        task_target_done = False
        action_target_done = False
        if self.local_target_crd:
            # Да. Проверяем её достижение
            # Расстояние до локальной цели
            local_target_dst = self.get_distance(self.local_target_crd, self.unit_crd)

            # Минимальный радиус до цели
            min_radius = self.walk_min_ct_radius
            if local_target_dst > 10:
                # При большом расстоянии до цели, перемещаемся свободно
                min_radius = self.walk_min_move_radius

            if local_target_dst < min_radius:
                main_target_dst = self.get_distance(self.main_target_crd, self.unit_crd)
                # Была ли достигнута глобальная цель:
                if main_target_dst < self.walk_main_ct_radius:
                    # Глобальная цель достигнута
                    task_target_done = True
                # Локальная цель достигнута, создаём новую.
                action_target_done = True

        if task_target_done:
            # Если цель достигнута. Возвращаемя назад
            if self.walk_progress == 0:
                # Ожидаем врагов. Только когда мы в режиме поиска.
                if self.target_done_wait_validate and not self.way_to_home:
                    curr_time = time.time()
                    if not self.target_done_time:
                        self.target_done_time = curr_time
                    wait_time = round(curr_time - self.target_done_time, 2)
                    if wait_time < self.target_done_wait_validate:
                        pr(self.module_name(), 'Ожидаем врагов', wait_time, '<', self.target_done_wait_validate)
                        return
                # Поверяем наличие врагов
                pr(self.module_name(), 'Сектор чист. Возвращаемся к команде')
                self.walk_progress = 1
                self.target_detected = False
                self.main_target_crd = self.unit_crd_first
                return
            else:
                pr(self.module_name(), 'Задание выполнено')
                self.status = 0
                return

        if action_target_done:
            # Мы достигли мини-цели. Создаём новую цель
            self.last_move_time = 0
            self.add_walk_action()
            return

        # Ожидаем прибытия к цели. Создаём новую цель, если герой стоит на месте.

        # Это первый вход в цикл?
        if not self.local_target_crd:
            # Создаём новую цель
            self.add_walk_action()
            return

        # Проверяем, когда была последняя команда движения
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 1:
                # Если это была команда передвижения
                # Ждём 1 сек.
                self.last_move_time = time.time()
                # Сбрасываем счётчик остановки
                self.unit_stop = 0

        # Проверка времени последней команды движения
        if self.last_move_time:
            curr_time = time.time()
            if curr_time - self.last_move_time < 1:
                # Последняя команда была менее секунды назад, выходим.
                return

        # Проверяем движение к цели. Прошло более секунды, после последней команды
        if self.max_unit_stop_count():
            # Цель стоит более 10 циклов. Создаём новую задачу перемещения
            self.add_walk_action()

        pass

    def set_unit_crd(self, target_abs):
        # Назначить стартовую КТ
        # Перевод в относительные координаты
        self.unit_crd_first = [target_abs[0] - cfg.mon_map['x'], target_abs[1] - cfg.mon_map['y']]
        pr(self.module_name(), 'Назначена стартовая точка ', self.unit_crd_first, ' для группы ', self.unit_group)
        pass

    def add_walk_action(self):
        # Создание действия перемещения героя к контрольной точке.
        self.next_step()
        self.action = action.Walk(self.unit_group, self.next_click_abs_crd)
        self.action.priority = 1

    def attack_action(self, hero_abs, target_id):
        target_code = ''
        self.action = action.Attack(self.unit_group, hero_abs, target_code, target_id)


class Group(WalkTask):
    # Сгруппироваться вокруг героя
    # Можно сделать динамическую группировку, но она пока не нужна. Сделаем предустановленную.
    # Есть класс группа, в котором распределены названия команд. Мы его один раз создаём, а затем добавляем в задачи.
    task_name = 'Группировка'
    # Допустимый разброс, при формировании группы
    max_dst = 1
    # Группа героев
    group = Group()

    # Список управляемых героев.
    heroes = dict()

    # Расположение группы, относительно танка.
    #   0 - сверху
    #   1 - спрва
    #   2-  снизу
    #   3 - слева
    rotate = 2

    # Максимальное количество остановок, до пропуска валидации
    max_stop = 5
    stop_count = dict()

    # Максимальное количество не найденых, для пропуска валидации
    max_not_found_count = 5
    not_found_count = dict()

    def __init__(self, group, max_dst=0, rotate=2, max_stop=5):
        self.heroes = dict()
        self.max_stop = max_stop
        self.stop_count = dict()
        self.max_not_found_count = 5
        self.not_found_count = dict()
        self.group = group
        self.max_dst = max_dst
        self.rotate = rotate

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Создаём список тех, кто ещё не ходил.
        if not self.heroes:
            if self.group.archers:
                for archer in self.group.archers:
                    # Ключ - номер группы
                    key = archer.group
                    self.heroes[key] = self.default_unit_info()
            if self.group.mages:
                for mage in self.group.mages:
                    key = mage.group
                    self.heroes[key] = self.default_unit_info(0)

        need_center = True

        # Проверяем последнее действие - действие активировано?
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            if self.last_action.act_type == 1:
                # Получаем юнита, который ходил последним
                unit = self.last_action.unit_group
                curr_time = time.time()
                # Юнит использовал свой ход
                self.heroes[unit][0] = 1
                # Время последнего хода
                self.heroes[unit][1] = curr_time
            else:
                # Последняя команда была - центрирование
                need_center = False

        # Создаём список координат прибытия и фактических координат героев
        if not self.get_groups_crd(need_center):
            # Координаты танка не найдены, назначаем центрирование танка
            # Центрируем
            self.action = action.Center(self.group.tank)

        # Сравниваем списки циклом перехода по героям, составляем список.
        valid_dst = 16 + 32 * self.max_dst

        # Кто-то не достиг координат?
        not_valid_crd = []

        for key, unit in self.heroes.items():
            if unit[2][0] and unit[3][0] and self.validate_crd(unit[2], unit[3], valid_dst) \
                    and not self.ms.hero_not_found.get(key):
                # Координаты есть, и они проходят валидацию
                pass
            else:
                if not self.not_found_count_validate(key):
                    # Если мы не отчаялись искать героя, добавляем его в список
                    # Проверяем валидацию остановки
                    if self.stop_count.get(key) and self.stop_count.get(key) > self.max_stop:
                        pr(self.module_name(), 'Герой', key, 'стоит на месте, пропускаем его валидацию')
                    else:
                        not_valid_crd.append(key)
                else:
                    pr(self.module_name(), 'Герой', key, 'не найден, пропускаем его валидацию')

        # 1. Нет - меняем статус задачи, выходим.
        if not not_valid_crd:
            self.status = 0
            return

        # Все герои ходили?
        if self.all_units_turn(not_valid_crd):
            # Да - обнуляем список тех, кто ходил
            self.next_units_turn()

        # 2. Да - проходим по списку.
        pr(self.module_name(), 'Группы, к перемещению:', not_valid_crd)
        for key in not_valid_crd:
            # Герой ходил?
            hero = self.heroes[key]
            # pr(self.module_name(),hero)
            if hero[0] == 0:
                # Координаты есть? Они реальные или из прошлого цикла?
                if hero[3][0] and hero[4][0] and not self.ms.hero_not_found.get(key):
                    self.reset_not_found_count(key)
                    # 2. Нет, Герой движется?
                    if self.validate_crd(hero[3], hero[4], 0):
                        # Кто-то занял место героя?
                        other_hero_in_crd = self.crd_not_empty(hero)
                        # pr(self.module_name(),other_hero_in_crd)
                        if other_hero_in_crd:
                            # self.update_stop_count(key)
                            # Отводим героя
                            # other_hero = self.heroes.get(other_hero_in_crd)
                            next_hero_crd = hero[2]
                            if self.rotate == 0:
                                if hero[5]:
                                    # Лучники двигаются ближе к герою
                                    next_hero_crd[1] += 32
                                else:
                                    # Маги отходят назад
                                    next_hero_crd[1] -= 32
                            elif self.rotate == 1:
                                if hero[5]:
                                    next_hero_crd[0] -= 32
                                else:
                                    next_hero_crd[0] += 32
                            elif self.rotate == 2:
                                if hero[5]:
                                    next_hero_crd[1] -= 32
                                else:
                                    next_hero_crd[1] += 32
                            elif self.rotate == 3:
                                if hero[5]:
                                    next_hero_crd[0] += 32
                                else:
                                    next_hero_crd[0] -= 32

                            # pr(self.module_name(),'walk back', key, next_hero_crd)
                            self.update_stop_count(key)
                            self.add_walk_action(key, next_hero_crd)
                            break
                        else:
                            # 2. Создаём действие.
                            # pr(self.module_name(),'walk', key, hero[2])
                            self.update_stop_count(key)
                            self.add_walk_action(key, hero[2])
                            break
                    else:
                        # Сбрасываем счётчик остановки
                        self.reset_stop_count(key)
                else:
                    # 3. Нет координат, не известно. Когда было последнее действие?
                    curr_time = time.time()
                    if curr_time - hero[1] > 3:
                        # 2. Более секунды назад. Создаём задание.
                        pr(self.module_name(), 'Нет координат, не известно когда было последнее действие', key)
                        self.update_not_found_count(key)
                        self.add_walk_action(key, hero[2])
                        break

            # Переходим к следующему.
            pass

    def reset_stop_count(self, key):
        self.stop_count[key] = 0

    def update_stop_count(self, key):
        if self.stop_count.get(key):
            self.stop_count[key] += 1
            # if self.stop_count.get(key) > self.max_stop:
            #    # Увеличиваем разброс
            #    self.max_dst += 1
            #    pr(self.module_name(), 'Увеличиваем разброс цели:', self.max_dst)
            #    self.stop_count[key] = 1
        else:
            self.stop_count[key] = 1
        pr(self.module_name(), 'Обновлён счётчик ошибок:', key, self.stop_count)

    def reset_not_found_count(self, key):
        self.not_found_count[key] = 0

    def update_not_found_count(self, key):
        if self.not_found_count.get(key):
            self.not_found_count[key] += 1
        else:
            self.not_found_count[key] = 1

    def not_found_count_validate(self, key):
        if self.not_found_count.get(key) and self.not_found_count.get(key) > self.max_not_found_count:
            return True
        return False

    def crd_not_empty(self, our_hero, valid_dst=16):
        # Проверка, занято ли место героя, другим героем
        ret = 0
        for key, hero in self.heroes.items():
            # pr(self.module_name(),our_hero[2], hero[3])
            if self.validate_crd(our_hero[2], hero[3], valid_dst):
                ret = key
        return ret

    def validate_crd(self, crd1, crd2, valid_dst=32):
        # Валидация координат
        # pr(self.module_name(),crd1, crd2)
        if abs(crd1[0] - crd2[0]) <= valid_dst and abs(crd1[1] - crd2[1]) <= valid_dst:
            return True
        return False

    def get_groups_crd(self, need_center):
        # Создаём список координат для доступных групп
        tank = self.group.tank
        tank_abs = self.ms.get_hero_abs(tank)
        ret = True
        if not tank_abs:
            ret = False
        if self.ms.hero_not_found.get(tank):
            # Координаты героя не найдены
            ret = False

        if not ret and need_center:
            # Если координаты не обнаружены, а мы уже центрировали, берём координаты по центру карты
            tank_screen = [425, 342]
            tank_x_y = self.ms.abs_to_screen(tank_screen[0], tank_screen[1])
            tank_abs = [tank_x_y[1], tank_x_y[0]]
            pr(self.module_name(), 'Координаты героя не найдены, берём по центру карты', tank_abs)
            pass

        tank_x = tank_abs[0]
        tank_y = tank_abs[1]
        if self.group.archers:
            self.get_group_crd(tank_x, tank_y, self.group.archers, self.group.archers_layer)
        if self.group.mages:
            self.get_group_crd(tank_x, tank_y, self.group.mages, self.group.mages_layer)
        return ret

    def get_group_crd(self, tank_x, tank_y, group, dst):
        # Список координат для конкретной группы
        # pr(self.module_name(), 'Расчёт координат группы:', tank_x, tank_y, group, dst)
        if self.rotate == 0 or self.rotate == 2:
            # Группа сверху от танка
            unit_y = tank_y - dst * 32
            if self.rotate == 2:
                # Группа - снизу от танка
                unit_y = tank_y + dst * 32
            unit_left = int(tank_x - (int(len(group) / 2)) * 32)
            unit_x = unit_left

        else:
            # self.rotate == 1 or self.rotate == 3:
            # Группа справа от танка
            unit_x = tank_x + dst * 32
            if self.rotate == 3:
                # Группа - слева от танка
                unit_x = tank_x - dst * 32
            unit_left = int(tank_y + (int(len(group) / 2)) * 32)
            unit_y = unit_left

        for hero in group:
            unit = hero.group
            # Создаём координаты прибытия для юнитов
            unit_dst = [unit_x, unit_y]
            self.heroes[unit][2] = unit_dst
            if self.rotate == 2 or self.rotate == 0:
                unit_x += 32
            elif self.rotate == 1 or self.rotate == 3:
                unit_y -= 32

            # Сохраняем текущие координаты как прошедшие
            self.heroes[unit][4] = self.heroes[unit][3]
            # Обновляем фактические координаты
            # Узнаём фактические координаты юнитов
            unit_abs = self.ms.get_hero_abs(unit)
            if unit_abs:
                # Обновляем координаты юнита, только в случае фактического получения данных.
                self.heroes[unit][3] = unit_abs

        # pr(self.module_name(), 'Результат расчёта:', self.heroes)

    def next_units_turn(self):
        # Переход хода. Сбрасываем данные о том, что юниты ходили.
        for key in self.heroes:
            self.heroes[key][0] = 0
            pass
        pr(self.module_name(), 'Переход хода')

    def all_units_turn(self, not_valid_crd):
        # Все юиниты сходили по очереди
        i = 0
        for key, unit in self.heroes.items():
            if unit[0] == 1:
                i += 1
        if i >= len(not_valid_crd):
            return True
        return False

    def default_unit_info(self, archer=1):
        # 0 - 0 - информация о том, ходила ли группа. 0-нет. 1-да
        # 1 - 0 - время последнего хода группы.
        # 2 - [0,0] - куда нужно двигаться
        # 3 - [0,0] - фактические координаты
        # 4 - [0,0] - прошлые координаты
        # 5 - 1 - лучник, 0 - маг
        return [0, 0, [0, 0], [0, 0], [0, 0], archer]

    def add_walk_action(self, unit, abs_crd):
        # Создание действия перемещения героя к контрольной точке.
        shift_crd = self.walk_shift_abs_crd(abs_crd)
        if self.validate_screen_crd(shift_crd):
            # Проверяем соответствие координат экрану
            self.action = action.Walk(unit, shift_crd)
        else:
            # Иначе центрируем
            self.action = action.Center(self.group.tank)


class EnemyDown(Task):
    task_name = 'Ожидание смерти врага'
    # Враг, соответствующий типу
    # Условия победы:
    #   враг должен появиться
    #   хп должно быть потрачено
    #   должен исчезнуть в пределах радиуса ближайшего из героев
    #   или дожно пройти определённое время, чтобы отменить цель

    # Основная задача
    weight = 1

    # Прогресс выполнения задачи.
    #   При появлении врага, меняется на 1.
    #   После смерти врага, задача снимается
    progress = 0

    # Определение врага по типу, а не по имени.
    # Этот алгоритм быстрее стандартного, так как не требует дополнительного навода мыши на цель.
    target_type = 0

    # Текущий цикл валидации
    validate_count = 0
    # Максимальное количество циклов, после которых валидация пройдена
    validate_max_count = 10

    def __init__(self, target_type=1, time_out=0):
        self.target_type = target_type
        self.close_time = time_out

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        self.ms.update_units()
        # pr(self.module_name(),'other', self.ms.other)
        # pr(self.module_name(),'valid', self.ms.p.get_valid_units())
        # Проверяем врагов
        # Смотрим прогресс
        if self.progress == 0:
            # 0 - ждём врагов
            # target = self.ms.get_by_type(self.target_type)
            target = self.ms.p.get_valid_by_type(self.target_type)
            if target:
                # Враг появлися?
                # [[225, 643, 1, 1]]
                self.progress = 1
                self.validate_count = 0
                pr(self.module_name(), 'Цель', self.target_type, 'обнаружена', target)
                return

        if self.progress == 1:
            # target = self.ms.get_by_type(self.target_type)
            target = self.ms.p.get_valid_by_type(self.target_type)
            if not target:
                # 1 - врага нет? Проводим валидацию
                self.validate_count += 1
            else:
                # 2 - враг снова появился - сбрасывам счётчик валидации.
                self.validate_count = 0

        # Валидация пройдена, снимаем задачу
        if self.validate_count > self.validate_max_count:
            self.status = 0
            pr(self.module_name(), 'Цель', self.target_type, 'устранена.')

    pass


class EnemyClear(WalkTask):
    # Ожидаем ликвидации врагов ближнего боя.
    # 1. Враг должен быть ближе 3х клеток
    # 2. Если нет врага, проходим валидацию
    task_name = 'Ожидание смерти врага'

    # Герои, расстояние до которых мы считаем
    units = []
    # Максимальная дистанция до врагов.
    min_dst = 3
    # Минимальное время ожидания между проверками
    min_wait = 1
    # Текущий цикл валидации
    validate_count = 0
    # Максимальное количество циклов, после которых валидация пройдена
    validate_max_count = 3
    # Последнее время валидации
    last_validate_time = 0
    # Интервал между валидациями
    last_validation = 0
    validation_interval = 0
    # Максимальное время ожидания, после которого снимается задача. 3 минуты
    max_wait = 180
    run_time = 0
    # Валидация только для врагов определённого типа
    enemy_type = -1

    # Опознание врага как своего, если он рядом и у всех полное хп
    found_friend_detect = False

    def __init__(self, units=[], min_dst=3, min_wait=1, validation_interval=0, max_wait=180, enemy_type=-1):
        self.units = units
        self.min_dst = min_dst
        self.min_wait = min_wait
        self.validation_interval = validation_interval
        self.max_wait = max_wait
        self.enemy_type = enemy_type
        if max_wait:
            self.run_time = time.time()

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()

        # Проверка max_wait
        if self.max_wait:
            if curr_time - self.run_time > self.max_wait:
                pr(self.module_name(), 'Истекло время ожидания убийства цели', self.max_wait)
                self.status = 0
                return

        enemy_close = False

        self.ms.update_units()
        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        enemy_dst = []
        if enemy:
            # Рассчитываем дистанцию до героев
            for unit in self.units:
                hero = self.ms.cmd.get(unit.group)
                if hero and hero[0] > 0:
                    for e in enemy:
                        if self.enemy_type != -1:
                            # Определённый тип врагов
                            if e[1][5] != self.enemy_type:
                                continue
                            pass
                        # pr(self.module_name(),hero): [427, 705]
                        # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                        dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                        # Дистанция, враг, герой
                        enemy_dst.append([dst, unit.group, e])
                        pass
                pass
            pass

        if enemy_dst:
            if len(enemy_dst) < 3 and self.found_friend_detect:
                # Проверка на неопредилившуюся команду союзников. Если осталось 1 или 2 врага
                # Если юнит находится вблизи нашей группы и у него зелёное хп, это дружеский юнит
                new_enemy_dst = []
                for enemy in enemy_dst:
                    e = enemy[2]
                    i = 0
                    for k, hero in self.ms.cmd.items():
                        c = False
                        for unit in self.units:
                            # Пропускаем героя на передовой
                            if k == unit.group:
                                c = True
                                break
                        if c:
                            continue

                        if hero and hero[0] > 0:
                            # Проверяем расстояние от неизвестного героя до других героев.
                            hp = e[1][4]
                            if hp == 0:
                                # У героя зелёное ХР
                                dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                                if dst < 64:
                                    # Если расстояние в пределах 64 пикселей, добавляем счётчик.
                                    i += 1
                    if i > 0:
                        # Если есть герой, который стоит рядом с целью и у цели полное ХП, пропускаем её
                        pr(self.module_name(), 'Юнит', e[0], 'опознан как свой:', enemy)
                        pass
                    else:
                        # Добавляем юнит в список врагов
                        new_enemy_dst.append(enemy)

                enemy_dst = new_enemy_dst
            pass

        if enemy_dst:
            # Враги обнаружены
            enemy_dst.sort(key=lambda x: x[0], reverse=False)
            # [163, 7, 2]
            # pr(self.module_name(),enemy_dst)
            min_dst = enemy_dst[0]
            if min_dst[0] <= self.min_dst * 32:
                # Враг близко
                enemy_close = True
                self.block = 1
                pr(self.module_name(), 'Враг обнаружен:', enemy_dst, 'cmd', self.ms.cmd)
                pass

        if not enemy_close:
            self.block = 0
            # Если враг далеко или отсутствует

            # Проверка времени последней команды движения
            if self.last_validate_time:
                if curr_time - self.last_validate_time < self.min_wait:
                    # Последняя команда была менее секунды назад, выходим.
                    return

            self.last_validate_time = curr_time

            if self.validation_interval:
                # Вадилация между интервалами
                if self.last_validation == 0:
                    self.last_validation = curr_time
                if curr_time - self.last_validation < self.validation_interval:
                    # Если назначен интервал валидации, используем проверку по его времени
                    wait_time = round(curr_time - self.last_validation, 1)
                    pr(self.module_name(), 'Ожидаем', wait_time, '<', self.validation_interval)
                    return
                else:
                    self.last_validation = curr_time

            self.validate_count += 1
            pr(self.module_name(), 'Врагов на дистанции', self.min_dst, 'нет, цикл валидации', self.validate_count)

        else:
            self.validate_count = 0

        # Валидация пройдена, снимаем задачу
        if self.validate_count >= self.validate_max_count:
            self.status = 0
            pr(self.module_name(), 'Ближние цели отсутствуют')

    pass


class ValidateHeroesGroup(WalkTask):
    # Поиск врагов на карте, если они найдены, проверяем их код. Если это наш герой - провал миссии.
    task_name = 'Валидация создания группы'
    max_dst = 5

    def __init__(self, max_dst=5):
        self.max_dst = max_dst

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        self.ms.update_units()
        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        enemy_dst = []
        if enemy:
            # Рассчитываем дистанцию до героев
            for key, hero in self.ms.cmd.items():
                if hero and hero[0] > 0:
                    min_dst = 10
                    for e in enemy:
                        # pr(self.module_name(),hero): [427, 705]
                        # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                        dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                        # Дистанция, враг, герой
                        if min_dst > dst:
                            min_dst = dst
                        pass
                    if min_dst > self.max_dst:
                        enemy_dst.append([min_dst, key])
                pass
            pass

        if enemy_dst:
            pr(self.module_name(), 'Вблизи враг, которого не должно быть')
            self.action = action.Defeat()
            return
        else:
            self.status = 0
            pr(self.module_name(), 'Ближние враги отсутствуют')

    pass


class GetLoot(Walk):
    # Сбор лута. Аллоды 1.
    task_name = 'Сбор лута'

    first_run = True
    progress = 0
    max_loot_dst = 5

    # Уменьшение приоритета задачи. Чем больше число, тем ниже приоритет.
    priority_down = 0
    priority = 4

    wait_interval = 2
    last_action_time = 0
    max_unit_stop = 5

    # Группа героев
    units = []
    # Первый элемент массива
    current_unit = 0

    def __init__(self, unit=1, units=[], wait_interval=2, priority_down=0):
        self.units = []
        if unit != 1:
            self.unit_group = unit.group
        self.priority_down = priority_down
        self.wait_interval = wait_interval
        self.get_current_unit(units)

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        curr_time = time.time()
        if self.units:
            if not self.last_action_time:
                self.last_action_time = curr_time

            if curr_time - self.last_action_time > self.wait_interval:
                self.next_unit()
                self.last_action_time = curr_time

        # Получаем координаты героя на мини-карте
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.get_hero_map_crd()
        if not self.unit_crd:
            pr(self.module_name(), 'Координаты героя не найдены', self.ms.cmd)
            if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id \
                    and self.last_action.act_type == 2:
                pr(self.module_name(), 'Берём координаты по центру карты')
                # Последняя выполненная задача - центрирование
                # Берём координаты по центру карты
                # y,x
                unit_abs = [332, 425]
                hero_shift = self.ms.get_shift(unit_abs)
                self.unit_crd = self.abs_to_map_crd(hero_shift)
            else:
                # Центрируем
                self.action = action.Center(self.unit_group)
                self.action.priority = self.get_priority()
                return

        # Получаем координаты героя на экране
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.ms.get_hero_abs(self.unit_group)

        # Требуется ли центрирование?
        if self.need_center():
            self.action = action.Center(self.unit_group)
            self.action.priority = self.get_priority()
            return

        # Проверяем, когда была последняя команда сбора мешков
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 3:
                # Если это была команда сбора мешков
                self.first_run = False
                # Ждём 1 сек.
                self.last_move_time = time.time()
                # Сбрасываем счётчик остановки
                self.unit_stop = 0
                if self.progress == 0:
                    self.progress = 1

        # Проверка времени последней команды движения
        if self.last_move_time:
            if curr_time - self.last_move_time < 1:
                # Последняя команда была менее секунды назад, выходим.
                return

        find = self.ms.find_loot()
        found = len(find)

        # Это первый вход в цикл?
        if self.first_run and len(find):
            # Создаём новую цель
            self.add_get_loot_action(find)
            return

        # Проверяем движение к цели. Прошло более секунды, после последней команды
        if self.max_unit_stop_count():
            # Цель стоит более 10 циклов. Создаём новую задачу перемещения
            # Ищем мешки
            pr(self.module_name(), 'Группа', self.unit_group, 'обнаружила мешков:', found)
            if found:
                self.add_get_loot_action(find)
                return

            if self.progress == 2:
                # Сбор завершён
                self.status = 0
                return

            self.progress = 2
            # Мешки не обнаружены? Выходим.

    def add_get_loot_action(self, find):
        if len(find):
            # Мешки обнаружены?
            # Рассчитываем дистанцию до мешков
            hero = self.ms.cmd.get(self.unit_group)
            loot_dst = []
            for e in find:
                # pr(self.module_name(),hero): [427, 705]
                # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                dst = int(self.get_distance(hero, e))
                # Дистанция, враг, герой
                loot_dst.append([dst, e])
                pass
            loot_dst.sort(key=lambda x: x[0], reverse=False)
            loot_closer = loot_dst[0]

            pr(self.module_name(), loot_closer)
            self.next_click_abs_crd = self.ms.screen_to_abs(loot_closer[1][1], loot_closer[1][0])
            self.action = action.GetLoot(self.unit_group, self.next_click_abs_crd)
            self.action.priority = self.get_priority()
            pr(self.module_name(), 'Идём к мешку', self.next_click_abs_crd, 'дистанция',
               loot_closer[0])
            return

        # Сбор лута
        self.action = action.GetLoot(self.unit_group)
        self.action.priority = 4 + self.priority_down

    def get_priority(self):
        return self.priority + self.priority_down

    def next_unit(self):
        # Выбор следующего персонажа
        self.current_unit += 1
        if self.current_unit >= len(self.units):
            self.current_unit = 0
        self.unit_group = self.units[self.current_unit].group
        pr(self.module_name(), 'Переход хода группе', self.unit_group)
        pass

    def get_current_unit(self, units):
        if units:
            self.units = units
            self.current_unit = 0
            self.unit_group = self.units[self.current_unit].group
        pass

    pass


class FindLoot(Walk):
    # Сбор лута по ходу движения. Аллоды 1.
    # Если нет врагов, то с интервалом в одну секунду мы сканируем карту на наличие мешков.
    # Если мешок обнаружен, отправляем к нему ближайший юнит и вычёркиваем координаты мешка на мини-карте
    # Юниту ставим таймер в 2 секунды на сбор мешка, а сами выбираем новый юнит и отправляем его к следующему мешку
    # Если свободных юнитов нет, пропускаем ход
    # Если юнит движется к мешку, пропускаем его ход
    task_name = 'Поиск лута'

    weight = 0

    # Интервал для сбора одного мешка. По истечении времени, свободный юнит отправляется к мешку
    get_loot_interval = 2

    # Интервал для юнита, на сбор мешка. По истечении времени, юнит направляется к новому мешку
    unit_move_interval = 2

    # Последнее время поиска
    last_find_time = 0
    find_interval = 1

    # Время блокировки других задач
    block_time = 3

    # Группа героев
    units = dict()
    loot = dict()
    units_crd = dict()

    war_mode = dict()

    # Игнорировать присутствие врагов и собриать лут, как не в чём не бывало
    ignore_enemy = False

    # Запускать фоном, или как отдельную задачу, закрывающуюся по выполнении.
    daemon = True

    # Максимальная дистания до мешка. Если лут далеко, не собираем.
    max_loot_dst = 0

    last_center_time = 0

    def __init__(self, units=[], find_interval=1, wait_interval=2, war_mode=[], daemon=True, ignore_enemy=False,
                 max_loot_dst=0):
        self.unit_move_interval = wait_interval
        self.find_interval = find_interval
        self.units_war_mode(war_mode)
        self.default_units_info(units)
        self.ignore_enemy = ignore_enemy
        self.daemon = daemon
        if not daemon:
            # Разовая задача
            self.weight = 1
        self.loot = dict()
        self.units_crd = dict()
        self.units_crd_last = dict()
        self.last_hero = -1
        self.max_loot_dst = max_loot_dst

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        curr_time = time.time()

        # Проверяем, когда была последняя команда сбора лута
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            if self.last_action.act_type == 2:
                # Если это была команда центрирования, обновляем таймер
                self.last_center_time = curr_time

            # Устанавливаем таймер на текущую группу.
            elif self.last_run_act_result == 1:
                # Обновляем таймер группы
                self.units[self.last_action.unit_group] = curr_time

                if self.last_action.act_type == 3:
                    # Если команда была по сбору лута, исключаем мешок из списка кондидатов
                    self.loot[self.last_action.loot_key] = curr_time

                    # Получаем координаты героя на мини-карте
                    self.units_crd_last[self.last_action.unit_group] = self.get_hero_map_crd(
                        self.last_action.unit_group)

        if self.last_find_time == 0:
            self.last_find_time = curr_time

        if curr_time - self.last_find_time < self.find_interval:
            # Время поиска мешков, раз в секунду
            return

        if not self.ignore_enemy:
            enemy = self.ms.p.get_valid_units()
            # Проверяем наличие врагов
            if enemy and not len(self.war_mode):
                # Не собираем мешки, когда рядом враги
                return

        find = self.ms.find_loot()
        found = len(find)

        if found:
            # Находим свободные мешки
            loot = dict()
            for e in find:
                # pr(self.module_name(),hero): [427, 705]
                map_crd = self.abs_to_map_crd(e)
                loot_key = str(map_crd[0]) + '-' + str(map_crd[1])
                if self.loot.get(loot_key):
                    last_loot = self.loot.get(loot_key)
                    if curr_time - last_loot < self.get_loot_interval:
                        # Время сбора этого мешка ещё не прошло, исключаем его из списка доступных
                        continue

                loot[loot_key] = e

            # Находим свободные группы
            free_units = []
            if len(loot):
                # pr(self.module_name(), 'Обнаружено мешков:', len(loot), loot)
                # Если есть, что собирать, ищем собирающих
                for unit, last_action in self.units.items():
                    if curr_time - last_action < self.unit_move_interval:
                        # Юнит занят, пропускаем его
                        continue

                    # Обновляем текущие координаты
                    curr_crd = self.get_hero_map_crd(unit)
                    if len(curr_crd):
                        self.units_crd[unit] = curr_crd

                    if not self.ignore_enemy and enemy:
                        # Если есть враг, проверяем бойцов, которые могут собриать в режиме боя
                        if not self.war_mode.get(unit):
                            continue

                    free_units.append(unit)

            loot_dst = []

            if free_units:
                # pr(self.module_name(), 'Свободных героев:', len(free_units), free_units)
                # Среди групп находим ближайшую к мешку
                for unit in free_units:
                    unit_crd = self.ms.cmd.get(unit)
                    for key, val in loot.items():
                        loot_crd = val
                        dst = int(self.get_distance(unit_crd, loot_crd))
                        if self.max_loot_dst and dst > self.max_loot_dst * 32:
                            continue
                        loot_dst.append([dst, unit, key, val])

            if loot_dst:
                pr(self.module_name(), 'Обнаружено мешков:', len(loot), loot_dst, 'Свободных героев:', len(free_units),
                   free_units)
                # Находим ближайшую точку
                loot_dst.sort(key=lambda x: x[0], reverse=False)
                loot_closer = loot_dst[0]
                hero = loot_closer[1]
                loot_key = loot_closer[2]
                loot_crd = loot_closer[3]
                pr(self.module_name(), 'loot_closer', loot_closer)
                loot_abs = self.ms.screen_to_abs(loot_crd[1], loot_crd[0])

                # Проверяем стоял ли юнит
                if hero in self.units_crd and len(self.units_crd.get(hero)) and \
                        hero in self.units_crd_last and len(self.units_crd_last.get(hero)):
                    curr_crd = self.units_crd.get(hero)
                    last_crd = self.units_crd_last.get(hero)
                    curr_key = str(curr_crd[0]) + '-' + str(curr_crd[1])
                    last_key = str(last_crd[0]) + '-' + str(last_crd[1])
                    if last_key == curr_key:
                        print(last_key, curr_key, hero)
                        # Если юнит стоит отправляем его пешком к мешку
                        next_step = [-32, 0, 32]
                        next_append = [0, 0]
                        while True:
                            next_append = [random.choice(next_step), random.choice(next_step)]
                            if next_append[0] != 0 or next_append[1] != 0:
                                break

                        walk_abs = [loot_abs[0] + next_append[0], loot_abs[1] + next_append[1]]
                        self.action = action.Walk(hero, walk_abs)

                if not self.action:
                    self.action = action.GetLoot(hero, loot_abs, loot_key)
                self.block = curr_time + self.block_time
            else:
                # Мешков не найдено
                if not self.daemon and len(free_units) > 0:
                    # Разовая задача. Центрируем на каждом из собирателей перед выходом.
                    self.status = 0

            if not self.action and not self.daemon and self.last_action:
                # Если действий нет и задача разовая центрируем каждую секунду
                if curr_time - self.last_center_time > 1:
                    # Если прошло больше 1 секунды, центрируем с низким приоритетом
                    self.action = action.Center(self.last_action.unit_group)
                    self.action.priority = 4
        else:
            # Мешков не найдено
            if not self.daemon:
                # Разовая задача. Центрируем на каждом из собирателей перед выходом.
                self.status = 0
            pass

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit.group] = float(0)

    def units_war_mode(self, units):
        # 0 - время последнего хода группы.
        self.war_mode = dict()
        for unit in units:
            self.war_mode[unit.group] = 1

    def unit_move_validate(self):
        # Валидация движения юнита
        move = True

        # Если кординаты на мини-карте равны
        if self.unit_crd_last and self.unit_crd_last[0] == self.unit_crd[0] \
                and self.unit_crd_last[1] == self.unit_crd[1]:
            move = False

        return move


class WalkGroup(Walk):
    task_name = 'Передвижение группы к цели'

    # Разброс дистации для контрольной точки
    walk_min_ct_radius = 5
    # Разброс дистацнии для локальнйо цели
    walk_min_move_radius = 5
    walk_main_ct_radius = 5
    walk_s = True
    walk_group = True

    def __init__(self, target_abs=[], unit=1, walk_s=True):
        self.set_main_target(target_abs)
        self.unit_group = unit.group
        self.walk_s = walk_s


class SelfHealthMon(Task):
    # Монитор здовровья персонажа

    # Уровень здоровья.
    # 0 - Желтое ХП.
    # 1 - Красное ХП
    health_level = 0

    # Последний каст
    last_cast_time = 0
    # Время межу кастами
    wait_time = 0.1

    # Дополнительная задача
    weight = 0

    # Количество зелий с красным ХП
    num_if_red = 1
    # Количество зелий
    num = 1

    def __init__(self, unit=1, health_level=0, wait_time=100, num=1, num_if_red=1):
        self.unit_group = unit.group
        self.health_level = health_level
        self.wait_time = wait_time / 1000
        if num > num_if_red:
            num_if_red = num
        self.num_if_red = num_if_red
        self.num = num
        self.task_name = 'Мониторинг своего здоровья:', unit.group

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда Атаки
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            self.last_cast_time = curr_time

        if curr_time - self.last_cast_time < self.wait_time:
            return

        hero = self.ms.cmd.get(self.unit_group)
        if hero[0] == 0:
            # Если координаты не найдены, пропускаем ход
            return

        hp = self.ms.health_status(hero)

        if hp == 1:
            # Половина ХП
            if self.health_level == 0:
                self.health_task(self.num)

        elif hp == 2:
            # Красное ХП
            self.health_task(self.num_if_red)

    def health_task(self, custom_num=1):
        self.action = action.SelfHeal(self.unit_group, custom_num)
        pass


class Retreat(WalkTask):
    # Отступление мага, если рядом враг.
    # Передаём скрипту группы магов.
    # Скрипт смотрит координаты монстров, если находит, рассчитвает дистанции до магов.
    # Маг, чья дистанция ближе допусимого, отступает.
    task_name = 'Отступление'
    # Список магов
    units = dict()

    # Минимальная дистанция до врага
    min_dst = 3

    # Дополнительная задача
    weight = 0

    all_units = []

    retreat_hero = 0

    # Время блокировки других задач
    block_time = 1

    def __init__(self, units=[], min_dst=1):
        self.units = dict()
        self.min_dst = min_dst
        self.default_units_info(units)
        # self.all_units = all_units

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Валидация отображения всех юнитов
        # if not self.find_all_heroes_validate():
        #    return

        curr_time = time.time()
        # Проверяем, когда была последняя команда отступления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.

            self.units[self.last_action.unit_group] = curr_time

        self.ms.update_units()

        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        enemy_dst = []

        if enemy:
            # Рассчитываем дистанцию до героев
            for unit, last_move_time in self.units.items():
                hero = self.ms.cmd.get(unit)
                if curr_time - last_move_time < 1:
                    # Герой уже отступал менее секунды назад
                    continue

                if hero and hero[0] > 0:
                    for e in enemy:
                        # pr(self.module_name(),hero): [427, 705]
                        # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                        dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                        # Дистанция, враг, герой
                        enemy_dst.append([dst, e[0], unit])
                        pass
                pass
            pass

        if enemy_dst:
            # Враги обнаружены
            enemy_dst.sort(key=lambda x: x[0], reverse=False)
            # [163, 7, 2]
            min_dst = enemy_dst[0]
            if min_dst[0] < self.min_dst * 32:
                # Враг близко
                hero = min_dst[2]

                # Создаём действие к оступлению
                self.cmd_retreat(hero)
                self.retreat_hero = 0
                self.block = curr_time + self.block_time
                pass

    def cmd_retreat(self, key):
        # Отступление юнита
        last_center = UnitCircleInfo.last_center
        curr_time = time.time()
        if last_center[0] > 0 and last_center[0] > curr_time - 1:
            # Центр валидный
            self.action = action.WalkRetreat(key, last_center[1])

        if not self.action:
            self.action = action.Retreat(key)

        pr(self.module_name(), 'Отступление группы, враг близко', key)

        # Приоритет для отступления наивысший
        self.action.priority = 1

    def find_all_heroes_validate(self):
        # Проверка правильного отображения юнитов
        ret = False
        if self.all_units and self.ms.cmd:
            ret = True
            for unit in self.all_units:
                hero = self.ms.cmd.get(unit)
                # pr(self.module_name(),unit, hero)
                if hero and hero[0]:
                    continue
                else:
                    ret = False
                    break
        return ret

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit.group] = 0

    pass


class MoveGroup(WalkTask):
    # Группа держится вместе. Если юнит далеко уходит от главного, он возвращается назад.

    task_name = 'Передвижение группой'
    # Список магов
    units = dict()

    # Минимальная дистанция до юнита
    min_dst = 2
    max_dst = 6

    # Дополнительная задача
    weight = 0

    all_units = []

    # Останавливаться при привышении дистанции. Необходимо, для ожидания отстающих юнитов
    stop = False

    def __init__(self, unit_group=1, units=[], min_dst=2, max_dst=6, stop=False):
        self.units = dict()
        self.unit_group = unit_group.group
        self.min_dst = min_dst
        self.max_dst = max_dst
        self.stop = stop
        self.default_units_info(units)
        # self.all_units = all_units

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем, когда была последняя команда отступления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            curr_time = time.time()
            self.units[self.last_action.unit_group] = curr_time

        self.ms.update_units()

        # Проверяем врагов

        main_dst = []

        main_hero = self.ms.cmd.get(self.unit_group)
        # Рассчитываем дистанцию до героев
        if main_hero[0]:
            for unit in self.units:
                hero = self.ms.cmd.get(unit)
                if hero and hero[0] > 0:
                    # pr(self.module_name(),hero): [427, 705]
                    # pr(self.module_name(),e): [21, [122, 79, 3, [22.0, 6.0, 689, 190], 0, 0], 1, '0-0']
                    dst = int(self.get_distance(hero, main_hero))
                    # Дистанция, враг, герой
                    main_dst.append([dst, unit])
                    pass
                pass
            pass

        if main_dst:
            # Дистанция слишком велика
            main_dst.sort(key=lambda x: x[0], reverse=True)
            max_dst = main_dst[0]
            if max_dst[0] > self.max_dst * 32:
                # Враг близко
                hero = max_dst[1]
                last_move_time = self.units.get(hero)
                curr_time = time.time()
                if curr_time - last_move_time < 1:
                    # Герой уже отступал менее секунды назад
                    return
                else:
                    # Создаём действие к оступлению
                    hero_crd = self.ms.cmd.get(hero)
                    x_path = main_hero[1] - hero_crd[1]
                    y_path = main_hero[0] - hero_crd[0]

                    min_dst = self.min_dst * 32
                    # Коэфициент отступления
                    k_dst = min_dst / max_dst[0]

                    x_path_sm = round(x_path * k_dst, 0)
                    y_path_sm = round(y_path * k_dst, 0)

                    abs_crd = self.ms.screen_to_abs(main_hero[1] - x_path_sm + 16, main_hero[0] - y_path_sm + 16)
                    pr(self.module_name(), 'Дистанция слишком велика:', max_dst[0], 'отходим', hero, abs_crd)
                    pr(self.module_name(), main_hero, [y_path, x_path], [y_path_sm, x_path_sm],
                       [abs_crd[1], abs_crd[0]], hero_crd, k_dst)

                    shift_crd = self.walk_shift_abs_crd(abs_crd)
                    if not self.stop:
                        self.action = action.Walk(hero, shift_crd)
                        self.action.priority = 6
                    else:
                        self.action = action.Stop(hero)
                        self.action.priority = 4

                    if self.ms.hero_health_not_full(hero):
                        # Елси не полное хп, увеличиваем приоритет
                        self.action.priority = 2
                        pass

                pass

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit.group] = 0

    pass


class WaitingForAnAttack(Task):
    # Ожидание атаки.
    # Ждём, когда у контрольной группы начнёт тратиться ХП,
    # затем снимаем задачу
    task_name = 'Ожидание атаки'

    units = []

    validate_count = 0
    validate_max_count = 3
    last_time = 0
    min_wait = 1

    def __init__(self, units=[], validate_max_count=3, min_wait=1):
        self.units = units
        self.validate_max_count = validate_max_count
        self.min_wait = min_wait

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        if self.last_time == 0:
            self.last_time = curr_time

        if curr_time - self.last_time < self.min_wait:
            return

        self.last_time = curr_time

        # Смотрим ХП юнитов
        for n in self.units:
            if self.ms.hero_health_not_full(n):
                pr(self.module_name(), 'У команды', n, 'не полное здоровье')
                self.validate_count += 1
                break

        # Валидация пройдена, снимаем задачу
        if self.validate_count > self.validate_max_count:
            self.status = 0


class Wait(Task):
    # Ожидание, сек
    wait = 0
    start_time = 0
    task_name = 'Ожидание'

    def __init__(self, wait=1):
        self.start_time = time.time()
        self.wait = wait
        self.task_name = 'Ожидание', wait, 'сек.'

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        if curr_time - self.start_time > self.wait:
            pr(self.module_name(), 'Ожидание', self.wait, 'закончено')
            self.status = 0


class Bless(WalkTask):
    # Каст удачи группы на избранную группу
    task_name = 'Каст удачи'

    # Список юнитов, на которых будет наложена магия
    units = dict()
    # Время ожидания повторного каста
    wait_time = 10

    # Дополнительная задача
    weight = 0

    # Если цель дальше этой дистанции, не кастуем магию
    max_dst = 6

    # Герой должен стоять для каста магии
    hero_is_stay = True

    # Кастовать в любом случае
    force = True

    def __init__(self, units=[], unit_group=Hero, wait_time=10, max_dst=6, force=True):
        # Список юнитов, на которых будет наложена магия
        self.units = dict()
        self.force = force
        self.unit_group = unit_group.group
        self.default_units_info(units)
        self.wait_time = wait_time
        self.max_dst = max_dst

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем, когда была последняя команда благославления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            curr_time = time.time()
            if self.last_run_act_result == 1:
                self.units[self.last_action.unit_target] = curr_time

        # Проходим по списку юнитов
        # Проверяем время, если время истекло - кастуем магию
        curr_time = time.time()
        group_abs = self.ms.get_hero_abs(self.unit_group)

        if not self.force:
            if not group_abs:
                # Если координаты героя не найдены, пропускаем ход.
                return
            if self.ms.hero_not_found.get(self.unit_group):
                # Если герой не найден, значит он может быть за экраном, пропускаем ход.
                return

        for key, last_time in self.units.items():
            if curr_time - last_time > self.wait_time:
                hero_abs = self.ms.get_hero_abs(key)
                if hero_abs:
                    if self.force:
                        self.cast_action(hero_abs, key)
                        break

                    # Проверяем дистанцию до цели
                    dst = int(self.get_distance(hero_abs, group_abs))
                    if dst < self.max_dst * 32:
                        # Герой стоит?
                        if self.hero_is_stay and not self.ms.hero_is_stay(key):
                            continue
                        self.cast_action(hero_abs, key)
                        break
        pass

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'bless', key, priority=7)

    def magic_cast_shift(self, crd_abs):
        # Сдвиг по X, для каста магии
        return [crd_abs[0] - 4, crd_abs[1]+10]

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit.group] = 0

    pass


class HealthMon(Bless):
    # Мониторинг здоровья.
    # Если у какой-то группы мало ХП, создаём задачу медику лечить эту группу
    # Уровень здоровья.
    # 0 - small hp
    # 5 - half hp
    # 10 - full hp

    health_level = 0

    # Заставлять юнит отступать, если хп меньше уровня
    force_retreat = -1

    # Возворащаться в центр группы, модуль UnitCircle
    retreat_unit_cicle_center = True

    # Последний каст
    last_cast_time = 0
    # Время межу кастами
    wait_time = 0.1

    # Если цель дальше этой дистанции, не кастуем магию
    # 0 - Не важно где цель, маг идёт и лечит
    # 1 - Лечит в рациусе цели, если маг прошёл валидацию координат
    max_dst = 0

    # Дополнительная задача
    weight = 0

    # Группа, которую нужно лечить. Если не выбрано - лечим всех
    target_group = []
    act = Actions()

    # Лечить всей группой
    health_group = False

    # Принудительно остановить героя перед лечением
    force_hero_stay = False

    def __init__(self, unit=1, health_level=10, wait_time=200, max_dst=0, target_group=[], force_retreat=-1,
                 priority_custom=0, health_group=False, force_hero_stay=False):
        # Группа, которую нужно лечить. Если не выбрано - лечим всех
        self.unit_group = unit.group
        self.health_level = health_level
        self.wait_time = wait_time / 1000
        self.max_dst = max_dst
        self.target_group = target_group
        self.task_name = 'Мониторинг здоровья группы ' + str(unit.group)
        self.force_retreat = force_retreat
        self.priority_custom = priority_custom
        self.force_hero_stay = force_hero_stay
        self.health_group = health_group

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда лечения
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Проверяем было ли это отступление
            if self.last_action.act_type == 1002 and self.last_run_act_result == 1:
                # Устанавливаем таймер на текущую группу.
                self.last_cast_time = curr_time

        if curr_time < self.wait_time + self.last_cast_time:
            return

        # Получаем список героев и их характеристики
        heroes = self.ms.cmd
        if self.target_group:
            heroes = dict()
            for item in self.target_group:
                heroes[item] = self.ms.cmd.get(item)

        status_hp = []

        for key, hero in heroes.items():
            if hero[0]:
                # 0 - small hp
                # 5 - half hp
                # 7 - not full hp
                # 10 - full hp
                hp = self.ms.health_status_v2(key)

                if hp <= self.health_level:
                    # Добавляем только если уровень здоровья ниже рамок
                    status_hp.append([hp, key])

        group_abs = [0, 0]
        if self.max_dst:
            # Если указана максимальная дистанция, проводим дополнительные проверки
            group_abs = self.ms.get_hero_abs(self.unit_group)
            if not group_abs or self.ms.hero_not_found.get(self.unit_group):
                # Если координаты героя не найдены, пропускаем ход.
                return

        # Лечим первого потраченого
        if len(status_hp):
            self.find_hero_to_cast(status_hp, group_abs)

    pass

    def find_hero_to_cast(self, status_hp, group_abs):
        # TODO
        #  Нуждаются в лечении:
        #  1. Герои с малым ХП вблизи врагов. <50% (отступление или принудительное лечение) Приоритет 1
        #  2. Герои с малым ХП <50% (лечение если стоят или принудительная остановка и лечение) Приоритет 1
        #  3. Герои с потраченым ХП вблизи врагов. <100% (лечение если стоят) Приоритет 1
        #  4. Герои с потраченым ХП <100%. (лечение если стоят) Приоритет 2
        #  Нуждаются в отступлении:
        #  1. Герои с малым ХП вблизи врагов <50% (отступление или принудительное лечение) Приоритет 1
        #  Должны идти в бой:
        #  1. Герои с полным ХП вдали от врагов (защита) Приоритет 1

        ret = False

        status_hp.sort(key=lambda x: x[0], reverse=False)


        for item in status_hp:
            hp = item[0]
            key = item[1]

            if hp == 10:
                # У всех полное ХП, выходим
                break

            # Проверка реального здоровья героя (эксперементальное)
            health = self.get_hero_real_hp(key)
            # print(key, hp, health)

            if len(health) and health[0] >= health[1]:
                # Полное ХП
                # print(key, 'Полное ХП')
                continue

            # Проверка отступления
            if self.force_retreat != -1 and UnitStatus.get(key) != UnitStatus.retreat:
                # Отступление юнита, если он уже не отступает
                health_level = (health[0] / health[1]) * 10
                # print(key, 'Real HP:', health_level, self.force_retreat)
                if health_level < self.force_retreat:
                    # Отступаем к центру
                    self.retreat_action(key)
                    break

            hero_abs = self.ms.get_hero_abs(key)
            # Проверяем дистанцию до цели.
            # Если у нас есть несколько магов с такими задачами, лечить должен ближайший
            if self.max_dst:
                dst = int(self.get_distance(hero_abs, group_abs))
                if dst > self.max_dst * 32:
                    # TODO Проверить дистанцию до всех магов.
                    #  Если некому лечить, пусть отступает
                    # print(key, 'некому лечить')
                    continue

            force_stop = False
            if self.hero_is_stay and not self.ms.hero_is_stay(key):
                # Герой должен стоять для каста магии, но он движется
                if not self.force_hero_stay:
                    # print(key, 'он движется')
                    # TODO Если мало ХП и отступление не предусмотрено, нужно принудиельное лечение
                    continue
                else:
                    force_stop = True

            action_group = self.unit_group
            if self.health_group:
                action_group = -1

            self.action = action.Heal(action_group, self.magic_cast_shift(hero_abs), key, force_stop)
            if self.priority_custom:
                self.action.priority += self.priority_custom

            ret = True
            break

        return ret

    def retreat_action(self, key):
        # Отступление юнита
        if self.retreat_unit_cicle_center:
            last_center = UnitCircleInfo.last_center
            print(last_center)
            curr_time = time.time()
            if last_center[0] > 0 and last_center[0] > curr_time - 1:
                # Центр валидный
                self.action = action.WalkRetreat(key, last_center[1])
                # Отступление
                self.action.act_type = 2002

        if not self.action:
            self.action = action.Retreat(key)

        pr(self.module_name(), 'Отступление группы, мало ХП', key)

        # Приоритет для отступления наивысший
        self.action.priority = 1
        self.action.priority += self.priority_custom

    def get_hero_real_hp(self, item):
        self.act.select_group(item)
        health = self.ms.ss.custom_unit_health()
        return health


class KillEnemyMagic(Bless):
    # Убийство дальних врагов магией.
    # Убиваем с приоритетом.
    # 1. Сначала магов, потом остальных.
    # 2. Сначала дальних, потом ближних

    task_name = 'Убийство врага магией'

    # Убийство именного врага, магией
    cast_magic = ''

    # Минимальная дистанция до врага. Если враг ближе, не атакуем. Это или свой или его убьют свои.
    min_dst = 5
    # Максимальная дистанция, дальше которой маг не атакует.
    max_dst = 10

    # Последний каст
    last_cast_time = 0
    # Время межу кастами
    wait_time = 0.1

    # Не обязательная задача
    weight = 0

    # Сперва атакуем дальние цели
    far_target = True

    # Атаковать только если наша группа видна на карте и определяется
    self_validate = False

    # Определение врага по типу, а не по имени.
    # Этот алгоритм быстрее стандартного, так как не требует дополнительного навода мыши на цель.
    target_type = 0

    # Имя врага
    target_code = ''
    # Проверять имя врага перед атакой.
    validate_target = False
    target_names = dict()
    # Останавливаться, если цель найдена.
    stop_if_target_found = True
    # Валидация магического курсора. Необходима, чтобы герой атакавал магией, я не шёл к цели к месту атаки.
    validate_magic_cursor = True
    # Атака магией через кнопку A
    magic_attack = False

    # Последнее полявление врага
    last_enemy_detect = 0
    # Блокировка других задач, в течении 3 сек., после появления врага
    enemy_detect_block = 0
    # Количество элексиров маны, выпиваемых после каста
    up_mana = 0
    # Только атака на цель. После успешной атаки, снимаем задачу
    just_attack = False

    def __init__(self, min_dst=5, unit_group=Hero, cast_magic="fireball", time_out=0, wait_time=100, target_type=-1,
                 far_target=True, max_dst=10, self_validate=False, target_name='', validate_target=False,
                 magic_attack=False, priority_custom=2, enemy_detect_block=0, magic_cursor=True,
                 stop_if_target_found=True, up_mana=0, just_attack=False):

        self.target_names = dict()
        self.min_dst = min_dst
        self.unit_group = unit_group.group
        self.cast_magic = cast_magic
        self.close_time = time_out
        self.wait_time = wait_time / 1000
        self.target_type = target_type
        self.far_target = far_target
        self.max_dst = max_dst
        self.self_validate = self_validate
        self.validate_target = validate_target
        self.magic_attack = magic_attack
        self.priority_custom = priority_custom
        self.enemy_detect_block = enemy_detect_block
        self.validate_magic_cursor = magic_cursor
        self.stop_if_target_found = stop_if_target_found
        self.up_mana = up_mana
        self.just_attack = just_attack
        if target_name:
            names = Names()
            target_code = names.get_code_by_name(target_name)
            # Если задано имя цели, получаем её код
            self.target_code = target_code
            # Сброс информации о врагах
            FoundEnemy.found = dict()

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        self.block = 0

        curr_time = time.time()
        # Проверяем, когда была последняя команда Атаки
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            if self.last_action.act_type == 10:
                # Поиск врага
                target_id = self.last_action.target_id
                if self.last_run_act_result:
                    # Добавляем полученную информацию о цели в список
                    FoundEnemy.found[target_id] = self.last_run_act_result
            else:
                # Атака
                if self.validate_target:
                    # Если была валидация цели, назначаем полученные валидацией значения
                    if self.last_run_act_result == self.target_code:
                        self.last_cast_time = curr_time
                    else:
                        # Атака провалена, цель отличается от заявленной
                        FoundEnemy.found[self.last_action.target_id] = self.last_run_act_result
                else:
                    if self.just_attack:
                        # Если это была разовая атака, снимаем задачу.
                        self.status = 0
                        return
                        # Устанавливаем таймер на текущую группу.
                    if self.last_run_act_result == 1:
                        self.last_cast_time = curr_time

        if curr_time - self.last_cast_time < self.wait_time:
            # pr(self.module_name(),'Блокировка побочных задач')
            self.block = 1
            return

        if curr_time - self.last_enemy_detect < self.enemy_detect_block:
            # pr(self.module_name(),'Блокировка побочных задач')
            self.block = 1

        if self.self_validate:
            unit_group = self.ms.cmd.get(self.unit_group)
            if not unit_group[0]:
                pr(self.module_name(), 'Герой', self.unit_group, 'не найден, пропуск хода')
                return
            if self.ms.hero_not_found.get(self.unit_group):
                pr(self.module_name(), 'Герой', self.unit_group, 'не найден, пропуск хода')

        self.ms.update_units()
        # Проверяем врагов
        target = self.ms.p.get_valid_units()
        hero = self.ms.cmd.get(self.unit_group)
        if not hero:
            return

        valid_target = []
        hp_target = []
        need_find = []
        # Цели выбранного типа врагов
        if target:
            for e in target:
                dst = int(self.get_distance(hero, [e[1][3][3], e[1][3][2]]))
                if self.min_dst * 32 < dst < self.max_dst * 32:
                    # Добавляем только элементы, на соответствующей дистанции
                    unit_type = e[1][5]
                    if 0 <= self.target_type != unit_type:
                        # Если назначена специальная цель, добавляем только её, остальных пропускаем
                        continue

                    if self.target_code:
                        # Если у цели должно быть имя, ищем его
                        if FoundEnemy.found.get(e[0]):
                            if self.target_code != FoundEnemy.found.get(e[0]):
                                # Имя отличается, пропускаем
                                continue
                            else:
                                # Проверка пройдена, добавляем цель в лист
                                pass
                        else:
                            # Имя не найдено, добавляем в список поиска имён.
                            need_find.append(e)
                            # И пропускаем
                            continue

                    valid_target.append([dst, e])
                    hp = e[1][4]
                    hp_target.append([hp, e])

        if valid_target:
            pr(self.module_name(), 'Враг обнаружен', valid_target)
            self.last_enemy_detect = curr_time
            # Выбираем самую опасную цель
            # [87, [69, 50, 3, [9.0, 10.0, 274, 320], 2, 3], 1, '0-0']
            enemy = []
            if self.target_type == -1:
                for e in valid_target:
                    unit_type = e[1][1][5]
                    if unit_type == 3:
                        # Находим некромансера
                        enemy = e[1]
                        break
            if not enemy:
                # Атакуем цель, у которой меньше всех хп
                hp_target.sort(key=lambda x: x[0], reverse=True)

                if hp_target[0][0] != 0:
                    enemy = hp_target[0][1]
                else:
                    # Атакуем самую дальнюю цель
                    reverse = True
                    if not self.far_target:
                        # Атакуем сперва ближние цели
                        reverse = False
                    valid_target.sort(key=lambda x: x[0], reverse=reverse)
                    enemy = valid_target[0][1]

            # Атакуем врага
            enemy_abs = self.ms.get_unit_abs_crd(enemy)
            pr(self.module_name(), 'Атака на цель', valid_target)
            self.cast_action(enemy_abs, enemy[0])

        if need_find:
            enemy = need_find[0]
            self.find_name_action(enemy)

    def get_enemy_abs(self, enemy):
        # Получить координаты врага
        # pr(self.module_name(),enemy)
        crd_abs = self.ms.screen_to_abs(enemy[1], enemy[0])
        shift_abs = self.ms.get_shift(crd_abs)
        return shift_abs

    def find_name_action(self, enemy):
        self.block = 1
        enemy_abs = self.ms.get_unit_abs_crd(enemy)
        pr(self.module_name(), 'Запрос информации о цели', enemy)
        self.action = action.TargetInfo(enemy_abs, enemy[0], self.unit_group, self.stop_if_target_found)

    def cast_action(self, hero_abs, target_id):
        self.block = 1
        # Создание каста магии
        priority = 2
        if self.priority_custom:
            priority = self.priority_custom
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), self.cast_magic, target_id,
                                       attack=self.magic_attack, cursor=self.validate_magic_cursor, priority=priority,
                                       up_mana=self.up_mana)


class KillEnemy(KillEnemyMagic):
    # Убийство врага
    task_name = 'Убийство врага'

    def cast_action(self, hero_abs, target_id):
        target_code = ''
        if self.validate_target:
            target_code = self.target_code

        self.action = action.Attack(self.unit_group, hero_abs, target_code, target_id, self.stop_if_target_found)


class LightResist(Bless):
    # Защита от магии воздуха
    task_name = 'Защита от магии воздуха'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'light_res', key, priority=8)


class FireResist(Bless):
    # Защита от магии огня
    task_name = 'Защита от магии огня'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'fire_res', key, priority=8)


class WaterResist(Bless):
    # Защита от магии воды
    task_name = 'Защита от магии воды'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'water_res', key, priority=8)


class EarthResist(Bless):
    # Защита от магии земли
    task_name = 'Защита от магии земли'

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(hero_abs), 'earth_res', key, priority=8)


class SelfHealMon(Bless):
    # Мониторинг здоровья
    task_name = 'Мониторинг здоровья (на себя)'

    # 0-10 - уровень здоровья
    health_level = 7
    # Использовать эликсиры здоровья.
    # -1 Не использовать.
    # 0-9 - уровень для использования
    use_potions = -1

    def __init__(self, hero='', wait_time=200, health_level=9, use_potions=-1):
        # Список юнитов, на которых будет наложена магия
        self.units = dict()
        self.unit = hero
        self.unit_group = hero.group
        self.wait_time = wait_time / 1000
        self.last_action_time = 0
        self.use_potions = use_potions
        self.health_level = health_level

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем, когда была последняя команда благославления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            curr_time = time.time()
            if self.last_run_act_result == 1:
                self.last_action_time = curr_time

        # Проверяем время, если время истекло - кастуем магию
        curr_time = time.time()
        hero_abs = self.ms.get_hero_abs(self.unit_group)

        if not hero_abs:
            # Если координаты героя не найдены, пропускаем ход.
            return
        if self.ms.hero_not_found.get(self.unit_group):
            # Если герой не найден, значит он может быть за экраном, пропускаем ход.
            return

        health_status = -1
        health_procent = 0

        if self.ms.ss.curr_code == self.unit.code_v2:
            # 0 - full hp
            # 1 - half hp
            # 2 - small hp

            # 3 - big hp
            # 4 - max hp

            health = self.ms.ss.curr_health
            if health[0] != -1:
                health_status = 10
                health_procent = int((health[0] / health[1]) * 100)
                if health_procent < 30:
                    health_status = 0
                elif health_procent < 50:
                    health_status = 5
                elif health_procent < 75:
                    health_status = 7
                elif health_procent < 100:
                    health_status = 9

        if health_status == -1:
            health_status = self.ms.health_status_v2(self.unit_group)
            pass

        if curr_time - self.last_action_time > self.wait_time:
            if self.use_potions != -1 and health_status <= self.use_potions:
                # Исползование эликсиров
                if UserPotions.name_hp in UserPotions.potions:
                    # Если есть эликсиры здоровья
                    if UserPotions.potions[UserPotions.name_hp][1] > 0:
                        pr(self.module_name(), 'Использование эликсира здоровья группы', self.unit_group, 'здоровье',
                           health_status,
                           health_procent, 'всего эликсиров', UserPotions.potions[UserPotions.name_hp][1])
                        self.use_potion_action()
                        return
                    pass
                pass

            if health_status != -1:
                if health_status < self.health_level:
                    pr(self.module_name(), 'Лечение группы', self.unit_group, 'здоровье', health_status, health_procent)
                    self.cast_action(hero_abs, self.unit_group)

    def use_potion_action(self):
        # Создание каста магии
        self.action = action.UsePotion(self.unit_group, UserPotions.name_hp, 1)

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.Heal(key, self.magic_cast_shift(hero_abs))


class SelfManaMon(Bless):
    # Мониторинг маны героя
    task_name = 'Мониторинг маны (на себя)'

    # 0-10 - уровень маны
    min_mana = 80
    # Использовать эликсиры здоровья.
    # -1 Не использовать.
    # 0-9 - уровень для использования
    use_potions = -1

    def __init__(self, hero='', wait_time=200, min_mana=80):
        # Список юнитов, на которых будет наложена магия
        self.units = dict()
        self.unit = hero
        self.unit_group = hero.group
        self.wait_time = wait_time / 1000
        self.last_action_time = 0
        self.min_mana = min_mana

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда благославления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            if self.last_run_act_result == 1:
                self.last_action_time = curr_time

        if curr_time - self.last_action_time > self.wait_time:
            # Получаем значение маны
            mana = self.ms.ss.curr_mana
            min_mana = self.min_mana
            user_mana = -1
            if mana[1] != -1:
                if min_mana > mana[1]:
                    # Максимальная мана не может быть больше, чем у юнита
                    min_mana = mana[1]
                user_mana = mana[0]

            if user_mana != -1 and user_mana < self.min_mana:
                # Маны мало. Сколько надо эликсиров, чтобы восстановить ману
                num = 0
                mana_to_up = user_mana
                while mana_to_up < self.min_mana:
                    mana_to_up += 30
                    num += 1

                # Проверяем наличие эликсиров маны
                if UserPotions.name_mp in UserPotions.potions:
                    if UserPotions.potions[UserPotions.name_mp][1] > 0:
                        pr(self.module_name(), 'Использование эликсира маны', num, 'шт. для группы ',
                           self.unit_group, 'мана', mana, '<', self.min_mana,
                           'всего эликсиров', UserPotions.potions[UserPotions.name_mp][1])

                        self.use_potion_action(num)

    def use_potion_action(self, num):
        # Создание каста магии
        self.action = action.UsePotion(self.unit_group, UserPotions.name_mp, num)


class SelfManaRegen(Bless):
    # Приём эликсиров регинеразции маны
    task_name = 'Регенерация маны (на себя)'

    def __init__(self, hero='', wait_time=30):
        # Список юнитов, на которых будет наложена магия
        self.units = dict()
        self.unit = hero
        self.unit_group = hero.group
        self.default_units_info([hero])
        self.wait_time = wait_time
        self.last_action_time = 0

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        curr_time = time.time()

        # Проверяем, когда была последняя команда благославления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.2
            if self.last_run_act_result == 1:
                self.last_action_time = curr_time

        if curr_time - self.last_action_time > self.wait_time:
            if UserPotions.name_mr in UserPotions.potions:
                # Если есть эликсиры здоровья
                if UserPotions.potions[UserPotions.name_mr][1] > 0:
                    self.action = action.ManaRegenByClick(self.unit_group)


class MagicShield(Bless):
    # Магический щит
    task_name = 'Магический щит'
    self_info = False

    def __init__(self, hero='', wait_time=5, self_info=False):
        # Список юнитов, на которых будет наложена магия
        self.units = dict()
        self.unit = hero
        self.unit_group = hero.group
        self.default_units_info([hero])
        self.wait_time = wait_time
        self.self_info = self_info
        self.last_action_time = 0

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем, когда была последняя команда благославления
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            curr_time = time.time()
            if self.last_run_act_result == 1:
                self.last_action_time = curr_time

        # Проходим по списку юнитов
        # Проверяем время, если время истекло - кастуем магию
        curr_time = time.time()
        hero_abs = self.ms.get_hero_abs(self.unit_group)

        if not hero_abs:
            # Если координаты героя не найдены, пропускаем ход.
            return
        if self.ms.hero_not_found.get(self.unit_group):
            # Если герой не найден, значит он может быть за экраном, пропускаем ход.
            return

        if curr_time - self.last_action_time > self.wait_time:
            # Проверяем статус щита
            shield_status = 0

            if not self.self_info:
                shield_status = self.ms.shield_info(self.unit_group)
            else:
                # новый алгоритм
                if self.ms.ss.curr_code == self.unit.code_v2:
                    armor = self.ms.ss.curr_armor
                    if armor > 0:
                        shield_status = 1
                    pass
                else:
                    shield_status = self.ms.shield_info(self.unit_group)
                pass

            if shield_status != 1:
                # Герой стоит?
                if self.hero_is_stay and not self.ms.hero_is_stay(self.unit_group):
                    return

                self.cast_action(hero_abs, self.unit_group)

    def cast_action(self, hero_abs, key):
        # Создание каста магии
        self.action = action.Shield(key, self.magic_cast_shift(hero_abs))


class StoneWall(Bless):
    # Каменная сена
    task_name = 'Каменная сена'
    # Расстояние, относительно мага в единицах измерения мини-карты: x,y
    target_shift = [1, 1]
    # Время последней активации
    last_action_time = 0
    # Дополнительная задача
    weight = 0

    def __init__(self, unit_group=1, target_shift=[], wait_time=10):
        self.unit_group = unit_group
        self.target_shift = target_shift
        self.wait_time = wait_time

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)
        curr_time = time.time()

        # Проверяем, когда была последняя команда
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            if self.last_run_act_result == 1:
                self.last_action_time = curr_time

        # Проходим по списку юнитов

        if curr_time - self.last_action_time > self.wait_time:
            hero_abs = self.ms.get_hero_abs(self.unit_group)
            if hero_abs:
                cast_abs = [hero_abs[0] + self.target_shift[0] * 32, hero_abs[1] + self.target_shift[1] * 32]
                self.cast_action(cast_abs)

    def cast_action(self, target_abs):
        # Создание каста магии
        self.action = action.StoneWall(self.unit_group, self.magic_cast_shift(target_abs))

    pass


class ShiftWalk(WalkTask):
    # Занимаем позицию, относительно отряда
    # Расстояние, относительно отряда в единицах измерения мини-карты: x,y

    target_shift = [1, 1]
    # Время последней активации
    last_action_time = 0
    # Группа, к которой мы идём
    target_group = 1
    # Валидация исполнения команды
    validate = True
    # Задача выполнена
    task_stage = 0
    target_center = False
    # 0 - не центрировать, 1 - центрирование на юните, 2 - на цели
    last_center_group = 0

    def __init__(self, unit_group=Hero, target_group=Hero, target_shift=[], last_center_group=0, validate=True):
        self.task_stage = 0
        self.target_center = False
        self.last_action_time = 0
        self.unit_group = unit_group.group
        self.target_group = target_group.group
        self.target_shift = target_shift
        self.validate = validate
        self.last_center_group = last_center_group
        self.task_name = 'Смещение группы ' + str(unit_group) + ' к группе ' + str(target_group) + str(target_shift)
        # Одинарная задача. Снимается после выполнения.

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            if self.last_action.act_type == 2:
                # Центрирование
                self.target_center = True
            else:
                # Последняя команда была движение. Меняем статус задачи
                if self.last_center_group == 0:
                    # Если центрировать не надо, пропускаем этот стаж
                    self.task_stage = +2
                else:
                    self.task_stage = +1

        if self.task_stage > 0:
            # Мы уже сходили - центрируем
            if self.task_stage > 1:
                self.status = 0
                # Мы сходили и отцентрировались
                return
            group_to_center = self.unit_group
            if self.last_center_group == 2:
                group_to_center = self.target_group
            self.action = action.Center(group_to_center)
            self.task_stage += 1
            return

        # Проверяем валидность целевой группы и группы смещения. Если целевая группа не валидна, центрируем её.
        unit_crd = self.ms.get_hero(self.unit_group)
        if not self.validate and not unit_crd:
            self.status = 0
            # Координаты героя не найдены, снимаем задачу
            return

        target_abs = self.ms.get_hero_abs(self.target_group)
        if target_abs and not self.ms.hero_not_found.get(self.target_group):
            # Цель найдена, двигаемся к ней
            self.walk_action(target_abs)
        else:
            if self.target_center:
                # Последняя выполненная задача - центрирование
                pr(self.module_name(), 'Берём координаты по центру карты')
                # Берём координаты по центру карты
                # y,x
                tank_screen = [425, 342]
                tank_x_y = self.ms.abs_to_screen(tank_screen[0], tank_screen[1])
                target_abs = [tank_x_y[1], tank_x_y[0]]
                self.walk_action(target_abs)
            else:
                pr(self.module_name(), 'Координаты целевой группы не найдены, центрируемся на ней:', self.target_group)
                self.action = action.Center(self.target_group)

    def walk_action(self, target_abs):
        abs_crd = [target_abs[0] + self.target_shift[0] * 32, target_abs[1] + self.target_shift[1] * 32]
        shift_crd = self.walk_shift_abs_crd(abs_crd)
        self.action = action.Shift(self.unit_group, self.target_group, shift_crd)


class ShiftWalkColor(TaskOne):
    # Занимаем позицию, уникального цвета на карте
    # Расстояние, относительно отряда в пикселях: x,y

    target_shift = [32, 32]
    # Валидация исполнения команды
    validate = True
    # Задача выполнена
    task_stage = 0
    target_color = []
    target_center = False

    def __init__(self, unit_group=1, target_shift=[], target_color=[]):
        self.unit_group = unit_group
        self.target_shift = target_shift
        self.target_color = target_color
        self.task_name = 'Смещение группы ' + str(unit_group) + ' по координатам ' + str(target_shift) + \
                         'относительно цвета' + str(target_color)

    def default_action(self):
        found = self.ms.find_color_crd(self.target_color)
        crd = []
        if len(found):
            crd = found[0]
            abs_crd = [crd[1] + self.target_shift[0], crd[0] + self.target_shift[1]]
            # shift_crd = self.walk_shift_abs_crd(abs_crd)
            self.action = action.Walk(self.unit_group, abs_crd)


class Center(TaskOne):
    # Центрирование группы

    def __init__(self, unit=1):
        self.unit_group = unit.group
        self.task_name = 'Центрирование группы', unit.group

    def default_action(self):
        self.action = action.Center(self.unit_group)


class JoinGroups(TaskOne):
    # Соединение двух групп в одну

    join_group = 1

    def __init__(self, master=Hero, slave=Hero):
        self.unit_group = master.group
        self.join_group = slave.group
        self.task_name = 'Соединение групп', master.group, slave.group

    def default_action(self):
        self.action = action.Join(self.unit_group, self.join_group)


class ClearChat(TaskOne):
    def __init__(self):
        self.task_name = 'Очистка чата'

    def default_action(self):
        self.action = action.ClearChat()


class AllStop(TaskOne):
    # Остановка группы
    task_name = 'Остановка всех групп'

    def __init__(self):
        pass

    def default_action(self):
        self.action = action.AllStop()


class AllGuard(TaskOne):
    # Остановка группы
    task_name = 'Защита всей группы'

    def __init__(self):
        pass

    def default_action(self):
        self.action = action.AllGuard()


class Guard(TaskOne):
    # Остановка группы
    task_name = 'Защита группы'

    def __init__(self, unit_group=Hero):
        self.unit_group = unit_group.group

    def default_action(self):
        self.action = action.Guard(self.unit_group)


class Stop(TaskOne):
    # Остановка группы
    task_name = 'Остановка группы'

    def __init__(self, unit_group=Hero):
        self.unit_group = unit_group.group

    def default_action(self):
        self.action = action.Stop(self.unit_group)


class HealRegen(TaskOne):
    # Регенирация здоровья
    task_name = 'Регенерация здоровья'

    def __init__(self, unit_group=1):
        self.unit_group = unit_group

    def default_action(self):
        self.action = action.HealRegen(self.unit_group)


class ManaRegen(TaskOne):
    # Регенирация маны
    task_name = 'Регенирация маны'

    def __init__(self, unit_group=1):
        self.unit_group = unit_group

    def default_action(self):
        self.action = action.ManaRegen(self.unit_group)


class SpeedPlus(TaskOne):
    # Увеличение скорости
    task_name = 'Увеличение скорости'

    speed = 4
    plus = True

    def __init__(self, speed=4):
        self.plus = True
        self.speed = speed

    def default_action(self):
        self.action = action.Speed(self.speed, self.plus)


class SpeedMinus(SpeedPlus):
    # Уменьшение скорости
    task_name = 'Уменьшение скорости'

    def __init__(self, speed=4):
        self.plus = False
        self.speed = speed


class SetHeroesGroup(TaskOne):
    # Назначить группы героев
    task_name = 'Назначить группы героев'
    units = []

    # Центрирование группы
    def __init__(self, units=[]):
        self.units = units

    def default_action(self):
        self.action = action.SetGroups(self.units, self.ms)


class SetHeroesGroupV2(Task):
    # Назначить группы героев. Более сложная версия
    task_name = 'Назначить группы героев'
    units = []

    # Центрирование группы
    def __init__(self, units=[]):
        self.units = units

    # Одинарная задача. Снимается после выполнения.
    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Проверяем список назначеных юнитов
        new_units = []
        for unit in self.units:
            hero = self.ms.cmd.get(unit.group)
            if hero and hero[0] > 0:
                pr(self.module_name(), 'Юнит ',unit.group,'есть, его не надо назначать',hero)
                # Юнит есть, его не надо назначать
                pass
            else:
                # Юнита нет, его нужно назначить
                new_units.append(unit)
                pass
        # Обновляем список юнитов, которых нужно назначить
        self.units = new_units

        if len(self.units):
            # print(self.units)
            # Назначаем юниты
            self.default_action()
        else:
            # Все юниты назначены
            self.status = 0

    def default_action(self):
        # Назначаем юниты
        self.action = action.SetGroupsV2(self.units, self.ms)
        pass


class SetMultiGroup(TaskOne):
    # Назначить команды на одну группу
    task_name = 'Назначить группы героев'
    units = []
    unit_group = 1

    # Центрирование группы
    def __init__(self, primary=Hero, units=[]):
        self.unit_group = primary.group
        self.units = units

    def default_action(self):
        # Назначаем юниты
        self.action = action.SetMultiGroup(self.unit_group, self.units, self.ms)


class MagicMenu(TaskOne):
    task_name = 'Меню магии (показать/скрыть)'
    show = True

    def __init__(self, show=True):
        self.show = show

    def default_action(self):
        self.action = action.MagicMenu(self.show)


class OpenInvent(TaskOne):
    task_name = 'Меню инвентаря (показать/скрыть)'
    show = True

    def __init__(self, show=True):
        self.show = show

    def default_action(self):
        self.action = action.InventMenu(self.show)


class Click(TaskOne):
    task_name = 'Клик мышкой по экрану'
    crd = []
    double = False

    def __init__(self, crd=[], double=False):
        self.crd = crd
        self.double = double

    def default_action(self):
        self.action = action.Click(self.crd, self.double)


class SetOtherHeroesGroup(TaskOne):
    task_name = 'Назначение других юнитов в группу'

    # Назначить остальных героев в группу
    def __init__(self, unit_group=1):
        self.unit_group = unit_group

    def default_action(self):
        self.action = action.SetOtherGroups(self.unit_group, self.ms)


class BindAutoMagic(TaskOne):
    # Назначение авто-магии
    task_name = 'Назначение авто-магии'
    # Название магии
    magic_name = ''

    def __init__(self, unit_group=1, magic_name=''):
        self.unit_group = unit_group
        self.magic_name = magic_name

    def default_action(self):
        self.action = action.BindAutoMagic(self.unit_group, self.magic_name)


class BindHotKeyMagic(TaskOne):
    # Назначение горячей клавиши на инвентарь
    task_name = 'Назначение горячей клавиши для магии'
    key = ''
    magic_name = ''

    def __init__(self, hero='', key='', magic_name=''):
        self.unit_group = hero.group
        self.key = key
        self.magic_name = magic_name

    def default_action(self):
        self.action = action.BindHotKeyMagic(self.unit_group, self.key, self.magic_name)


class BindHotKeyInvent(TaskOne):
    # Назначение горячей клавиши на инвентарь
    task_name = 'Назначение горячей клавиши на инвентарь'
    key = ''
    color = []

    def __init__(self, unit_group=1, key='', color=[]):
        self.unit_group = unit_group
        self.key = key
        self.color = color

    def default_action(self):
        self.action = action.BindHotKeyInvent(self.unit_group, self.key, self.color)


class Teleport(TaskOne):
    # Телепорт
    task_name = 'Телепорт'
    # Расстояние, относительно мага в единицах измерения мини-карты: x,y
    target_shift = [1, 1]

    def __init__(self, unit_group=1, target_shift=[]):
        self.unit_group = unit_group
        self.target_shift = target_shift

    def default_action(self):
        hero_abs = self.ms.get_hero_abs(self.unit_group)
        if hero_abs:
            cast_abs = [hero_abs[0] + self.target_shift[0] * 32, hero_abs[1] + self.target_shift[1] * 32]
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(cast_abs), 'teleport')

    def magic_cast_shift(self, crd_abs):
        # Сдвиг по X, для каста магии
        return [crd_abs[0] - 10, crd_abs[1]]


class CastMagic(TaskOne):
    # Каст магии по координатам
    task_name = 'Телепорт'
    # Расстояние, относительно мага в единицах измерения мини-карты: x,y
    target_shift = [1, 1]
    magic = 'fireball'

    def __init__(self, unit_group=Hero, target_shift=[], magic='fireball'):
        self.unit_group = unit_group.group
        self.target_shift = target_shift
        self.magic = magic

    def default_action(self):
        hero_abs = self.ms.get_hero_abs(self.unit_group)
        if hero_abs:
            cast_abs = [hero_abs[0] + self.target_shift[0] * 32, hero_abs[1] + self.target_shift[1] * 32]
        self.action = action.CastMagic(self.unit_group, self.magic_cast_shift(cast_abs), self.magic)

    def magic_cast_shift(self, crd_abs):
        # Сдвиг по X, для каста магии
        return [crd_abs[0] - 10, crd_abs[1]]


class DarkDetector(Bless):
    # Детектор затемнения героя.
    # Идея. Когда героя затемняет некромант или дракон, герой это обнаруживает и искользует свиток оствеления на себя.
    # Так как дракон может превращать в камень, то лучше если свиток использует другой герой.
    # Класс должен принимать два аргумента: целевой герой и герой со свитком.
    # В случае затемнения целевого героя, герой со свитком освещает его и ставит паузу на 20 сек.
    task_name = 'Детектор затемнения героя'

    weight = 0
    last_action_time = 0
    wait_time = 1
    allways = False

    def __init__(self, unit_group=Hero, target_group=Hero, wait_time=3, allways=False):
        self.unit_group = unit_group.group
        self.target_group = target_group.group
        self.wait_time = wait_time
        self.allways = allways

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            self.last_action_time = curr_time

        if curr_time - self.last_action_time > self.wait_time:
            if self.ms.dark_detect(self.target_group) or self.allways:
                # Обнаружено затемнение
                hero_abs = self.ms.get_hero_abs(self.target_group)
                if hero_abs:
                    self.cast_action_light(hero_abs)

    def cast_action_light(self, target_abs):
        # Создание каста магии
        self.action = action.CastMagic(self.unit_group, target_abs, 'light', self.target_group)

    pass


class LifeHiredUnits(Task):
    # Проверка жизни наёмников. Цикл мониторит статус ошибок нахождения бойцов.
    # Если ошибка больше определённого числа, проверяет состояние бойца.
    # Если состояние стабильное, ожидает следующей проверки, иначе выдаёт исключение, прерывающее цикл.

    # Список наёмников
    units = dict()

    # Валидация смерти наёмников
    unit_validate = dict()
    max_validate_count = 2

    # Дополнительна задача
    weight = 0

    # Проверяем жизнь юнита каждые 3 секунды
    default_wait_time = 3
    not_found_wait_time = 1
    wait_time = default_wait_time

    def __init__(self, units=[]):
        self.unit_validate = dict()
        self.default_units_info(units)
        self.task_name = 'Валидация жизни наёмников:', units

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()
        # Проверяем, когда была последняя команда проверки
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Устанавливаем таймер на текущую группу.
            if self.last_run_act_result:
                # Меняем вес задачи. Она не будет закрыта, пока валидация не будет пройдана
                self.weight = 1
                self.wait_time = self.not_found_wait_time
                # Герой не найден, обновляем счётчик
                if self.update_validate_units():
                    self.defeat_action()
                    return
            else:
                if self.reset_validate_units():
                    # Нет ошибок нахождения наёмников, меняем задачу на дополнительную
                    self.weight = 0
                    self.wait_time = self.default_wait_time

            # Обновляем счётик для группы
            self.units[self.last_action.unit_group][0] = curr_time

        # Проходим по списку юнитов
        # Проверяем время, если время истекло - перепроверяем
        for key, unit_info in self.units.items():
            last_time = unit_info[0]
            hero = unit_info[1]
            if curr_time - last_time > self.wait_time:
                # Проверка жизни героя
                hero_error = self.ms.hero_not_found.get(key)
                if hero_error and hero_error > 10:
                    # Если ошибок больше 10, запрашиваем проверку жизни героя
                    self.set_action(hero)
                    break

    def reset_validate_units(self):
        key = self.last_action.unit_group
        self.unit_validate[key] = 0
        # pr(self.module_name(),'Сброс счётчика смерти группы', key)
        i = 0
        for key, val in self.unit_validate.items():
            i += val
        if i > 0:
            return False
        return True

    def update_validate_units(self):
        key = self.last_action.unit_group
        ret = False
        pr(self.module_name(), 'Обновление счётчика смерти группы', key)
        if self.unit_validate.get(key):
            self.unit_validate[key] += 1
            if self.unit_validate[key] > self.max_validate_count:
                ret = True
        else:
            self.unit_validate[key] = 1

        return ret

    def defeat_action(self):
        self.action = action.Defeat()

    def set_action(self, hero):
        # Создание каста магии
        self.action = action.ValidateLife(hero)

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit.group] = [0, unit]

    pass


class LifeHiredUnits2(Task):
    # Проверка жизни наёмников. Цикл проверяет нахождение бойцов
    # Если боец не найден на экране, скрипт выбирает бойца и смотрит его статус
    # Если состояние стабильное, ожидает следующей проверки, иначе выдаёт исключение, прерывающее цикл.

    # Список наёмников
    units = dict()

    # Валидация смерти наёмников
    unit_validate = dict()
    max_validate_count = 2

    # Дополнительна задача
    weight = 0

    # Проверяем жизнь юнита каждые 3 секунды
    default_wait_time = 1
    not_found_wait_time = 1
    wait_time = 1

    act = Actions()

    def __init__(self, units=[]):
        self.unit_validate = dict()
        self.default_units_info(units)
        self.task_name = 'Валидация жизни наёмников:', units

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        curr_time = time.time()

        # Проверяем список юнитов
        weight = 0
        for key, unit in self.units.items():
            # print(self.ms.hero_not_found)
            hero_error = self.ms.hero_not_found.get(key)

            if hero_error and hero_error > 3:
                weight = 1
                # Когда была последняя валидация юнита?
                last_time = unit[0]
                if curr_time - last_time > self.wait_time:
                    # Прошло много времени, надо проверять
                    pr(self.module_name(), 'Проверка жизни юнита', key)
                    hp = self.get_hero_real_hp(key)
                    if hp[0] != -1:
                        # Юнит жив
                        pr(self.module_name(), 'Юнит жив', key)
                        self.units[key][0] = curr_time
                        weight = 0
                    else:
                        # Юнит мёртв
                        pr(self.module_name(), 'Юнит мёртв', key)
                        weight = 0
                        self.action = action.Defeat()
                        break

        self.weight = weight

    def get_hero_real_hp(self, item):
        self.act.select_group(0)
        self.act.select_group(item, 20)
        health = self.ms.ss.custom_unit_health()
        return health

    def default_units_info(self, units):
        # 0 - время последнего хода группы.
        self.units = dict()
        for unit in units:
            self.units[unit.group] = [0, unit]

    pass


class UnitCircle(Walk):
    # Если радиус больше положеного, находим крайних юнитов, у кого самая большая дистанция до радиуса
    # и направляем их внутрь радиуса. Радиус расширяется когда мы идём или когда боремся с врагами.
    # Когда идём к контрольной точке, те кто ближе к точке должны останавливаться при превышении радиуса.
    # И продолжать движение когда сократят дистанцию до центра

    # TODO
    #  1. +Средний радиус
    #  2. Отступление к центру команды
    #  3. Время на действия
    max_radius = 4
    weight = 1

    # Центр группы
    center = []
    # Центр группы на экране
    center_abs = []

    # Минимальное время остановки и отступления для юнита
    min_stop_time = 2

    # Дистанция мага до КТ, дальше которой он останавливается, если находится ближе всего к КТ
    mage_stop_dst_ct = 2

    task_target_done = False

    def __init__(self, target_abs=[], unit=False, max_radius=5, use_neuromap=True, walk_min_ct_radius=3):
        Walk.__init__(self, target_abs, unit)
        self.center = []
        self.center_abs = []
        self.max_radius = max_radius
        self.use_neuromap = use_neuromap
        self.max_unit_stop = 5
        self.walk_min_ct_radius = walk_min_ct_radius
        pass

    # Тест содержания юнитов в определённом круге
    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        #
        #   1. Инициализация модуля
        #

        curr_time = time.time()

        # 1. Первое центрирование
        # Проверяем, когда была последняя команда центрирования
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 2:
                # Если это была команда центрирования
                # Ждём 1 сек.
                self.last_center_time = curr_time

        if self.last_center_time == 0:
            self.action = action.Center(self.unit_group)
            return

        # 2. Информация о героях
        self.ms.update_units()
        heroes = self.ms.cmd

        # 3. Находим центр группы
        self.get_group_center(heroes)
        if not self.center:
            # Центр не найден, надо центрировать
            self.action = action.Center(self.unit_group)
            return

        # 4. Группа находится в движении к Контрольной Точке
        is_walking = True
        if not len(self.main_target_crd):
            # Если нет координат, мониторим
            self.weight = 0
            is_walking = False

        # 5. Контрольная точка назначена?

        # 5.1. Назначаем координаты центра
        self.unit_crd_last = self.unit_crd
        self.unit_crd = self.abs_to_map(self.center_abs)

        if is_walking:
            # Назначаем контрольную точку в зависимости от нашего центра
            if not self.local_target_crd:
                # Назначаем ближайшую КТ
                self.next_step()

        # Группа находится в движении к Контрольной Точке
        if is_walking:
            # Позиция КТ на экране
            ct_screen = self.hero_screen(self.local_target_crd)
            # Определяем относительные координаты мыши
            scr_crd = [ct_screen[0] * self.k_size, ct_screen[1] * self.k_size]
            target_point = (int(scr_crd[0] // 2), int(scr_crd[1] // 2))

        # 6. Находим дистанцию юнитов до центра и КТ
        radius = 0
        max_dst = 0
        min_dst = 1000

        hero_dst_sort = []
        hero_dst = dict()
        hero_radius = dict()
        center_point = ((self.center[0]) // 2, (self.center[1]) // 2)

        # Герой ближайший к центру
        closer_hero = self.unit_group

        for key, hero in heroes.items():
            y = hero[0]
            x = hero[1]
            if x > 0 and y > 0:
                color = (100, 255, 100)
                x1 = x + 16
                y1 = y + 32
                # Дистанция до центра
                dst = ((self.center[0] - x1) ** 2 + (self.center[1] - y1) ** 2) ** 0.5

                if dst > radius:
                    # Максималный радиус
                    radius = dst

                dst_sector = round(dst / 32, 2)
                if dst_sector > self.max_radius:
                    color = (100, 100, 255)

                if max_dst < dst_sector:
                    max_dst = dst_sector

                if min_dst > dst_sector:
                    min_dst = dst_sector
                    closer_hero = key

                # Расстояние от центра до героя
                hero_radius[key] = dst_sector

                # if UnitStatus.get(key) == UnitStatus.retreat:
                # Герой отступает к центру?
                # color = (255, 100, 100)

                # Рисуем линии юнитов до центра
                self.draw_line((x1 // 2, y1 // 2), center_point, color)

                # Тип героя
                hero_type = 0
                if len(hero) > 2:
                    hero_type = hero[3]

                ct_dst_sector = 0
                if is_walking:
                    # Расстояние до локальной цели
                    ct_dst = ((scr_crd[0] - x1) ** 2 + (scr_crd[1] - y1) ** 2) ** 0.5
                    ct_dst_sector = round(ct_dst / 32, 2)

                    # Расстояние от КТ до героя
                    hero_dst[key] = [ct_dst_sector, hero_type]

                    color = (255, 100, 100)
                    self.draw_line((x1 // 2, y1 // 2), target_point, color)
                    pass

                hero_dst_sort.append([dst_sector, ct_dst_sector, key, hero_type])

        # Рисуем круг
        color = (100, 255, 100)
        if radius / 32 > self.max_radius:
            color = (100, 100, 255)
        center_radius = int(radius) // 2
        self.draw_circle(center_point, center_radius, color)

        dst_segment = (max_dst - min_dst) / 3
        # Верхняя граница дистанции
        dst_mid_top = max_dst - dst_segment
        # Нижняя граница дистанции
        dst_mid_bottom = min_dst + dst_segment

        # Это первый вход в цикл?
        walk_active = False
        if self.last_move_time:
            # Команда группового передвижения уже была?
            walk_active = True

        #
        # 2. Логика действий
        #

        # TODO Если есть координаты передвижения, двигаемся к ним. Если нет стоим.
        #  Определение задач:
        #  1. Отступление
        #   Если мало хп и рядом враг
        #   Если герой убежал вперёд
        #  2. Центрирование. Если Это необходимо
        #  3. Группировка.
        #   Если герой далеко от центра.
        #   Если герой останавливался и стоит без дела
        #  4. Движение к цели.
        #   Если всё в порядке, двигаемся дальше.
        #  5. Достижение цели.
        #   Возможно только если другие задачи завершены.

        # 2.0 Назначаем группу, ближайшую к центру
        self.unit_group = closer_hero

        # 2.1 Отступление

        #  2.1.1. Отступление мага, который слишком близко к КТ
        #  Маги не должны идти впереди всех.
        #  Если маг в первой трети КТ, он останавливается.
        first_hero_type = 0
        # ближайший воин к КТ
        min_warrior_dst_to_ct = 10000
        if is_walking and walk_active:
            # Назначена КТ и герои идут к цели
            if len(hero_dst_sort):
                hero_dst_sort.sort(key=lambda x: x[1], reverse=False)
                for hero in hero_dst_sort:
                    # Где герой:
                    hero_type = hero[3]
                    if hero_type == 3:

                        # Какова дистанция до КТ?
                        if hero[1] < self.mage_stop_dst_ct:
                            # Маг слишком близко к КТ, чтобы его останавливать
                            continue

                        first_hero_type = 3
                        # Ближайших к КТ - маг
                        # Он стоит?
                        key = hero[2]

                        if UnitStatus.get(key) == UnitStatus.stop:
                            # Переходим к следующему
                            continue
                        else:
                            # Останавливаем героя
                            pr(self.module_name(), 'Запрос остановки мага', hero)
                            self.cmd_stop(key)
                    else:
                        min_warrior_dst_to_ct = hero[1]
                        # Дошли до воина, прекрашящем цикл
                        break

        #  TODO Маги должны быть прекрыты воинами со стороны атаки врага

        # Блокировка дальнейшего движения к следующей КТ
        block_next_ct = False
        # 2.1.2 Запускаем стоящих без дела
        if not self.action:
            # Если с радиусом всё в порядке,
            # Находим стоячих и отступающих
            for key, status in UnitStatus.items():

                # Проверяем статус героя, вышедшего за радиус:
                hero_status = status[0]
                unit_time = status[1]

                if hero_status == UnitStatus.stop or hero_status == UnitStatus.retreat:

                    if curr_time < unit_time + self.min_stop_time:
                        # Время последнего действия юнита ещё не прошло
                        # Запрещаем двигатья к КТ, пока отступившие юниты не завершат свой ход.
                        block_next_ct = True
                        # print('Блокировка движения к КТ', unit_time + self.min_stop_time - curr_time)
                        continue

                    if is_walking and walk_active:

                        # Проверяем, это маг, который стоит впереди?
                        if first_hero_type == 3 and key in hero_dst:
                            curr_hero = hero_dst.get(key)
                            if curr_hero[1] == 3:
                                # Это стоячий герой-маг
                                # Как далеко ближайший воин до КТ?
                                if min_warrior_dst_to_ct >= curr_hero[0]:
                                    # Ближе ближайшего воина, продолжаем стоять
                                    continue

                        # Проверяем количество ХП у юнита
                        hp = self.ms.health_status_v2(key)
                        if hp < 10:
                            # Не полное ХП, пропускаем
                            first_hero_type = 1
                            continue

                        # Запускаем юнита к КТ
                        ct_abs = self.ms.screen_to_abs(scr_crd[0], scr_crd[1])
                        if first_hero_type == 0:
                            # Впереди воин, идём группой
                            pr(self.module_name(), 'Впереди воин, идём группой')
                            self.cmd_walk_group_to_ct(ct_abs)
                        else:
                            # Впереди маг, поэтому ходим по одному
                            pr(self.module_name(), 'Впереди маг, поэтому ходим по одному')
                            self.cmd_walk(key, ct_abs)
                        pass
                    else:
                        # Заставляем обороняться
                        pr(self.module_name(), 'Всем обороняться')
                        self.cmd_all_guard()
                        pass

                if self.action:
                    # Действие назначено - выходим из цикла
                    break
            pass

        if self.action:
            # Если действие назначено, дальше не идём
            return

        #  2.1.3. Проверка нарушения радиуса
        #  Находим самого дальнего от центра
        if max_dst > self.max_radius and len(hero_dst_sort):
            # Радиус нарушен
            hero_dst_sort.sort(key=lambda x: x[0], reverse=True)
            for hero in hero_dst_sort:
                key = hero[2]
                hero_r = hero[0]
                if hero_r < self.max_radius:
                    # Все герои в пределах максимального радиуса
                    break

                # Проверяем статус героя, вышедшего за радиус:
                hero_status = UnitStatus.get(key)
                unit_time = UnitStatus.time(key)

                if curr_time < unit_time + self.min_stop_time:
                    # Время последнего действия юнита ещё не прошло
                    continue

                if is_walking and walk_active:
                    # Назначена кт и герои идут к цели
                    # print(hero, hero_status)
                    # Если герои идут к цели
                    hero_ct = hero[1]

                    # 1 - Движется к цели
                    if hero_status == UnitStatus.walk or hero_status == UnitStatus.walks:

                        # Где герой:
                        if hero_ct < dst_mid_top:
                            # Впереди, - остановка.
                            # Ожидаем остальных
                            pr(self.module_name(), 'Герой убежал вперёд. Запрос остановки', hero)
                            self.cmd_stop(key)

                        elif hero_ct < dst_mid_bottom:
                            # В середине, - отступление к центру
                            pr(self.module_name(), 'Отступление к центру из середины', key, self.center_abs)
                            self.cmd_retreat(key, self.center_abs)

                        else:
                            # Сзади - догоняет

                            # close_ct_hero = self.find_move_hero_close_ct(hero_dst_sort)
                            # if close_ct_hero != -1:
                            #    #    pr(self.module_name(), 'Отступление к центру', key, self.center_abs)
                            #    #    self.cmd_retreat(key, self.center_abs)
                            #    pr(self.module_name(), 'Запрос остановки ближайшего к КТ', close_ct_hero)
                            #    self.cmd_stop(close_ct_hero)

                            # Запускаем юнита к КТ
                            pr(self.module_name(), 'Отставшая группа движется к КТ', key, self.center_abs)
                            ct_abs = self.ms.screen_to_abs(scr_crd[0], scr_crd[1])
                            self.cmd_walk(key, ct_abs)
                            # pr(self.module_name(), 'Герой далеко от середины. Отступление к центру', key, self.center_abs)
                            # self.cmd_retreat(key, self.center_abs)

                    # 2 - Возвращается назад - пропуск.
                    # Ожидаем сокращения дистанции до центра.
                    elif hero_status == UnitStatus.retreat:
                        # Где герой:
                        if hero_ct < dst_mid_top:
                            # Впереди, - остановка.
                            pr(self.module_name(), 'Отступающий герой убежал вперёд. Запрос остановки', key, hero_ct,
                               dst_mid_top)
                            self.cmd_stop(key)
                            pass

                        elif hero_ct < dst_mid_bottom:
                            # В середине, - отступление к центру
                            pr(self.module_name(), 'Герой далеко от середины. Отступление к центру', key,
                               self.center_abs)
                            self.cmd_retreat(key, self.center_abs)
                            pass

                        else:
                            # Сзади - отступление к центру
                            pr(self.module_name(), 'Герой далеко от середины. Отступление к центру', key,
                               self.center_abs)
                            self.cmd_retreat(key, self.center_abs)
                            pass
                        pass

                    # 3 - Стоит - пропуск.
                    # Ожидаем сокращения дистанции до центра
                    elif hero_status == UnitStatus.stop or hero_status == UnitStatus.guard:
                        # Где герой:
                        if hero_ct < dst_mid_top:
                            # Впереди, - остановка.
                            # Ожидаем остальных
                            pass

                        elif hero_ct < dst_mid_bottom:
                            # В середине, - отступление к центру
                            pr(self.module_name(), 'Герой стоит далеко от середины. Отступление к центру', key,
                               self.center_abs)
                            self.cmd_retreat(key, self.center_abs)
                            pass

                        else:

                            # Сзади - отступление к центру
                            pr(self.module_name(), 'Герой стоит далеко сзади. Отступление к центру', key,
                               self.center_abs)
                            self.cmd_retreat(key, self.center_abs)
                            pass
                        pass

                elif not is_walking:
                    # Если герои обороняются или уже достигли цели
                    # Отправляем к центру по КД
                    pr(self.module_name(), 'Герой далеко от центра. Отступление к центру', key, self.center_abs)
                    self.cmd_retreat(key, self.center_abs)
                    pass

                if self.action:
                    # Действие назначено - выходим из цикла
                    break

        if self.action:
            # Если действие назначено, дальше не идём
            return

        # 2.2. Центрирование
        if self.need_center():
            self.action = action.Center(self.unit_group)
            return

        # 2.4. Движение к цели
        if is_walking and not block_next_ct and self.need_walk():
            next_click = self.ms.screen_to_abs(scr_crd[0], scr_crd[1])
            self.cmd_walk_group_to_ct(next_click)
            return

        # 2.5. Достижение цели
        if is_walking and self.task_target_done:
            self.status = 0
            return

        # 2.6 Центрирование каждую секунду с низким приоритетом
        if curr_time - self.last_center_time > 1:
            # Если прошло больше 1 секунды, центрируем с низким приоритетом
            self.action = action.Center(self.unit_group)
            self.action.priority = 4
            return

        # Конец цикла обновления

    def get_group_center(self, heroes):
        # Находим центр группы
        self.center = []
        self.center_abs = []

        left = 10000
        right = 0
        top = 10000
        bottom = 0

        # Вычисление среднего арифмитеческого радиуса
        total_x = 0
        total_y = 0
        total_count = 0

        for key, hero in heroes.items():
            y = hero[0]
            x = hero[1]
            if x > 0 and y > 0:

                total_count += 1
                total_x += x
                total_y += y

                # Находим крайние координаты героев
                if x < left:
                    left = x
                if x > right:
                    right = x
                if y < top:
                    top = y
                if y > bottom:
                    bottom = y

            # if is_walking:
            #    if UnitStatus.get(key) == UnitStatus.unknown:
            #        # Назначаем движение к КТ
            #        UnitStatus.set(key, UnitStatus.walk)

        if total_count == 0:
            # На карте нет юнитов
            return False

        # Находим средний арифмитический центр
        center_ar = [total_x // total_count + 16, total_y // total_count + 32]

        # Находим центр по рамке
        # width = right - left
        # height = bottom - top

        # Центр квадрата
        # center_kv = [left + width // 2 + 16, top + height // 2 + 32]

        # Максимальный радиус
        # max_d = width
        # if height > max_d:
        #    max_d = height

        # radius_int = max_d // 2 + 16
        # radius_ext = int((((width+32)**2 + (height+32)**2)**0.5)/2)
        # radius = (radius_int + radius_ext) // 2
        # radius_sector = radius/32

        # В качестве центра, берём средний арифмитический центр
        center = center_ar

        center_abs = self.ms.screen_to_abs(center[0], center[1])

        # Сохраняем переменную центра для других модулей
        curr_time = time.time()
        UnitCircleInfo.last_center = [curr_time, center_abs]

        # Рисуем рамку героев
        self.draw_kv(left, top, right, bottom)

        self.center = center
        self.center_abs = center_abs
        self.unit_abs_last = self.unit_abs
        self.unit_abs = self.center_abs

    def draw_kv(self, left, top, right, bottom):
        # Рисуем квадрат
        image = self.ms.ss.img_map
        start_point = (left // 2, (top + 16) // 2)
        end_point = ((right + 32) // 2, (bottom + 32 + 16) // 2)
        color = (200, 200, 200)
        thickness = 1
        # Using cv2.rectangle() method
        image = cv2.rectangle(image, start_point, end_point, color, thickness)
        self.ms.ss.img_map = image
        pass

    def draw_circle(self, pt1, pt2, color):
        # Рисуем круг
        thickness = 1
        image = self.ms.ss.img_map
        image = cv2.circle(image, pt1, pt2, color, thickness)
        self.ms.ss.img_map = image
        pass

    def draw_line(self, pt1, pt2, color):
        # Рисуем линию
        thickness = 1
        image = self.ms.ss.img_map
        image = cv2.line(image, pt1, pt2, color, thickness=thickness)
        self.ms.ss.img_map = image

    def need_center(self):
        curr_time = time.time()
        if curr_time - self.last_center_time < 1:
            # Если прошло меньше 1 секунды, возвращаемся.
            return False

        # Логика центрирования карты вокруг героя
        need_center = True
        # Наши действия:
        # 1. Проверяем положение целевой группы на карте. Группа на карте?
        if self.unit_abs:
            # 1. Да.
            # Проверяем группа в центре карты или с краю. Группа в центре?
            if self.center_map_validate():
                # 1. Да - центровка не требуется
                need_center = False
                self.need_center_count = 0
            else:
                # 2. Нет. Мы с краю мини-карты или нет?
                if self.map_far_away_border_validate():
                    # 1. Далеко от края. Центруем
                    self.need_center_count += 1
                    pr(self.module_name(), '# Мы не в центре карты и далеко от края')
                else:
                    # 2. С краю - создаём задание для перемещения.
                    need_center = False
                    self.need_center_count = 0
        else:
            # 2. Нет - центруем карту вогкруг группы.
            need_center = True
            pr(self.module_name(), '# Координаты героя не найдены, центрируем', self.need_center_count)
            self.need_center_count += 1
            # Возможно группа просто не найдена
            pass

        if self.need_center_count > self.need_center_max_count:
            self.need_center_count = 0
            need_center = False

        return need_center

    def need_walk(self):
        curr_time = time.time()

        # Расстояние до локальной цели
        local_target_dst = self.get_distance(self.local_target_crd, self.unit_crd)

        # Минимальный радиус до цели
        min_radius = self.walk_min_ct_radius + self.append_move_radius
        if local_target_dst > 10:
            # При большом расстоянии до цели, перемещаемся свободно
            min_radius = self.walk_min_move_radius + self.append_move_radius

        if local_target_dst < min_radius:
            main_target_dst = self.get_distance(self.main_target_crd, self.unit_crd)
            # Была ли достигнута глобальная цель:
            if main_target_dst < self.walk_main_ct_radius + self.append_move_radius:
                # Глобальная цель достигнута
                # Если цель достигнута. Ставим статус задачи - на удаление. Выходим из цикла.
                pr(self.module_name(), 'Мы прибыли к КТ')
                self.task_target_done = True

                self.local_target_crd = []
                WalkInfo.local_target_crd = []
                return False

            # Локальная цель достигнута, создаём новую.
            # Обнуление текущей КТ
            self.local_target_crd = []
            # Мы достигли мини-цели. Создаём новую цель со следующего цикла
            self.last_move_time = 0
            return False

        # Ожидаем прибытия к цели. Создаём новую цель, если герой стоит на месте.

        # Проверяем, когда была последняя команда движения
        if self.last_action and self.last_run_act_id and self.last_run_act_id == self.last_action.act_id:
            # Наша предыдущая команад была запущена
            if self.last_action.act_type == 1:
                # Если это была команда передвижения
                # Ждём 1 сек.
                self.last_move_time = curr_time
                # Сбрасываем счётчик остановки
                self.unit_stop = 0

        # Это первый вход в цикл?
        if not self.last_move_time:
            # Создаём новую цель
            return True

        # Проверка времени последней команды движения
        if self.last_move_time:
            if curr_time - self.last_move_time < 1:
                # Последняя команда была менее секунды назад, выходим.
                return False

        # Проверяем движение к цели. Прошло более секунды, после последней команды
        if self.max_unit_stop_count():
            pr(self.module_name(), 'Герой стоит более', self.max_unit_stop, 'циклов. Создаём новую задачу перемещения.')
            # Цель стоит более 10 циклов. Создаём новую задачу перемещения
            self.local_target_crd = []
            self.last_move_time = 0
            return False

        return False

    def find_move_hero_close_ct(self, heroes):
        ret = -1
        min_ct = 10000
        for hero in heroes:
            hero_ct = hero[1]
            key = hero[2]
            if UnitStatus.get(key) == UnitStatus.walk:
                if hero_ct < min_ct:
                    ret = key
                    min_ct = hero_ct

        return ret

    def cmd_retreat(self, key, crd):
        # Команда отступления к центру
        self.action = action.WalkRetreat(key, crd)

    def cmd_magic_shield(self, key):
        # Магический щит для мага. Применяется для магов на передовой
        hero_abs = self.ms.get_hero_abs(key)
        cast_abs = self.magic_cast_shift(hero_abs)
        self.action = action.Shield(key, cast_abs, True)

    def cmd_walk(self, key, crd):
        # Команда движения к цели
        self.action = action.WalkS(key, crd)

    def cmd_retreat_group(self, crd):
        # UNUSED
        # Команда движения к цели
        group = -1
        self.action = action.GroupWalk(group, crd)

    def cmd_walk_group_to_ct(self, crd):
        # Команда движения к цели
        group = -1
        self.action = action.GroupWalkS(group, crd)

    def cmd_all_guard(self):
        # Команда защиты позиции
        self.action = action.AllGuard()

    def cmd_guard(self, key):
        # Команда защиты позиции
        self.action = action.Guard(key)

    def cmd_stop(self, key):
        # Команда оставновки юнита
        self.action = action.Stop(key)

    def magic_cast_shift(self, crd_abs):
        # Сдвиг по X, для каста магии
        return [crd_abs[0] - 2, crd_abs[1]]


class FindPotions(TaskOne):
    #  Поиск эликсиров в инвентаре и сохранение их количества и координат.
    #  Ищем:
    #  - зелье восстановления маны;
    #  - мазь регенирации маны;
    #  - зелье восстановления ХП.
    #  Сохраняем данные в хранилище

    def __init__(self, hero=Hero):
        self.unit = hero

    def default_action(self):
        self.action = action.FindPotions(self.unit.group, self.ms)

        pass


class DeEquipPosoh(TaskOne):
    #  Деэкипировка посоха
    crd = []

    def __init__(self, hero=Hero, crd=[]):
        self.unit = hero
        self.crd = crd

    def default_action(self):
        self.action = action.DeEquipPosoh(self.unit.group, self.crd)
        pass


class ScanScreenSpeedTest(Task):
    # UNUSED
    # Сканирование карты.
    # Идея. Вся видимая область экрана, размечается на фрагменты по 32 пикселя.
    # Каждый фрагмент может быть проанализирован с помощью нейронной сети и мы узнаем, что это за поверхность.
    # Кроме того, скрипт может сохранять фрагменты в виде файлов.
    task_name = 'Сканирование экрана'

    weight = 0
    last_action_time = 0
    wait_time = 0.1
    model_name = cfg.neuro_map_model
    model_8_name = cfg.neuro_map_model_8
    map_size = 32

    def __init__(self):
        if self.map_size == 8:
            self.model = keras.models.load_model(self.model_8_name)
        else:
            self.model = keras.models.load_model(self.model_name)

    def update(self, ms, last_run_act_id, last_run_act_result):
        # Обновление задачи
        Task.update(self, ms, last_run_act_id, last_run_act_result)

        # Сканируем карту
        a = self.ms.matrix

        len_a = len(a)
        row = 0
        len_row = len(a[row])

        time1 = time.time()
        # Список к расопзнаванию
        # Имена изображений
        img_names = []
        to_indentify = {}
        while row < len_a:
            # print(i, a[i, 0])
            col = 0
            col_data = []
            while col < len_row:
                # Получаем данные картинки
                sector = np.array(a[row:row + 32, col:col + 32])
                # Получаем имя картинки
                name = str(sha1(sector).hexdigest())
                col_data.append(name)
                to_indentify[name] = sector
                col += 32

            img_names.append(col_data)
            row += 32

        found = {}
        # Распознаём данные
        if len(to_indentify):
            predict_value = []
            predict_key = []
            for name, sector in to_indentify.items():
                predict_key.append(name)

                if self.map_size == 8:
                    sector = cv2.resize(sector, dsize=(8, 8), interpolation=cv2.INTER_CUBIC)

                matrix = np.flip(sector[:, :, :3], 1)

                predict_value.append(matrix)

            np_predict = np.array(predict_value)
            ind_sectors = self.model.predict(np_predict, batch_size=32, verbose=0)
            if len(ind_sectors):
                i = 0
                for sector in ind_sectors:
                    if sector.shape[-1] > 1:
                        score = sector.argmax(axis=-1)
                    else:
                        score = (sector > 0.5).astype('int32')

                    result_int = int(score)
                    if result_int == 0:
                        # Добавляем два порядка нулевому элементу, чтобы сохранить его в массив
                        result_int = 100

                    name = predict_key[i]
                    found[name] = result_int
                    i += 1

        # Заполняем данные
        row = 0
        img_data = []

        for row in img_names:
            col_data = []
            for name in row:
                # Проверяем наличие картинки в памяти
                ind_sector = 10000
                if found.get(name):
                    ind_sector = found.get(name)

                col_data.append(ind_sector)
                pass

            img_data.append(col_data)
            pass

        time2 = time.time()
        total = time2 - time1
        sec_len = len(to_indentify)
        on_req = 0
        if sec_len:
            on_req = total / sec_len
        pr(self.module_name(), "sectors:" + str(sec_len), "on_req:" + str(on_req), "total:" + str(total))

        self.show_map(img_data)

    def show_map(self, img_data):
        # Тестовое отображение карты на экране
        map_data = {
            100: [(78, 64, 53), 'gray'],
            1: [(39, 102, 32), 'grass'],
            2: [(74, 223, 56), 'forest'],
            3: [(143, 143, 143), 'rock'],
            4: [(26, 115, 197), 'water'],
            5: [(75, 107, 71), 'graygrass'],
            6: [(54, 48, 44), 'forestblack'],
            7: [(118, 102, 89), 'prerock'],
            8: [(41, 63, 113), 'bereg'],
            # 9: [(231, 82, 207), 'unit'],
            # 10: [(72, 90, 233), 'structure'],
        }

        img = []

        for col in img_data:
            img_col = []
            for row in col:
                map_info = map_data.get(row)
                i = map_info[0]
                map_row = (i[2], i[1], i[0], 255)
                img_col.append(map_row)
            img.append(img_col)

        np_img = np.array(img)

        img = np_img.astype(np.uint8)
        # print(np_img.shape)
        # print(img)

        # print('Original Dimensions : ', img.shape)

        scale_percent = 1600  # percent of original size
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        resized = cv2.resize(img, dim, interpolation=cv2.INTER_NEAREST)

        # print('Resized Dimensions : ', resized.shape)

        cv2.imshow("Resized image", resized)

        # Press "q" to quit
        if cv2.waitKey(25) & 0xFF == ord("q"):
            cv2.destroyAllWindows()
            exit()
        # cv2.imshow("Allods map", np_img)

        pass

    def identify_img(self, a):
        # print(matrix)
        # BGRA to BGR
        time1 = time.time()
        matrix = np.flip(a[:, :, :3], 1)
        if self.map_size == 8:
            res = cv2.resize(matrix, dsize=(8, 8), interpolation=cv2.INTER_CUBIC)
            x_img_r = np.reshape(res, (1, 8, 8, 3))
        else:
            x_img_r = np.reshape(matrix, (1, 32, 32, 3))

        # result_int = int(self.model.predict_classes(x_img_r, batch_size=32, verbose=0))
        sector = self.model.predict(x_img_r, batch_size=32, verbose=0)
        if sector.shape[-1] > 1:
            score = sector.argmax(axis=-1)
        else:
            score = (sector > 0.5).astype('int32')

        result_int = int(score)
        time2 = time.time()
        # print(time2 - time1)
        return result_int

    def save_image(self, img, custom_folder=''):
        path = "../img/map/"
        name = str(sha1(img).hexdigest())
        path_name = path + custom_folder + name + '.jpg'
        if not os.path.exists(path_name):
            print(self.module_name(), 'img add', path_name)
            cv2.imwrite(path_name, img)
