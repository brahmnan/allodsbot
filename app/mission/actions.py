# Набор команд управления героем

from app.commands import Keys, Mouse, Color
from app.mission.store import InventStatus
from app.mission.store import allow_action

class Actions:
    # Нажатие клавиш
    keys = Keys()
    mouse = Mouse()
    color = Color()

    # Ожидание 0 ms
    sleep = 10
    # Горячие клавиши
    magic_heal_key = 'f5'
    magic_shield_key = 'f6'
    magic_teleport_key = 'f7'

    # Управление героем
    @allow_action
    def speed_plus(self, n=4):
        i = 0
        while i < n:
            self.keys.press('add')
            i += 1

    @allow_action
    def speed_minus(self, n=4):
        i = 0
        while i < n:
            self.keys.press('subtract')
            i += 1

    @allow_action
    def select_all(self):
        # Выбрать всех героев
        self.keys.press('e', sleep=self.sleep)
        pass

    @allow_action
    def make_group(self, n):
        # Создать группу
        self.keys.press_two('ctrl', str(n), sleep=self.sleep)
        pass

    @allow_action
    def ctrl_key(self, key):
        self.keys.press_two('ctrl', key, sleep=self.sleep)
        pass

    @allow_action
    def shift_key(self, key):
        self.keys.press_two('shift', key, sleep=self.sleep)
        pass

    @allow_action
    def select_group(self, n, sleep=0):
        # Выбрать группу
        self.keys.press(str(n), sleep)
        pass

    @allow_action
    def center_group(self, n, sleep=20):
        # Отцентрировать экран на группе
        self.keys.press_two('alt', str(n), sleep)
        pass

    @allow_action
    def a(self, sleep=0):
        # Атаковать
        self.keys.press('a', sleep)
        pass

    @allow_action
    def d(self):
        # Защищать
        self.keys.press('d', sleep=self.sleep)
        pass

    @allow_action
    def g(self):
        # Держать позицию
        self.keys.press('g', sleep=self.sleep)
        pass

    @allow_action
    def m(self):
        # Идит
        self.keys.press('m', sleep=self.sleep)
        pass

    @allow_action
    def p(self):
        # Сбор мешков
        self.keys.press('p', sleep=self.sleep)
        pass

    @allow_action
    def s(self):
        # Идти в боевой готовности
        self.keys.press('s', sleep=self.sleep)
        pass

    @allow_action
    def t(self):
        # Остановка
        self.keys.press('t', sleep=self.sleep)
        pass

    @allow_action
    def r(self):
        # Отступать
        self.keys.press('r', sleep=self.sleep)
        pass

    @allow_action
    def chat(self, type):
        # Написать в чат
        self.keys.press('enter', sleep=self.sleep)
        self.keys.type(type, sleep=self.sleep)
        self.keys.press('enter', sleep=self.sleep)
        pass

    @allow_action
    def auto_magic(self):
        # Включить автомагию
        self.keys.press_two('ctrl', 'a', sleep=self.sleep)
        pass

    @allow_action
    def magic_heal(self):
        # Лечение
        self.keys.press(self.magic_heal_key, sleep=self.sleep)
        pass

    @allow_action
    def use_shield(self, sleep=0):
        # Магический щит
        self.keys.press(self.magic_shield_key, sleep)
        pass

    @allow_action
    def teleport(self, sleep=0):
        # Телепорт
        self.keys.press(self.magic_teleport_key, sleep)
        pass

    @allow_action
    def shift_down(self):
        self.keys.down('shift')
        pass

    @allow_action
    def shift_up(self):
        self.keys.up('shift')
        pass

    @allow_action
    def heal_regen(self):
        # Регенерация здоровья
        self.keys.press(self.heal_regen_key, sleep=self.sleep)
        pass

    @allow_action
    def heal_up(self):
        # Восстановление здоровья
        self.keys.press(self.heal_up_key, sleep=self.sleep)
        pass

    @allow_action
    def mana_up(self):
        # Восстановление здоровья
        self.keys.press(self.mana_up_key, sleep=self.sleep)
        pass

    @allow_action
    def mana_regen(self):
        # Регенерация маны
        self.keys.press(self.mana_regen_key, sleep=self.sleep)
        pass

    @allow_action
    def fire_arrow(self):
        # Лечение
        self.keys.press(self.magic_fire_key, sleep=self.sleep)
        pass

    @allow_action
    def light(self):
        # Освещение
        self.keys.press(self.light_key, sleep=self.sleep)
        pass55
    pass


class Magic:
    # Выбор магии

    mouse = Mouse()
    keys = Keys()
    color = Color()

    # Ожидание 200 ms
    sleep = 0

    # Типы магии
    spell = {
        'fire': [222, 739],
        'fireball': [260, 739],
        'firewall': [300, 739],
        'fire_res': [376, 739],
        'light_res': [493, 739],
        'light': [645, 739],
        'heal': [222, 778],
        'tuman': [300, 778],
        'water_res': [374, 778],
        'teleport': [450, 778],
        'earth_res': [493, 778],
        'grad': [526, 778],
        'prok': [565, 778],
        'shield': [640, 778],
    }

    # Magic
    def is_auto(self, magic_name=''):
        # Эта магия уже включена на автомат
        name_crd = self.spell.get(magic_name)
        auto_crd = [name_crd[0] - 11, name_crd[1] + 12]
        if self.color.validate(auto_crd[0], auto_crd[1], 208, 208, 208):
            return True
        return False

    def select_magic(self, magic_name, sleep=10):
        # Выбрать магию
        if self.spell.get(magic_name):
            # Проверяем, открыто ли меню инветаря
            x = self.spell[magic_name][0]
            y = self.spell[magic_name][1]
            if InventStatus.invent:
                # Сдвигаем координаты вверх
                y -= 90
                pass
            self.mouse.left(x, y, sleep=sleep)
            return True
        return False

    def use_magic(self, magic_name, x, y):
        # Использовать магию на координаты
        if self.select_magic(magic_name):
            self.mouse.left(x, y)
            return True
        return False

    def bind_auto_magic(self, type):
        # Выбрать автомагию
        if self.spell.get(type):
            self.mouse.left(self.spell[type][0], self.spell[type][1], sleep=self.sleep)
            self.keys.press_two('ctrl', 'a', sleep=self.sleep)
        pass

    pass
