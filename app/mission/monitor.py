# Чтение данных с экрана монитора
import app.config as cfg
from app.mission.store import Mss
from app.commands import Color
import time
from hashlib import sha1
import numpy as np
import os.path
import cv2
import keras
from app.funcitons import pr
import tensorflow as tf
from app.neuronet.findtext import FindText
from app.mission.store import FoundInScreen, UserPotions, Cmd
import asyncio


class MiniMapScreen:
    # Экран карты
    map_top_left = [0, 0]
    map_btn_right = [0, 0]

    def get_screen(self):
        return [self.map_top_left, self.map_btn_right]

    def get_screen_hw(self):
        # Размер экрана, x, y
        return [self.map_btn_right[0] - self.map_top_left[0], self.map_btn_right[1] - self.map_top_left[1]]

    def reset(self):
        self.map_top_left = [0, 0]
        self.map_btn_right = [0, 0]
        pass

    def add(self, x, y):
        # Назначение крайних точек экрана мини-карты
        if self.map_top_left[0] == 0:
            self.map_top_left = [x, y]
            pass
        if self.map_btn_right[0] < x:
            self.map_btn_right[0] = x
            pass
        if self.map_btn_right[1] < y:
            self.map_btn_right[1] = y
            pass

        pass

    pass


class MiniMap:
    # Анализ карты
    # Чтение мини карты и нахождение на ней значимых объектов, героев, врагов (нейтралов), положения экрана

    mms = MiniMapScreen()
    # Размер героя на карте
    map_pixel_size = 1

    # Положение экрана. Верхняя левая и нижняя правая точки
    screen = [[0, 0], [0, 0]]
    heroes = []
    enemy = []
    matrix = None

    # Объекты поиск, r,g,b
    objects = [
        [248, 252, 248, 'map'],  # map
        [128, 228, 80, 'hero'],  # hero
        [80, 120, 224, 'hero'],
        [224, 84, 120, 'enemy'],  # enemies
        [224, 112, 80, 'enemy'],  # civilian and enemy
    ]

    found = {}

    def find(self):
        # Поиск экрана на мини-карте
        # Поиск объектов на карте. mon_map = {'x': 880, 'y': 49, 'w': 128}
        mon_map = {'top': cfg.mon_map['y'], 'left': cfg.mon_map['x'], 'width': cfg.mon_map['w'],
                   'height': cfg.mon_map['w']}

        # Получаем скришот
        self.matrix = Mss.sct.grabCache(mon_map)

        # Сброс настроек
        self.mms.reset()

        # Карта. Цвета карты, bgr
        map_c = (255, 255, 255)
        indices = np.where(np.all(self.matrix == map_c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        for crd in map_crd:
            self.mms.add(crd[1], crd[0])

        # Сохраняем данные о положении экрана
        self.screen = self.mms.get_screen()

    def k_size(self):
        # Коэффициент масштаба карты
        k_size_one = round((cfg.main_window['x'] / self.screen_hw()[0]) / 16, 0)
        k_size = int(k_size_one * 16)
        return k_size

    def screen_hw(self):
        # Размер экрана
        return self.mms.get_screen_hw()

    # TODO unused
    def clear_double(self, old_heroes):
        # Удаляем дубли героев, размером больше одной клетки
        new_heroes = []
        for old_hero in old_heroes:
            if not new_heroes:
                # Если объект пуст - создаём
                new_heroes.append(old_hero)
            else:
                for new_hero in new_heroes:
                    # Иначе проверяем наличие героев
                    if new_hero[0] == old_hero[0] or new_hero[0] == (old_hero[0] - 1) \
                            or new_hero[1] == old_hero[1] or new_hero[1] == (old_hero[1] - 1):
                        break
                    else:
                        new_heroes.append(old_hero)
        return new_heroes

    # TODO unused
    def clear_triple(self, heroes):
        # Удаляем дубли героев, размером больше одной клетки

        new_heroes = []
        exlude = []
        i = 0
        len = heroes.__len__()
        while i < len:
            hero = heroes[i]
            j = i + 1
            k = 1
            x_hero = hero[0]
            if j >= len:
                break
            x_next = heroes[j][0] - k
            to_exclude = [hero]
            while x_hero == x_next:
                to_exclude.append(heroes[j])
                j += 1
                if j >= len:
                    break
                k += 1
                x_next = heroes[j][0] - k

            if k > self.map_pixel_size:
                exlude.append(to_exclude)
            else:
                new_heroes.append(hero)
            i += k

        # print('inc', new_heroes)
        # print('ex', exlude)
        return new_heroes

    # TODO unused
    def find_old(self):
        # Поиск объектов на карте. mon_map = {'x': 880, 'y': 49, 'w': 128}
        mon_map = {'top': cfg.mon_map['y'], 'left': cfg.mon_map['x'], 'width': cfg.mon_map['w'],
                   'height': cfg.mon_map['w']}
        img = Mss.sct.grab(mon_map)
        matrix = np.array(img)

        x = 0
        y = 0
        self.mms.reset()
        self.heroes = []
        self.enemy = []
        self.found = {'hero': [], 'enemy': []}

        for row in matrix:
            for item in row:
                r = item[0]
                g = item[1]
                b = item[2]

                # Отсеиваем все тёмные пиксели
                if r < 70 or g < 70 or b < 70:
                    x += 1
                    continue

                # Поиск координат карты по светлым пикселям
                for pixel in self.objects:
                    if pixel[2] == r:
                        if pixel[1] == g:
                            if pixel[0] == b:
                                type = pixel[3]
                                if type == 'map':
                                    self.mms.add(x, y)
                                else:
                                    self.found[type].append([x, y])
                                break
                x += 1
            y += 1
            x = 0

        self.find_validate()

    # TODO unused
    def find_validate(self):
        # Сохраняем данные о положении экрана
        self.screen = self.mms.get_screen()

        if self.found.get('hero'):
            self.heroes = self.found['hero']
        else:
            # Если герои не найдены, добавляем метку героя по центру карты
            print('Герой не найден, добавляем метку героя по центру карты')
            hw = self.mms.get_screen_hw()
            hero = [self.screen[0][0] + round(hw[0] / 2, 0), self.screen[0][1] + round(hw[1] / 2, 0)]

            self.heroes = [hero]

        # Add enemy
        if self.found.get('enemy'):
            self.enemy = self.clear_triple(self.found['enemy'])

        # Удаляем лишние данные, для маленьких карт
        if self.map_pixel_size > 1:
            # print('Карта маленькая, удаляем лишние пиксели, mk:', mk)
            self.heroes = self.clear_double(self.heroes)
            if self.enemy:
                self.enemy = self.clear_double(self.enemy)

        pass

    # TODO unused
    def find_full(self):
        # Тестирование поиска индексов numpy
        # Поиск объектов на карте. mon_map = {'x': 880, 'y': 49, 'w': 128}
        mon_map = {'top': cfg.mon_map['y'], 'left': cfg.mon_map['x'], 'width': cfg.mon_map['w'],
                   'height': cfg.mon_map['w']}
        img = Mss.sct.grab(mon_map)
        matrix = np.array(img)

        # Сброс настроек
        self.mms.reset()
        self.heroes = []
        self.enemy = []
        self.found = {'hero': [], 'enemy': []}

        # Карта. Цвета карты, bgr
        map_c = (248, 252, 248)
        indices = np.where(np.all(matrix == map_c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        for crd in map_crd:
            self.mms.add(crd[1], crd[0])

        # Герой
        hero_c = [
            (80, 228, 128),
            (224, 120, 80),
        ]
        for c in hero_c:
            indices = np.where(np.all(matrix == c, axis=-1))
            hero_crd = np.transpose(indices)
            if hero_crd.any():
                for crd in hero_crd:
                    self.found['hero'].append([crd[1], crd[0]])
        # Враги
        enemy_c = [
            (120, 84, 224),  # enemies
            (80, 112, 224),  # civilian and enemy
        ]
        for c in enemy_c:
            indices = np.where(np.all(matrix == c, axis=-1))
            enemy_crd = np.transpose(indices)
            if enemy_crd.any():
                for crd in enemy_crd:
                    self.found['enemy'].append([crd[1], crd[0]])

        self.find_validate()


class Positions:
    # Отслеживание позиций героев на карте
    # Получаем данные с экрана и мини-карты
    # Находим для всех героев положение на мини-карте
    # Обходим всех найденных героев и привязываем им классы
    # При обновлении карты, сравниваем новые позиции, со старыми и фиксируем перемещение героев
    # Для новых герев совершаем обход и привязку классов
    # Выводим список найденых героев и их статус
    map_crd = []
    units = []
    enemy = []
    screen = []
    num = 0

    new_status = 0
    stay_status = 1
    move_status = 2
    old_status = 3

    exclude_status = 3
    no_name = '0-0'
    no_name_cont = 0

    def map_position(self, items):
        # Находим позиции героев на карте
        map_crd = []

        # Сдвиг в пикселях, относительно точки поиска
        shift = 16

        if items:
            for item in items:
                # Координаты со сдвигом на центр модельки
                xs = item[1] + shift
                ys = item[0] + shift
                # Координаты на мини экране
                x0 = round(xs / 32, 0)
                y0 = round(ys / 32, 0)
                # Координаты на мини-карте
                x = int(x0 + self.screen[0][0] + cfg.mm_x_shift)
                y = int(y0 + self.screen[0][1] + cfg.mm_y_shift)

                health = item[2]
                custom_type = item[3]
                map_crd.append([x, y, self.new_status, [x0, y0, xs, ys], health, custom_type])
                pass

        self.map_crd = map_crd
        # print('map_crd',self.map_crd)
        pass

    def get_mouse_position(self, unit):
        # Найти позицию мыши для юнита
        # Юнит [0, [32, 84, 0, [11.0, 1.0, 337, 29]], 1]
        crd = unit[1]
        # Находим координаты на мини экране
        x = crd[3][2]
        y = crd[3][3]
        # Координаты с учётом сдвига
        x1 = x + cfg.screen_shift['x']
        y1 = y + cfg.screen_shift['y']
        return [x1, y1]

    def get_mouse_position_old(self, unit):
        # TODO UNUSED
        # Найти позицию мыши для юнита
        # Юнит [0, [32, 84, 0, [11.0, 1.0, 337, 29]], 1]
        crd = unit[1]
        # Находим координаты на мини экране
        screen_crd = [crd[0] - self.screen[0][0] - cfg.mm_x_shift, crd[1] - self.screen[0][1] - cfg.mm_y_shift]
        old_screen = [crd[3][0], crd[3][1]]

        if screen_crd[0] == old_screen[0] and screen_crd[1] == old_screen[1]:
            # Экран в том-же положении, возвращаем сохранённые данные
            return [crd[3][2], crd[3][3]]
        else:
            # Положение экрана изменилось, ищем разницу
            x1 = (old_screen[0] - screen_crd[0]) * 32
            y1 = (old_screen[1] - screen_crd[1]) * 32
            return [crd[3][2] + x1, crd[3][3] + y1]
            pass

        pass

    def search_enemy(self, enemy_name):
        # Поиск координат врага
        names = Names()
        ret = []
        if self.units:
            for unit in self.units:
                # Имя юнита
                n = names.name(unit[3])
                if n[1] == enemy_name:
                    ret.append(unit)
        return ret

    def remove_units_by_name(self, enemy_name):
        # Убрать из списка юнитов всех врагов, с определённым именем
        names = Names()
        i = 0
        new_units = []
        while i < self.units.__len__():
            # Имя юнита
            n = names.name(self.units[i][3])
            if n[1] != enemy_name:
                new_units.append(self.units[i])

            i += 1
        self.units = new_units

    def enemy_spot(self):
        # Поиск врагов на карте
        name = Names()
        self.enemy = []
        enemy = 0
        if self.units:
            for unit in self.units:
                # Имя юнита
                n = name.name(unit[3])
                if n[0] == 1:
                    enemy += 1
                    self.enemy.append(unit)
        return enemy

    def remove_enemy(self):
        # Убрать из списка всех врагов
        name = Names()
        i = 0
        new_units = []
        while i < self.units.__len__():
            # Имя юнита
            n = name.name(self.units[i][3])
            # Если это не враг, добавляем в новый массив
            if n[0] != 1:
                new_units.append(self.units[i])

            i += 1
        self.units = new_units

    def units_info(self):
        name = Names()
        status = {self.stay_status: 0, self.move_status: 0, self.new_status: 0}
        type = dict()
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                status[unit[2]] += 1
                if not type.get(unit[3]):
                    type[unit[3]] = 1
                else:
                    type[unit[3]] += 1
                pass

        items = type.items()
        ret = ''
        # if self.units:
        # print(self.units)
        # print(items)
        for item in items:
            ret += name.name(item[0])[1] + ': ' + str(item[1]) + ', '
        if ret:
            print(ret, self.units)
        # print('Всего:', self.units.__len__(), ' Стоят:', status[self.stay_status], ' Идут:', status[self.move_status],
        #      ' Новые:', status[self.new_status])

    def get_new_units(self):
        return self.get_units(self.new_status)

    def get_stay_units(self):
        return self.get_units(self.stay_status)

    def get_units(self, status):
        # Возвращаем элементы определённого статуса
        ret = []
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                if unit[2] == status:
                    ret.append(unit)

        return ret

    def get_valid_units(self):
        # Возвращаем элементы статуса: идут и стоят
        ret = []
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                if unit[2] == self.stay_status or unit[2] == self.move_status:
                    ret.append(unit)

        return ret

    def get_valid_by_type(self, t=0):
        # Получить элемент по типу
        # [225, 643, 0, 1]
        # [20, [119, 109, 3, [19.0, 6.0, 595, 203], 0, 1], 1, '0-0']
        ret = []
        if self.units:
            for unit in self.units:
                if unit[2] == self.stay_status or unit[2] == self.move_status:
                    if unit[1][5] == t:
                        ret.append(unit)
        return ret

    def get_noname_units(self):
        # Получаем список юнитов, для которых не получилось получить имена
        ret = []
        if self.units:
            for unit in self.units:
                # Список юнитов и их статус
                if unit[3] == self.no_name:
                    ret.append(unit)

        return ret
        pass

    def update_units(self, items, screen):
        self.screen = screen
        # Обновление юнитов на карте
        self.map_position(items)
        # print('units', self.units)
        units = []
        if self.units:
            # Карта уже есть, сравниваем результаты
            new_units = []
            units = self.units
            map_crd = self.map_crd
            # print('map_crd', map_crd)
            # Поиск неподвижных координат
            i = 0
            map_crd_used = 0
            while i < units.__len__():
                # Назначаем устаревший статус для юнита
                units[i][2] = self.old_status
                j = 0
                while j < map_crd.__len__():
                    if map_crd[j][2] != self.old_status:
                        if units[i][1][0] == map_crd[j][0] and units[i][1][1] == map_crd[j][1] \
                                and units[i][1][5] == map_crd[j][5]:
                            # Юнит находится на месте
                            units[i][2] = self.stay_status
                            # Обновляем координаты мыши для юнита
                            units[i][1] = map_crd[j]
                            # Удаляем найденные объекты
                            map_crd_used += 1
                            map_crd[j][2] = self.old_status
                            break
                    j += 1
                i += 1
            # print('map_crd', map_crd)
            # Поиск движения
            if map_crd.__len__() > map_crd_used:
                # Если координаты ещё остались
                i = 0
                while i < units.__len__():
                    if units[i][2] == self.old_status:
                        # Проверяем толко устаревшие элементы
                        j = 0
                        while j < map_crd.__len__():
                            if map_crd[j][2] != self.old_status:
                                # Проверяем только не использованные координаты
                                if units[i][1][5] == map_crd[j][5]:
                                    # Проверяем только однотипные элементы
                                    x = units[i][1][0]
                                    y = units[i][1][1]
                                    xc = map_crd[j][0]
                                    yc = map_crd[j][1]
                                    if x == xc - 1 or x == xc or x == xc + 1:
                                        if y == yc - 1 or y == yc or y == yc + 1:
                                            # Передвижение на одну клетку
                                            units[i][2] = self.move_status
                                            # Обновляем координаты мыши для юнита
                                            units[i][1] = map_crd[j]
                                            # Удаляем найденные объекты
                                            map_crd_used += 1
                                            map_crd[j][2] = self.old_status
                                            pass
                            pass
                            j += 1
                    i += 1
                    pass

            # Удаление старых, добавление новых
            i = 0
            new_units = []
            use_crd = dict()
            while i < units.__len__():
                if units[i][2] != self.old_status:
                    crd_key = int(str(units[i][1][0]) + '0000' + str(units[i][1][1]))
                    if not use_crd.get(crd_key):
                        use_crd[crd_key] = 1
                        new_units.append(units[i])
                i += 1
                pass

            if map_crd.__len__() > map_crd_used:
                j = 0
                while j < map_crd.__len__():
                    if map_crd[j][2] != self.old_status:
                        crd_key = int(str(map_crd[j][0]) + '0000' + str(map_crd[j][1]))
                        if not use_crd.get(crd_key):
                            use_crd[crd_key] = 1
                            unit = [self.num, map_crd[j], self.new_status, self.no_name]
                            new_units.append(unit)
                        self.num += 1
                    j += 1
                    pass
            units = new_units

        else:
            # Карты ещё нет, первый запуск
            # print('first run')
            for item in self.map_crd:
                units.append([self.num, item, self.new_status, self.no_name])
                self.num += 1

        # print('units', units)
        self.units = units
        pass

    def set_name(self, num, name):
        # Добавляем имя к юниту
        # Пропускаем имена героев
        names = Names()
        n = names.name(name)
        if n[0] == 2:
            # Пропускаем имена героев
            return

        i = 0
        while i < self.units.__len__():
            if self.units[i][0] == num:
                self.units[i][3] = name
                break
            i += 1


class Names:
    # Получение и хранение имён юнитов

    # Список имён юнитов и их тип
    units = []

    names = {
        # Типы персонажей
        # '59-79' - цветовая метка
        # 2 - Тип персонажа:
        #   0 - мирный
        #   1 -враг
        #   2 -герой
        # 'Иглез' - Имя персонажа.
        # 1 - Вид юнита:
        #   0 - обычный
        #   1 -людоед / тролль
        #   2 - дракон
        #   3 - маг
        # Пропуск
        '59-79': [2, 'Иглез', 0],
        '73-91': [2, 'Дайна', 3],
        '115-162': [2, 'Гильдариус', 3],

        # Мирные
        '0-0': [0, 'Не известен', 0],
        '58-88': [0, 'Рекрут', 0],
        '74-115': [0, 'Скракан', 3],
        '77-119': [0, 'Староста', 0],
        '102-153': [0, 'Крестьянка', 0],
        '103-139': [0, 'Командир', 0],
        '111-153': [0, 'Крестьянин', 0],

        # Враги
        '30-49': [1, 'Дух', 0],
        '32-51': [1, 'Урд', 3],
        '41-70': [1, 'Волк', 0],
        '56-71': [1, 'Ящер', 0],
        '59-85': [1, 'Белка', 0],
        '64-85': [1, 'Пчёлы', 0],
        '62-88': [1, 'ДРУИД', 3],
        '60-86': [1, 'Зомби', 0],
        '70-104': [1, 'Дракон', 2],
        '74-101': [1, 'Рыцарь', 0],
        '82-117': [1, 'Людоед', 1],
        '83-99': [1, 'Атаман', 0],
        '93-125': [1, 'Черепаха', 0],
        '105-152': [1, 'Разбойник', 0],
        '107-144': [1, 'СКЕЛЕТ-МАГ', 3],
        '119-187': [1, 'Сороконожка', 0],
        '129-167': [1, 'Летучая мышь', 0],
        '140-176': [1, 'Арбалетчица', 0],
        '131-181': [1, 'Некромансер', 3],
    }

    names_v2 = {
        '0-0': [0, 'Не известен', 0],
        '89-121': [2, 'Фергард', 3],
        '74-122': [2, 'Скракан', 3],
        '64-91': [2, 'Бриан', 0],
        '222-288': [2, 'Наёмный воин с мечом', 0],
        '68-93': [2, 'Найра', 0],
        '97-131': [2, 'Рениеста', 3],
    }

    def name(self, code):
        # TODO deprecated
        # Получить имя по коду
        if self.names.get(code):
            return self.names.get(code)
        return self.names.get('0-0')

    def name_v2(self, code):
        # Получить имя по коду. Версия 80*20px
        if self.names_v2.get(code):
            return self.names_v2.get(code)
        name = self.names_v2.get('0-0')
        # name[1] = name[1]+' '+code
        return name

    def get_code_by_name(self, name):
        ret = '0-0'
        for code, item in self.names.items():
            if item[1] == name:
                ret = code
                break
        return ret

    def get_name(self):
        # Получаем имя с экрана
        img = Mss.sct.grab(cfg.name_window)
        matrix = np.array(img)
        return self.get_name_by_matrix(matrix)

    def get_hero_tavern_name(self):
        # Получаем имя с экрана в левом верхнем углу таверны
        img = Mss.sct.grab(cfg.hero_name_tavern)
        matrix = np.array(img)
        return self.get_name_by_matrix(matrix)

    def get_name_by_matrix(self, matrix):
        # Жёлтые буквы, b,g,r
        c = (74, 158, 189)
        # Чёрная тень
        c1 = (8, 8, 8)

        indices = np.where(np.all(matrix == c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        c1_len = map_crd.__len__()

        indices = np.where(np.all(matrix == c1, axis=-1))
        # Координаты: y, x
        map2_crd = np.transpose(indices)
        c2_len = map2_crd.__len__()

        ret = str(c1_len) + '-' + str(c2_len)

        return ret

    def get_hero_name(self):
        # TODO deprecated
        # Получаем имя с экрана
        img = Mss.sct.grab(cfg.hero_name_window)
        matrix = np.array(img)

        # Жёлтые буквы, b,g,r
        c = (74, 158, 189)

        indices = np.where(np.all(matrix == c, axis=-1))
        # Координаты: y, x
        map_crd = np.transpose(indices)
        c1_len = map_crd.__len__()
        return c1_len

    def get_current_unit_info(self):
        # Получаем имя юнита с экрана

        # Получаем скришот
        a = Mss.sct.grab(cfg.hero_current_info)

        name_sector = np.array(a[0:cfg.name_window_v2['height'], 20:20 + cfg.name_window_v2['width']])
        name_code = self.get_name_by_matrix(name_sector)
        # name = self.name_v2(name_code)

        w = 50
        h = 9
        # Жизнь

        y1 = 36
        y2 = 56
        y3 = 70
        x1 = 85
        x2 = 113

        # Матрицы текста
        hp = np.array(a[y1:y1 + h, x1:x1 + w])
        mana = np.array(a[y2:y2 + h, x1:x1 + w])
        armor = np.array(a[y3:y3 + h, x2:x2 + 22])
        ret = [name_code, hp, mana, armor]

        return ret


class ScanScreen:
    # Сканирование карты.
    # Идея. Вся видимая область экрана, размечается на фрагменты по 32 пикселя.
    # Каждый фрагмент может быть проанализирован с помощью нейронной сети и мы узнаем, что это за поверхность.
    # Кроме того, скрипт может сохранять фрагменты в виде файлов.

    model = ''
    findtext = FindText()
    ms = ''

    min_valid_val = 50
    curr_code = '0-0'
    curr_name = ''
    curr_health = [-1, -1]
    curr_mana = [-1, -1]
    curr_armor = -1
    img_names = []
    # Изображение карты
    img_map = []

    save_images = False
    save_images_path = "../img/mapdata/screen/"

    # Информация о юните
    show_current_unit_radius = True

    invalid_enemy_places = dict()
    valid_places = dict()
    names = Names()

    # Данные сети
    # (78, 64, 53), - цвет
    # 'gray', - папка
    # 1-3 - тип поверхности:
    #   1 - проходимые (Земля, мост, предскалье)
    #   2 - труднопроходимые (Лес)
    #   3 - не проходимые

    map_data = {
        100: [(78, 64, 53), 'gray', 1],
        1: [(39, 102, 32), 'grass', 1],
        2: [(12, 58, 7), 'forest', 2],
        3: [(215, 163, 80), 'savana', 1],
        4: [(143, 143, 143), 'rock', 3],
        5: [(26, 115, 197), 'water', 3],
        6: [(118, 102, 89), 'prerock', 1],
        7: [(41, 63, 113), 'bereg', 3],
        8: [(150, 150, 150), 'most', 1],
        9: [(21, 35, 16), 'forestblack', 2],
        10: [(47, 12, 64), 'struct', 3],
        11: [(60, 12, 12), 'mon', 3],
        # 12: [(70, 72, 22), 'graygrass', 1],
        9000: [(50, 50, 50), 'undefined', 3],
        10000: [(0, 0, 0), 'black', 3],

    }

    @classmethod
    def __init__(cls):
        cls.model = keras.models.load_model(cfg.neuro_map_model)
        cls.findtext.init_model()

    # async def update_and_show(self, ms):
    #     await asyncio.gather(
    #         # 3. Запускаем монитор
    #         self.update(ms),
    #         # 3. Отображаем монитор
    #         self.show_map(),
    #     )

    def update(self, ms):

        self.ms = ms

        # Сканируем карту
        a = self.ms.matrix

        len_a = len(a)
        row = 0
        len_row = len(a[row])
        time1 = time.time()
        # Поиск распознаных изображений

        # Список к расопзнаванию
        to_indentify = {}

        # Убрать кеш
        # FoundInScreen.found = {}

        # Имена изображений
        img_names = []
        while row < len_a:
            # print(i, a[i, 0])
            col = 0
            col_data = []
            while col < len_row:
                # Получаем данные картинки
                sector = np.array(a[row:row + 32, col:col + 32])

                # Получаем имя картинки
                name = str(sha1(sector.copy(order='C')).hexdigest())
                col_data.append(name)
                # Проверяем наличие картинки в памяти
                if not FoundInScreen.found.get(name):
                    # Проверяем черноту картинки
                    if self.isBlack(sector):
                        # Добавляем информацию по чёрному сектору
                        result_int = 10000
                        FoundInScreen.found[name] = result_int
                        pass
                    else:
                        # Если нет, добавляем картинку в очередь распознавания
                        to_indentify[name] = sector
                col += 32

            img_names.append(col_data)
            row += 32

        self.img_names = img_names

        # Распознаём данные
        if len(to_indentify):
            predict_value = []
            predict_key = []
            for name, sector in to_indentify.items():
                predict_key.append(name)

                matrix = np.flip(sector[:, :, :3], 1)
                # x_img_r = np.reshape(matrix, (1, 32, 32, 3))
                # print(matrix)
                # exit()
                predict_value.append(matrix)

            np_predict = np.array(predict_value)

            ind_sectors = self.identify_img(np_predict)
            if len(ind_sectors):
                i = 0
                for sector in ind_sectors:
                    sector = np.array(sector)
                    if sector.shape[-1] > 1:
                        score = sector.argmax(axis=-1)
                    else:
                        score = (sector > 0.5).astype('int32')

                    result_int = int(score)
                    if result_int == 0:
                        # Добавляем два порядка нулевому элементу, чтобы сохранить его в массив
                        result_int = 100

                    # Проверяем валидность распознваания
                    max_val = 0

                    for item in sector:
                        item_val = int(item * 100)
                        if item_val > max_val:
                            max_val = item_val

                    if max_val < self.min_valid_val:
                        result_int = 9000

                    name = predict_key[i]
                    FoundInScreen.found[name] = result_int
                    i += 1

        # Заполняем данные
        row = 0
        img_data = []

        for row in img_names:
            col_data = []
            for name in row:
                # Проверяем наличие картинки в памяти
                ind_sector = 10000
                if FoundInScreen.found.get(name):
                    ind_sector = FoundInScreen.found.get(name)
                    # save image

                    if self.save_images:
                        save_folder = self.map_data[ind_sector][1]
                        # print(name)
                        if len(to_indentify):
                            save_data = to_indentify.get(name)
                            if save_data is not None:
                                self.save_image(save_data, name, save_folder)

                col_data.append(ind_sector)
                pass

            img_data.append(col_data)
            pass

        time2 = time.time()
        total = time2 - time1
        sec_len = len(to_indentify)
        on_req = 0
        if sec_len:
            on_req = total / sec_len
        # pr('ScanScreen', "sectors:" + str(sec_len), "on_req:" + str(on_req), "total:" + str(total))

        self.render_map(img_data)

    def isBlack(self, sector):
        # Проверяем черноту картинки
        data = [[0, 0], [0, 31], [15, 15], [31, 0], [31, 31]]
        i = 0
        black = False
        for item in data:
            rgba = sector[item[0], item[1]]
            if rgba[0] == 0 and rgba[1] == 0 and rgba[2] == 0:
                i += 1
            if i >= 3:
                # Любые три точки должны быть чёрными
                black = True
                break

        return black

    def render_map(self, img_data):
        # Рендер карты распознавания
        img = []
        for col in img_data:
            img_col = []
            for row in col:
                map_info = self.map_data.get(row)
                i = map_info[0]
                map_row = (i[2], i[1], i[0])
                img_col.append(map_row)
            img.append(img_col)

        np_img = np.array(img)

        img = np_img.astype(np.uint8)
        # print(np_img.shape)
        # print(img)

        # print('Original Dimensions : ', img.shape)

        scale_percent = 1600  # percent of original size
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        resized = cv2.resize(img, dim, interpolation=cv2.INTER_NEAREST)

        # Создаём холст
        width = 820
        height = 500
        blank_image = np.zeros((height, width, 3), np.uint8)
        blank_image[0:resized.shape[0], 0:resized.shape[1]] = resized

        self.img_map = blank_image

    def show_map(self):
        cv2.imshow("Bot view", self.img_map)
        cv2.waitKey(1)

    def curr_unit_info(self):
        # Информация о выбраном юните (0.03 sec)
        # [name_code, hp, mana, armor]
        units_info = self.names.get_current_unit_info()

        # RGB text color
        info_text_color = (107, 154, 123)
        # time1 = time.time()
        self.curr_code = units_info[0]
        self.curr_name = self.names.name_v2(units_info[0])
        self.curr_health = -1
        self.curr_mana = -1
        self.curr_armor = -1

        predict_info = {}
        if self.curr_code not in ['0-0', '0-7']:
            predict_info = self.findtext.predict_user_all_info(units_info, info_text_color)

        self.curr_health = self.validate_predictions(predict_info.get(0, ''))
        self.curr_mana = self.validate_predictions(predict_info.get(1, ''))
        self.curr_armor = self.validate_prediction(predict_info.get(2, ''))

        # self.curr_health = self.validate_predictions(self.findtext.predict_user_info(units_info[1], info_text_color))
        # self.curr_mana = self.validate_predictions(self.findtext.predict_user_info(units_info[2], info_text_color))
        # self.curr_armor = self.validate_prediction(self.findtext.predict_user_info(units_info[3], info_text_color))

        # time2 = time.time()
        # print(time2 - time1)


    def custom_unit_health(self):
        # RGB text color
        units_info = self.names.get_current_unit_info()
        info_text_color = (107, 154, 123)
        curr_health = self.validate_predictions(self.findtext.predict_user_info(units_info[1], info_text_color))
        return curr_health

    def get_valid_places(self, unit_group=2, radius=8, enemy_radius=3):

        if enemy_radius > 0:
            self.units_radius(enemy_radius)

        image = self.img_map
        scale = 32
        crd_yx = self.ms.cmd.get(unit_group)
        self.valid_places = {}
        if crd_yx[0] > 0 or crd_yx[1] > 0:
            # Только для валидных координат
            # Учитываем смещение 32px по y и 15px по x. От координат верхней планки здоровья до центра юнита.
            crd_scale = [int((crd_yx[0] + 32) / scale), ((crd_yx[1] + 15) / scale)]
            a = self.img_names
            len_a = len(a)
            len_row = len(a[0])

            y = 0
            while y < len_a:
                x = 0
                while x < len_row:
                    x1 = crd_scale[1] - x
                    y1 = crd_scale[0] - y
                    summ_xy = (x1 ** 2 + y1 ** 2) ** 0.5
                    if summ_xy <= radius:
                        xs = x
                        if x1 < 0:
                            # Для равномерности круга вычитаем единичку из x, чтобы круг был равномерным
                            xs = x - 1
                        place_key = str(xs) + '-' + str(y)
                        if place_key in self.valid_places:
                            x += 1
                            continue

                        # Проверка врагов
                        if enemy_radius > 0 and place_key in self.invalid_enemy_places:
                            x += 1
                            continue

                        name = a[y][xs]
                        result_int = FoundInScreen.found.get(name)
                        result_data = self.map_data.get(result_int)
                        result_num = result_data[2]
                        if result_num != 3:

                            self.valid_places[place_key] = [xs, y, result_num, name, summ_xy]

                            if self.show_current_unit_radius:
                                # Draw
                                start_point = (xs * 16, y * 16)
                                end_point = (start_point[0] + 16, start_point[1] + 16)
                                color = (200, 200, 200)
                                if result_num == 2:
                                    color = (50, 50, 150)

                                thickness = 1
                                # Using cv2.rectangle() method
                                image = cv2.rectangle(image, start_point, end_point, color, thickness)

                        pass
                    x += 1
                    pass
                y += 1

        # print(len(self.img_names),len(self.img_names[0])) # 21, 26
        self.img_map = image

    def draw_unit_info(self, fps=0, curr_action_name=''):
        # Прорисовка информации о юните
        image = self.img_map
        # font
        font = cv2.FONT_HERSHEY_COMPLEX
        # org
        org = (5, 350)
        row = 15
        # fontScale
        fontScale = 0.35

        # Blue color in BGR
        color = (255, 255, 255)

        # Line thickness of 2 px
        thickness = 0

        fps_test = f'Действие: {curr_action_name}'
        image = cv2.putText(image, fps_test, org, font,
                            fontScale, color, thickness, cv2.LINE_AA)

        fps_test = f'МЗ: {int(round(fps,0))}'
        org = (org[0], org[1] + row)
        image = cv2.putText(image, fps_test, org, font,
                            fontScale, color, thickness, cv2.LINE_AA)

        name = self.curr_name[1]
        org = (org[0], org[1] + row)
        image = cv2.putText(image, name, org, font,
                            fontScale, color, thickness, cv2.LINE_AA)

        if self.curr_health[0] != -1:
            health = 'Ж: ' + str(self.curr_health[0]) + '/' + str(self.curr_health[1])
            org = (org[0], org[1] + row)
            image = cv2.putText(image, health, org, font,
                                fontScale, color, thickness, cv2.LINE_AA)

        if self.curr_mana[0] != -1:
            mana = 'М: ' + str(self.curr_mana[0]) + '/' + str(self.curr_mana[1])
            org = (org[0], org[1] + row)
            image = cv2.putText(image, mana, org, font,
                                fontScale, color, thickness, cv2.LINE_AA)

        if self.curr_armor != -1:
            armor = 'Б: ' + str(self.curr_armor)
            org = (org[0], org[1] + row)
            image = cv2.putText(image, armor, org, font,
                                fontScale, color, thickness, cv2.LINE_AA)



        self.img_map = image

    def render_mini_map(self, wp):
        # Рендер миникарты с контрольными точками
        image = self.img_map
        mini_map = np.asarray(self.ms.mm.matrix)

        # Увеличиваем миникарту
        scale_percent = 300  # percent of original size
        width = int(mini_map.shape[1] * scale_percent / 100)
        height = int(mini_map.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        mini_map_big = cv2.resize(mini_map, dim, interpolation=cv2.INTER_NEAREST)

        # Контрольные точки
        if wp:
            last_point = []
            for key, point_data in wp.items():
                # Рисуем линии КТ
                point = point_data[0]
                x_crd = int((point[0] - cfg.mon_map.get('x')) * scale_percent / 100)
                y_crd = int((point[1] - cfg.mon_map.get('y')) * scale_percent / 100)
                color = (233, 151, 120)
                if last_point:
                    mini_map_big = cv2.line(mini_map_big, [x_crd, y_crd], last_point, color, thickness=1)
                last_point = [x_crd, y_crd]

            for key, point_data in wp.items():
                point = point_data[0]
                # Рисуем КТ
                x_crd = int((point[0] - cfg.mon_map.get('x'))*scale_percent / 100)
                y_crd = int((point[1] - cfg.mon_map.get('y'))*scale_percent / 100)

                start_point = (int(x_crd-1), int(y_crd-1))
                end_point = (int(x_crd + 2), int(y_crd + 2))

                color = (233, 151, 120)
                if point_data[1]==1:
                    color = (0, 0, 255)

                # Line thickness of -1 px
                # Thickness of -1 will fill the entire shape
                thickness = -1

                # Using cv2.rectangle() method
                # Draw a rectangle of black color of thickness -1 px
                mini_map_big = cv2.rectangle(mini_map_big, start_point, end_point, color, thickness)

                # font
                font = cv2.FONT_HERSHEY_COMPLEX

                # org
                org = (start_point[0] + 5, start_point[1] + 5)

                # fontScale
                fontScale = 0.35

                # Blue color in BGR
                color = (255, 255, 255)

                # Line thickness of 2 px
                thickness = 0
                text = str(key)

                # Using cv2.putText() method
                mini_map_big = cv2.putText(mini_map_big, text, org, font,
                                    fontScale, color, thickness, cv2.LINE_AA)



            pass

        x_offset = 430
        image[0:mini_map_big.shape[0], x_offset:x_offset+mini_map_big.shape[1]] = mini_map_big

        self.img_map = image
        pass

    def validate_prediction(self, text):
        # Валидация значения, полученного от распознавания информации пользователя
        # Типы значения:
        # броня - 10
        try:
            ret = int(text)
        except:
            ret = -1
        return ret

    def validate_predictions(self, text):
        # Валидация значений
        # Типы значения:
        # зодровье - 100/100
        # Возвращает: [-1, -1], [100,100]
        ret = [-1, -1]
        if text and text.find('/'):
            text_arr = text.split('/')
            if len(text_arr) > 1:
                try:
                    ret = [int(text_arr[0]), int(text_arr[1])]
                except ValueError:
                    ret = [-1, -1]
                    pass

        return ret

    def units_radius(self, enemy_radius=3):
        # Исключать из валидных мест радиус вокруг вражеских юнитов
        self.invalid_enemy_places = dict()
        self.ms.update_units()

        # Проверяем врагов
        enemy = self.ms.p.get_valid_units()
        if enemy:
            for e in enemy:
                unit_crd = [e[1][3][2], e[1][3][3]]
                unit_type = e[1][5]
                unit_w = 16
                if unit_type == 1:
                    unit_w = 32

                self.set_invalid_enemy_radius(unit_crd, unit_w, enemy_radius)
        pass

    def draw_units(self):
        image = self.img_map
        # Рисуем найденные юниты


        # Проверяем врагов
        hero = self.ms.cmd
        enemy = self.ms.p.get_valid_units()

        if hero:
            # {1: [172, 161], 2: [103, 225], 3: [0, 0], 4: [0, 0], 5: [0, 0], 6: [107, 193], 7: [202, 33], 8: [170, 193], 9: [0, 0], 0: [0, 0]}
            for group, crd in hero.items():
                if crd[0] == 0 and crd[1] == 0:
                    continue

                unit_crd = crd
                unit_group = group
                health = crd[2]
                unit_type = crd[3]

                start_point = (int(unit_crd[1] / 2), int(unit_crd[0] / 2) + 8)
                end_point = (start_point[0] + 16, start_point[1] + 16)

                color = (233, 151, 120)
                if unit_type == 3:
                    color = (115, 228, 233)
                # Line thickness of -1 px
                # Thickness of -1 will fill the entire shape
                thickness = -1

                # Using cv2.rectangle() method
                # Draw a rectangle of black color of thickness -1 px
                image = cv2.rectangle(image, start_point, end_point, color, thickness)

                # font
                font = cv2.FONT_HERSHEY_SIMPLEX

                # org
                org = (start_point[0] + 2, start_point[1] + 14)

                # fontScale
                fontScale = 0.5

                # Blue color in BGR
                color = (0, 0, 0)

                # Line thickness of 2 px
                thickness = 0

                text = str(unit_group)

                # Using cv2.putText() method
                image = cv2.putText(image, text, org, font,
                                    fontScale, color, thickness, cv2.LINE_AA)

        if enemy:
            for e in enemy:
                unit_crd = [e[1][3][2], e[1][3][3]]
                unit_type = e[1][5]
                unit_w = 16
                if unit_type == 1:
                    unit_w = 32

                # print(unit_type, unit_crd)
                start_point = (int(unit_crd[0] / 2) - 8, int(unit_crd[1] / 2))
                end_point = (start_point[0] + unit_w, start_point[1] + unit_w)
                # color in BGR
                color = (100, 100, 255)
                if unit_type == 3:
                    color = (240, 130, 255)
                # Line thickness of -1 px
                # Thickness of -1 will fill the entire shape
                thickness = -1

                # Using cv2.rectangle() method
                # Draw a rectangle of black color of thickness -1 px
                image = cv2.rectangle(image, start_point, end_point, color, thickness)

                # font
                font = cv2.FONT_HERSHEY_SIMPLEX

                # org
                org = (start_point[0] + 2, start_point[1] + 14)

                # fontScale
                fontScale = 0.5

                # Blue color in BGR
                color = (0, 0, 0)

                # Line thickness of 2 px
                thickness = 0

                text = str(unit_type)

                # Using cv2.putText() method
                image = cv2.putText(image, text, org, font,
                                    fontScale, color, thickness, cv2.LINE_AA)

        self.img_map = image

    def set_invalid_enemy_radius(self, unit_crd, unit_w, enemy_radius):
        # Добавление  невалидных данных врагов в библиотеку
        matrix_x = int(unit_crd[0] / 32)
        matrix_y = int(unit_crd[1] / 32)
        matrix_point = str(matrix_x) + '-' + str(matrix_y)
        # print(matrix_point)
        self.invalid_enemy_places[matrix_point] = unit_w
        r_max = enemy_radius ** 2
        if enemy_radius > 0:
            ys = matrix_y - enemy_radius
            ye = matrix_y + enemy_radius
            xs = matrix_x - enemy_radius
            xe = matrix_x + enemy_radius
            # print(ys, ye)
            while ys < ye:
                while xs < xe:
                    # Относительные координаты цели
                    # print(xs, ys)
                    x1 = matrix_x - xs
                    y1 = matrix_y - ys
                    summ_xy = x1 ** 2 + y1 ** 2
                    # print(summ_xy, '<', r_max)
                    if summ_xy <= r_max:
                        matrix_point = str(xs) + '-' + str(ys)
                        self.invalid_enemy_places[matrix_point] = unit_w

                    xs += 1
                    pass
                xs = matrix_x - enemy_radius
                ys += 1
                pass

        # print(self.invalid_enemy_places)
        # exit()
        pass

    def identify_img(self, x_img_r):
        # time1 = time.time()
        # predictions = self.model.predict(x_img_r, batch_size=32, verbose=0)
        predictions = self.serve(x_img_r)
        # time2 = time.time()
        # print(time2 - time1)
        # Win 10. TS 2.4 GPU:
        # model 8    0.03559541702270508
        # old model  0.03559541702270508
        # new model  0.03760027885437012

        # win 8. TC 2.0 CPU
        # old model  0.008021831512451172
        # model 8    0.001003265380859375
        # win 8. TC 2.0 GPU
        # old model  0.003008604049682617
        # model8     0.0020051002502441406

        # win 8 TS 2.4 GPU
        # old model  0.028075456619262695
        # win 8 TS 2.4 CPU
        # old model  0.029048442840576172

        # win 10 TS 2.10 GPU
        # 0.02992081642150879
        return predictions

    @tf.function
    def serve(self, x):
        return self.model(x, training=False)

    def save_image(self, img, name, custom_folder=''):
        path = self.save_images_path
        self.check_and_create_dir(path)
        path_folder = path + custom_folder
        path_name = path_folder + '/' + name + '.jpg'
        if not os.path.exists(path_name):
            self.check_and_create_dir(path_folder)
            pr('ScanScreen', 'img add', path_name)
            cv2.imwrite(path_name, img)

    def check_and_create_dir(self, folder_path):
        # Проверка наличия папки. Если папки нет и её не удаётся создать, выходим
        if not os.path.exists(folder_path):
            try:
                os.mkdir(folder_path)
            except OSError:
                print("Создать директорию %s не удалось" % folder_path)
                exit()
            else:
                print("Успешно создана директория %s " % folder_path)

    def tranlit(self, text):
        symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",
                   u"abvgdeejzijklmnoprstufhzcss_y_euaABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUA")

        tr = {ord(a): ord(b) for a, b in zip(*symbols)}
        return text.translate(tr)


class UserInvent:

    # Инвентарь юнита в бою
    def find(self, items=dict()):
        if not len(items):
            return

        findtext = FindText()
        findtext.init_model()

        # Получаем матрицу экрана инвентаря

        # Получаем скришот
        a = Mss.sct.grabCache(cfg.invent_game_window)

        # Разбиваем её на объекты по 80 пикселей
        sectors = []
        col = 0

        # cv2.imshow("invent", a)
        while col < len(a[0]):
            # Получаем данные картинки: y, x
            sector = np.array(a[0:80, col:col + 80])

            sectors.append(sector)
            col += 80

        i = 1
        if len(sectors):
            for sector in sectors:
                for key, param in items.items():
                    # Поиск соответствующих параметров
                    # param = [3, 10, 82, 150, 255]
                    color = sector[39 + param[1]][39 + param[0]]
                    # print(i, color, param)
                    # 1 [115 105 231 255] [3, 10, 231, 105, 115]
                    if color[0] == param[4] and color[1] == param[3] and color[2] == param[2]:
                        # Найден компонент, рассчитываем количество
                        count = np.array(sector[71:79, 2:26])
                        predict = self.validate_prediction(findtext.predict_user_info(count, min_color=80))
                        pass
                        if predict > 0:
                            # Добавляем значение в память: номер, количество.
                            UserPotions.potions[key] = [i, predict]
                            pass
                    pass
                i += 1
                pass
        pr('UserInvent', UserPotions.potions)

    def validate_prediction(self, text):
        # Валидация значения, полученного от распознавания информации пользователя
        ret = -1
        try:
            ret = int(text)
        except ValueError:
            ret = -1
        return ret


class MainScreen:
    # Поиск данных на основном экране

    mm = MiniMap()
    p = Positions()
    ss = ScanScreen()
    ui = UserInvent()

    find_result = []
    # Тип юнита: 0 - обычный, 1 - людоед, 2 - Дракон, 3 - маг
    custom_type = []
    matrix = []

    window = {'top': cfg.screen_shift['y'], 'left': cfg.screen_shift['x'], 'width': cfg.view_window['x'],
              'height': cfg.view_window['ym']}

    # Координаты команд героев
    cmd = {}
    # Прошлые координаты героев
    last_cmd = {}
    # Позапрошлые координаты героев
    last_last_cmd = {}
    other = []
    # Валидация положения героев
    validate_groups = []
    # Счётчик поиска героев
    hero_not_found = dict()
    # Максимальное расстояние, для признания координат валидными.
    max_dst = 16

    # Поиск чёрных секторов
    find_black_sector = False

    found_in_black_sector = dict()

    units_updated = False

    def find(self):
        # Обновление мини-карты

        a = Mss.sct.grabCache(self.window, True)

        self.matrix = a

        # Поиск мини карты
        self.mm.find()

        # cv2.imshow('main', self.matrix)
        # cv2.waitKey(1)

        # Герои: g,b,r
        c = (107, 146, 206)
        # c = (106, 145, 205)

        # Обновление юнитов
        self.units_updated = False

        self.found_in_black_sector = dict()
        if self.find_black_sector:
            # center_window = {x: 218, y: 181, x1: 638, y1: 571}
            cw = cfg.center_window
            # Поиск чёрных секторов в центре экрана
            center_crop = a[cw['y']:cw['y1'], cw['x']:cw['x1']]

            c1 = (0, 4, 0)
            indices_1 = np.where(np.all(center_crop == c1, axis=-1))
            find_с1 = np.transpose(indices_1)

            c2 = (0, 0, 0)
            indices_2 = np.where(np.all(center_crop == c2, axis=-1))
            find_с2 = np.transpose(indices_2)

            # Добавляем найденные пиксели в словарь
            self.validate_found_in_black_sector(find_с1, a)
            self.validate_found_in_black_sector(find_с2, a)

            # Находим действительные пиксели
            if len(self.found_in_black_sector):
                # Меняем цвета матрицы
                # print('bs', self.found_in_black_sector)
                for key, matrix_crd in self.found_in_black_sector.items():

                    crop_key = str(matrix_crd[0]) + '-' + str(matrix_crd[1] + 28)

                    if self.found_in_black_sector.get(crop_key):
                        # Найден второй элемент
                        # Добавляем его в матрицу
                        # print('before', a[matrix_crd[0], matrix_crd[1]])
                        a[matrix_crd[0], matrix_crd[1]] = c
                        # print('after', a[matrix_crd[0], matrix_crd[1]])
                        a[matrix_crd[0], matrix_crd[1] + 28] = c
                        pass

        indices = np.where(np.all(a == c, axis=-1))
        find = np.transpose(indices)

        first = []
        res = []
        custom_type = []

        if find.any():
            for f in find:
                # Поиск левого верхнего угла

                # Герой, но не мана мага
                if f[1] + 28 < cfg.view_window['x'] and f[0] + 4 < cfg.view_window['ym'] and f[0] - 4 > 0 and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 28]):
                    # Герой или мана
                    if not self.compare_items(a[f[0], f[1]], a[f[0] - 4, f[1]]):
                        # print('hero')
                        if self.compare_items(a[f[0], f[1]], a[f[0] + 4, f[1]]):
                            custom_type.append([f[0], f[1], 3])

                        res.append([f[0], f[1]])

                elif f[1] + 56 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 56]):
                    # Людоед
                    # print('ludoed')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 1])

                elif f[1] + 92 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 92]):
                    # Дракон
                    # print('dragon')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 2])

                elif f[1] + 60 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 60]):
                    # Катапульта
                    # print('Катаульта')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 4])
                elif f[1] + 44 < cfg.view_window['x'] and \
                        self.compare_items(a[f[0], f[1]], a[f[0], f[1] + 44]):
                    # Всадник
                    # print('Всадник')
                    res.append([f[0], f[1]])
                    custom_type.append([f[0], f[1], 5])
                pass

        self.custom_type = custom_type
        self.find_result = res
        # Поиск команд героев
        self.find_heroes()

        # Удаление дубликатов
        unique_other = dict()
        new_other = []
        if self.other:
            for item in self.other:
                item_key = str(item[0]) + '-' + str(item[1])
                if unique_other.get(item_key):
                    continue
                unique_other[item_key] = 1
                new_other.append(item)
        self.other = new_other

        return res

    def validate_found_in_black_sector(self, find, a):
        if find.any():
            cw = cfg.center_window
            for found in find:
                x = found[1] + 1 + cw['x']
                y = found[0] - 2 + cw['y']

                x1 = found[1] + cw['x']
                y1 = found[0] + cw['y']
                # Если у чёрного цвета есть верхний пиксель R больше 80 (96)
                # и следующий писель не такой-же
                if a[y, x][2] > 80 \
                        and not self.compare_items(a[y1, x1 + 1], a[y1, x1]) \
                        and not self.compare_items(a[y1, x1 - 1], a[y1, x1]) \
                        and not self.compare_items(a[y1 + 1, x1], a[y1, x1]) \
                        and not self.compare_items(a[y1 - 1, x1], a[y1, x1]):
                    self.found_in_black_sector[str(y) + '-' + str(x)] = [y, x]

    def set_validate_groups(self, groups):
        # Валидация групп героев
        self.validate_groups = groups

    def find_loot(self):
        # Поиск мешков на карте
        c_arr = [(90, 166, 198), (99, 174, 214)]

        find = np.array([])
        for c in c_arr:
            indices = np.where(np.all(self.matrix == c, axis=-1))
            found = np.transpose(indices)
            if len(found):
                if not find.any():
                    find = found
                else:
                    find = np.append(find, found, axis=0)

        item_use = dict()
        find_valid = []

        if len(find):
            for found in find:
                f_key = str(found[0] // 32) + '-' + str(found[1] // 32)
                if item_use.get(f_key):
                    continue
                find_valid.append(found)
                item_use[f_key] = 1

        return find_valid

    def get_hero_abs(self, n=1):
        # Получить абсолютные координаты героя x,y
        hero = self.get_hero(n)
        if hero:
            # Координаты с учётом сдвига экрана
            x1 = hero[1] + cfg.screen_shift['x']
            y1 = hero[0] + cfg.screen_shift['y']
            return [x1, y1]

        return []

    def get_hero(self, n=1):
        # Получить координаты героя: y,x
        hero = self.cmd.get(n)
        if hero and hero[0] > 0:
            # Добавляем 16 px, чтобы метка указывала на центр героя
            hero = self.get_shift(hero)
            return hero

        return []

    def get_shift(self, hero):
        # Получить данные со сдвигом 16 px
        return [hero[0] + 16, hero[1] + 16]

    def screen_to_abs(self, x, y):
        x1 = x + cfg.screen_shift['x']
        y1 = y + cfg.screen_shift['y']
        return [x1, y1]

    def abs_to_screen(self, x, y):
        # Перевод абсолютных координат, в координаты экрана
        screen_crd = [x - cfg.screen_shift['x'], y - cfg.screen_shift['y']]
        return screen_crd

    def find_heroes(self):
        # Поиск героев трёх команд
        self.last_last_cmd = self.last_cmd
        self.last_cmd = self.cmd
        self.cmd = {1: [0, 0], 2: [0, 0], 3: [0, 0], 4: [0, 0], 5: [0, 0], 6: [0, 0], 7: [0, 0], 8: [0, 0], 9: [0, 0],
                    0: [0, 0]}

        self.other = []
        if self.find_result.__len__() > 0:
            # Координаты команд x,y: 8,8,8
            # 1 - 1,8
            # 2 - 2,13
            # 3 - 3,14
            a = self.matrix
            for f in self.find_result:

                # Здоровье
                health = self.health_status(f)
                f.append(health)
                # Тип
                custom_type = self.get_unit_type(f)
                f.append(custom_type)

                # Проверка на вход в ОДЗ
                if f[0] + 14 < cfg.view_window['ym'] and f[1] + 3 < cfg.view_window['x']:

                    if self.compare_items(a[f[0] + 7, f[1] + 1], [8, 8, 8], '1,4,6,7,8,9'):
                        # Команда 1,4,6,7,8
                        if not self.compare_items(a[f[0] + 7, f[1]], [8, 8, 8], 'n 1') \
                                and self.compare_items(a[f[0] + 11, f[1] + 2], [8, 8, 8], 'n 1'):
                            self.cmd[1] = f
                        else:
                            # 4,6,7,8
                            if not self.compare_items(a[f[0] + 9, f[1] + 1], [8, 8, 8], 'n 7') and self.compare_items(
                                    a[f[0] + 10, f[1] + 2], [8, 8, 8], 7):
                                # 7
                                self.cmd[7] = f
                            else:
                                # 4,6,8,9
                                if self.compare_items(a[f[0] + 7, f[1] + 2], [8, 8, 8], 6):
                                    self.cmd[6] = f
                                else:
                                    # 4,8,9
                                    if not self.compare_items(a[f[0] + 7, f[1]], [8, 8, 8], 4):
                                        self.cmd[4] = f
                                    else:
                                        if self.more_item(a[f[0] + 9, f[1] - 1], [200, 200, 200], 8) and self.less_item(
                                                a[f[0] + 8, f[1] + 2], [170, 170, 170], 8):
                                            self.cmd[8] = f
                                        else:
                                            self.cmd[9] = f
                    elif self.compare_items(a[f[0] + 13, f[1] + 2], [8, 8, 8], 2):
                        # Команда 2
                        self.cmd[2] = f
                    elif self.compare_items(a[f[0] + 14, f[1] + 3], [8, 8, 8], '3,4,5,9,0'):
                        # Команда 3,4,5
                        if self.compare_items(a[f[0] + 14, f[1] + 1], [8, 8, 8], 4):
                            self.cmd[4] = f
                        elif self.compare_items(a[f[0] + 13, f[1] + 1], [8, 8, 8], '5,9'):
                            if self.compare_items(a[f[0] + 11, f[1] + 2], [8, 8, 8], 5):
                                self.cmd[5] = f
                            else:
                                # 9 мага
                                self.cmd[9] = f
                        elif self.compare_items(a[f[0] + 13, f[1]], [8, 8, 8], 0):
                            self.cmd[0] = f
                        else:
                            self.cmd[3] = f
                    else:
                        self.other.append(f)


            # Валидация данных. Отслеживаем прошлые координаты героев и сравниваем с текущими.
            if self.validate_groups and self.last_cmd:
                invalid = []
                for hero in self.validate_groups:
                    # Проверяем все ли герои найдены.
                    crd = self.cmd.get(hero.group)
                    if not crd[0]:
                        invalid.append(hero.group)
                    else:
                        # Удаляем валидные
                        self.remove_not_found_hero(hero.group)

                # Если не все, проверяем новые элементы.
                if invalid:
                    new_other = []
                    # Добавляем невалидные элементы
                    for hero in invalid:
                        last_hero_crd = self.last_cmd.get(hero)
                        if self.other:
                            for item in self.other:
                                #   1. Если есть, проверяем координаты, и добавляем цель в пределах 16 рх.
                                if last_hero_crd[0] - self.max_dst < item[0] < last_hero_crd[0] + self.max_dst and \
                                        last_hero_crd[1] - self.max_dst < item[1] < last_hero_crd[1] + self.max_dst:
                                    # Добавляем координаты героя из неизвестных
                                    self.cmd[hero] = item
                                    self.append_not_found_hero(hero)
                                    # print('Команда', hero, 'не найдена, берём из непознанных координат')
                                else:
                                    # Добавляем неизвестный элемент
                                    new_other.append(item)
                                    self.append_not_found_hero(hero)
                                pass
                        else:
                            # 2. Если нет, берём координаты из старых. Добавляем счётчик не найденной группы.
                            # Он нужен, чтобы определить смерть наёмника. Если счётчик больше 10.
                            # По окончании цикла проверим, жив ли наёмник. Нахождение наёмника сбрасывает счётчик.
                            # TODO счётчик не найденного героя
                            self.cmd[hero] = self.last_cmd.get(hero)
                            self.append_not_found_hero(hero)
                            # print('Команда', hero, 'не найдена, берём из прошлых координат')
                            # print(self.hero_not_found)
                            pass
                    self.other = new_other

                else:
                    # Валидация пройдена успешно
                    self.clear_not_found_hero()
        pass

    def compare_items(self, item1, item2, show=0):
        # Сравнение цветов двух элементов
        if show:
            # print('compare', show, item1, item2)
            pass
        if item1[0] == item2[0] and item1[1] == item2[1] and item1[2] == item2[2]:
            return True
        return False

    def more_item(self, item1, item2, show=0):
        # Сравнение цветов двух элементов
        if show:
            # print('more', show, item1, item2)
            pass
        if item1[0] > item2[0] and item1[1] > item2[1] and item1[2] > item2[2]:
            return True
        return False

    def less_item(self, item1, item2, show=0):
        # Сравнение цветов двух элементов
        if show:
            # print('less', show, item1, item2)
            pass
        if item1[0] < item2[0] and item1[1] < item2[1] and item1[2] < item2[2]:
            return True
        return False

    def append_not_found_hero(self, hero):
        if hero not in self.hero_not_found:
            self.hero_not_found[hero] = 1
        else:
            self.hero_not_found[hero] += 1
        pass

    def remove_not_found_hero(self, hero):
        if hero in self.hero_not_found:
            del self.hero_not_found[hero]

    def clear_not_found_hero(self):
        self.hero_not_found = dict()
        pass

    def get_by_type(self, t=0):
        # Получить элемент по типу
        # [[225, 643, 0, 1]]
        ret = []
        if self.other:
            for item in self.other:
                if item[3] == t:
                    ret.append(item)
        return ret

    def get_unit_type(self, f):
        # Тип юнита
        custom_type = 0

        for u in self.custom_type:
            if u[0] == f[0] and u[1] == f[1]:
                custom_type = u[2]
                break
        return custom_type

    def health_status_v2(self, unit_group):
        # Количество здоровья персонажа
        # ret:
        # 0 - small hp
        # 5 - half hp
        # 7 - not full
        # 10 - full hp

        f = self.cmd.get(unit_group)
        if f[0] == 0:
            # Если координаты не найдены, пропускаем ход
            return -1

        a = self.matrix

        # b,g,r
        full_hp = (0, 252, 0)
        half_hp = (0, 252, 248)
        small_hp = (0, 0, 200)

        ret = -1

        if f[0] == 0:
            return ret

        # Проверка ОДЗ
        if f[1] + 4 < cfg.view_window['x']:
            hp = a[f[0], f[1] + 4]

            r = hp[0]
            g = hp[1]
            b = hp[2]

            hpl = a[f[0], f[1] + 26]
            rl = hpl[0]
            gl = hpl[1]
            bl = hpl[2]

            g_sm = gl / 5
            # Полное ХП
            if rl < gl - g_sm and gl > 120 and bl < gl - g_sm:
                # full hp
                ret = 10

            # Проверка ХП
            elif g > 120 and b > 120:
                # half hp
                ret = 5

            elif g < 70 and b > 120:
                # small hp
                ret = 0

            else:
                # Не полное хп
                ret = 7

        return ret

    def health_status(self, f):
        # Количество здоровья персонажа
        # ret:
        # 0 - full hp
        # 1 - half hp
        # 2 - small hp
        a = self.matrix

        # b,g,r
        full_hp = (0, 252, 0)
        half_hp = (0, 252, 248)
        small_hp = (0, 0, 200)

        ret = 0

        if f[0] == 0:
            return ret

        # Проверка ОДЗ
        if f[1] + 4 < cfg.view_window['x']:
            hp = a[f[0], f[1] + 4]

            # Проверка ХП
            if hp[1] > 120 and hp[2] > 120:
                # half hp
                ret = 1
            elif hp[1] < 70 and hp[2] > 120:
                # small hp
                ret = 2
            else:
                # full hp
                pass
            pass

        return ret

    def hero_health_not_full(self, n):
        f = self.cmd.get(n)
        ret = False
        if f[0]:
            a = self.matrix
            hp = a[f[0], f[1] + 26]
            if hp[0] < 100 and hp[1] > 120 and hp[2] < 100:
                # Полное ХП
                pass
            else:
                # Потраченое ХП
                ret = True
                # print(hp)
                pass

        return ret

    def dark_detect(self, n):
        # Детектор затемнения
        f = self.cmd.get(n)
        ret = False
        if f[0]:
            a = self.matrix
            # Цвета пикселей
            pix1 = a[f[0] - 1, f[1] - 1]
            pix2 = a[f[0] - 1, f[1] + 2]
            pix3 = a[f[0] + 2, f[1] + 2]
            # print(pix1, pix2, pix3)
            # Средний цвет
            c1 = (int(pix1[0]) + int(pix1[1]) + int(pix1[2])) / 3
            c2 = (int(pix2[0]) + int(pix2[1]) + int(pix2[2])) / 3
            c3 = (int(pix3[0]) + int(pix3[1]) + int(pix3[2])) / 3
            # print(c1, c2, c3)
            if c1 < 30 and c2 < 30 and c3 < 30:
                ret = True
        return ret

    def get_unit_abs_crd(self, unit):
        # Получение абсолютных координат на экране юнита
        # [11, [95, 64, 3, [10.0, 15.0, 305, 491]], 1, '74-101']
        # Координаты экрана
        x = unit[1][3][2]
        y = unit[1][3][3]
        abs_crd = self.screen_to_abs(x, y)
        return abs_crd

    def hero_is_stay(self, n):
        # Герой стоит
        f1 = self.cmd.get(n)
        f2 = self.last_cmd.get(n)
        f3 = self.last_last_cmd.get(n)
        # print(f1,f2,f3)
        if f1 and f2 and f3:
            # Все координаты получены
            if f1[0] and f2[0] and f3[0]:
                # Не равны нулю
                if f1[0] == f2[0] == f3[0] and f1[1] == f2[1] == f3[1]:
                    # Все координаты равны
                    # print(self.hero_not_found.get(n))
                    if not self.hero_not_found.get(n):
                        # Ошибок нет
                        # print('Герой', n, 'стоит')
                        return True

        # print('Герой', n, 'движется', f1, f2, f3)
        return False

    def get_distance(self, path, hero):
        # Дистанция до мини цели
        x_path_sm = path[0] - hero[0]
        y_path_sm = path[1] - hero[1]
        # Дистанция до мини цели
        cpm_dist = round((abs(x_path_sm) ** 2 + abs(y_path_sm) ** 2) ** 0.5, 1)
        return cpm_dist

    def find_color_crd(self, color=[]):
        a = self.matrix
        # Герои: g,b,r
        c = (color[2], color[1], color[0], 255)
        indices = np.where(np.all(a == c, axis=-1))
        find = np.transpose(indices)
        return find

    def shield_info(self, unit_group):
        a = self.matrix
        f = self.cmd.get(unit_group)
        # Герой не найден
        ret = 0
        min_color = 127
        if f[0] != 0:
            # Поиск пикселя в матрице
            # Координаты команд x,y: 8,8,8
            # 15, -9
            # Проверка ОДЗ
            if f[0] - 9 < cfg.view_window['ym'] and f[1] + 15 < cfg.view_window['x']:
                # print(a[f[0] - 9, f[1] + 15])
                if a[f[0] - 9, f[1] + 15][0] > min_color and a[f[0] - 9, f[1] + 15][1] > min_color \
                        and a[f[0] - 9, f[1] + 15][2] > min_color:
                    ret = 1

                else:
                    # Щит отключён
                    ret = 2

        return ret

    def update_units(self):
        # Обновление юнитов
        if not self.units_updated:
            self.p.update_units(self.other, self.mm.screen)
            self.units_updated = True
        pass
