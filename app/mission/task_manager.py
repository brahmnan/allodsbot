# Система управления задачами
import time
from operator import attrgetter
from app.mission.monitor import MainScreen
from app.mission.store import ActionsDone, UnitStatus
from app.ineterface import MainMenu, pr
from app.mission.store import TM
from app.neurotext.neurotext import NeuroText

import threading


class TaskManager:
    # Мэнеджер задач
    # Цикл запуска и отслеживания выполнения задач.
    # Идея: создаётся экземляр класса, в него добавляются задачи. Затем он запускает задачи в соответствии с их
    # приоритетом, давая каждой задачи выполнить одно действие.
    # Задачи крутятся в цикле до тех пор, пока:
    #   1. Не останется задач.
    #   2. Не возникнет фатальная ошибка, вынуждающая загрузить игру.
    #   3. Задачи не будут прерваны, в связи с победой.
    # В начале каждого цикла, менеджер опрашивает профильные модули и они формируют задачи.
    # Модули рассматиривают статус выполнения текущих задач и обновляют их, и добавляют новые.
    # Менеджер сортирует задачи по степени важности, удаляет выполненные и последовательно выполняет остальные.
    # Задачи могут добаляться, как заранее запланированные - управляющие задачи - задаются в стратегии.
    # Так и по ходу выполонения - текущие. Задаются модулями.
    # Задачи скармливаются профильным модулям, которые, на время выполнения (шага) задачи, перехватывают управление.
    # Выполнение задачи, должно быть не более 100мс.
    #
    # Реализация: задачи добавляются из вне.
    # Каждая задача содержит в себе функционал, обрабатывающий данные с экрана.
    # В результате обработки данных, задача создаёт действие с определённым приоритетом.
    # Менеджер задач выполняет самое приоритетное действие и цикл повторяется.
    # По факту выполнения действий, задача меняет свой статус.
    # Если задача выполнена, она меняет свой статус и удаляется.

    # Массив текущих задач
    module_name = 'МЗ'
    tasks = dict()
    # Для каждой задачи назначается уникальный номер
    task_id = 1
    # Массив действий
    actions = []
    act_id = 1
    # Последнее запущенное задание
    last_run_action_id = 0
    # Результат выполнения последнего задания
    last_run_action_result = 0
    # Ожидание после каждого цикла
    sleep = 10

    # Информация об игре
    ms = None
    # Управление игрой
    menu = None

    # Победа при появлении окна
    win_final = 1
    # Победа при появлении любого диалога
    any_dialog_win = 0
    # Победа в миссии
    win = 0
    # Выход из цикла
    win_exit = 0

    run_action = ''

    # Контрольные точки
    wp = {}

    def __init__(self, validate=None, bs=False, wp=None):
        self.wp = wp if wp else {}
        self.ms = MainScreen()
        self.menu = MainMenu()
        self.tasks = dict()
        self.actions = []
        self.find_in_black_sector(bs)

        # Поражение в миссии
        TM.defeat = 0

        if validate:
            self.set_validate_group(validate)

        UnitStatus.reset()

    def add_task(self, task, parent_id=0, block=[]):
        if TM.defeat or self.win_exit:
            return
        # Добавление задачи в цикл
        # Назначение id задачи
        task_id = self.task_id
        task.task_id = task_id
        self.task_id += 1
        if parent_id > 0:
            task.task_parent = parent_id
            task.status = 2
            pr(self.module_name, 'Добавлена дочерняя задача:', str(parent_id) + '.', task_id, task.task_name)
        else:
            pr(self.module_name, 'Добавлена задача:', str(task_id) + '.', task.task_name)

        if block:
            # Задача, которая будет блокировать все, ссылающиеся на неё.
            if isinstance(block, int):
                block_arr = [block]
            else:
                block_arr = block
            task.block_task_ids = block_arr

        # Добавление задачи в цикл
        self.tasks[task_id] = task
        return task_id

    def run_task(self, task='', parent_id=0, block=[]):
        task_id = 0
        if task:
            task_id = self.add_task(task, parent_id, block)
        self.run()
        return task_id

    def remove_task(self, task_id):
        if TM.defeat or self.win_exit:
            return
        # Удаление задачи
        if self.tasks.get(task_id):
            pr(self.module_name, 'Удаление задачи:', task_id)
            self.tasks[task_id].status = 0

    def clear_tasks(self):
        self.tasks = dict()

    def add_action(self, action):
        if TM.defeat or self.win_exit:
            return
        # Добавление действия
        act_id = self.act_id
        action.act_id = act_id
        self.act_id += 1
        self.actions.append(action)
        pr(self.module_name, 'Добавлено действие:', str(act_id) + '.', action.act_name)
        return act_id

    def run(self):
        # Запуск цикла выполнения задач.
        while True:
            t1 = time.time()
            if TM.defeat or self.win_exit:
                break

            # time1 = time.time()
            if not self.tasks:
                # Если нет задач, значит мы победили.
                break

            main_tasks = 0
            for key, task in self.tasks.items():
                if task.weight == 1:
                    main_tasks += 1

            if not main_tasks:
                # Нет основных задач
                break

            # 1. Обновляем информацию об игре
            self.ms.find()

            # 2. Проверяем появление сообщений.
            dialog = self.mod_dialog()
            if dialog:
                if dialog == 1:
                    # Победа
                    self.win = 1
                    pr(self.module_name, "Мы победили")
                    if self.win_final == 1:
                        self.win_exit = 1
                        self.clear_tasks()
                        self.menu.win_menu_click()
                    else:
                        self.menu.win_cancel()
                    pass
                elif dialog == 2:
                    # Стандартный диалог
                    if self.any_dialog_win:
                        self.any_dialog_win = 2

                    NeuroText.read()
                    # self.menu.propusk('enter', 335, 335)
                    continue
                elif dialog == 3 or dialog == 4:
                    # Поражение
                    self.menu.to_main_menu()
                    TM.defeat = 1
                    break
                elif dialog == 5:
                    # Поражение. Главное меню
                    TM.defeat = 1
                    break

            if self.any_dialog_win == 2:
                self.win = 1
                self.win_exit = 1
                self.clear_tasks()

            # 3. Запускаем монитор
            self.ms.ss.update(self.ms)
            # time1 = time.time()

            # Получение информации о выбраном юните (0.03 sec)
            self.ms.ss.curr_unit_info()

            # Враг обнаружен?
            self.ms.update_units()

            # time2 = time.time()
            # print(time2 - time1)
            if self.run_action == '':
                # Асинхронных задач не запущено

                # Обнуляем список действий, которые нужно выполнить
                self.actions = []

                # 3. Опрашиваем профильные модули, создаём список задач
                new_tasks = dict()
                task_to_remove = []
                block_tasks = dict()

                for task_id, task in self.tasks.items():
                    # Обновляем задачу
                    if task.status == 1:
                        # Рассматриваем только активные задачи
                        task.update(self.ms, self.last_run_action_id, self.last_run_action_result)
                        # Получаем действие
                        action = task.get_action()
                        if action:
                            action.block_task_ids = task.block_task_ids
                            action.task_id = task_id
                            act_id = self.add_action(action)
                            # Сохраняем в задачу id последнего действия
                            task.act_id = act_id

                        new_tasks[task_id] = task
                        # Проверяем блокировку
                        if task.block:
                            block_tasks[task_id] = task.block
                    elif task.status == 2:
                        # Ожидающие задачи не обновляем
                        new_tasks[task_id] = task
                    elif task.status == 0:
                        # Отработанные задачи, удаляем
                        pr(self.module_name, 'Задача:', str(task_id) + '.', task.task_name, 'выполнена')
                        task_to_remove.append(task.task_id)

                # Обновляем список задач
                self.tasks = new_tasks
                # Меняем статус дочерних задач, если их родители были удалены
                if task_to_remove:
                    for parent in task_to_remove:
                        for task_id, task in self.tasks.items():
                            if task.status == 2 and task.task_parent == parent:
                                # Активируем задачу
                                task.status = 1
                                pr(self.module_name, 'Дочерняя задача активирована:', str(task.task_id) + '.',
                                   task.task_name)
                                self.tasks[task_id] = task

                # time2 = time.time()
                # print(time2 - time1)
                if self.actions and block_tasks:
                    # Проверяем заблокированные задачи
                    new_actions = []
                    for act in self.actions:
                        if act.blocked:
                            block_time = 0
                            for block_task_id in act.block_task_ids:
                                if block_tasks.get(block_task_id):
                                    act_time_blocked = block_tasks.get(block_task_id)
                                    if block_time < act_time_blocked:
                                        # Выбираем самую длительную блокировку
                                        block_time = act_time_blocked

                            curr_time = time.time()
                            if curr_time < block_time:
                                blocked_task_id = act.task_id
                                blocked_task = self.tasks[blocked_task_id]
                                blocked_task.task_blocked(True)
                                self.tasks[blocked_task_id] = blocked_task
                                # pr(self.module_name, 'Блокировка задачи', block_task_id, blocked_task.task_name,
                                # 'и действия', act.act_id, act.act_name)
                                continue
                        new_actions.append(act)
                    self.actions = new_actions

                if self.actions:
                    # Если есть действия к выполнению
                    # 4. Сортируем действия по приоритету
                    # Если есть задачи
                    act_sort = sorted(self.actions, key=attrgetter('priority'))
                    # print(act_sort)
                    # 5. Выполняем самое важное действие
                    action = act_sort[0]
                    self.last_run_action_id = action.act_id
                    pr(self.module_name,
                       'Запуск действия: Задача-' + str(action.task_id) + ' - ' + str(action.act_id) + '.',
                       action.act_name, action.priority)

                    i = 0
                    not_run_name = []
                    if len(act_sort) > 1:
                        for unrun in act_sort:
                            if i > 0:
                                not_run_name.append([unrun.act_name, action.priority])
                            i += 1

                    if len(not_run_name):
                        pr(self.module_name, 'Отброшены действия:', not_run_name)

                    # Асинхронный запуск задачи
                    self.run_action = action

                    # self.do_action(action)
                    t = threading.Thread(target=self.do_action, args=(action,))
                    pr(self.module_name, 'Действие добавлено:', action.act_name, action.act_id)
                    t.start()
                else:
                    self.last_run_action_id = 0

            # Пауза между проверками
            time.sleep(self.sleep / 1000)

            t2 = time.time()
            total = t2 - t1
            fps = 0
            if total > 0:
                fps = 1 / total

            # Рисуем юниты героев и врагов
            self.ms.ss.draw_units()
            # Отображаем информацию о текущем юните
            curr_action_name = ''
            if self.run_action != '':
                curr_action_name = self.run_action.act_name
            self.ms.ss.draw_unit_info(fps, curr_action_name)

            # Рендерим миникарту
            self.ms.ss.render_mini_map(self.wp)

            # 3. Отображаем монитор
            self.ms.ss.show_map()

            # pr(self.module_name, total, fps)
        pass

    def mod_dialog(self):
        # Мониторинг начавшегося диалога
        ret = 0
        # Типы диалогов
        #   1. Победа:
        #   2. Сообщение:
        #   3  Поражение:
        #   4. Поражение: Иглез погиб
        #   5. Главное меню
        dialogs = {1: [[[433, 365], [24, 44, 41]], [[393, 351], [132, 85, 16]]],
                   2: [[[327, 320], [24, 44, 41]], [[297, 502], [165, 113, 49]]],
                   3: [[[323, 316], [40, 24, 40]], [[293, 561], [248, 220, 152]]],  # TODO UNUSED
                   4: [[[304, 384], [24, 44, 41]], [[297, 350], [165, 105, 33]]],
                   5: [[[449, 186], [107, 0, 0]], [[590, 186], [82, 0, 0]]],
                   }
        for key, val in dialogs.items():
            # Переводим относительные координаты в абсолютные
            r = 0
            for item in val:
                screen_crd = self.ms.abs_to_screen(item[0][0], item[0][1])
                # print(key, self.ms.matrix[screen_crd[1], screen_crd[0]])
                if self.ms.matrix[screen_crd[1], screen_crd[0]][0] == item[1][2] \
                        and self.ms.matrix[screen_crd[1], screen_crd[0]][1] == item[1][1] \
                        and self.ms.matrix[screen_crd[1], screen_crd[0]][2] == item[1][0]:
                    r += 1
                    pass
            if r == 2:
                # Если оба цвета прошли валидацию, возвращаем ключ сообщения
                ret = key
                break
            pass
        return ret

    def do_action(self, action):
        action.do()
        self.last_run_action_result = action.result
        ActionsDone.actions[action.act_id] = action
        if action.defeat:
            # Если возникло поражение, прерываем цикл.
            TM.defeat = 1

        pr(self.module_name, 'Действие завершено:', action.act_name, action.act_id)
        self.run_action = ''

    def set_validate_group(self, groups):
        # Установить валидацию нахождения групп юнитов
        pr(self.module_name, 'Добавлена группа валидации:', groups)
        self.ms.set_validate_groups(groups)

    def find_in_black_sector(self, find=True):
        # Поиск врагов в темноте
        pr(self.module_name, 'Поиск врагов в темноте включён')
        self.ms.find_black_sector = find

    pass
