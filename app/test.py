import os
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

# Проверка функций
from app.mission.monitor import MiniMap, MainScreen
import app.commands as cmd
import time
from app.commands import Mouse
from app.mission.tactic import *
from app.mission.task_manager import TaskManager
import app.mission.task as task
from app.mission.heroes import *
# from app.shop import Shop
import app.ineterface as inter
import app.tavern as tavern

mouse = Mouse()
# mm = MiniMap()
# mouse.left(904, 116, sleep=100)

# ms.find()
cp = CheckPoint()
tm = TaskManager()
act = Actions()
mouse = Mouse()
mm = MainMenu()


def tm_test_clear_sector():
    # Тест движения к кт
    mm.enter_game()
    tm.run_task(task.Center(SwordMan_1))
    tm.run_task(task.ClearSector([930, 127], SwordMan_1))

    pass


def tm_test_hp():
    # Тест лечения
    unit = 2
    # Воин идёт к месту сбора
    mouse.left(896, 167, sleep=100)
    act.center_group(unit)
    tm.add_task(task.HealthMon(2))
    tm.run()
    pass


def tm_test_enemy_down():
    # Тест движения к кт
    # Идём к мосту
    unit = 1
    # Воин идёт к месту сбора
    mouse.left(896, 167, sleep=100)
    act.center_group(unit)
    tm.add_task(task.EnemyDown(1))
    tm.run()
    pass


def tm_test_multitask():
    # Тест движения к кт
    # Идём к мосту
    unit = 8
    # Воин идёт к месту сбора
    mouse.left(1000, 161, sleep=100)
    act.center_group(unit)
    tm.add_task(task.Walk([1000, 161], unit))
    enemy_down = tm.add_task(task.EnemyDown(1))
    tm.add_task(task.Walk([995, 151], unit), enemy_down)
    tm.run()

    pass


def tm_test_bind_magic():
    # Тест автомагии
    unit = 3
    # Воин идёт к месту сбора
    mm.enter_game()

    tm.add_task(task.Center(unit))
    tm.run()

    tm.add_task(task.BindAutoMagic(unit, 'heal'))
    tm.run()

    pass


def tm_group_test():
    # Группа героев
    group = Group()
    # Классы героев
    igles = Igles()
    hildar = Hildar()
    diana = Diana()
    # Наёмники
    healer = Healer()
    elf = Elf()
    ork = Ork()
    archer = Archer()
    warrior = Warrior()

    # Назначения для формирования строя
    tank = warrior.group
    archers = [ork.group,
               archer.group,
               igles.group]
    mages = [elf.group,
             healer.group,
             hildar.group,
             diana.group]
    group.set_group(tank, archers, mages)

    # Воин идёт к месту сбора
    mouse.left(999, 159, sleep=100)
    act.center_group(tank)
    walk_id = tm.add_task(task.Walk([1000, 159], tank))
    tm.add_task(task.Group(group), walk_id)
    # Убить людоеда
    enemy_down = tm.add_task(task.EnemyDown(1))
    tm.run()

    # Поднятся на север и убить волков
    wolf = tm.add_task(task.EnemyDown(0))
    walk_top = tm.add_task(task.Walk([999, 155], tank))
    tm.add_task(task.Group(group), walk_top)
    tm.add_task(task.GetLoot(1), wolf)
    tm.run()
    pass


def tm_test_life():
    # Тест жизни наёмника

    # Воин идёт к месту сбора
    mm.enter_game()

    elf = Elf()
    en1 = Enchantress1

    validate_group = [elf.group, en1.group]
    tm.set_validate_group(validate_group)
    tm.add_task(task.Center(elf.group))
    tm.add_task(task.LifeHiredUnits([elf, en1]))
    tm.add_task(task.Wait(60))
    tm.run()

    pass


def tm_test_dark():
    mm.enter_game()

    tm.run_task(task.Center(Skrakan))
    # Тест затемнения
    tm.run_task(task.DarkDetector(Fergard, Skrakan, 3, True))
    tm.run_task(task.Wait(30000))
    tm.run()

    pass


def tm_test_resist():
    # Тест затемнения
    unit = 3
    target = 1
    # Воин идёт к месту сбора
    mm.enter_game()

    tm.add_task(task.Center(unit))

    resist = tm.add_task(task.WaterResist([1, 2], unit, wait_time=3, max_dst=6))

    tm.add_task(task.EnemyClear([1, 2], min_dst=10))
    tm.run()

    pass


def tm_group_test_2():
    # Выстраиваем группу
    mm.enter_game()

    # Маги
    hildar = Hildar()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()

    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en3.group]
    mages = [en2.group, en1.group, hildar.group, diana.group, elf.group]

    group.archers_layer = 2
    group.mages_layer = 3

    group.set_group(tank, archers, mages)

    tm.add_task(task.Center(igles.group))
    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 0))
    tm.run()

    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 1))
    tm.run()

    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 2))
    tm.run()

    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 3))
    tm.run()


def tm_test_group_3():
    # Выстраиваем группу
    mm.enter_game()

    # Маги
    hildar = Hildar()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()

    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    # Выстраиваем группу
    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en1.group, en2.group, en3.group]
    mages = [hildar.group, diana.group, elf.group]

    group.archers_layer = 6
    group.mages_layer = 7

    group.set_group(tank, archers, mages)

    tm.add_task(task.Center(igles.group))
    tm.run()
    # Выстраиваем группу
    tm.add_task(task.Group(group, 0, 1))
    tm.run()


def tm_test_find_by_name():
    # Выстраиваем группу
    mm.enter_game()

    igles = Igles()
    diana = Diana()
    en2 = Enchantress2()

    tm.find_in_black_sector(True)

    tm.set_validate_group([igles.group, diana.group, en2.group])

    tm.add_task(task.Center(igles.group))
    tm.add_task(task.SelfHealthMon(igles.group, 0, num=2, num_if_red=4))

    tm.add_task(task.SpeedMinus())
    tm.run()

    # Иглез убивает некромантов и возвращается к команде
    enemy_name = 'СКЕЛЕТ-МАГ'
    kill = tm.add_task(task.KillEnemy(0, igles.group, max_dst=6, far_target=False,
                                      target_name=enemy_name, wait_time=1000, validate_target=True))
    walk1 = tm.add_task(task.Walk([962, 76], igles.group), block_task_id=kill)
    walk2 = tm.add_task(task.Walk([957, 76], igles.group), walk1, block_task_id=kill)

    # Дайна, в это время, кастует град с низким приоритетом.
    # grad = tm.add_task(task.KillEnemyMagic(5, diana.group, cast_magic='grad', max_dst=20, wait_time=1000,
    #                                       priority_custom=6))

    tm.run()
    tm.add_task(task.SpeedPlus())
    tm.add_task(task.ShiftWalk(igles.group, en2.group, [7, 0]))
    tm.run()


def tm_test_clear_sector_2():
    # Выстраиваем группу
    mm.enter_game()

    igles = Igles()
    en2 = Enchantress2()

    tm.add_task(task.Center(igles.group))
    tm.add_task(task.SelfHealthMon(igles.group, 0, num=2, num_if_red=4))

    tm.run()

    # Иглез приманивает врагов
    tm.add_task(task.ClearSector([940, 76], igles.group, min_dst=6, max_dst=8, return_dst=11))
    tm.run()

    tm.add_task(task.ShiftWalk(igles.group, en2.group, [7, 0]))
    tm.run()


def tm_test_cursor():
    # Выстраиваем группу
    mm.enter_game()

    igles = Igles()

    tm.add_task(task.Center(igles.group))
    tm.run()
    act.mouse.move(550, 350)
    act.select_group(2)
    # 80 180 248 b g r
    print(act.color.color(547, 350))


def tm_test_group_attack():
    # Выстраиваем группу
    mm.enter_game()

    # Маги
    hildar = Hildar()
    healer = Healer()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()
    warrior = Warrior()
    troll = Troll()
    # Лучники
    ork = Ork()
    archer = Archer()
    group = Group()
    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    tm.add_task(task.Center(igles.group))
    tm.run()

    grad = tm.add_task(task.KillEnemyMagic(5, diana.group, cast_magic='fire', max_dst=20, wait_time=10000,
                                           magic_attack=True))
    grad = tm.add_task(task.KillEnemyMagic(5, hildar.group, cast_magic='fire', max_dst=20, wait_time=10000,
                                           magic_attack=True))
    grad = tm.add_task(task.KillEnemyMagic(5, elf.group, cast_magic='grad', max_dst=20, wait_time=5000))
    grad = tm.add_task(task.KillEnemyMagic(5, en1.group, cast_magic='chain', max_dst=20, wait_time=1000))
    grad = tm.add_task(task.KillEnemyMagic(5, en2.group, cast_magic='chain', max_dst=20, wait_time=1000))
    grad = tm.add_task(task.KillEnemyMagic(5, en3.group, cast_magic='chain', max_dst=20, wait_time=1000))

    clear = tm.add_task(task.EnemyClear([igles.group], min_dst=20, validation_interval=2))
    tm.run()


def tm_test_fire_ball():
    # Выстраиваем группу
    mm.enter_game()

    tm.run_task(task.Center(Fergard))

    tm.run_task(task.OpenInvent(True))
    tm.run_task(task.FindPotions(Fergard))
    tm.run_task(task.SelfManaRegen(Fergard, 30))
    tm.run_task(task.SelfManaMon(Fergard, 200, 80))

    tm.run_task(task.Click([940, 129]))

    # Убиваем появившихся магов огненным шаром
    tm.run_task(task.KillEnemyMagic(5, Fergard, "fireball", target_type=3, max_dst=20))
    tm.run_task(task.EnemyClear([SwordMan_1], min_dst=15, validation_interval=2, enemy_type=3))


def tm_test_group_2():
    # Выстраиваем группу
    mm.enter_game()
    # Маги
    hildar = Hildar()
    healer = Healer()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()
    warrior = Warrior()
    troll = Troll()
    # Лучники
    ork = Ork()
    archer = Archer()
    group = Group()
    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    tm.add_task(task.Center(igles.group))
    tm.run()

    # Выстраиваем группу
    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en1.group, en2.group, en3.group]
    mages = [hildar.group, diana.group, elf.group]
    group.archers_layer = 6
    group.mages_layer = 7
    group.set_group(tank, archers, mages)

    tm.run_task(task.Group(group, 0, 0))
    tm.run_task(task.Group(group, 0, 1))
    tm.run_task(task.Group(group, 0, 2))
    tm.run_task(task.Group(group, 0, 3))


def tm_test_group_3():
    # Выстраиваем группу
    mm.enter_game()
    # Маги
    hildar = Hildar()
    healer = Healer()
    diana = Diana()
    elf = Elf()
    # Воины
    igles = Igles()
    warrior = Warrior()
    troll = Troll()
    # Лучники
    ork = Ork()
    archer = Archer()
    group = Group()
    # Волшебницы из последней миссии
    en1 = Enchantress1()
    en2 = Enchantress2()
    en3 = Enchantress3()

    tm.run_task(task.Center(igles.group))

    # Выстраиваем группу
    group = Group()
    # Назначения для формирования строя
    tank = igles.group
    archers = [en1.group, en2.group, en3.group]
    mages = [hildar.group, diana.group, elf.group]
    group.archers_layer = 6
    group.mages_layer = 7
    group.set_group(tank, archers, mages)

    tm.run_task(task.Group(group, 0, 1))


def tm_test_shift():
    # Выстраиваем группу
    mm.enter_game()

    tm.run_task(task.Center(1))
    tm.run_task(task.ShiftWalk(6, 1, [-3, -3]))


def tm_test_enemy_clear():
    # Выстраиваем группу
    mm.enter_game()
    igles = Igles()

    tm.run_task(task.Center(igles.group))
    # Иглез приманивает врагов
    tm.run_task(task.EnemyClear([igles.group], 10))


def sell_test():
    s = Shop()
    s.findtext.save = False
    s.findtext.print_data = True
    s.findtext.init_model()
    s.sell_all()


def tm_test_get_loot():
    mm.enter_game()
    tm.run_task(task.Center(Fergard))
    tm.run_task(task.GetLoot(Fergard))
    pass


def tm_test_walk():
    mm.enter_game()
    tm.run_task(task.Center(Fergard))
    tm.run_task(task.Walk([882, 55], Fergard))
    tm.run_task(task.Walk([884, 53], Fergard))
    tm.run_task(task.Walk([887, 55], Fergard))
    pass


def tm_test_shield():
    mm.enter_game()
    tm.run_task(task.Center(Fergard))
    tm.run_task(task.MagicShield(Fergard, 5))
    tm.run_task(task.MagicShield(Ren, 5))
    tm.run_task(task.Wait(60))
    pass


def tm_test_walk_group():
    mm.enter_game()
    tm.run_task(task.UnitCircle([991, 170], SwordMan_1))
    tm.run_task(task.UnitCircle([995, 105], SwordMan_1))
    pass


def tm_test_walk_group_war():
    # TODO научить бота быстрее реагировать на потраченое ХП.
    #  Оступление героев с малым ХП, с последующим наступлением.
    #   Если мало ХП и враг близко, а центр далеко отступаем к центру. Затем, если ХП больше половины, снова наступаем.
    #   Нельзя останавливать героя, который не успел отступить, лучше его пропустить и лечить другого
    #  Учёт типа врагов, нападаение на людоедов и троллей группой, либо выстраивание в ряд перед ними.
    #   Например при виде людоеда, вся группа воинов выстраивается в ряд, затем нападает. Не подходит по одному.
    mm.enter_game()
    tm.set_validate_group([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
    tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

    tm.run_task(task.HealthMon(Magican, wait_time=200, health_level=9, health_group=True,
                               force_hero_stay=False, force_retreat=3))

    # tm.run_task(task.MagicShield(Fergard, 5))
    # tm.run_task(task.MagicShield(Ren, 5))
    # tm.run_task(task.MagicShield(Magican, 5))

    tm.run_task(task.Center(SwordMan_1))
    tm.run_task(task.Retreat([Fergard, Ren, Brian, Nayra], 3))
    tm.run_task(task.UnitCircle([979, 81], SwordMan_1, use_neuromap=False))
    tm.run_task(task.EnemyClear([SwordMan_1], min_dst=5))
    tm.run_task(task.UnitCircle([995, 77], SwordMan_1, use_neuromap=False))
    # tm.run_task(task.EnemyClear([SwordMan_1], min_dst=5, enemy_type=1))
    tm.run_task(task.UnitCircle([975, 76], SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
    tm.run_task(task.UnitCircle([978, 59], SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
    tm.run_task(task.UnitCircle([944, 57], SwordMan_1, walk_min_ct_radius=4, use_neuromap=False))
    pass


def tm_test_healing():
    mm.enter_game()
    # tm.set_validate_group([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
    # tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))

    tm.run_task(task.HealthMon(Magican, wait_time=200, priority_custom=-1, health_level=9, health_group=True,
                               force_hero_stay=True))

    tm.run_task(task.Wait(30000))
    pass


def tm_test_self_healing():
    mm.enter_game()
    tm.run_task(task.Center(Fergard))

    tm.run_task(task.MagicMenu(False))
    tm.run_task(task.OpenInvent(True))
    tm.run_task(task.FindPotions(Fergard))

    tm.run_task(task.SelfManaRegen(Fergard, 30))
    tm.run_task(task.MagicShield(Fergard, 1, True))
    tm.run_task(task.SelfHealMon(Fergard, 100, 9, 7))
    tm.run_task(task.SelfManaMon(Fergard, 200, 80))

    tm.run_task(task.Wait(30000))
    pass


def tm_test_loot():
    mm.enter_game()
    tm.run_task(task.Center(Fergard))
    tm.run_task(task.FindLoot([Brian, Nayra], daemon=False, ignore_enemy=True))
    pass


def tm_test_movegroup():
    mm.enter_game()
    tm.set_validate_group([Fergard, Ren, Dubin_1, Dubin_2, Dubin_3])
    tm.run_task(task.Center(Fergard))
    tm.run_task(task.MoveGroup(Fergard, [Ren, Dubin_1, Dubin_2, Dubin_3]))
    tm.run_task(task.Wait(60))
    pass


def tm_unit_circle_wait():
    mm.enter_game()
    tm.run_task(task.Center(Fergard))
    # tm.set_validate_group([Fergard, Ren, Brian, Nayra, Magican, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4])
    # tm.run_task(task.LifeHiredUnits2([SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4, Magican]))
    tm.run_task(
        task.HealthMon(Magican, wait_time=200, priority_custom=-1, health_level=9, health_group=True, force_retreat=7,
                       force_hero_stay=False))
    tm.run_task(task.UnitCircle())
    tm.run_task(task.Wait(30000))
    pass


def tm_scan_screen():
    mm.enter_game()
    tm.run_task(task.Center(Skrakan))
    tm.run_task(task.SelfHealMon(Skrakan, 100, 9))
    tm.run_task(task.Wait(30000))
    pass


def tm_shift_walk():
    mm.enter_game()
    tm.run_task(task.Center(Magican))
    tm.run_task(task.ShiftWalk(SwordMan_1, Magican, [-1, 0], 1))
    tm.run_task(task.ShiftWalk(SwordMan_2, Magican, [0, -1], 1))
    tm.run_task(task.ShiftWalk(SwordMan_3, Magican, [1, 0], 1))
    tm.run_task(task.ShiftWalk(SwordMan_4, Magican, [0, 1], 1))
    tm.run_task(task.Wait(30000))
    pass


def tm_test_clear_chat():
    mm.enter_game()
    tm.run_task(task.ClearChat())


def find_unit_by_code():
    mm.enter_game()
    tv = tavern.Tavern()
    tv.talk_hero_by_code(tv.code_brian)
    # tv.hire_by_code(tv.code_swordman)
    pass


def set_group():
    mm.enter_game()
    tm.run_task(task.SetHeroesGroupV2([Fergard, Brian, Ren, Nayra, SwordMan_1, SwordMan_2, SwordMan_3, SwordMan_4]))
    pass


def tm_test_teleport():
    mm.enter_game()
    tm.run_task(task.Center(Fergard))
    tm.run_task(task.MagicMenu(False))
    tm.run_task(task.OpenInvent(True))
    tm.run_task(task.SelfManaRegen(Fergard, 30))
    tm.run_task(task.MagicShield(Fergard, 1, True))
    tm.run_task(task.SelfHealMon(Fergard, 100, 9))
    tm.run_task(task.MultiPort([920, 90], Fergard, min_mana=100))
    tm.run_task(task.MultiPort([889, 53], Fergard, war_mode=True, health_level=10, min_mana=100))
    pass


def tm_test_teleport_warmode():
    mm.enter_game()
    tm.run_task(task.Center(Skrakan))

    tm.run_task(task.MagicMenu(False))
    tm.run_task(task.OpenInvent(True))
    tm.run_task(task.FindPotions(Skrakan))

    hp = tm.run_task(task.SelfHealMon(Skrakan, 100, 9, 7))
    mp = tm.run_task(task.SelfManaMon(Skrakan, 200, 80))

    tm.run_task(task.SelfManaRegen(Skrakan, 30))
    tm.run_task(task.MagicShield(Skrakan, 1, True))

    tm.run_task(task.MultiPort([890, 114], Skrakan, 100, war_mode=True, health_level=10))

    # loot = tm.run_task(task.FindLoot([Fergard], war_mode=[Fergard])) # , block=[loot]

    tm.run_task(task.MultiPort([900, 71], Skrakan, 100, war_mode=True, health_level=10))


    pass


def talk_tavern():
    tv = tavern.Tavern()
    mm = inter.MainMenu()
    # Переход в таверну
    tv.go_tavern_plagat()
    tv.talk_1()
    tv.talk_2()
    tv.talk_3()
    tv.talk_4()
    tv.talk_5()
    tv.talk_6()

# time1 = time.time()

# set_group()

# time2 = time.time()
# print(time2 - time1)

# cp.update_speed_test()

# cp.find_in_screen_test()
# cp.numpy_test()
# cp.update_speed_test()
# cp.find_hero_test()
# mm.find()
# tm_test_walk()
# tm_group_test()
# tm_test_get_loot()
# tm_test_hp()
# tm_test_clear_sector()
# tm_test_bind_magic()
# tm_test_life()
# tm_test_dark()
# tm_test_resist()
# tm_test_group_3()
# tm_test_find_by_name()
# tm_test_clear_sector_2()
# tm_test_group_2()
# tm_test_shift()
# tm_test_enemy_clear()
# tm_test_cursor()
# tm_test_group_attack()
# tm_test_group_3()
# tm_test_movegroup()
# find_unit_by_code()
# tm_test_teleport()
# tm_test_walk_group()
# tm_unit_circle_wait()
# tm_test_walk_group_war()
# tm_test_healing()
# tm_test_clear_chat()
# tm_test_loot()
# tm_test_fire_ball()
# tm_shift_walk()
# tm_test_clear_sector()

#tm_scan_screen()

# tm_test_self_healing()
#tm_test_teleport_warmode()


talk_tavern()