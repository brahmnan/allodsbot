import asyncio

async def async_function():
    # Your async code here
    await asyncio.sleep(1)
    return "Async result"

def callback_function(future):
    # Your callback code here
    print(future.result())

loop = asyncio.get_event_loop()
future = asyncio.ensure_future(async_function())
future.add_done_callback(callback_function)
print('run async')
loop.run_until_complete(future)


print('async complete')