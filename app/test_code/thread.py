import time
import threading

def my_function(callback):
    # Your function code goes here
    result = 42  # For example
    time.sleep(1)
    callback(result)

def my_callback(result):
    # Your callback code goes here
    print(result)

t = threading.Thread(target=my_function, args=(my_callback,))
t.start()
print('run')