# Стратегия управления героем.
# Создание персонажа, закупка в магазине, диалоги, запуск карт.

# Вся игра делится на этапы, для каждого этапа есть своя функция с набором комманд. Для упрощения тестирования, каждый
# этап можно запустить отдельно, он должен иметь сохранённую запись с номером этапа. По умолчанию игра запускается
# с 0 этапа, затем это значение меняется, запуская новые функции.

import app.ineterface as inter
import app.tavern as tavern
import time
from app.mission.heroes import *
from app.mission.mission import *
from app.funcitons import pr
from app.neurotext.neurotext import NeuroText
from app.shop import Shop
from app.mission.store import Items
from app.colors import Colors
from app.mission.store import TM


class Strategy:
    # Управление стратегией при прохождении игры

    # Этап прохождения
    stage = 0
    ms = Mission()
    mm = inter.MainMenu()
    tv = tavern.Tavern()
    shop = Shop()
    name = 'СТР'

    defeats = 0
    max_defeats = 3
    win_game = 0

    # Этапы прохождения
    stages = {
        0: 'new_char',
        1: 'mission_1',
        2: 'mission_1_2',
        3: 'mission_2',
        4: 'plagat_2_sell',
        5: 'plagat_2_tv',
        6: 'mission_3',
        7: 'mission_3_2',
        9: 'plagat_3_sell',
        10: 'plagat_3_tv',
        11: 'mission_4',
        12: 'mission_4_2',
        13: 'mission_4_3',
        14: 'mission_4_4',
        15: 'mission_4_5',
        16: 'plagat_4_sell',
        17: 'plagat_4_training',
        18: 'plagat_4_tv',
        19: 'mission_5',
        20: 'plagat_5_sell',
        21: 'plagat_5_buy_bow',
        22: 'plagat_5_tv',
        23: 'mission_6',
        24: 'mission_6_1',
        25: 'mission_6_2',
        26: 'plagat_6_sell',
        27: 'plagat_6_tv',
        28: 'mission_7',
        29: 'mission_7_1',
        30: 'mission_7_2',
        31: 'plagat_7_sell',
        32: 'plagat_7_buy_mana_regen',
        33: 'plagat_7_training',
        34: 'plagat_7_tv',
        35: 'mission_8',
        36: 'mission_8_1',
        37: 'mission_8_2',
        38: 'mission_8_3',
        39: 'plagat_8_sell',
        40: 'plagat_8_tv',
        41: 'mission_9',
        42: 'mission_9_1',
        43: 'mission_9_2',
        44: 'mission_9_3',
        45: 'mission_9_4',
        46: 'mission_9_5',
        47: 'mission_9_6',
        48: 'mission_9_7',
        49: 'mission_9_8',
        50: 'plagat_10_sell',
        51: 'plagat_10_buy_mana',
        52: 'plagat_10_buy_health',
        53: 'plagat_10_buy_fireball',
        54: 'plagat_10_tv',
        55: 'mission_10',
        56: 'plagat_11',
        57: 'mission_11',
        58: 'plagat_12_shop',
        59: 'plagat_12_buy_tuman',
        60: 'plagat_12_tv',
        61:  'mission_12',
        62: 'mission_12_1',
        63: 'mission_12_2',
        64: 'mission_12_3',
        65: 'mission_12_4',
        66: 'mission_12_5',
        67: 'mission_12_6',
        68: 'mission_12_7',
        69: 'mission_12_8',
        70: 'plagat_13_sell',
        71: 'plagat_13_tv',
        72: 'mission_13',
        73: 'mission_13_2',
        74: 'mission_13_3',
        75: 'mission_13_4',
        76: 'mission_13_5',
        77: 'mission_13_6',
        78: 'mission_13_7',
        79: 'plagat_14_sell',
        80: 'plagat_14_buy_mana',
        81: 'plagat_14_buy_health',
        82: 'plagat_14_training',
        83: 'plagat_14_tv',
        84: 'mission_14',
        85: 'mission_14_2',
        86: 'mission_14_3',
        87: 'mission_14_4',
        88: 'plagat_15',
        89: 'plagat_15_training',
        90: 'plagat_15_tv',
        91: 'mission_15',
        92: 'mission_15_2',
        93: 'mission_15_3',
        94: 'mission_15_4',
        95: 'mission_15_5',
        96: 'mission_15_6',
        97: 'mission_15_7',
        98: 'mission_15_8',
        99: 'mission_15_9',
        100: 'mission_15_10',
        101: 'mission_15_11',
        102: 'mission_15_12',
        103: 'mission_15_13',
        104: 'mission_15_14',
        105: 'mission_15_15',
        106: 'mission_15_16',
        107: 'mission_15_17',
        108: 'mission_15_18',
        109: 'mission_15_19',

    }

    def __init__(self):
        self.shop.findtext.init_model()
        pass

    # запуск стратегии
    def run(self):
        time1 = time.time()
        # Запускаем текущий этап
        if self.stages.get(self.stage):
            f = getattr(self, self.stages[self.stage])
            f()
        time2 = time.time()
        total = time2 - time1
        i = 0
        m = 0
        while total - i >= 60:
            i += 60
            m += 1
        sec = total - i
        print('total time: ', m, ':', sec)

    def set_stage(self, stage):
        self.stage = stage
        # Сохраняем переменную для других функций
        TM.stage = stage

    def ns(self):
        # Следующий стаж
        ck = self.stage
        nk = ck
        br = False
        for k in self.stages.keys():
            nk = k
            if br:
                break
            if k == ck:
                br = True
        return nk

    def ps(self):
        # Предыдущий стаж
        ck = self.stage
        pk = ck
        for k in self.stages.keys():
            if k == ck:
                break
            pk = k
        return pk

    def next_stage(self, stage=0):
        # Сохранение игры и выбор следующего стажа
        if not stage:
            stage = self.ns()

        if self.win_game:
            return
        self.defeats = 0
        sg = inter.SaveGame()
        sg.save_game(stage)
        self.set_stage(stage)
        # Запускаем следующий стаж
        self.run()

    def retry_stage(self, stage=0):
        TM.defeat = 0
        self.defeats += 1
        pr(self.name, 'Миссия провалена', self.defeats)
        sg = inter.SaveGame()

        if self.defeats > self.max_defeats:
            # Загруаем предыдущий уровень.
            self.defeats = 0
            stage = self.ps()

        findtext = self.shop.findtext
        sg.load_from_menu(stage, findtext)

        self.set_stage(stage)
        self.run()

    def tol(self, result=True):
        # try or load
        if result:
            self.next_stage(self.ns())
        else:
            self.retry_stage(self.stage)
        pass

    def new_char(self):
        pr(self.name, 'Начало')

        # Проверка стартового меню
        mm = self.mm
        if not mm.is_start():
            return
        # Новая игра
        mm.new_game()
        mm.wait_for_color(*Colors.select_char)
        # Пропуск заставки
        # mm.skip_video()
        # Выбираем сложность
        mm.select_level()
        # Выбираем класс
        mm.select_char()
        # Подтверждаем изменения
        inter.validate_left_cicle(725, 570)
        # Выбор магии воды
        inter.validate_left_cicle(452, 353)
        # Принимаем настройки героя
        inter.validate_left_cicle(750, 240)
        # Пропуск заставки
        # mm.skip_video()
        # Меняем стаж
        mm.wait_for_color(*Colors.dialog)
        # mm.propusk('enter', 330, 330)
        NeuroText.read_cicle()
        mm.game_options()
        self.next_stage()

    def mission_1(self):
        self.tol(M1.m_1())

    def mission_1_2(self):
        pr(self.name, 'Идём на миссию')
        result = M1.m_1_2()
        if result:
            mm = self.mm
            # Ожидаем начала диалога второй миссии
            # time.sleep(10000 / 1000)
            # Ускоряем переход
            # mm.skip_video()
            
            mm.wait_for_color(*Colors.dialog)            
            NeuroText.read_cicle()
            # Пропускаем диалог
            # mm.propusk('enter', 330, 330)
            
        self.tol(result)

    def mission_2(self):
        pr(self.name, 'Вторая миссия')
        result = M2.m_1()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_2_sell(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        mm = self.mm
        print('Продажа лута')
        s.go_plagat_shop()
        NeuroText.lisen_cicle(120)
        # mm.propusk('enter', 330, 330)
        s.select_hero(Fergard)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_2_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_dubin)
        tv.talk_hero_by_code(tv.code_ren)
        # tv.talk_1()
        # tv.talk_2()
        # mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        self.next_stage()

    def mission_3(self):
        self.tol(M3.m_1())


    def mission_3_2(self):
        result = M3.m_2()

        if result:
            mm = self.mm
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_3_sell(self):
        # Заходим в магазин и покупаем ускорители здоровья и маны
        s = self.shop
        mm = self.mm
        print('Продажа лута')
        s.go_plagat_shop()
        s.select_hero(Fergard)
        s.sell_all()
        s.select_hero(Ren)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_3_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        # tv.hire_2()
        # tv.talk_3()
        # mm.propusk('enter', 330, 330)
        tv.hire_by_code(tv.code_dubin)
        tv.talk_hero_by_code(tv.code_ren)

        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)

        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        
        self.next_stage()

    def mission_4(self):
        self.tol(M4.m_1())

    def mission_4_2(self):
        self.tol(M4.m_2())

    def mission_4_3(self):
        self.tol(M4.m_3())

    def mission_4_4(self):
        self.tol(M4.m_4())

    def mission_4_5(self):
        result = M4.m_5()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_4_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        s.select_hero(Fergard)
        s.sell_all()
        s.select_hero(Brian)
        s.sell_all()
        s.select_hero(Ren)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_4_training(self):
        mm = inter.MainMenu()
        s = self.shop
        mm.to_training()
        s.select_hero(Fergard)
        mm.training_earth()
        mm.training(5)
        s.select_hero(Ren)
        mm.training_earth()
        mm.training(5)
        mm.quit_training()
        self.next_stage()

    def plagat_4_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        #tv.talk_4()
        #mm.propusk('enter', 330, 330)
        tv.talk_hero_by_code(tv.code_brian)
        tv.talk_5(True, 170)
        # mm.propusk('enter', 330, 330)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_5(self):
        result = M5.m_1()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_5_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        s.select_hero(Fergard)
        s.sell_all()
        s.select_hero(Brian)
        s.sell_all()
        s.select_hero(Ren)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_5_buy_bow(self):
        # Покупка меча
        price = 333
        print('Арбалета')
        s = self.shop
        s.find_or_load(price, hero=Brian, place='weapon')
        self.next_stage()

    def plagat_5_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_swordman)
        tv.talk_hero_by_code(tv.code_brian)

        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_6(self):
        self.tol(M6.m_1())

    def mission_6_1(self):
        self.tol(M6.m_2())

    def mission_6_2(self):
        result = M6.m_3()

        if result:
            mm = self.mm
            # # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_6_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        s.select_hero(Fergard)
        s.sell_all()
        s.select_hero(Brian)
        s.sell_all()
        s.select_hero(Ren)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_6_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_swordman)
        tv.talk_hero_by_code(tv.code_noname, wait_k=120)

        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_7(self):
        self.tol(M7.m_1())

    def mission_7_1(self):
        self.tol(M7.m_2())

    def mission_7_2(self):
        result = M7.m_3()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_7_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        s.select_hero(Brian)
        s.sell_all()
        s.select_hero(Nayra)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_7_buy_mana_regen(self):
        print('Покупаем регенерацию магии Фергарду')
        s = self.shop
        price = 50
        s.find_or_load(price, 'books', color_validate=Items.regen_magic, buy_all=True, hero=Fergard, equip=False)
        self.next_stage()

    def plagat_7_training(self):
        mm = inter.MainMenu()
        s = self.shop
        mm.to_training()
        s.select_hero(Fergard)
        mm.training_astral()
        mm.training(10)
        mm.quit_training()
        self.next_stage()

    def plagat_7_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_swordman)
        tv.talk_hero_by_code(tv.code_naira, wait_k=90)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_8(self):
        self.tol(M8.m_1())

    def mission_8_1(self):
        self.tol(M8.m_2())

    def mission_8_2(self):
        self.tol(M8.m_3())

    def mission_8_3(self):
        result = M8.m_4()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_8_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        NeuroText.lisen_cicle(140)
        # self.mm.propusk('enter', 330, 330)
        s.select_hero(Brian)
        s.sell_all()
        s.select_hero(Nayra)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_8_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_swordman)
        tv.hire_by_code(tv.code_mage, tv.face_mage_water)
        tv.talk_hero_by_code(tv.code_noname, wait_k=130)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_9(self):
        self.tol(M9.m_1())

    def mission_9_1(self):
        self.tol(M9.m_2())

    def mission_9_2(self):
        self.tol(M9.m_3())

    def mission_9_3(self):
        self.tol(M9.m_4())

    def mission_9_4(self):
        self.tol(M9.m_5())

    def mission_9_5(self):
        self.tol(M9.m_6())

    def mission_9_6(self):
        self.tol(M9.m_7())

    def mission_9_7(self):
        self.tol(M9.m_8())

    def mission_9_8(self):
        result = M9.m_9()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_10_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        s.select_hero(Brian)
        s.sell_all()
        s.select_hero(Nayra)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_10_buy_mana(self):
        print('Покупаем эликсиры магии Фергарду')
        s = self.shop
        price = 500
        s.find_or_load(price, 'books', color_validate=Items.potion_magic, buy_all=True, hero=Fergard, equip=False)
        self.next_stage()

    def plagat_10_buy_health(self):
        print('Покупаем эликсиры здоровья Фергарду')
        s = self.shop
        price = 500
        s.find_or_load(price, 'books', color_validate=Items.potion_health, buy_all=True, hero=Fergard, equip=False)
        self.next_stage()

    def plagat_10_buy_fireball(self):
        print('Покупаем файрбол Фергарду')
        s = self.shop
        price = 30000
        s.find_or_load(price, 'books', hero=Fergard, equip=True)
        self.next_stage()

    def plagat_10_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.talk_hero_by_code(tv.code_brian)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_10(self):
        result = M10.m_1()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_11(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        NeuroText.lisen_cicle(150)
        #self.mm.propusk('enter', 330, 330)
        s.quit()
        self.mm.plagat_quit()
        self.mm.mission_1()
        time.sleep(500 / 1000)
        self.next_stage()

    def mission_11(self):
        result = M11.m_1()

        if result:
            mm = self.mm
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_12_shop(self):
        # Снаряжаем посох Фергарду, арбалет Данасу
        s = self.shop
        print('Снаряжаем посох')
        s.go_plagat_shop()
        s.select_hero(Fergard)
        price = 84730
        # Ищем у Гильдариуса
        posoh = s.find_by_price(price, 'invent')
        if posoh:
            s.equip(posoh[0], posoh[1])

        print('Снаряжаем арбалет')
        price = 69316
        arbalet = s.find_by_price(price, 'invent')
        if arbalet:
            s.drug_to_shop(arbalet[0], arbalet[1])
            s.select_hero(Danas)
            s.drug_to_invent()
            arbalet = s.find_by_price(price, 'invent')
            if arbalet:
                s.equip(arbalet[0], arbalet[1])

        print('Снаряжаем старый посох Ферграда')
        s.select_hero(Fergard)
        price = 913
        posoh = s.find_by_price(price, 'invent')
        if posoh:
            s.drug_to_shop(posoh[0], posoh[1])
            s.select_hero(Ren)
            s.drug_to_invent()
            posoh = s.find_by_price(price, 'invent')
            if posoh:
                s.equip(posoh[0], posoh[1])
        s.quit()
        self.next_stage()

    def plagat_12_buy_tuman(self):
        print('Покупаем туман Фергарду')
        s = self.shop
        price = 60000
        s.find_or_load(price, 'books', hero=Fergard, equip=True)
        self.next_stage()

    def plagat_12_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_swordman)
        tv.hire_by_code(tv.code_mage, tv.face_mage_water)
        tv.talk_hero_by_code(tv.code_brian)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_12(self):
        self.tol(M12.m_1())

    def mission_12_1(self):
        self.tol(M12.m_2())

    def mission_12_2(self):
        self.tol(M12.m_3())

    def mission_12_3(self):
        self.tol(M12.m_4())

    def mission_12_4(self):
        self.tol(M12.m_5())

    def mission_12_5(self):
        self.tol(M12.m_6())

    def mission_12_6(self):
        self.tol(M12.m_7())

    def mission_12_7(self):
        self.tol(M12.m_8())

    def mission_12_8(self):
        result = M12.m_9()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_13_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        NeuroText.lisen_cicle(150)
        s.select_hero(Fergard)
        s.sell_all()
        s.select_hero(Danas)
        s.sell_all()
        s.quit()
        self.next_stage()

    def plagat_13_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_swordman)
        tv.hire_by_code(tv.code_arbalet)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        mm.wait_for_color(*Colors.dialog)
        NeuroText.read_cicle()
        # mm.propusk('enter', 330, 330)
        self.next_stage()

    def mission_13(self):
        self.tol(M13.m_1())

    def mission_13_2(self):
        self.tol(M13.m_2())

    def mission_13_3(self):
        self.tol(M13.m_3())

    def mission_13_4(self):
        self.tol(M13.m_4())

    def mission_13_5(self):
        self.tol(M13.m_5())

    def mission_13_6(self):
        self.tol(M13.m_6())

    def mission_13_7(self):
        result = M13.m_7()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_14_sell(self):
        s = self.shop
        print('Продажа лута')
        s.go_plagat_shop()
        s.select_hero(Fergard)
        s.de_equip_posoh()
        s.exclude_price = [535417]
        s.sell_all(equip_exclude=True)
        s.quit()
        self.next_stage()

    def plagat_14_buy_mana(self):
        print('Покупаем эликсиры магии Фергарду')
        s = self.shop
        price = 500
        s.find_or_load(price, 'books', color_validate=Items.potion_magic, buy_all=True, hero=Fergard, equip=False)
        self.next_stage()

    def plagat_14_buy_health(self):
        print('Покупаем эликсиры здоровья Фергарду')
        s = self.shop
        price = 500
        s.find_or_load(price, 'books', color_validate=Items.potion_health, buy_all=True, hero=Fergard, equip=False)
        self.next_stage()

    def plagat_14_training(self):
        mm = inter.MainMenu()
        s = self.shop
        mm.to_training()
        s.select_hero(Fergard)
        mm.training_earth()
        mm.training(30)
        mm.quit_training()
        self.next_stage()

    def plagat_14_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.talk_hero_by_code(tv.code_brian, wait_k=130)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        self.next_stage()

    def mission_14(self):
        self.tol(M14.m_1())

    def mission_14_2(self):
        self.tol(M14.m_2())

    def mission_14_3(self):
        self.tol(M14.m_3())

    def mission_14_4(self):
        result = M14.m_4()

        if result:
            mm = self.mm
            # mm.skip_video()
            # Переход в город
            mm.wait_for_color(549, 334, 222, 113, 90)

        self.tol(result)

    def plagat_15(self):
        s = self.shop
        print('Снаряжаем посох')
        s.go_plagat_shop()
        s.select_hero(Fergard)
        price = 535417
        posoh = s.find_by_price(price, 'invent')
        if posoh:
           s.equip(posoh[0], posoh[1])
        s.quit()
        self.next_stage()

    def plagat_15_training(self):
        mm = inter.MainMenu()
        s = self.shop
        mm.to_training()
        s.select_hero(Danas)
        mm.wait_for_color(440, 470, 41, 28, 8)
        mm.training_bow()
        mm.training(35)
        mm.quit_training()
        self.next_stage()

    def plagat_15_tv(self):
        tv = tavern.Tavern()
        mm = inter.MainMenu()
        # Переход в таверну
        tv.go_tavern_plagat()
        tv.hire_by_code(tv.code_swordman)
        tv.talk_hero_by_code(tv.code_brian, wait_k=130)
        # Выход
        tv.quit()
        mm.plagat_quit()
        mm.mission_1()
        time.sleep(500 / 1000)
        self.next_stage()

    def mission_15(self):
        # self.mm.options_stroy_off()
        self.tol(M15.m_1())

    def mission_15_2(self):
        self.tol(M15.m_2())

    def mission_15_3(self):
        self.tol(M15.m_3())

    def mission_15_4(self):
        self.tol(M15.m_4())

    def mission_15_5(self):
        self.tol(M15.m_5())

    def mission_15_6(self):
        self.tol(M15.m_6())

    def mission_15_7(self):
        self.tol(M15.m_7())

    def mission_15_8(self):
        self.tol(M15.m_8())

    def mission_15_9(self):
        self.tol(M15.m_9())

    def mission_15_10(self):
        self.tol(M15.m_10())

    def mission_15_11(self):
        self.tol(M15.m_11())

    def mission_15_12(self):
        self.tol(M15.m_12())

    def mission_15_13(self):
        self.tol(M15.m_13())

    def mission_15_14(self):
        self.tol(M15.m_14())

    def mission_15_15(self):
        self.tol(M15.m_15())

    def mission_15_16(self):
        self.tol(M15.m_16())

    def mission_15_17(self):
        self.tol(M15.m_17())

    def mission_15_18(self):
        result = M15.m_18()
        if result:
            print('Игра пройдена')
            exit()

        self.tol(result)
