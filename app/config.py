# Константы и настройки скрипта
import os
import ctypes

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

use_d3dshot = False

neuro_text_model = BASE_DIR + "/data/model_2"
#neuro_text_model = BASE_DIR + "/model/text_4.h5"
neuro_map_model_8 = BASE_DIR + "/data/model_map_8_1"  # Тестовая модель на 8 пикселей
neuro_map_model = BASE_DIR + "/data/model_220221_9"  # Новая модель с 12-ю выходами
#neuro_map_model = BASE_DIR + "/data/m_3.h5"  # Новая модель с 12-ю выходами

# Смещение экрана
screen_shift = {'x': 8, 'y': 31}

# Координаты мини-карты
mon_map = {'x': 880, 'y': 49, 'w': 128}

# Размер Главного окна
main_window = {'x': 864, 'ym': 704, 'y': 768}
view_window = {'x': 832, 'ym': 672, 'y': 768}
full_window = {'x': 1024, 'y': 768}

# center_window = {'x': 218, 'y': 181, 'x1': 638, 'y1': 571}
center_window = {'x': 232, 'y': 159, 'x1': 648, 'y1': 575} # test_code


# Размер окошка с именем
name_window = {'left': 910, 'top': 576, 'width': 71, 'height': 20}
name_window_v2 = {'left': 905, 'top': 576, 'width': 80, 'height': 20}

hero_name_window = {'left': 720, 'top': 432, 'width': 68, 'height': 9}
hero_current_info = {'left': 877, 'top': 576, 'width': 135, 'height': 79}

hero_name_tavern = {'left': 248, 'top': 194, 'width': 77, 'height': 20}

# Параметры инвентаря в игре
invent_game_window = {'left': 72, 'top': 715, 'width': 720, 'height': 80}

# Смещение экрана
hero_center = {'x': 368, 'y': 271, 'width': 150, 'height': 150}

# Сдавиг пикселей при переводе из данных экрана, в миникарту
mm_x_shift = 0
mm_y_shift = 2

# Окошко диалога
dialog_window = {'left': 404, 'top': 336, 'width': 300, 'height': 119}

face_window = {'left': 314, 'top': 362, 'width': 72, 'height': 92}

# Проверка раскладки клавиатуры
u = ctypes.windll.LoadLibrary("user32.dll")
pf = getattr(u, "GetKeyboardLayout")
curr_lang = pf(0)
# en - 67699721
# ru - 68748313
if curr_lang != 67699721:
    print('Включите английскую раскладку клавиатуры')
    exit()

