# Тест Ресайз картинок
import os
import numpy as np
import cv2
from hashlib import sha1
from os import walk


path = "../../img/mapdata/big/"
save_path = "../../img/mapdata/small/"

# 'gray':  - папка
# 1 - Проходимый
# 2 - Трудно проходимый
# 3 - Не проходимый
# 4 - Юнит

dirs = {
    'gray': 1,
    'grass': 1,
    'savana': 1,
    'most': 2,
    'forest': 2,
    'forestblack': 2,
    'prerock': 2,
    'rock': 3,
    'water': 3,
    'struct': 3,
    'mon': 4,
}

# Получаем список файлов, для каждой рубрики
for key, value in dirs.items():
    f = []
    curr_path = path + key
    for (dirpath, dirnames, filenames) in walk(curr_path):
        f.extend(filenames)
        break
    dirs[key] = f


words = []
# Создаём единый массив с тренировочными данными
for folder, files in dirs.items():
    path_folder = save_path + folder
    source_folder = path + folder
    dir_exist = False
    if not os.path.exists(path_folder):
        try:
            os.mkdir(path_folder)
        except OSError:
            print("Создать директорию %s не удалось" % path_folder)
        else:
            print("Успешно создана директория %s " % path_folder)
            dir_exist = True
    else:
        dir_exist = True

    if dir_exist:
        # Копируем картинки если их нет
        for file in files:
            source_name = source_folder + '/' + file

            # разбираем картинку на составляющие
            a = cv2.imread(source_name)
            len_a = len(a)
            row = 0
            len_row = len(a[row])

            while row < len_a:
                # print(i, a[i, 0])
                col = 0
                while col < len_row:
                    # Получаем данные картинки
                    sector = np.array(a[row:row + 32, col:col + 32])
                    # Получаем имя картинки
                    name = str(sha1(sector).hexdigest())
                    # Проверяем наличие картинки в памяти
                    path_name = path_folder + '/' + name + '.jpg'
                    if not os.path.exists(path_name):
                        cv2.imwrite(path_name, sector)
                    col += 32
                row += 32



