import time

import cv2
from PIL import Image
import mss
import numpy

# Different possibilities to convert raw BGRA values to RGB:

def mss_rgb(im):
    """ Better than Numpy versions, but slower than Pillow. """
    return im.rgb


def numpy_flip(im):
    """ Most efficient Numpy version as of now. """
    frame = numpy.array(im, dtype=numpy.uint8)
    return numpy.flip(frame[:, :, :3], 2).tobytes()


def numpy_slice(im):
    """ Slow Numpy version. """
    return numpy.array(im, dtype=numpy.uint8)[..., [2, 1, 0]].tobytes()


def pil_frombytes(im):
    """ Efficient Pillow version. """
    return Image.frombytes('RGB', im.size, im.bgra, 'raw', 'BGRX').tobytes()


def numpy_bgra_to_bgr(im):
    frame = numpy.array(im, dtype=numpy.uint8)
    return numpy.flip(frame[:, :, :3], 1)


with mss.mss() as sct:
    im = sct.grab(sct.monitors[1])
    # rgb = pil_frombytes(im)

    time1 = time.time()
    # ima = numpy.array(im)
    # print(ima)

    np_flip = numpy_bgra_to_bgr(im)
    # numpy.array(im)
    print(np_flip)
    time2 = time.time()
    print(time2 - time1)