from keras import Sequential
from keras.layers import Embedding, Bidirectional, LSTM, Dense, GRU, Input
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense, Convolution2D, Dropout, Flatten, MaxPooling2D


def lstm_model(num_alphabets=100, name_length=18, embedding_dim=128):
    model = Sequential([
        Embedding(num_alphabets, embedding_dim, input_length=name_length),
        # Bidirectional(GRU(units=128, dropout=0.5, return_sequences=True)),
        # Bidirectional(GRU(units=64, dropout=0.5)),
        # Bidirectional(LSTM(units=256, dropout=0.5, return_sequences=True)),
        # Bidirectional(LSTM(units=128, dropout=0.5)),
        GRU(units=16, dropout=0.5, return_sequences=True),
        GRU(units=8, dropout=0.5),
        Dense(1, activation="sigmoid")
    ])

    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer=Adam(learning_rate=0.001),
                  metrics=['accuracy'])

    return model

def lstm_model_b(num_alphabets=100, name_length=18, embedding_dim=128):
    model = Sequential([
        Embedding(num_alphabets, embedding_dim, input_length=name_length),
        Bidirectional(LSTM(units=64, return_sequences=True)),
        Bidirectional(LSTM(units=32)),
        Dense(1, activation="sigmoid")
    ])

    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer=Adam(learning_rate=0.001),
                  metrics=['accuracy'])

    return model

def gru_model(num_alphabets=100, name_length=18, embedding_dim=128):
    model = Sequential([
        Embedding(num_alphabets, embedding_dim, input_length=name_length),
        GRU(units=64, return_sequences=True),
        GRU(units=32),
        Dense(1, activation="sigmoid")
    ])

    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer=Adam(learning_rate=0.001),
                  metrics=['accuracy'])

    return model


def gru_input_model(num_alphabets=30, name_length=100):
    model = Sequential([
        Input(num_alphabets, name_length),
        GRU(units=16, return_sequences=True),
        GRU(units=8),
        Dense(1, activation="sigmoid")
    ])

    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer=Adam(learning_rate=0.001),
                  metrics=['accuracy'])

    return model

def text_model_1():
    model = Sequential()
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), padding='valid', input_shape=(32, 32, 3), activation='relu'))
    model.add(Convolution2D(filters=64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(1024, activation='relu'))
    #model.add(Dropout(0.5))

    # model.add(Dense(3072, activation='relu'))
    # model.add(Dropout(0.5))
    #
    # model.add(Dense(3072, activation='relu'))
    # model.add(Dropout(0.5))

    model.add(Dense(12, activation='softmax'))

    # sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    # model.compile(loss='categorical_crossentropy', optimizer=sgd)

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model

def text_model_2():
    model = Sequential()
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), padding='valid', input_shape=(32, 32, 3), activation='relu'))
    model.add(Convolution2D(filters=64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(256, activation='relu'))
    model.add(Dense(12, activation='softmax'))


    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model

def text_model_3():
    model = Sequential()
    model.add(Convolution2D(filters=16, kernel_size=(3, 3), padding='valid', input_shape=(32, 32, 3), activation='relu'))
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())

    model.add(Dense(128, activation='relu'))

    model.add(Dense(12, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model


def text_model_4():
    model = Sequential()
    model.add(Convolution2D(filters=16, kernel_size=(3, 3), padding='valid', input_shape=(32, 32, 3), activation='relu'))
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())

    model.add(Dense(256, activation='relu'))

    model.add(Dense(12, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model