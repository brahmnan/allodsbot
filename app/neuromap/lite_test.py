import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import numpy as np
import tensorflow as tf
from timeit import default_timer as timer
from app.neuromap.lite_model import LiteModel

kmodel = tf.keras.models.Sequential([
    tf.keras.layers.Dense(384, activation='elu', input_shape=(256,)),
    tf.keras.layers.Dense(384, activation='elu'),
    tf.keras.layers.Dense(256, activation='elu'),
    tf.keras.layers.Dense(128, activation='elu'),
    tf.keras.layers.Dense(32, activation='tanh')
])

batch_size = 10
epsilon = 1e-5
data_in = np.random.rand(batch_size, 256).astype(np.float32)
data_out_true = kmodel.predict(data_in)

print(f'batch_size = {batch_size}')

data_out = kmodel.predict(data_in)  # warm-up
start_time = timer()
data_out = kmodel.predict(data_in)
print(f'kmodel-predict-batch: {timer() - start_time:.6f} seconds')
print('correct: ' + str((abs(data_out_true - data_out) < epsilon).all()))

data_out[0] = kmodel.predict(data_in[0:1])[0]  # warm-up
start_time = timer()
for i in range(batch_size):
    data_out[i] = kmodel.predict(data_in[i:i + 1])[0]
print(f'kmodel-predict-single: {timer() - start_time:.6f} seconds')
print('correct: ' + str((abs(data_out_true - data_out) < epsilon).all()))

data_out[0] = kmodel(data_in[0:1])[0]  # warm-up
start_time = timer()
for i in range(batch_size):
    data_out[i] = kmodel(data_in[i:i + 1])[0]
print(f'kmodel-direct-single: {timer() - start_time:.6f} seconds')
print('correct: ' + str((abs(data_out_true - data_out) < epsilon).all()))

lmodel = LiteModel.from_keras_model(kmodel)  # warm-up
start_time = timer()
lmodel = LiteModel.from_keras_model(kmodel)
print(f'conversion time: {timer() - start_time:.6f} seconds')

data_out = lmodel.predict(data_in)  # warm-up
start_time = timer()
data_out = lmodel.predict(data_in)
print(f'lmodel-pseudobatch: {timer() - start_time:.6f} seconds')
print('correct: ' + str((abs(data_out_true - data_out) < epsilon).all()))

data_out[0] = lmodel.predict_single(data_in[0])  # warm-up
start_time = timer()
for i in range(batch_size):
    data_out[i] = lmodel.predict_single(data_in[i])
print(f'lmodel-single: {timer() - start_time:.6f} seconds')
print('correct: ' + str((abs(data_out_true - data_out) < epsilon).all()))