# Тест нейронной сети
import random
import time
from os import walk

from tensorflow.keras import layers

import cv2
import numpy as np
from keras.layers import Dense, Convolution2D, Dropout, Flatten, MaxPooling2D
from keras.models import Sequential
from keras.utils import to_categorical
from tensorflow import keras

from tensorflow.python.client import device_lib
import tensorflow as tf
import os

def make_model(input_shape, num_classes):
    inputs = keras.Input(shape=input_shape)
    # Image augmentation block
    data_augmentation = keras.Sequential(
        [
            layers.experimental.preprocessing.RandomFlip("horizontal"),
            layers.experimental.preprocessing.RandomRotation(0.1),
        ]
    )

    x = data_augmentation(inputs)

    # Entry block
    x = layers.experimental.preprocessing.Rescaling(1.0 / 255)(x)
    x = layers.Conv2D(32, 3, strides=2, padding="same")(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    x = layers.Conv2D(64, 3, padding="same")(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    previous_block_activation = x  # Set aside residual

    for size in [128, 256, 512, 728]:
        x = layers.Activation("relu")(x)
        x = layers.SeparableConv2D(size, 3, padding="same")(x)
        x = layers.BatchNormalization()(x)

        x = layers.Activation("relu")(x)
        x = layers.SeparableConv2D(size, 3, padding="same")(x)
        x = layers.BatchNormalization()(x)

        x = layers.MaxPooling2D(3, strides=2, padding="same")(x)

        # Project residual
        residual = layers.Conv2D(size, 1, strides=2, padding="same")(
            previous_block_activation
        )
        x = layers.add([x, residual])  # Add back residual
        previous_block_activation = x  # Set aside next residual

    x = layers.SeparableConv2D(1024, 3, padding="same")(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    x = layers.GlobalAveragePooling2D()(x)
    if num_classes == 2:
        activation = "sigmoid"
        units = 1
    else:
        activation = "softmax"
        units = num_classes

    x = layers.Dropout(0.5)(x)
    outputs = layers.Dense(units, activation=activation)(x)
    return keras.Model(inputs, outputs)


# Количество выходов - 7

path = "../../img/map/"

# 'gray': [
# 0 - номер элемента,
# 1 - количество повторов
# ],
dirs = {
    'gray': [0, 1],
    'grass': [1, 3],
    'forest': [2, 3],
    'rock': [3, 1],
    'water': [4, 5],
    'graygrass': [5, 5],
    'forestblack': [6, 15],
    'prerock': [7, 6],
    'bereg': [8, 1],
    # 'unit': 9,
    # 'structure': 10,
}

# Получаем список файлов, для каждой рубрики
for key, value in dirs.items():
    f = []
    curr_path = path + key
    for (dirpath, dirnames, filenames) in walk(curr_path):
        f.extend(filenames)
        break
    dirs[key] = [value, f]

# print(dirs)

words = []
# Создаём единый массив с тренировочными данными
for key, value in dirs.items():
    item_id = value[0][0]
    item_count = value[0][1]
    i = 0
    while i < item_count:
        for img in value[1]:
            words.append([key, item_id, img])
        i += 1

# Случайная сортировка массива
random.shuffle(words)
# print(words)
# print(len(words))
# exit()

# 9206 единиц

# images = []
my_x_train = []
my_y_train = []
for word in words:
    # print(word)
    # 'forest', 2, '5f55e7f79ca214ddce82f948632e584ed2c91a95.jpg'

    file_name = path + word[0] + '/' + word[2]
    img = cv2.imread(file_name)
    matrix = np.array(img)

    # x_train_r = np.reshape(matrix, (32, 32, 3))
    # print(x_train_r)

    my_x_train.append(matrix)
    my_y_train.append(word[1])

# Загружаем данные x_train и x_test содержат двухмерный массив с изображение цифр
# x_test, y_test массив с проверочными данными сети.
# (x_train, y_train), (x_test, y_test) = mnist.load_data()

my_y_train_cat = to_categorical(my_y_train)
my_x_train_np = np.array(my_x_train)
my_x_train_np = my_x_train_np.reshape(my_x_train_np.__len__(), 32, 32, 3)

image_size = (32, 32)
model = make_model(input_shape=image_size + (3,), num_classes=9)

#keras.utils.plot_model(model, show_shapes=True)

# Train

callbacks = [
    keras.callbacks.ModelCheckpoint("model/model_2_{epoch}.h5"),
]
model.compile(
    optimizer=keras.optimizers.Adam(1e-3),
    loss="binary_crossentropy",
    metrics=["accuracy"],
)

# print(my_x_train[100])
# print(my_y_train_cat[100])
time1 = time.time()
model.fit(my_x_train_np, my_y_train_cat, epochs=1, callbacks=callbacks,
          validation_data=(my_x_train_np, my_y_train_cat), batch_size=128)



time2 = time.time()

print(time2 - time1)
# cup 67.69053363800049 sec
# gpu 5 sec