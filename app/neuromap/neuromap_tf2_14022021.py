# Тест нейронной сети
import numpy as np
import cv2
import app.neuronet.words as w
from keras.models import Sequential
from keras.layers import Dense, Convolution2D, Dropout, Flatten, MaxPooling2D
from keras.datasets import mnist
from keras.utils import to_categorical
from keras.optimizers import SGD

import time
from os import walk
import random

def img_model():
    model = Sequential()
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), padding='valid', input_shape=(32, 32, 3), activation='relu'))
    model.add(Convolution2D(filters=64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(3072, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(3072, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(12, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model


def img_model_2():
    model = Sequential()

    # Entry block
    model.add(Convolution2D(filters=32, kernel_size=(3, 3), padding='same', input_shape=(32, 32, 3), activation='relu'))
    model.add(Convolution2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu'))

    for size in [32, 64, 128, 256]:
        model.add(Convolution2D(size, (3, 3), padding="same", activation='relu'))
        model.add(Convolution2D(size, (3, 3), padding="same", activation='relu'))
        model.add(MaxPooling2D(pool_size=(3, 3), strides=2, padding="same"))
        model.add(Dropout(0.25))

    model.add(Convolution2D(512, (3, 3), padding="same", activation='relu'))
    model.add(Dropout(0.5))

    model.add(Flatten())

    model.add(Dense(3072, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(3072, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(3072, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(12, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model


# Количество выходов - 10
filepath = 'model/model_140221_7'
save_path = "../../img/new_data/small/"

# 'gray': [
# 0 - номер элемента,
# 1 - количество повторов
# ],
dirs = {
    'gray':     [0, 1], # 5670
    'grass':    [1, 1], # 1376
    'forest':   [2, 1], # 2443
    'savana':   [3, 1],  # 1300
    'rock':     [4, 1], # 4191
    'water':    [5, 1], # 239
    'prerock':  [6, 1], # 1559
    'bereg':    [7, 1], # 788
    'most':     [8, 5], # 155
    'graygrass':[9, 1], # 1008
    'struct':   [10, 3], # 2200
    'mon':      [11, 3] # 2600
}

# Получаем список файлов, для каждой рубрики
for key, value in dirs.items():
    f = []
    curr_path = save_path + key
    for (dirpath, dirnames, filenames) in walk(curr_path):
        f.extend(filenames)
        break
    dirs[key] = [value, f]
    print(key, len(f))


words = []
# Создаём единый массив с тренировочными данными
for key, value in dirs.items():
    item_id = value[0][0]
    item_count = value[0][1]
    i = 0
    while i < item_count:
        for img in value[1]:
            words.append([key, item_id, img])
        i += 1

# Случайная сортировка массива
random.shuffle(words)
# print(words)
print(len(words))
# exit()

# 27948 единиц

# images = []
my_x_train = []
my_y_train = []
for word in words:
    # print(word)
    # 'forest', 2, '5f55e7f79ca214ddce82f948632e584ed2c91a95.jpg'

    file_name = save_path + word[0] + '/' + word[2]
    img = cv2.imread(file_name)
    matrix = np.array(img)

    # x_train_r = np.reshape(matrix, (32, 32, 3))
    # print(x_train_r)

    my_x_train.append(matrix)
    my_y_train.append(word[1])

# Загружаем данные x_train и x_test содержат двухмерный массив с изображение цифр
# x_test, y_test массив с проверочными данными сети.
# (x_train, y_train), (x_test, y_test) = mnist.load_data()

my_y_train_cat = to_categorical(my_y_train)
my_x_train_np = np.array(my_x_train)
my_x_train_np = my_x_train_np.reshape(my_x_train_np.__len__(), 32, 32, 3)

model = img_model()

# print(my_x_train[100])
# print(my_y_train_cat[100])
time1 = time.time()
history = model.fit(my_x_train_np, my_y_train_cat, validation_data=(my_x_train_np, my_y_train_cat), epochs=10, batch_size=128)

model.save(filepath)
time2 = time.time()

print(time2 - time1)
# 173.3752520084381
# 91.5715901851654
# 202.29318523406982
# 136.30194473266602

#50k*10 - 214.42813849449158
#40*10 171.66345143318176