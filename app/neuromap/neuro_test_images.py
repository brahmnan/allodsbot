# Тест Ресайз картинок
import os
import numpy as np
import cv2
from hashlib import sha1
from os import walk
import keras
import time


def check_and_create_dir(folder_path):
    # Проверка наличия папки. Если папки нет и её не удаётся создать, выходим
    if not os.path.exists(folder_path):
        try:
            os.mkdir(folder_path)
        except OSError:
            print("Создать директорию %s не удалось" % folder_path)
            exit()
        else:
            print("Успешно создана директория %s " % folder_path)


source_folder = "../../img/mapdata/combine/"
invalid_folder = "../../img/mapdata/invalid/"
sort_folder = "../../img/mapdata/sort/"

model_name = "model/model_220221_9"
# 'gray': [
# 0 - номер элемента,
# 1 - перенос элемента в новую папку: 0 - не переносим, 1 - переносим.
# ],
dirs = {
    'gray': [0, 0],
    'grass': [1, 0],
    'forest': [2, 1],
    'savana': [3, 0],
    'rock': [4, 0],
    'water': [5, 0],
    'prerock': [6, 0],
    'bereg': [7, 0],
    'most': [8, 1],
    'forestblack': [9, 0],
    'struct': [10, 0],
    'mon': [11, 0],
}

# Значение совпадения, %.
# Если совпадение выше данного значения, но результат не соответствует папке,
#   переносим файл в соответствующую папку "sort".
# Если совпадение ниже данного значения, переносим файл в паку "invalid"
val_to_remove = 0

dirs_path = {}

# Получаем список файлов, для каждой рубрики
for key, value in dirs.items():
    if key:
        dirs_path[value[0]] = key

# Список папок для тестирования
dirs_test = dirs


# Получаем список файлов, для каждой рубрики
dirs_data = {}
for key, value in dirs_test.items():
    f = []
    curr_path = source_folder + key
    for (dirpath, dirnames, filenames) in walk(curr_path):
        f.extend(filenames)
        break
    dirs_data[key] = f

model = keras.models.load_model(model_name)

# Проверка наличия директорий
check_and_create_dir(sort_folder)
check_and_create_dir(invalid_folder)

# Создаём единый массив с тренировочными данными
for folder, files in dirs_data.items():
    curr_path = source_folder + folder
    current_folder = dirs[folder][0]

    invalid_path = invalid_folder + folder
    check_and_create_dir(invalid_path)
    # Копируем картинки если их нет
    for file in files:
        source_name = curr_path + '/' + file
        invalid_name = invalid_path + '/' + file
        # разбираем картинку на составляющие
        a = cv2.imread(source_name)

        time1 = time.time()
        matrix = np.flip(a[:, :, :3], 1)
        x_img_r = np.reshape(matrix, (1, 32, 32, 3))

        # result_int = int(self.model.predict_classes(x_img_r, batch_size=32, verbose=0))

        predictions = model.predict(x_img_r)
        items = []
        max_val = 0
        i = 0

        for item in predictions[0]:
            item_val = int(item * 100)
            if item_val > max_val:
                max_val = item_val

            items.append(item_val)
            i += 1

        if predictions.shape[-1] > 1:
            score = predictions.argmax(axis=-1)
        else:
            score = (predictions > 0.5).astype('int32')
        result_int = int(score)

        time2 = time.time()
        totaltime = time2 - time1

        if current_folder != result_int and max_val >= val_to_remove:
            # Проверяем, разрешено ли перемещение
            if dirs[dirs_path[result_int]][1] == 1:

                # Перемещаем в правильную папку
                new_folder_path = sort_folder + dirs_path[result_int]

                check_and_create_dir(new_folder_path)
                sort_dir_exist = False

                new_folder_path_file = new_folder_path + '/' + file
                if not os.path.exists(new_folder_path_file):
                    os.rename(source_name, new_folder_path_file)

                print('Не верный файл', result_int, current_folder, max_val, items, totaltime)
                pass

        if max_val < val_to_remove:
            # Переносим файлы в другую папку
            if not os.path.exists(invalid_name):
                os.rename(source_name, invalid_name)
            print('Низкий процент', result_int, current_folder, max_val, items, totaltime)
            pass
