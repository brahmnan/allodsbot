# Тест нейронной сети 8x8 px
import numpy as np
import cv2
import app.neuronet.words as w
from keras.models import Sequential
from keras.layers import Dense, Convolution2D, Dropout, Flatten, MaxPooling2D
from keras.datasets import mnist
from keras.utils import to_categorical
from keras.optimizers import SGD

import time
from os import walk
import random


def text_model():
    model = Sequential()
    model.add(Convolution2D(filters=32, kernel_size=(2, 2), padding='valid', input_shape=(8, 8, 3), activation='relu'))
    model.add(Convolution2D(filters=64, kernel_size=(2, 2), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Convolution2D(64, (2, 2), activation='relu'))
    model.add(Convolution2D(64, (2, 2), activation='relu'))
    #model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(192, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(192, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(9, activation='softmax'))

    # sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    # model.compile(loss='categorical_crossentropy', optimizer=sgd)

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model


# Количество выходов - 7

path = "../../img/map_8/"

# 'gray': [
# 0 - номер элемента,
# 1 - количество повторов
# ],
dirs = {
    'gray': [0, 1],
    'grass': [1, 3],
    'forest': [2, 3],
    'rock': [3, 1],
    'water': [4, 5],
    'graygrass': [5, 5],
    'forestblack': [6, 15],
    'prerock': [7, 6],
    'bereg': [8, 1],
    # 'unit': 9,
    # 'structure': 10,
}

# Получаем список файлов, для каждой рубрики
for key, value in dirs.items():
    f = []
    curr_path = path + key
    for (dirpath, dirnames, filenames) in walk(curr_path):
        f.extend(filenames)
        break
    dirs[key] = [value, f]

# print(dirs)

words = []
# Создаём единый массив с тренировочными данными
for key, value in dirs.items():
    item_id = value[0][0]
    item_count = value[0][1]
    i = 0
    while i < item_count:
        for img in value[1]:
            words.append([key, item_id, img])
        i += 1

# Случайная сортировка массива
random.shuffle(words)
# print(words)
# print(len(words))
# exit()

# 9206 единиц

# images = []
my_x_train = []
my_y_train = []
for word in words:
    # print(word)
    # 'forest', 2, '5f55e7f79ca214ddce82f948632e584ed2c91a95.jpg'

    file_name = path + word[0] + '/' + word[2]
    img = cv2.imread(file_name)
    matrix = np.array(img)

    # x_train_r = np.reshape(matrix, (32, 32, 3))
    # print(matrix)

    my_x_train.append(matrix)
    my_y_train.append(word[1])

# Загружаем данные x_train и x_test содержат двухмерный массив с изображение цифр
# x_test, y_test массив с проверочными данными сети.
# (x_train, y_train), (x_test, y_test) = mnist.load_data()

my_y_train_cat = to_categorical(my_y_train)
my_x_train_np = np.array(my_x_train)
my_x_train_np = my_x_train_np.reshape(my_x_train_np.__len__(), 8, 8, 3)

model = text_model()

# print(my_x_train[100])
# print(my_y_train_cat[100])
time1 = time.time()
history = model.fit(my_x_train_np, my_y_train_cat, validation_data=(my_x_train_np, my_y_train_cat), epochs=30, batch_size=128)
filepath = 'model_map_8_1'
model.save(filepath)
time2 = time.time()

print(time2 - time1)
