# Тест Ресайз картинок
import os
import numpy as np
import cv2

import time
from os import walk
import random


# Количество выходов - 7

path = "../../img/map/"
save_path = "../../img/map_8/"

# 'gray': [
# 0 - номер элемента,
# 1 - количество повторов
# ],
dirs = {
    'gray': [0, 1],
    'grass': [1, 3],
    'forest': [2, 3],
    'rock': [3, 1],
    'water': [4, 5],
    'graygrass': [5, 5],
    'forestblack': [6, 15],
    'prerock': [7, 6],
    'bereg': [8, 1],
    # 'unit': 9,
    # 'structure': 10,
}

# Получаем список файлов, для каждой рубрики
for key, value in dirs.items():
    f = []
    curr_path = path + key
    for (dirpath, dirnames, filenames) in walk(curr_path):
        f.extend(filenames)
        break
    dirs[key] = f

words = []
# Создаём единый массив с тренировочными данными
for folder, files in dirs.items():
    path_folder = save_path + folder
    source_folder = path + folder
    dir_exist = False
    if not os.path.exists(path_folder):
        try:
            os.mkdir(path_folder)
        except OSError:
            print("Создать директорию %s не удалось" % path_folder)
        else:
            print("Успешно создана директория %s " % path_folder)
            dir_exist = True
    else:
        dir_exist = True

    if dir_exist:
        # Копируем картинки если их нет
        for file in files:
            source_name = source_folder + '/' + file
            file_name = path_folder + '/' + file

            if not os.path.exists(file_name):
                img = cv2.imread(source_name)
                res = cv2.resize(img, dsize=(8, 8), interpolation=cv2.INTER_CUBIC)
                cv2.imwrite(file_name, res)
                print("Файл %s сохранён" % file_name)
        pass
