import os
import random
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping
import numpy as np
import cv2
from keras.utils import to_categorical
from app.neuromap.model import text_model_2
from os import walk

import logging
import tensorflow as tf

print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

if __name__ == '__main__':
    # Load the data
    path =os.path.dirname(__file__)


    batch_size = 128
    model_name = 'm_2'

    model_path = os.path.join(path, 'model/'+model_name+'.h5')
    fig_path = os.path.join(path, 'model/img/'+model_name+'.png')
    save_path = "../../img/mapdata/combine/"

    # Количество выходов - 12

    # 'gray': [
    # 0 - номер элемента,
    # 1 - количество повторов
    # ],
    dirs = {
        'gray': [0, 1],
        'grass': [1, 1],
        'forest': [2, 1],
        'savana': [3, 1],
        'rock': [4, 1],
        'water': [5, 1],
        'prerock': [6, 1],
        'bereg': [7, 1],
        'most': [8, 1],
        'forestblack': [9, 1],
        'struct': [10, 1],
        'mon': [11, 1],
    }
    # Минимальное количество элементов в папке
    min_count = 3000

    # Получаем список файлов, для каждой рубрики
    total = 0
    dirs_count = dict()
    for key, value in dirs.items():
        f = []
        curr_path = save_path + key
        for (dirpath, dirnames, filenames) in walk(curr_path):
            f.extend(filenames)
            break
        count = len(f)
        total += count
        if count < min_count:
            k0 = value[1]
            k = int(min_count / count)
            if k > 1 and k > k0:
                value[1] = k
                print(key, "слишком мало элементов", count, "увеличиваем множитель:", k)
                count = count * k
        dirs_count[key] = count
        dirs[key] = [value, f]

    # Выводим информацию о тренировочных данных
    for key, count in dirs_count.items():
        print(key, count, str(round((count / total) * 100, 2)) + '%')

    words = []
    # Создаём единый массив с тренировочными данными
    for key, value in dirs.items():
        item_id = value[0][0]
        item_count = value[0][1]
        i = 0
        while i < item_count:
            for img in value[1]:
                words.append([key, item_id, img])
            i += 1

    # Случайная сортировка массива
    random.shuffle(words)
    # print(words)
    print('total', len(words))
    # exit()

    # images = []
    my_x_train = []
    my_y_train = []
    words_cache = dict()
    for word in words:
        file_name = save_path + word[0] + '/' + word[2]
        # Кеширование повторяющихся данных
        if file_name in words_cache:
            matrix = words_cache.get(file_name)
        else:
            img = cv2.imread(file_name)
            matrix = np.array(img)
            words_cache[file_name] = matrix

        my_x_train.append(matrix)
        my_y_train.append(word[1])

    del words_cache
    # exit()
    # Загружаем данные x_train и x_test содержат двухмерный массив с изображение цифр

    my_y_train_cat = to_categorical(my_y_train)
    my_x_train_np = np.array(my_x_train)
    my_x_train_np = my_x_train_np.reshape(len(my_x_train_np), 32, 32, 3)

    X = np.asarray(my_x_train_np)
    Y = np.asarray(my_y_train_cat)

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

    model = text_model_2()

    # Train the model
    callbacks = [
        EarlyStopping(monitor='val_accuracy',
                      min_delta=1e-3,
                      patience=5,
                      mode='max',
                      restore_best_weights=True,
                      verbose=1),
    ]

    history = model.fit(x=X_train,
                        y=y_train,
                        batch_size=batch_size,
                        epochs=100,
                        validation_data=(X_test, y_test),
                        callbacks=callbacks)

    # Plot accuracies
    plt.plot(history.history['accuracy'], label='train')
    plt.plot(history.history['val_accuracy'], label='val')
    plt.legend()
    plt.savefig(fig_path)

    # Save the model
    model.save(model_path)
