# Тест нейронной сети
import numpy as np
import cv2
from keras.models import Sequential
from keras.layers import Dense, Convolution2D, Dropout, Flatten, MaxPooling2D
from keras.utils import to_categorical
import matplotlib.pyplot as plt

import time
from os import walk
import random


def img_model():
    model = Sequential()
    model.add(
        Convolution2D(filters=32, kernel_size=(3, 3), padding='valid', input_shape=(32, 32, 3), activation='relu'))
    model.add(Convolution2D(filters=64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(3072, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(3072, activation='relu'))
    model.add(Dropout(0.5))

    #model.add(Dense(3072, activation='relu'))
    #model.add(Dropout(0.5))

    model.add(Dense(12, activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])
    return model


# Количество выходов - 12
filepath = 'model/model_220221_11'
save_path = "../../img/mapdata/combine/"

# 'gray': [
# 0 - номер элемента,
# 1 - количество повторов
# ],
dirs = {
    'gray': [0, 1],
    'grass': [1, 1],
    'forest': [2, 1],
    'savana': [3, 1],
    'rock': [4, 1],
    'water': [5, 1],
    'prerock': [6, 1],
    'bereg': [7, 1],
    'most': [8, 1],
    'forestblack': [9, 1],
    'struct': [10, 1],
    'mon': [11, 1],
}
# Минимальное количество элементов в папке
min_count = 3000

# Получаем список файлов, для каждой рубрики
total = 0
dirs_count = dict()
for key, value in dirs.items():
    f = []
    curr_path = save_path + key
    for (dirpath, dirnames, filenames) in walk(curr_path):
        f.extend(filenames)
        break
    count = len(f)
    total += count
    if count < min_count:
        k0 = value[1]
        k = int(min_count / count)
        if k > 1 and k > k0:
            value[1] = k
            print(key, "слишком мало элементов", count, "увеличиваем множитель:", k)
            count = count * k
    dirs_count[key] = count
    dirs[key] = [value, f]

# Выводим информацию о тренировочных данных
for key, count in dirs_count.items():
    print(key, count, str(round((count / total) * 100, 2)) + '%')

words = []
# Создаём единый массив с тренировочными данными
for key, value in dirs.items():
    item_id = value[0][0]
    item_count = value[0][1]
    i = 0
    while i < item_count:
        for img in value[1]:
            words.append([key, item_id, img])
        i += 1

# Случайная сортировка массива
random.shuffle(words)
# print(words)
print('total', len(words))
# exit()

# images = []
my_x_train = []
my_y_train = []
words_cache = dict()
for word in words:
    file_name = save_path + word[0] + '/' + word[2]
    # Кеширование повторяющихся данных
    if file_name in words_cache:
        matrix = words_cache.get(file_name)
    else:
        img = cv2.imread(file_name)
        matrix = np.array(img)
        words_cache[file_name] = matrix

    my_x_train.append(matrix)
    my_y_train.append(word[1])

del words_cache
# exit()
# Загружаем данные x_train и x_test содержат двухмерный массив с изображение цифр

my_y_train_cat = to_categorical(my_y_train)
my_x_train_np = np.array(my_x_train)
my_x_train_np = my_x_train_np.reshape(len(my_x_train_np), 32, 32, 3)
model = img_model()

print(model.summary())

time1 = time.time()

history = model.fit(my_x_train_np, my_y_train_cat, epochs=10, batch_size=128, validation_split=0.2)

model.save(filepath)
time2 = time.time()

print(time2 - time1)

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.show()



# 48k, 10e, 200.03968048095703
# 50k, 7e, 149.22662591934204
# 48k, 15e, 389.6757094860077
# 28k, 10e,  106.53114581108093
# 76k, 320.0183849334717