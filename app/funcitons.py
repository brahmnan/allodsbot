import time


def pr(module='', *args):
    a = time.strftime("%H:%M:%S", time.localtime())
    print(a, module, *args)