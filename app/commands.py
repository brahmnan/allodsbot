# Команды взаимодействия с игрой
# Клики мышкой, нажатие клавиш, определение цвета, захват области экрана
from mss import mss
import numpy as np
import pyautogui as pg
import app.config as cfg
import time
from app.mission.store import Mss
from app.funcitons import pr
from app.mission.store import allow_action


class Keys:
    # Управление нажатием клавиш
    def module_name(self):
        return 'Keys'

    @allow_action
    def press(self, keys, sleep=0):
        # Нажать клавишу
        pg.press(keys)
        if sleep > 0:
            time.sleep(sleep / 1000)
        pass

    @allow_action
    def keyDown(self, key, sleep=0):
        pg.keyDown(key)
        if sleep > 0:
            time.sleep(sleep / 1000)

    @allow_action
    def keyUp(self, key, sleep=0):
        pg.keyUp(key)
        if sleep > 0:
            time.sleep(sleep / 1000)

    @allow_action
    def press_two(self, alt, key, sleep=0):
        # Нажать клавишу с альтом, шифтом и др.
        pg.keyDown(alt)
        pg.press(key)
        pg.keyUp(alt)
        if sleep > 0:
            time.sleep(sleep / 1000)
        pass

    @allow_action
    def down(self, key):
        pg.keyDown(key)

    @allow_action
    def up(self, key):
        pg.keyUp(key)

    @allow_action
    def type(self, text, sleep=100, interval=100):
        interval_ms = interval / 1000
        pg.typewrite(text, interval_ms)
        if sleep > 0:
            time.sleep(sleep / 1000)

    @allow_action
    def press_validate(self, keys, color_x=0, color_y=0, sleep=100):
        # Нажатие задержкой в ms. После задержки проверяется цвет места, указанного в настройках
        # Он должен поменяться
        color = Color()
        c1 = color.color(color_x, color_y)
        self.press(keys)
        time.sleep(sleep / 1000)
        c2 = color.color(color_x, color_y)
        if c1[0] == c2[0] and c1[1] == c2[1] and c1[2] == c2[2]:
            # pr(self.module_name(),'  цвета равны')
            return False
        else:
            # pr(self.module_name(),'  цвета разные')
            return True

    @allow_action
    def press_for_color(self, keys, color_x=0, color_y=0, sleep=100, r=0, g=0, b=0):
        # Нажатие задержкой в ms. После задержки проверяется цвет места, указанного в настройках
        # Он должен соответствовать указанному
        color = Color()
        if color.validate(color_x, color_y, r, g, b):
            pr(self.module_name(), '  цвета равны, нажатие не требуется')
            return True

        self.press(keys)
        time.sleep(sleep / 1000)

        if color.validate(color_x, color_y, r, g, b):
            pr(self.module_name(), '  цвета равны, нажатие не требуется')
            return True
        else:
            pr(self.module_name(), '  цвета разные. Ошибка включения меню')
            return False

    pass


class Mouse:
    # Управление мышкой

    def module_name(self):
        return 'Mouse'

    @allow_action
    def validate(self, x, y):
        # Валидация координат
        if cfg.screen_shift['x'] < x < 1024 + cfg.screen_shift['x'] and cfg.screen_shift['y'] < y < 768 + \
                cfg.screen_shift['y']:
            return True
        return False

    @allow_action
    def move(self, x, y, sleep=30):
        pg.moveTo(x, y)
        if sleep > 0:
            time.sleep(sleep / 1000)
        pass

    @allow_action
    def mouse_hide(self):
        # Убираем мышку с экрана
        pg.moveTo(886, 520)

    @allow_action
    def left(self, x, y, double=False, sleep=0):
        pg.moveTo(x, y)
        if not double:
            pg.click(x, y)
        else:
            pg.doubleClick(x, y)
        # Убираем мышку с экрана
        pg.moveTo(886, 520)

        if sleep > 0:
            time.sleep(sleep / 1000)

        pass

    @allow_action
    def left_validate(self, x, y, sleep=100, color_x=0, color_y=0, double=False, more_pos=False):
        # Клик с задержкой в ms. После задержки проверяется цвет места, куда кликали.
        # Он должен поменяться
        color = Color()
        # Назначение цветов
        if color_x == 0 and color_y == 0:
            color_x = x
            color_y = y

        c1 = color.color(color_x, color_y)
        if more_pos:
            c1_1 = color.color(color_x - 10, color_y - 10)
            c1_2 = color.color(color_x + 10, color_y + 10)
            # pr(self.module_name(),color_x, color_y, c1,c1_1,c1_2)
        self.left(x, y, double)
        time.sleep(sleep / 1000)
        c2 = color.color(color_x, color_y)
        if more_pos:
            c2_1 = color.color(color_x - 10, color_y - 10)
            c2_2 = color.color(color_x + 10, color_y + 10)

            # pr(self.module_name(),color_x,color_y,c2,c2_1,c2_2)
        if c1[0] == c2[0] and c1[1] == c2[1] and c1[2] == c2[2]:
            if more_pos:
                if c1_1[0] == c2_1[0] and c1_1[1] == c2_1[1] and c1_1[2] == c2_1[2] and \
                        c1_2[0] == c2_2[0] and c1_2[1] == c2_2[1] and c1_2[2] == c2_2[2]:
                    pr(self.module_name(), '  цвета равны, ошибка нажатия', c1, '=', c2, c1_1, '=', c2_1, c1_2, '=',
                       c2_2)
                    return False
                else:
                    return True

            pr(self.module_name(), '  цвета равны, ошибка нажатия', c1, c2)
            return False
        else:
            pr(self.module_name(), '  цвета разные, нажатие успешно', c1, c2)
            return True

    @allow_action
    def drug_validate(self, x, y, x1, y1, sleep=200, color_x=0, color_y=0):
        # Перемещение с задержкой в ms. После задержки проверяется цвет места, куда кликали.
        # Он должен поменяться
        color = Color()
        # Назначение цветов
        if color_x == 0 and color_y == 0:
            color_x = x
            color_y = y

        c1 = color.color(color_x, color_y)
        # pr(self.module_name(),'c1', c1)
        self.drug_to(x, y, x1, y1, sleep)
        time.sleep(sleep / 1000)

        c2 = color.color(color_x, color_y)
        # pr(self.module_name(),'c2', c2)
        if c1[0] == c2[0] and c1[1] == c2[1] and c1[2] == c2[2]:
            pr(self.module_name(), '  цвета равны, ошибка нажатия')
            return False
        else:
            pr(self.module_name(), '  цвета разные, нажатие успешно')
            return True

    @allow_action
    def left_down(self, x, y, wait, sleep=100):
        # Нажатие мышки и задержка
        pg.moveTo(x, y)
        pg.mouseDown()
        time.sleep(wait / 1000)
        pg.mouseUp()
        time.sleep(sleep / 1000)
        pass

    @allow_action
    def drug_to(self, x, y, x1, y1, sleep=200):
        # Перемещение предмета
        pg.moveTo(x, y)
        time.sleep(10 / 1000)
        pg.dragTo(x1, y1, duration=sleep / 1000)
        pg.mouseUp()
        # Убираем мышку с экрана
        pg.moveTo(886, 520)
        pass

    pass


class Color:
    # работа с цветом

    def validate(self, x, y, r, g, b):
        # Валидация цвета пикселя на экране
        c = self.color(x, y)
        pr(self.module_name(), x, y, [r, g, b], [c[2], c[1], c[0]])
        m = 0  # Маржа погрешности на ошибку вывода
        if c[2] + m >= r >= c[2] - m \
                and c[1] + m >= g >= c[1] - m \
                and c[0] + m >= b >= c[0] - m:
            return True

        return False

    def color(self, x, y):
        # Взять пробу цвета по координатам
        coords = {'top': y, 'left': x, 'width': 1, 'height': 1}
        img = Mss.sct.grab(coords)
        img_arr = np.array(img)
        # return bgr
        return img_arr[0][0]

    def module_name(self):
        return 'Color'

    pass
