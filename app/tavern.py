# Управление диалогами в таверне

import app.ineterface as inter

from app.commands import Color, Mouse, Keys
from app.mission.monitor import Names
from app.funcitons import pr
from app.neurotext.neurotext import NeuroText


class Tavern:
    # Код имён юнитов
    code_swordman = '224-284'
    code_archer = '178-222'
    code_arbalet = '224-283'
    code_arbalet = '224-283'
    code_mage = '147-174'
    code_dubin = '232-305'

    face_mage_water = [285, 456, 181, 134, 107] # x,y,r,g,b

    code_fergard = '89-114'
    code_ren = '97-124'
    code_brian = '64-84'
    code_naira = '68-86'
    code_noname = '0-0'


    color = Color()
    mouse = Mouse()
    names = Names()
    keys = Keys()

    tavern_units = []

    # Таверна

    def module_name(self):
        return 'Таверна'

    def talk_hero_by_code(self, code, wait_dialog=True, wait_k=100):
        # Поговорить с героем по его коду
        unit = self.get_unit_by_code(code, True)
        if unit:
            pr(self.module_name(), code, unit)
            # Нажимаем на разговор
            self.mouse.left(750, 290)

            if wait_dialog:
                # Слушаем диалог
                NeuroText.lisen_cicle(wait_k)
                pass
            else:
                # Пропускаем диалог
                self.propusk('enter', 330, 330)
        else:
            pr(self.module_name(), code, 'Unit not found. Exit')
            exit()

    def hire_by_code(self, code, face_pixel=[]):
        # Наём юнита по его коду имени
        unit = self.get_unit_by_code(code, False, face_pixel)

        if unit:
            pr(self.module_name(), code, unit)
            # Нажимаем кнопка нанять
            self.mouse.left(750, 240)
        else:
            pr(self.module_name(), code, 'Unit not found. Exit')
            exit()

    def find_units(self):
        # Найти позицию юнита по коду
        if len(self.tavern_units):
            return self.tavern_units
        # Находим клетки, содержащие юниты
        first_crd = [377, 592]
        cell_weight = 48
        cell_height = 64
        unit_color = [66, 81, 66]  # 1
        hero_color = [24, 113, 57]  # 2
        i = 0
        x = first_crd[0]
        y = first_crd[1]
        units = []
        while i < 12:
            clr = self.color.color(x, y)
            if clr[0] == unit_color[2] and clr[1] == unit_color[1] and clr[2] == unit_color[0]:
                units.append([x, y, 1])
            elif clr[0] == hero_color[2] and clr[1] == hero_color[1] and clr[2] == hero_color[0]:
                units.append([x, y, 2])

            x += cell_weight
            i += 1
            if i == 6:
                # Поднимаемся на верхнюю строку
                x = first_crd[0]
                y -= cell_height

        self.tavern_units = units
        return units

    def get_unit_by_code(self, code=0, hero=False, fp=[]):
        # Получить юнит по коду

        # Находим список юнитов
        units = self.find_units()
        unit_ret = []
        for unit in units:
            if unit[2] == 2 and hero or unit[2] == 1 and not hero:
                unit_code = self.get_unit_code(unit)
                pr(self.module_name(), 'Код юнита', unit_code, unit)
                # print(unit_code)
                if unit_code == code:
                    if len(fp):
                        # Дополнительная валидация на пиксель
                        if not self.color.validate(fp[0], fp[1], fp[2], fp[3], fp[4]):
                            continue
                    unit_ret = unit
                    break
                pass
        return unit_ret

    def propusk(self, keys, color_x, color_y):
        # Пропуск разговора
        wait = 0
        ret = False
        while not ret:
            ret = self.keys.press_validate(keys, color_x, color_y, 10)
            if ret:
                break

            wait += 1
            if wait > 100:
                pr(self.module_name(), keys, 'аварийная остановка скрипта нажатия клавиши')
                break
        pass

    def get_unit_code(self, unit):
        # Получить код текущего юнита
        # Кликаем по иконке юнита
        self.mouse.left(unit[0] + 24, unit[1] + 32, sleep=50)
        # Получаем код имени юнита
        code = self.names.get_hero_tavern_name()
        return code

    def go_tavern_kaarg(self):
        # Переход в таверну
        inter.validate_left_cicle(760, 480)
        pass

    def go_tavern_plagat(self):
        # Переход в таверну
        inter.validate_left_cicle(346, 540)
        pass

    def go_tavern_elf(self):
        # Переход в таверну
        inter.validate_left_cicle(346, 430)
        pass

    def hire_1(self):
        inter.validate_left_cicle(397, 623, double=True)
        print('Разгвор')
        pass

    def hire_2(self):
        inter.validate_left_cicle(445, 623, double=True)
        print('Разгвор')
        pass

    def hire_3(self):
        inter.validate_left_cicle(493, 623, double=True)
        print('Разгвор')
        pass

    def hire_4(self):
        inter.validate_left_cicle(541, 623, double=True)
        print('Разгвор')
        pass

    def hire_5(self):
        inter.validate_left_cicle(589, 623, double=True)
        print('Разгвор')
        pass

    def talk_1(self, wait_dialog=True, wait_k=100):
        inter.validate_left_cicle(397, 623, 100, 329, 329)
        print('Разгвор')
        if wait_dialog:
            # Слушаем диалог
            NeuroText.lisen_cicle(wait_k)
            pass
        else:
            # Пропускаем диалог
            self.propusk('enter', 330, 330)
        pass

    def talk_2(self, wait_dialog=True, wait_k=100):
        inter.validate_left_cicle(445, 623, 100, 329, 329)
        print('Разгвор')
        if wait_dialog:
            # Слушаем диалог
            NeuroText.lisen_cicle(wait_k)
            pass
        else:
            # Пропускаем диалог
            self.propusk('enter', 330, 330)
        pass

    def talk_3(self, wait_dialog=True, wait_k=100):
        inter.validate_left_cicle(493, 623, 100, 329, 329)
        print('Разгвор')
        if wait_dialog:
            # Слушаем диалог
            NeuroText.lisen_cicle(wait_k)
            pass
        else:
            # Пропускаем диалог
            self.propusk('enter', 330, 330)
        pass

    def talk_4(self, wait_dialog=True, wait_k=100):
        inter.validate_left_cicle(541, 623, 100, 329, 329)
        print('Разгвор')
        if wait_dialog:
            # Слушаем диалог
            NeuroText.lisen_cicle(wait_k)
            pass
        else:
            # Пропускаем диалог
            self.propusk('enter', 330, 330)
        pass

    def talk_5(self, wait_dialog=True, wait_k=100):
        inter.validate_left_cicle(589, 623, 100, 329, 329)
        print('Разгвор')
        if wait_dialog:
            # Слушаем диалог
            NeuroText.lisen_cicle(wait_k)
            pass
        else:
            # Пропускаем диалог
            self.propusk('enter', 330, 330)
        pass

    def talk_6(self, wait_dialog=True, wait_k=100):
        inter.validate_left_cicle(637, 623, 100, 329, 329)
        print('Разгвор')
        if wait_dialog:
            # Слушаем диалог
            NeuroText.lisen_cicle(wait_k)
            pass
        else:
            # Пропускаем диалог
            self.propusk('enter', 330, 330)
        pass

    def go_tavern(self):
        # Переход в таверну
        inter.validate_left_cicle(350, 540)
        pass

    def quit(self):
        # Выход из таверны
        inter.validate_left_cicle(752, 335)
        pass

    pass
