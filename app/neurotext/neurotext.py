from app import config as cfg
from app.mission.store import Mss
import numpy as np
import cv2
from hashlib import sha1
from app.neurotext.words import words_hash
import os
import torch
from playsound import playsound
import shutil
from app.commands import Keys, Color
from app.mission.store import TM
from urllib.parse import unquote
import time


class NeuroText:
    """
    Чтение диалогов героев
    TODO
    Запрашиваем кусок экрана с текстом
    Разбиваем окошко на строки
    нормализуем картинку
    разбиваем на буквы
    транслируем в текст
    скармливаем сети
    если есть файл, загружаем из кеша, елси нет компилируем
    воспроизводим файл
    нажимаем ентер, бля открытия следующего диалога
    """
    save_letters = False
    save_faces = False
    dialogs = {}
    model = None

    speaker_def = 'eugene'
    speaker_models = {
        'fe92fb5a880cefa6475be4cf40975b9cdaf7769c': 'baya',
        '11fd76e95d1540ec53eaf848f02cf66e9b431193': 'baya',
        '4b0458482219b36be8c4a38e24ab90c22f7ff12a': 'baya',
        'e8e290285c1cebdbe766971965bc929842fd5979': 'baya',
        '5fc36b5446a0647dede45d1653720654bbfb88d7': 'baya',
    }

    @classmethod
    def load_model(cls):
        if cls.model:
            return True

        torch.hub.set_dir("G:/workspace/python/allodsbot/ext/")
        language = 'ru'
        model_id = 'v3_1_ru'
        device = torch.device('cpu')
        model, example_text = torch.hub.load(repo_or_dir='snakers4/silero-models',
                                             model='silero_tts',
                                             language=language,
                                             speaker=model_id)
        model.to(device)  # gpu or cpu
        cls.model = model

    @classmethod
    def validate_mesage_screen(cls):
        validate_crd = [[[327, 320], [24, 44, 41]], [[297, 502], [165, 113, 49]]]
        # Проверяем наличие окна диалога
        valid = True
        color = Color()
        for item in validate_crd:
            if not color.validate(item[0][0], item[0][1], item[1][0], item[1][1], item[1][2]):
                valid = False

        return valid

    @classmethod
    def lisen_cicle(cls, wait_k=100, keys='enter', color_x=330, color_y=330):
        # Прослушивание диалогов в таверне
        dialog_map = {'top': cfg.dialog_window['top'], 'left': cfg.dialog_window['left'],
                      'width': cfg.dialog_window['width'],
                      'height': cfg.dialog_window['height']}

        k = Keys()
        ret = False
        while not ret:
            # Получаем скришот
            dialog_matrix = Mss.sct.grabCache(dialog_map, True)
            valid = cls.validate_mesage_screen()
            if valid:
                dialog_name = str(sha1(dialog_matrix.tostring()).hexdigest())
                # if dialog_name not in cls.dialogs:
                # cls.dialogs[dialog_name] = 1
                words = cls.custom_letters(dialog_matrix)
                words_text = cls.words_to_text(words)
                wait_time = round(len(words_text) * wait_k / 1300, 2)
                print(words_text)
                print(f'Ожидаем {wait_time} сек.')
                time.sleep(wait_time)

                ret = k.press_validate(keys, color_x, color_y, 10)

            else:
                # Нет окошка диалога
                ret = 1

            if ret:
                break

    @classmethod
    def read_cicle(cls, keys='enter', color_x=330, color_y=330):
        k = Keys()
        ret = False
        while not ret:
            cls.read(False)
            ret = k.press_validate(keys, color_x, color_y, 10)
            if ret:
                break

    @classmethod
    def read(cls, next_dialog=True):
        dialog_map = {'top': cfg.dialog_window['top'], 'left': cfg.dialog_window['left'],
                      'width': cfg.dialog_window['width'],
                      'height': cfg.dialog_window['height']}

        face_map = {'top': cfg.face_window['top'], 'left': cfg.face_window['left'],
                    'width': cfg.face_window['width'],
                    'height': cfg.face_window['height']}

        # Получаем скришот
        dialog_matrix = Mss.sct.grabCache(dialog_map, True)
        dialog_name = str(sha1(dialog_matrix.tostring()).hexdigest())

        valid = cls.validate_mesage_screen()
        if valid:
            # Лицо говорящего
            face_matrix = Mss.sct.grabCache(face_map)
            face_name = str(sha1(face_matrix.tostring()).hexdigest())
            if cls.save_faces:
                # Сохраняем лицо говорящего
                cls.save_image(face_matrix, 'faces')

            speaker = cls.speaker_models.get(face_name, cls.speaker_def)

            # if dialog_name not in cls.dialogs:
            #     cls.dialogs[dialog_name] = 1

            curr_stage = 'None'
            if TM.stage:
                curr_stage = TM.stage

            dialog_folder = f"{cfg.BASE_DIR}/app/neurotext/wave/{curr_stage}/{speaker}/"
            if not os.path.exists(dialog_folder):
                os.makedirs(dialog_folder)
            dialog_path = f"{dialog_folder}{dialog_name}.wav"
            words = cls.custom_letters(dialog_matrix)
            words_text = cls.words_to_text(words)
            words_text = cls.validate_numbers(words_text)
            print(words_text)

            if not os.path.exists(dialog_path):
                # Если аудиофайла нет, создаём

                cls.load_model()
                sample_rate = 48000

                # TODO speakers from head code

                put_accent = True
                put_yo = True
                audio_path = cls.model.save_wav(text=words_text,
                                                speaker=speaker,
                                                sample_rate=sample_rate,
                                                put_accent=put_accent,
                                                put_yo=put_yo,
                                                )

                shutil.copyfile(audio_path, dialog_path)

            playsound(unquote(dialog_path))
            if next_dialog:
                cls.next_dialog()

    @classmethod
    def next_dialog(cls):
        k = Keys()
        k.press('enter')

    @classmethod
    def validate_numbers(cls, text=''):
        if text.find('500') != -1:
            text.replace('500', 'пятьсот')
        return text

    @classmethod
    def words_to_text(cls, words=[]):
        result = ''
        for word in words:
            for letter in word:
                letter_hash = cls.get_letter_hash(letter)
                word_str = words_hash.get(letter_hash, '')
                if word_str:
                    result += word_str
            result += ' '
        return result

    @classmethod
    def custom_letters(cls, matrix=[], min_color=90):
        if matrix.size == 0:
            return []

        gray = cv2.cvtColor(matrix, cv2.COLOR_BGR2GRAY)
        ret, gray_thresh = cv2.threshold(gray, min_color, 255, cv2.THRESH_BINARY_INV)

        # tr_img = gray_thresh.T
        # pr(cls.module_name(), tr_img)

        row_height = 17
        main_row = []
        i = 1
        rows = []
        for row in gray_thresh:
            # Проходим по колонкам
            main_row.append(row)

            if i >= row_height:
                i = 1
                rows.append(np.asarray(main_row))
                main_row = []

            else:
                i += 1

        letters = {}
        words = []
        last_divider = 0
        # Находим слова
        for row in rows:
            row_t = row.T
            i = 0
            for col in row_t:
                # Проходим по колонкам
                divider = True
                is_word = False
                for r in col:
                    # Проходим по строкам
                    if r == 0:
                        # Найдена буква
                        is_word = True
                        divider = False
                        last_divider = 0
                        pass
                if divider:
                    if last_divider > 1:
                        # Новое предложение
                        if letters:
                            words.append(letters)
                        letters = {}
                        i = 0
                    else:
                        i += 1
                        last_divider += 1
                if is_word:
                    if not letters.get(i):
                        letters[i] = []
                    letters[i].append(col)
                pass
            if letters:
                words.append(letters)

            last_divider = 1
            letters = {}

        # Нормализуем буквы
        words_norm = []
        for word in words:
            letters_norm = []
            for num, letter in word.items():
                letter = np.asarray(letter)
                letter_t = letter.T
                if cls.save_letters:
                    cls.save_image(letter_t)
                letters_norm.append(letter_t)
            words_norm.append(letters_norm)

        # for word in words_norm:
        #     print('word', word)

        # cv2.imshow("gray_thresh", np.asarray(words_norm))
        # cv2.waitKey(0)

        return words_norm

    @classmethod
    def get_letter_hash(cls, letter):
        name = str(sha1(letter.tostring()).hexdigest())
        return name

    @classmethod
    def save_image(cls, img, folder='letters'):
        path = cfg.BASE_DIR + f"/app/neurotext/img/{folder}/"
        name = cls.get_letter_hash(img)
        path_name = path + name + '.jpg'
        if not os.path.exists(path_name):
            cv2.imwrite(path_name, img)
            print(path_name)
