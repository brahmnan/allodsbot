from app.mission.monitor import MainScreen
from app.ineterface import MainMenu, pr
from app.neurotext.neurotext import NeuroText


class TestText:
    """
    TODO мониторим появляющуюся табличку, если она появляется,
        распознаём текст,
        сохраняем буквы,
        выводим в консоль
    """

    def __init__(self):
        self.ms = MainScreen()
        self.menu = MainMenu()
        self.nt = NeuroText()
        self.tasks = dict()
        self.actions = []

    def run(self):
        # Запуск цикла выполнения задач.
        while True:
            # 1. Обновляем информацию об игре
            self.ms.find()

            # 2. Проверяем появление сообщений.
            dialog = self.mod_dialog()
            if dialog:
                if dialog == 2:
                    # self.nt.read()
                    # self.menu.propusk('enter', 335, 335)
                    continue

    def mod_dialog(self):
        # Мониторинг начавшегося диалога
        ret = 0
        # Типы диалогов
        #   1. Победа:
        #   2. Сообщение:
        #   3  Поражение:
        #   4. Поражение: Иглез погиб
        #   5. Главное меню
        dialogs = {1: [[[433, 365], [24, 44, 41]], [[393, 351], [132, 85, 16]]],
                   2: [[[327, 320], [24, 44, 41]], [[297, 502], [165, 113, 49]]],
                   3: [[[323, 316], [40, 24, 40]], [[293, 561], [248, 220, 152]]],  # TODO UNUSED
                   4: [[[304, 384], [24, 44, 41]], [[297, 350], [165, 105, 33]]],
                   5: [[[449, 186], [107, 0, 0]], [[590, 186], [82, 0, 0]]],
                   }
        for key, val in dialogs.items():
            # Переводим относительные координаты в абсолютные
            r = 0
            for item in val:
                screen_crd = self.ms.abs_to_screen(item[0][0], item[0][1])
                # print(key, self.ms.matrix[screen_crd[1], screen_crd[0]])
                if self.ms.matrix[screen_crd[1], screen_crd[0]][0] == item[1][2] \
                        and self.ms.matrix[screen_crd[1], screen_crd[0]][1] == item[1][1] \
                        and self.ms.matrix[screen_crd[1], screen_crd[0]][2] == item[1][0]:
                    r += 1
                    pass
            if r == 2:
                # Если оба цвета прошли валидацию, возвращаем ключ сообщения
                ret = key
                break
            pass
        return ret


tt =TestText()
tt.run()