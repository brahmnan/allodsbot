import wave
import os
import torch
from playsound import playsound
from omegaconf import OmegaConf
import contextlib


def write_wave(path, audio, sample_rate):
    """Writes a .wav file.
    Takes path, PCM audio data, and sample rate.
    """
    with contextlib.closing(wave.open(path, 'wb')) as wf:
        wf.setnchannels(1)
        wf.setsampwidth(2)
        wf.setframerate(sample_rate)
        wf.writeframes(audio)

models = OmegaConf.load('latest_silero_models.yml')

# see latest avaiable models
available_languages = list(models.tts_models.keys())
print(f'Available languages {available_languages}')

for lang in available_languages:
    _models = list(models.tts_models.get(lang).keys())
    print(f'Available models for {lang}: {_models}')


torch.hub.set_dir("G:/workspace/python/allodsbot/ext/")
language = 'ru'
model_id = 'v3_1_ru'
device = torch.device('cpu')


model, example_text = torch.hub.load(repo_or_dir='snakers4/silero-models',
                                     model='silero_tts',
                                     language=language,
                                     speaker=model_id)
model.to(device)  # gpu or cpu

# model.speakers
# ['aidar', 'baya', 'kseniya', 'xenia', 'eugene', 'random']

sample_rate = 48000
speaker = 'baya'
put_accent=True
put_yo=True
# example_text = 'В недрах тундры выдры в г+етрах т+ырят в вёдра ядра к+едров.'
example_text = 'Получи свою награду 500 монет'

audio_path = model.save_wav(text=example_text,
                        speaker=speaker,
                        sample_rate=sample_rate,
                        put_accent=put_accent,
                        put_yo=put_yo,
                        )

playsound(audio_path)


print(audio_path)
